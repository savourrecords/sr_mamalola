<?php /* Smarty version Smarty-3.1.19, created on 2018-11-08 12:34:31
         compiled from "module:tt_bestsellers/views/templates/hook/tt_bestsellers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12176397695be41f47b76e95-54064897%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9eb91118876b66d5ea09de80bf30567df3044bf4' => 
    array (
      0 => 'module:tt_bestsellers/views/templates/hook/tt_bestsellers.tpl',
      1 => 1523439482,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '12176397695be41f47b76e95-54064897',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
    'tt_total' => 0,
    'tt_cnt' => 0,
    'product' => 0,
    'allBestSellers' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be41f47b88aa8_22869616',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be41f47b88aa8_22869616')) {function content_5be41f47b88aa8_22869616($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars["tt_cnt"] = new Smarty_variable("1", null, 0);?>
<?php $_smarty_tpl->tpl_vars["tt_total"] = new Smarty_variable("0", null, 0);?>
<?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
	<?php $_smarty_tpl->tpl_vars['tt_total'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_total']->value+1, null, 0);?>
<?php } ?>

<section class="ttbestseller-products clearfix col-sm-4">
  <h3 class="tab-title"><?php echo smartyTranslate(array('s'=>'Best Sellers','d'=>'Modules.Bestsellers.Shop'),$_smarty_tpl);?>
</h3>
	  <div class="ttbestseller-content products">
		<?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
		  <?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
				<!-- Start TemplateTrip 2 product slide code -->
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2!=0) {?>
				<ul>
					<li class="bestsellerli">
						<ul>
						<li class="item">
				<?php }?>
			<?php }?>
				<!-- End TemplateTrip 2 product slide code -->

				<?php echo $_smarty_tpl->getSubTemplate ("catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>


				<!-- Start TemplateTrip 2 product slide code -->
			<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
						</li>
						</ul>
					</li>
					</ul>
				<?php }?>
				<?php }?>

				<?php $_smarty_tpl->tpl_vars['tt_cnt'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_cnt']->value+1, null, 0);?>
				<!-- End TemplateTrip 2 product slide code -->
        <?php } ?>
		<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
			<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
					</li>
					</ul>
				</li>
				</ul>
			<?php }?>
		<?php }?>
	  </div>
	<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>4) {?>
	<div class="customNavigation">
		<a class="btn prev ttbestseller_prev"><?php echo smartyTranslate(array('s'=>'Prev','d'=>'Modules.Bestsellers.Shop'),$_smarty_tpl);?>
</a>
		<a class="btn next ttbestseller_next"><?php echo smartyTranslate(array('s'=>'Next','d'=>'Modules.Bestsellers.Shop'),$_smarty_tpl);?>
</a>
	</div>
	<?php }?>
	   <!-- Left and right controls -->
   <div class="allproduct"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['allBestSellers']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'All best sellers','d'=>'Modules.Bestsellers.Shop'),$_smarty_tpl);?>
</a></div>
</section>
<?php }} ?>
