<?php /* Smarty version Smarty-3.1.19, created on 2018-11-08 12:34:31
         compiled from "module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8844291255be41f47bd6c38-46573700%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl',
      1 => 1531297869,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '8844291255be41f47bd6c38-46573700',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'value' => 0,
    'conditions' => 0,
    'msg' => 0,
    'nw_error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be41f47bdd536_38667943',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be41f47bdd536_38667943')) {function content_5be41f47bdd536_38667943($_smarty_tpl) {?>
<div id="newslatter" class="col-sm-12">
<div class="block_newsletter">
  <div class="row">
	<div class="tt-content col-sm-6">
    <h4 class="tt-title sr-nl-title"><?php echo smartyTranslate(array('s'=>'Suscribete a nuestra newsletter para ser la primera en recibir nuestras últimas noticias, las promociones más exclusivas y cositas divertidas','d'=>'Shop.Theme.Global'),$_smarty_tpl);?>
</h4>
	</div>

    <div class="block_content col-sm-5">
      <form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&id=c0482f22a7" method="post">
        <div class="row">
		          <div class="ttinput_newsletter">
		  	<!--i class="material-icons">&#xE163;</i-->
            <input style="display:none;"
              class="btn btn-primary sr-btn-newsletter float-xs-right hidden-xs-down"
              name="submitNewsletter"
              type="submit"
              value=">"
            >
            <input
              class="btn btn-primary float-xs-right hidden-sm-up"
              name="submitNewsletter"
              type="submit"
              value="<?php echo smartyTranslate(array('s'=>'OK','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>
"
            >
			  <div class="input-wrapper" style="border-bottom:1px solid white;">
              <input style="border-bottom:0px;"
                name="EMAIL"
                type="text"
                value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
                placeholder="<?php echo smartyTranslate(array('s'=>'Escribe tu mail','d'=>'Shop.Forms.Labels'),$_smarty_tpl);?>
"
                aria-labelledby="block-newsletter-label"
              >
			  <img id="img-sr-arrow" style="float:right; margin-top: 5px; z-index: 999; width: 5%; color: white;cursor:pointer;" width="30px;" src="https://mamalolashoes.com/img/sr-img/PB_ICON_ARROW.svg" />
			</div>
            <input type="hidden" name="action" value="0">
            <div class="clearfix"></div>
          </div>
          <div class="col-xs-12">
              <?php if ($_smarty_tpl->tpl_vars['conditions']->value) {?>
                <p class="newsletter-desc"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conditions']->value, ENT_QUOTES, 'UTF-8');?>
</p>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
                <p class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
                  <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

                </p>
              <?php }?>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div><?php }} ?>
