<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:20
         compiled from "modules/redsys/views/templates/hook/hookorderconfirmation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17451417475be62228d727c1-15773135%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '94ccb2b394b7edc43ea1714b9f5e2dcaebf129ce' => 
    array (
      0 => 'modules/redsys/views/templates/hook/hookorderconfirmation.tpl',
      1 => 1524472289,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17451417475be62228d727c1-15773135',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'status' => 0,
    'shop_name' => 0,
    'base_dir_ssl' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be62228e8bb60_31982754',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be62228e8bb60_31982754')) {function content_5be62228e8bb60_31982754($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['status']->value=='ok') {?>
	<p><?php echo smartyTranslate(array('s'=>'Your order on','mod'=>'redsys'),$_smarty_tpl);?>
 <span class="bold"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_name']->value, ENT_QUOTES, 'UTF-8');?>
</span> <?php echo smartyTranslate(array('s'=>'is complete.','mod'=>'redsys'),$_smarty_tpl);?>

		<br /><br /><span class="bold"><?php echo smartyTranslate(array('s'=>'Your order will be sent as soon as possible.','mod'=>'redsys'),$_smarty_tpl);?>
</span>
		<br /><br /><?php echo smartyTranslate(array('s'=>'For any questions or for further information, please contact our','mod'=>'redsys'),$_smarty_tpl);?>
 <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
contact-form.php"><?php echo smartyTranslate(array('s'=>'customer support','mod'=>'redsys'),$_smarty_tpl);?>
</a>.
	</p>
<?php } else { ?>
	<p class="warning">
		<?php echo smartyTranslate(array('s'=>'We noticed a problem with your order. If you think this is an error, you can contact our','mod'=>'redsys'),$_smarty_tpl);?>
 
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
contact-form.php"><?php echo smartyTranslate(array('s'=>'customer support','mod'=>'redsys'),$_smarty_tpl);?>
</a>.
	</p>
<?php }?><?php }} ?>
