<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:20
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/checkout/order-confirmation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14997046405be62228f145e1-00427149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5163b49dadd4f42b66f6f389361b057688b4f20c' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/checkout/order-confirmation.tpl',
      1 => 1525798169,
      2 => 'file',
    ),
    'ddbde55704625c918a77693f37da38417ee293d9' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/page.tpl',
      1 => 1541663748,
      2 => 'file',
    ),
    'd6f97fbdd575d8eade60309b2c54d58910e67d94' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/layouts/layout-full-width.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    '529598cdcf8f8351e1ff7724ab8b5da150dbfe41' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/layouts/layout-both-columns.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    '76498a6ed9feea84c5e018c9c537621598e8faa2' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/stylesheets.tpl',
      1 => 1538554246,
      2 => 'file',
    ),
    '3056214427af2d8ec81af2e6acd89476389dedee' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/javascript.tpl',
      1 => 1538135115,
      2 => 'file',
    ),
    'bd2f81ed2c2772cd0287a66e12c53f7f81e19dde' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/head.tpl',
      1 => 1540376853,
      2 => 'file',
    ),
    '1db4faaf2395d418f766b5b7fbce60035fc3613e' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/product-activation.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    '65c214a7f641755bc7eb7784bb403660b89675e2' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/header.tpl',
      1 => 1541665139,
      2 => 'file',
    ),
    '5e3ec37c9904e26d87bd2b4c7ac81e0ac88f3e25' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/notifications.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    '1fd85d8c03c81c7573bd7c77d6488cd5fc09b37f' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/breadcrumb.tpl',
      1 => 1531297784,
      2 => 'file',
    ),
    '7563478c12d6c491ff30e76bbd6b644013745ad0' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/checkout/_partials/order-confirmation-table.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    '4091cb481f600d7dc8a235fff81018c8189998d5' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/_partials/footer.tpl',
      1 => 1541679259,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14997046405be62228f145e1-00427149',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'layout' => 0,
    'language' => 0,
    'page' => 0,
    'javascript' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be622292bc242_72651940',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be622292bc242_72651940')) {function content_5be622292bc242_72651940($_smarty_tpl) {?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    
      <?php /*  Call merged included template "_partials/head.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be622290c7f58_27790713($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/head.tpl" */?>
    
  </head>

  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['classnames'][0][0]->smartyClassnames($_smarty_tpl->tpl_vars['page']->value['body_classes']), ENT_QUOTES, 'UTF-8');?>
">

    
      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl);?>

    

    <main>
			<div id="page" class="">
      
        <?php /*  Call merged included template "catalog/_partials/product-activation.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be6222916fca0_76171834($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "catalog/_partials/product-activation.tpl" */?>
      

      <header id="header">
        
          <?php /*  Call merged included template "_partials/header.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be62229179191_21842604($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/header.tpl" */?>
        
      </header>
	 <div id="page" class="">
      
        <?php /*  Call merged included template "_partials/notifications.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be622291871f5_53282659($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/notifications.tpl" */?>
      

      <section id="wrapper">
	            
            <?php /*  Call merged included template "_partials/breadcrumb.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be6222919c587_57363819($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/breadcrumb.tpl" */?>
          
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayWrapperTop"),$_smarty_tpl);?>

        <div class="<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?>full-container<?php } else { ?>container<?php }?>">
          

          
  <div id="content-wrapper">
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayContentWrapperTop"),$_smarty_tpl);?>

    
<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?><div class="sr-orange-category-block">

	<!-- New Banner mobile-->
	<a class="sr-mobile" href="https://mamalolashoes.com/20-new-collection"><img class="sr-mobile" width="100%" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
img/sr-img/MAMALOLA-MOVIL-sng.jpg" /></a>
	
	<a  class="sr-orange-category-block-item sr-orange-category-block-item-left" href="https://mamalolashoes.com/18-botines">		<?php echo smartyTranslate(array('s'=>"#Botines"),$_smarty_tpl);?>
<br/><br/>		<span class="sr-btn-white"><?php echo smartyTranslate(array('s'=>"Ver todas"),$_smarty_tpl);?>
</span>	</a>		<a class="sr-orange-category-block-item sr-orange-category-block-item-right" href="https://mamalolashoes.com/13-sneakers">		<?php echo smartyTranslate(array('s'=>"#Sneakers"),$_smarty_tpl);?>
<br/><br/>		<span class="sr-btn-white"><?php echo smartyTranslate(array('s'=>"Ver todas"),$_smarty_tpl);?>
</span>	</a></div><?php }?>
  <section id="main">

    
      
    

    
    <section id="content-hook_order_confirmation" class="card">
      <div class="card-block">
        <div class="row">
          <div class="col-md-12">

            
              <h3 class="h1 card-title">
                <i class="material-icons done">&#xE876;</i><?php echo smartyTranslate(array('s'=>'Your order is confirmed','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>

              </h3>
            

            <p>
              <?php echo smartyTranslate(array('s'=>'An email has been sent to your mail address %email%.','d'=>'Shop.Theme.Checkout','sprintf'=>array('%email%'=>$_smarty_tpl->tpl_vars['customer']->value['email'])),$_smarty_tpl);?>

              <?php if ($_smarty_tpl->tpl_vars['order']->value['details']['invoice_url']) {?>
                
                <?php echo smartyTranslate(array('s'=>'You can also [1]download your invoice[/1]','d'=>'Shop.Theme.Checkout','sprintf'=>array('[1]'=>"<a href='".((string)$_smarty_tpl->tpl_vars['order']->value['details']['invoice_url'])."'>",'[/1]'=>"</a>")),$_smarty_tpl);?>

              <?php }?>
            </p>

            
              <?php echo $_smarty_tpl->tpl_vars['HOOK_ORDER_CONFIRMATION']->value;?>

            

          </div>
        </div>
      </div>
    </section>

  <section id="content" class="page-content page-order-confirmation card">
    <div class="card-block">
      <div class="row">

        
          <?php /*  Call merged included template "checkout/_partials/order-confirmation-table.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('checkout/_partials/order-confirmation-table.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['order']->value['products'],'subtotals'=>$_smarty_tpl->tpl_vars['order']->value['subtotals'],'totals'=>$_smarty_tpl->tpl_vars['order']->value['totals'],'labels'=>$_smarty_tpl->tpl_vars['order']->value['labels'],'add_product_link'=>false), 0, '14997046405be62228f145e1-00427149');
content_5be622291fc9e7_73057759($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "checkout/_partials/order-confirmation-table.tpl" */?>
        

        
          <div id="order-details" class="col-md-4">
            <h3 class="h3 card-title"><?php echo smartyTranslate(array('s'=>'Order details','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
:</h3>
            <ul>
              <li><?php echo smartyTranslate(array('s'=>'Order reference: %reference%','d'=>'Shop.Theme.Checkout','sprintf'=>array('%reference%'=>$_smarty_tpl->tpl_vars['order']->value['details']['reference'])),$_smarty_tpl);?>
</li>
              <li><?php echo smartyTranslate(array('s'=>'Payment method: %method%','d'=>'Shop.Theme.Checkout','sprintf'=>array('%method%'=>$_smarty_tpl->tpl_vars['order']->value['details']['payment'])),$_smarty_tpl);?>
</li>
              <?php if (!$_smarty_tpl->tpl_vars['order']->value['details']['is_virtual']) {?>
                <li>
                  <?php echo smartyTranslate(array('s'=>'Shipping method: %method%','d'=>'Shop.Theme.Checkout','sprintf'=>array('%method%'=>$_smarty_tpl->tpl_vars['order']->value['carrier']['name'])),$_smarty_tpl);?>
<br>
                  <em><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['carrier']['delay'], ENT_QUOTES, 'UTF-8');?>
</em>
                </li>
              <?php }?>
            </ul>
          </div>
        

      </div>
    </div>
  </section>

  
    <?php if (!empty($_smarty_tpl->tpl_vars['HOOK_PAYMENT_RETURN']->value)) {?>
    <section id="content-hook_payment_return" class="card definition-list">
      <div class="card-block">
        <div class="row">
          <div class="col-md-12">
            <?php echo $_smarty_tpl->tpl_vars['HOOK_PAYMENT_RETURN']->value;?>

          </div>
        </div>
      </div>
    </section>
    <?php }?>
  

  
    <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_guest']) {?>
      <div id="registration-form" class="card">
        <div class="card-block">
          <h4 class="h4"><?php echo smartyTranslate(array('s'=>'Save time on your next order, sign up now','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</h4>
          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['render'][0][0]->smartyRender(array('file'=>'customer/_partials/customer-form.tpl','ui'=>$_smarty_tpl->tpl_vars['register_form']->value),$_smarty_tpl);?>

        </div>
      </div>
    <?php }?>
  

  
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayOrderConfirmation1'),$_smarty_tpl);?>

  

  
    <section id="content-hook-order-confirmation-footer">
      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayOrderConfirmation2'),$_smarty_tpl);?>

    </section>
  
  
  
  
   <p id="srTotalPrice" style="display:none;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['totals']['total']['amount']+$_smarty_tpl->tpl_vars['order']->value['subtotals']['tax']['amount'], ENT_QUOTES, 'UTF-8');?>
</p>
  
	
		<script>
			var srTotalPrice = parseFloat($('#srTotalPrice').text());
			fbq('track', 'Purchase', {
				value: srTotalPrice,
				currency: 'EUR',
			});
		</script>
		
		<!-- Google Code for Compra Conversion Page -->
		<script type="text/javascript">
		var srTotalPrice2 = parseFloat($('#srTotalPrice').text());
		/* <![CDATA[ */
		var google_conversion_id = 804972302;
		var google_conversion_label = "Tt-PCIubqYIBEI7O6_8C";
		var google_conversion_value = srTotalPrice2;
		var google_conversion_currency = "EUR";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/804972302/?value="+srTotalPrice2+"&amp;currency_code=EUR&amp;label=Tt-PCIubqYIBEI7O6_8C&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		
		

	<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?>	<div style="display:none;" class="sr-block-home-image-bottom">		<img src="https://mamalolashoes.com/img/img-bottom-left.jpg" class="sr-block-home-image-bottom-left sr-desktop" />		<img src="https://mamalolashoes.com/img/img-bottom-right.jpg" class="sr-block-home-image-bottom-right sr-desktop" />	
    <div class="sr-block-home-image-bottom-top sr-mobile"><img src="https://mamalolashoes.com/img/sr-img/M_BANNERLOOKBOOK_MOBILE1.jpg" /> </div>

      <img src="https://mamalolashoes.com/img/sr-img/M_BANNERLOOKBOOK_MOBILE2.jpg" class="sr-block-home-image-bottom-bottom sr-mobile" />
      

    <div style="clear: both;"></div>
  </div>	<?php }?>
  <div style="clear: both;"></div>

  <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?>
    <p style="display:none;" class="sr-link-home-bottom"><?php echo smartyTranslate(array('s'=>"Lookbook Primavera / Verano 18 "),$_smarty_tpl);?>
<a href="https://mamalolashoes.com/content/15-campaigns"><?php echo smartyTranslate(array('s'=>"Ver Lookbook >"),$_smarty_tpl);?>
</a></p>
  <?php }?>


    
      <footer class="page-footer">
        
          <!-- Footer content -->
        
      </footer>
    

  </section>


    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayContentWrapperBottom"),$_smarty_tpl);?>

  </div>


          
        </div>
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayWrapperBottom"),$_smarty_tpl);?>

      </section>

      <footer id="footer">
        
          <?php /*  Call merged included template "_partials/footer.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0, '14997046405be62228f145e1-00427149');
content_5be622292a5052_75365706($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/footer.tpl" */?>
        
      </footer>
	</div>
	</div>
    </main>

    
      <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, '14997046405be62228f145e1-00427149');
content_5be62229136e43_63202126($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>
    

    
      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl);?>

    
  </body>

</html>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/head.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be622290c7f58_27790713')) {function content_5be622290c7f58_27790713($_smarty_tpl) {?>

  <meta charset="utf-8">


  <meta http-equiv="x-ua-compatible" content="ie=edge">



  
    <!--<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
      var OneSignal = window.OneSignal || [];
      OneSignal.push(function() {
        OneSignal.init({
          appId: "704c3f4e-0d61-4b59-a500-29409befff23",
          autoRegister: true,
        });
      });
    </script>-->
  



  <title><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
</title>
  <meta name="description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">
  <meta name="keywords" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['keywords'], ENT_QUOTES, 'UTF-8');?>
">
  <?php if ($_smarty_tpl->tpl_vars['page']->value['meta']['robots']!=='index') {?>
    <meta name="robots" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['robots'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['page']->value['canonical']) {?>
    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['canonical'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>

<meta name="google-site-verification" content="TVc0TWfd6v3hKDcZYvlY6BGFLBwCx9p9SLDphOfsGSM" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

<!-- TemplateTrip theme google font-->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,500,700" rel="stylesheet"> 
	<!--link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alex+Brush:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet"--> 
<!-- TemplateTrip theme google font-->

  <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon'], ENT_QUOTES, 'UTF-8');?>
?<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['favicon_update_time'], ENT_QUOTES, 'UTF-8');?>
">



  <?php /*  Call merged included template "_partials/stylesheets.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/stylesheets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('stylesheets'=>$_smarty_tpl->tpl_vars['stylesheets']->value), 0, '14997046405be62228f145e1-00427149');
content_5be622290fc109_24153301($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/stylesheets.tpl" */?>

<link href="https://mamalolashoes.com/js/jquery/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./savour/slick/slick.css">
<link rel="stylesheet" type="text/css" href="./savour/slick/slick-theme.css">

  <?php /*  Call merged included template "_partials/javascript.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate("_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['head'],'vars'=>$_smarty_tpl->tpl_vars['js_custom_vars']->value), 0, '14997046405be62228f145e1-00427149');
content_5be62229136e43_63202126($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "_partials/javascript.tpl" */?>



  <?php echo $_smarty_tpl->tpl_vars['HOOK_HEADER']->value;?>




	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Facebook Pixel Code --><script>  !function(f,b,e,v,n,t,s)  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?  n.callMethod.apply(n,arguments):n.queue.push(arguments)};  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';  n.queue=[];t=b.createElement(e);t.async=!0;  t.src=v;s=b.getElementsByTagName(e)[0];  s.parentNode.insertBefore(t,s)}(window, document,'script',  'https://connect.facebook.net/en_US/fbevents.js');  fbq('init', '1618055778308789');  fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"  src="https://www.facebook.com/tr?id=1618055778308789&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code --><?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/stylesheets.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be622290fc109_24153301')) {function content_5be622290fc109_24153301($_smarty_tpl) {?>
<?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
  <?php if (!strstr($_smarty_tpl->tpl_vars['stylesheet']->value['uri'],"theme.css")) {?>
  <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['uri'], ENT_QUOTES, 'UTF-8');?>
"  type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }?>
<?php } ?>
  <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
/savour/savour.css?1"  type="text/css" >
<?php  $_smarty_tpl->tpl_vars['stylesheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['stylesheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stylesheets']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->key => $_smarty_tpl->tpl_vars['stylesheet']->value) {
$_smarty_tpl->tpl_vars['stylesheet']->_loop = true;
?>
  <style>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['content'], ENT_QUOTES, 'UTF-8');?>

  </style>
<?php } ?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/javascript.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be62229136e43_63202126')) {function content_5be62229136e43_63202126($_smarty_tpl) {?>
<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['external']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
	<?php if ($_smarty_tpl->tpl_vars['js']->value['uri']=="https://mamalolashoes.com/themes/PRS01/assets/js/custom.js") {?>
		<script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['uri'], ENT_QUOTES, 'UTF-8');?>
?2" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['attribute'], ENT_QUOTES, 'UTF-8');?>
></script>
	<?php } else { ?>
		<script type="text/javascript" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['js']->value['attribute'], ENT_QUOTES, 'UTF-8');?>
></script>
	<?php }?>
<?php } ?>

<?php  $_smarty_tpl->tpl_vars['js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['javascript']->value['inline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js']->key => $_smarty_tpl->tpl_vars['js']->value) {
$_smarty_tpl->tpl_vars['js']->_loop = true;
?>
  <script type="text/javascript">
    <?php echo $_smarty_tpl->tpl_vars['js']->value['content'];?>

  </script>
<?php } ?>

<?php if (isset($_smarty_tpl->tpl_vars['vars']->value)&&count($_smarty_tpl->tpl_vars['vars']->value)) {?>
  <script type="text/javascript">
    <?php  $_smarty_tpl->tpl_vars['var_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['var_value']->_loop = false;
 $_smarty_tpl->tpl_vars['var_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vars']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['var_value']->key => $_smarty_tpl->tpl_vars['var_value']->value) {
$_smarty_tpl->tpl_vars['var_value']->_loop = true;
 $_smarty_tpl->tpl_vars['var_name']->value = $_smarty_tpl->tpl_vars['var_value']->key;
?>
    var <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
 = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['var_value']->value);?>
;
    <?php } ?>
  </script>
<?php }?>

<script type="text/javascript" src="https://mamalolashoes.com/savour/dynlib.js"></script>
<script type="text/javascript" src="https://mamalolashoes.com/savour/bouncingimages.js"></script>
<script type="text/javascript" src="https://mamalolashoes.com/js/jquery/plugins/fancybox/jquery.fancybox.js"></script>

<script>
<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?>
  var isHompage = true;
<?php } else { ?>
  var isHompage = false;
<?php }?>
 



</script>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/product-activation.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be6222916fca0_76171834')) {function content_5be6222916fca0_76171834($_smarty_tpl) {?>   
<?php if ($_smarty_tpl->tpl_vars['page']->value['admin_notifications']) {?>
  <div class="alert alert-warning row" role="alert">
    <div class="container">
      <div class="row">
        <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['page']->value['admin_notifications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
          <div class="col-sm-12">
            <i class="material-icons float-xs-left">&#xE001;</i>
            <p class="alert-text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['notif']->value['message'], ENT_QUOTES, 'UTF-8');?>
</p>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php }?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/header.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be62229179191_21842604')) {function content_5be62229179191_21842604($_smarty_tpl) {?>

 
 <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50279320 = new Ya.Metrika2({
                    id:50279320,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50279320" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
 

 <div class="sr-cintillo sr-cintillo-desktop sr-desktop">Babe, ¿celebramos el SINGLES' DAY?: 20% DTO. con el código SINGLESDAY</div>

  <div class="header-banner">
    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayBanner'),$_smarty_tpl);?>

  </div>



  <nav class="header-nav">
    <div class="container">
        <div class="row">
          <div class="hidden-sm-down top-nav">
            <div class="col-md-4 col-xs-12">
              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNav1'),$_smarty_tpl);?>

            </div>
            <div class="col-md-8 right-nav"><div><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['contact'], ENT_QUOTES, 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>"Contacto"),$_smarty_tpl);?>
 </a></div>
                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNav2'),$_smarty_tpl);?>

            </div>
          </div>
          <div class="hidden-md-up text-sm-center mobile">
            <div class="float-xs-left" id="menu-icon">
              <i class="material-icons">&#xE5D2;</i>
            </div>
            <div class="float-xs-right" id="_mobile_cart"></div>
            <div class="float-xs-right" id="_mobile_user_info"></div>
            <div class="top-logo" id="_mobile_logo"></div>
            <div class="clearfix"></div>
          </div>
        </div>
    </div>
  </nav>



  <div class="header-top">
    <div class="container">
       <div class="row">
        <div class="col-md-2 hidden-sm-down" id="_desktop_logo">
          <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
">
            <img class="logo img-responsive" src="https://mamalolashoes.com/img/logo1.svg" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">
          </a>
        </div>
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
	<div class="col-md-12 col-sm-12 position-static container">
	  <div class="row">
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayTop'),$_smarty_tpl);?>

		<div class="clearfix"></div>
	  </div>
	</div>	
  </div>
  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayNavFullWidth'),$_smarty_tpl);?>


<div style="display:none;" id="sr-page-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
</div>
<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name']=='index') {?>
	
		<div id="top_column" class="">
			<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayTopColumn'),$_smarty_tpl);?>

		 </div>
	
<?php }?>
<div style="clear:both"></div>
 <div class="sr-cintillo sr-mobile">Babe, ¿celebramos el SINGLES' DAY?: 20% DTO. con el código SINGLESDAY</div><?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/notifications.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be622291871f5_53282659')) {function content_5be622291871f5_53282659($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['notifications']->value)) {?>
<aside id="notifications">
  <div class="container">
    <?php if ($_smarty_tpl->tpl_vars['notifications']->value['error']) {?>
      
        <article class="alert alert-danger" role="alert" data-alert="danger">
          <ul>
            <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['error']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
              <li><?php echo $_smarty_tpl->tpl_vars['notif']->value;?>
</li>
            <?php } ?>
          </ul>
        </article>
      
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['notifications']->value['warning']) {?>
      
        <article class="alert alert-warning" role="alert" data-alert="warning">
          <ul>
            <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['warning']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
              <li><?php echo $_smarty_tpl->tpl_vars['notif']->value;?>
</li>
            <?php } ?>
          </ul>
        </article>
      
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['notifications']->value['success']) {?>
      
        <article class="alert alert-success" role="alert" data-alert="success">
          <ul>
            <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['success']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
              <li><?php echo $_smarty_tpl->tpl_vars['notif']->value;?>
</li>
            <?php } ?>
          </ul>
        </article>
      
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['notifications']->value['info']) {?>
      
        <article class="alert alert-info" role="alert" data-alert="info">
          <ul>
            <?php  $_smarty_tpl->tpl_vars['notif'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['notif']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['notifications']->value['info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['notif']->key => $_smarty_tpl->tpl_vars['notif']->value) {
$_smarty_tpl->tpl_vars['notif']->_loop = true;
?>
              <li><?php echo $_smarty_tpl->tpl_vars['notif']->value;?>
</li>
            <?php } ?>
          </ul>
        </article>
      
    <?php }?>
  </div>
</aside>
<?php }?>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/breadcrumb.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be6222919c587_57363819')) {function content_5be6222919c587_57363819($_smarty_tpl) {?>
<nav data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['breadcrumb']->value['count'], ENT_QUOTES, 'UTF-8');?>
" class="breadcrumb hidden-sm-down">
<div class="container">
<div class="breadcrumb-img1">

  </div>
 <div class="breadcrumb-img2">

  </div>
  <ol itemscope itemtype="https://schema.org/BreadcrumbList">
    <?php  $_smarty_tpl->tpl_vars['path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['breadcrumb']->value['links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['breadcrumb']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['path']->key => $_smarty_tpl->tpl_vars['path']->value) {
$_smarty_tpl->tpl_vars['path']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['breadcrumb']['iteration']++;
?>
      
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a itemprop="item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
            <span itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
          </a>
          <meta itemprop="position" content="<?php echo htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['breadcrumb']['iteration'], ENT_QUOTES, 'UTF-8');?>
">
        </li>
      
    <?php } ?>
  </ol>
  </div>
</nav>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/checkout/_partials/order-confirmation-table.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be622291fc9e7_73057759')) {function content_5be622291fc9e7_73057759($_smarty_tpl) {?>
<div id="order-items" class="col-md-12">

  
    <h3 class="card-title h3"><?php echo smartyTranslate(array('s'=>'Order items','d'=>'Shop.Theme.Checkout'),$_smarty_tpl);?>
</h3>
  

  <div class="order-confirmation-table">

    
      <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
        <div class="order-line row">
          <div class="col-sm-2 col-xs-3">
            <span class="image">
              <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['medium']['url'], ENT_QUOTES, 'UTF-8');?>
" />
            </span>
          </div>
          <div class="col-sm-4 col-xs-9 details">
            <?php if ($_smarty_tpl->tpl_vars['add_product_link']->value) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
" target="_blank"><?php }?>
              <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span>
            <?php if ($_smarty_tpl->tpl_vars['add_product_link']->value) {?></a><?php }?>
            <?php if (count($_smarty_tpl->tpl_vars['product']->value['customizations'])) {?>
              <?php  $_smarty_tpl->tpl_vars["customization"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["customization"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value['customizations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["customization"]->key => $_smarty_tpl->tpl_vars["customization"]->value) {
$_smarty_tpl->tpl_vars["customization"]->_loop = true;
?>
                <div class="customizations">
                  <a href="#" data-toggle="modal" data-target="#product-customizations-modal-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['customization']->value['id_customization'], ENT_QUOTES, 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'Product customization','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>
</a>
                </div>
                <div class="modal fade customization-modal" id="product-customizations-modal-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['customization']->value['id_customization'], ENT_QUOTES, 'UTF-8');?>
" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"><?php echo smartyTranslate(array('s'=>'Product customization','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>
</h4>
                      </div>
                      <div class="modal-body">
                        <?php  $_smarty_tpl->tpl_vars["field"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["field"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['customization']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["field"]->key => $_smarty_tpl->tpl_vars["field"]->value) {
$_smarty_tpl->tpl_vars["field"]->_loop = true;
?>
                          <div class="product-customization-line row">
                            <div class="col-sm-3 col-xs-4 label">
                              <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['label'], ENT_QUOTES, 'UTF-8');?>

                            </div>
                            <div class="col-sm-9 col-xs-8 value">
                              <?php if ($_smarty_tpl->tpl_vars['field']->value['type']=='text') {?>
                                <?php if ((int)$_smarty_tpl->tpl_vars['field']->value['id_module']) {?>
                                  <?php echo $_smarty_tpl->tpl_vars['field']->value['text'];?>

                                <?php } else { ?>
                                  <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['text'], ENT_QUOTES, 'UTF-8');?>

                                <?php }?>
                              <?php } elseif ($_smarty_tpl->tpl_vars['field']->value['type']=='image') {?>
                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['image']['small']['url'], ENT_QUOTES, 'UTF-8');?>
">
                              <?php }?>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php }?>
            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"unit_price"),$_smarty_tpl);?>

          </div>
          <div class="col-sm-6 col-xs-12 qty">
            <div class="row">
              <div class="col-xs-5 text-sm-right text-xs-left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
</div>
              <div class="col-xs-2"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
</div>
              <div class="col-xs-5 text-xs-right bold"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['total'], ENT_QUOTES, 'UTF-8');?>
</div>
            </div>
          </div>
        </div>
      <?php } ?>

      <hr>

      <table>
        <?php  $_smarty_tpl->tpl_vars['subtotal'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subtotal']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subtotals']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subtotal']->key => $_smarty_tpl->tpl_vars['subtotal']->value) {
$_smarty_tpl->tpl_vars['subtotal']->_loop = true;
?>
          <?php if ($_smarty_tpl->tpl_vars['subtotal']->value['type']!=='tax') {?>
            <tr>
              <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['label'], ENT_QUOTES, 'UTF-8');?>
</td>
              <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['value'], ENT_QUOTES, 'UTF-8');?>
</td>
            </tr>
          <?php }?>
        <?php } ?>
        <?php if ($_smarty_tpl->tpl_vars['subtotals']->value['tax']['label']!==null) {?>
          <tr class="sub">
            <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotals']->value['tax']['label'], ENT_QUOTES, 'UTF-8');?>
</td>
            <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotals']->value['tax']['value'], ENT_QUOTES, 'UTF-8');?>
</td>
          </tr>
        <?php }?>
        <tr class="font-weight-bold">
          <td><span class="text-uppercase"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['totals']->value['total']['label'], ENT_QUOTES, 'UTF-8');?>
</span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['labels']->value['tax_short'], ENT_QUOTES, 'UTF-8');?>
</td>
          <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['totals']->value['total']['value'], ENT_QUOTES, 'UTF-8');?>
</td>
        </tr>
      </table>
    

  </div>
</div>
<?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2018-11-10 01:11:21
         compiled from "/home/mamalolamz/www/themes/PRS01/templates/_partials/footer.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5be622292a5052_75365706')) {function content_5be622292a5052_75365706($_smarty_tpl) {?>
 <div class="footer-top">
<div class="container">
  <div class="row">
    
      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayFooterBefore'),$_smarty_tpl);?>

    
  </div>
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="row">

      <div class="sr-social-mobile sr-mobile"></div>
      
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayFooter'),$_smarty_tpl);?>

      
    </div>
    <div class="row">
      
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayFooterAfter'),$_smarty_tpl);?>

      
    </div>
  </div>
</div>
<div class="bottom-footer">
	<div class="container">    
	 <div class="row">
      <div class="col-md-12">
        <p class="text-sm-center">
          
              <?php echo smartyTranslate(array('s'=>'%copyright% %year% - MAMALOLA','sprintf'=>array('%prestashop%'=>'PrestaShop™','%year%'=>date('Y'),'%copyright%'=>'©'),'d'=>'Shop.Theme.Global'),$_smarty_tpl);?>

          
        </p>
      </div>
	  </div>
	  </div>
</div>
<div class="backtotop-img">
	<a href="#" class="goToTop show" title="Back to top"> <i class="material-icons arrow-up"></i></a>
	</div>
<a class="goToTop ttbox-img show" href="#"> </a>

<div id="cookieBlock" class="my_cookies" style="display:none">
  <div class="sr-alert sr-alert-info sr-alert-dismissible" >
	<p class="coockies-text"><?php echo smartyTranslate(array('s'=>"Utilizamos cookies propias y de terceros para mejorar su experiencia de usuario. Si continua navegando consideramos que acepta su uso."),$_smarty_tpl);?>
<a href="https://mamalolashoes.com/content/10-politica-de-cookies"> <?php echo smartyTranslate(array('s'=>"Leer más"),$_smarty_tpl);?>
</a> <span id="btn-close-cookie"><?php echo smartyTranslate(array('s'=>"OK"),$_smarty_tpl);?>
</span></p>
  </div>
</div>

<!--TMP MODAL POPUP-->
<script src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
/savour/jquery.colorbox.js"></script>
<script src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
/savour/modal-popup.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8');?>
/savour/modal-popup.css?a=<?php echo htmlspecialchars(rand(1,1000), ENT_QUOTES, 'UTF-8');?>
" />

<!-- Trigger/Open The Modal -->
<div id="modal-movil" style="display:none;">
		<button id="btn_LaunchNewsletterModal" style="display:none;">Open Modal</button>
		<!-- The Modal -->
		<div id="LaunchNewsletterModal" class="modal">
		<!-- Layer Rayos -->
		<!--<div class="LayerRayos"><img src="https://mamalolashoes.com/savour/GIF_POP-UP.gif"></div>-->
		<div class="LayerFormTop _modal_BORDER_RADIUS_BOTTOM" style="top:62% !important;">
			<div id="_modal_FORM" class="_modal_CONTENT">
				<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
					<input type="text" name="NAME" class="_modal_FORM_INPUT" placeholder="Nombre">
					<input type="text" name="EMAIL" class="_modal_FORM_INPUT" placeholder="Correo electrónico"><br>
					<input class="_modal_CHEKBOX_GDPR" type="checkbox" name="GDPR" id="GDPR"> <span class="_modal_TEXTO_GDPR"> <input type="checkbox" required style="width:12px;height:12px;opacity:1;display:none;" /> He leído y acepto las <a href="https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui" target="_blank" style="color:#000 !important; font-weight:bold;">bases legales del sorteo</a> y la <a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank" style="color:#000 !important; font-weight:bold;">política de privacidad</a> de Mamalola..</span><br><br>
					<input type="submit" class="_modal_FORM_SUBMIT" value="APUNTARME >">
					<div class="_modal_FORM_SUBMIT_MOBILE">></div>
				</form>
			</div>
			<div id="_modal_LEGAL" class="_modal_CONTENT _modal_LEGAL _modal_BORDER_RADIUS_BOTTOM" style="padding-bottom:40px !important;display:none;">
				<a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">Más información sobre política de privacidad</a>
			</div>
		</div>
		<!-- Modal content -->
		<div class="modal-content moda-popup popup-background-mobile">
			<span class="close">&times;</span>
			<div class="_modal_CONTENT _modal_HEADER _modal_BORDER_RADIUS_TOP" style="margin-bottom:5%; font-size:2.2em !important;"></div>
			<!--div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;"><strong>¿Quieres unas Mamalola por la face?</strong></div-->
			<div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase; line-height:1.5em !important; margin-top:-30px;">Suscríbete a nuestra newsletter, participa y gana nuestras Bugui.</div>
			<div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;">¡En el color que tú elijas!</div>
			<!--div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;"><?php echo smartyTranslate(array('s'=>"modelo más top"),$_smarty_tpl);?>
.</div-->
			<div class="_modal_RESERVED_CONTENT"></div>
		</div>
		</div>
</div>
</div>

<!--<div style="display:none;">
	<a href="#popup-nl" class="nl-fancybox" >link</a>
	<table id="popup-nl" class="table-newsletter-popup">
		<tr>
			<td class="nl-img-left content-table-newsletter-popup">
				<h3>¿Quieres unas Mamalola<br>por la face?</h3>
				<p class="title-table-newsletter-popup">
					Suscríbete a nuestra <strong>Newsletter</stong>,<br>
					participa y gana nuestras Bugui.<br>
					¡En el color que tú elijas!
				</p>
				<div class="NW_FRM_Modal">	
					<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
						<input type="text" name="NAME" placeholder="Nombre" class="input-name-content-newsletter-popup"><br>
						<input type="text" name="EMAIL" placeholder="Correo electrónico" class="input-email-content-newsletter-popup"><br>
						<input type="submit" value="No me lo pierdo >" class="input-submit-content-newsletter-popup">
					</form>
					<br><br>
					<p class="link-content-newsletter-popup">
						He leído y acepto las <a href="https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui" target="_blank">bases legales del sorteo</a> y la <a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">política de privacidad</a> de Mamalola.
					</p>
				</div>
			</td>
		</tr>
	</table>
</div>-->

<div style="display:none;">
	<a href="#popup-nl" class="nl-fancybox" >link</a>
	
	<table id="popup-nl">
		<tr>
			<td style="width:2%;"></td>
			<td style="width:39%;text-align:center;background:#FFFFFF;-webkit-background-size: cover;background-size: cover; color:#000 !important;">
				<!--p class="nl-title" style="color:black;text-transform:uppercase;margin-top:0%; font-size:2.2em; letter-spacing:5px; padding: 0 40px;margin-bottom:40px;"><strong>10% dto.</strong></p-->
				<h1 style="margin-top:0px; text-align: left; font-size: 27px;">¿Quieres unas Mamalola<br>por la face?</h1>
				<p style="color:#000; font-size:21px; line-height:1.5em;">
					Suscríbete a nuestra newsletter,<br>participa y gana nuestras Bugui.<br>
					¡En el color que tú elijas!
				</p>
				<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
					<div id="mc_embed_signup_scroll">
					<img src="https://mamalolashoes.com/img/ICONO_SOBRE.png" width="42px" style="display:none;" class="nl-img-left">
					<div class="mc-field-group" style="margin-top: 10px;">
						<input type="text" required value="" name="FNAME" class="required email" id="mce-FNAME" placeholder='<?php echo smartyTranslate(array('s'=>"Nombre"),$_smarty_tpl);?>
' style="border-bottom:1px solid #000; width:70%;">
					</div>
					<div class="indicates-required hidden" style="display:none;"><span class="asterisk">*</span> <?php echo smartyTranslate(array('s'=>"indicates required"),$_smarty_tpl);?>
</div>

						<input type="email" placeholder='<?php echo smartyTranslate(array('s'=>"Correo electrónico"),$_smarty_tpl);?>
' value="" name="EMAIL" class="required email" id="mce-EMAIL" style="border-bottom:1px solid #000;margin-bottom:1px; width:70%; margin-bottom:4% !important;">
					</div>
					<div class="mc-field-group hidden" style="display:none;">
						<label for="mce-LNAME">Last Name </label>
						<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div> 
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9f55b42ec392a5921c498348a_815046a7f3" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value='<?php echo smartyTranslate(array('s'=>"No me lo pierdo >"),$_smarty_tpl);?>
' name="subscribe" id="mc-embedded-subscribe" class="button" style="background-color: #000000 !important; color:#FFF !important; width: 70%; padding: 14px !important; margin-bottom:9%; margin-top:20px;"></div>
					<p style="font-size:9px;color:#000;margin-bottom:0px;width:98%; margin-left:2%; line-height:0px; color:#000 !important; text-align:left;"><input type="checkbox" required style="font-weight:bold; width:12px;height:12px;opacity:1;" /> He leído y acepto <a target="_blank" style="font-weight:bold; color:#000 !important; text-decoration:underline;" href='https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui'>bases legales del sorteo</a> <?php echo smartyTranslate(array('s'=>'y la'),$_smarty_tpl);?>
 <a target="_blank" style="font-weight:bold; color:#000 !important; text-decoration:underline;" href='https://mamalolashoes.com/content/7-politica-de-privacidad'><?php echo smartyTranslate(array('s'=>'política de privacidad'),$_smarty_tpl);?>
</a> <?php echo smartyTranslate(array('s'=>'de MAMALOLA'),$_smarty_tpl);?>
.</p>
				</form>
			</td>
			<td style="width:59%; text-align:right !important;;" class="nl-img-left">
				<img src="https://mamalolashoes.com/img/sr-img/banner3/COMPLETO.png" style="width:100% !important;" id="fotoDisplay"/>
			</td>	
		</tr>
	</table>
</div>


<script src="./savour/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117775943-1"></script>-->
<script>
  /*window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117775943-1');*/


	function ShowNewsletterModal(){
		jQuery('body').css('overflow','hidden');
		jQuery('#btn_LaunchNewsletterModal').click();
		setCookie('showNewsletterPopup', 'true', 1);
	}

  function showNewsletterPopup(){
		setCookie('showNewsletterPopup', 'true', 1);
		$(".nl-fancybox").fancybox({  maxWidth  : 823,
		   maxHeight : 449,
		   fitToView : false,
		   width     : '100%',
		   height    : '70%',
		   afterShow: function() {
			  var imagen = $('#fotoDisplay');
			  var maxHeightLong = 823;
			  var maxWidthLong = 449;
			  var ratio = 0;
			  var width = imagen.width();
			  var height = imagen.height();
	 
				if(width > maxWidthLong){
					ratio = maxWidthLong / width;   // get ratio for scaling image
					$('#fotoDisplay').css("width", maxWidthLong); // Set new width
					$('#fotoDisplay').css("height", height * ratio);  // Scale height based on ratio
					height = height * ratio;    // Reset height to match scaled image
				}

				var width = $(this).width();    // Current image width
				var height = $(this).height();  // Current image height

			 if(height > maxHeightLong){
				ratio = maxHeightLong / height; // get ratio for scaling image
				$('#fotoDisplay').css("height", maxHeightLong);   // Set new height
				$('#fotoDisplay').css("width", width * ratio);    // Scale width based on ratio
				width = width * ratio;				
			 }
			resizeModal();
		  },		  
			autoSize  : false
		}).trigger('click');
	}
	
	function checkCookieNewsletter() {
		var lastShow = getCookie("showNewsletterPopup");
		var registredNewsletter = getCookie("registredNewsletter");

		if (lastShow == undefined && registredNewsletter == undefined) {
			
			setTimeout(function(){ 
				if(window.outerWidth>991){
					showNewsletterPopup()
				}else{
					jQuery('#modal-movil').css('display','block');
					ShowNewsletterModal();
				}
			}, 4000);
		}
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	} 
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
	}

	jQuery(document).ready(function(){
		checkCookieNewsletter();

		var dominio = window.location.href;
		var urlBlog = dominio.substring(0,dominio.indexOf(".com/")+4) + "/blog";
		jQuery('#footer_sub_menu_13236').append('<li style="display:none;"><a href="' + urlBlog + '">Magazine</a></li>');

		var urlNow = window.location.href;
		/*if(urlNow == 'https://mamalolashoes.com/')
			loadBouncingImages(numeroAleatorio(1,5));*/

		//var LinkRebajas = '<li class="category menuRebajas" id="category-16"><a class="dropdown-item enlaceRebajas" href="https://mamalolashoes.com/16-rebajas" data-depth="0">#REBAJAS &#39;18</a></li>';
		//jQuery('.top-menu').append(LinkRebajas);

	});

</script>

<style>
  .fancybox-overlay {
      z-index: 1000000 !important;
  }
</style>

<!--JS para lookboo-->
<script language="javascript" src="https://mamalolashoes.com/savour/jquery.slides.min.js"></script>

<!-- Google Code para etiquetas de remarketing -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información personal identificable o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte https://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 804972302;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/804972302/?guid=ON&amp;script=0"/>
</div>
</noscript>




<div id="field-required" class="DIV_contentModal">
	<div class="DIV_textRequired">
		Debe aceptar los términos y condiciones para continuar con el proceso.<br>
		<button class="btn-aceptar-modal btn btn-primary center-block">Aceptar</buton><br>
	</div>
</div><?php }} ?>
