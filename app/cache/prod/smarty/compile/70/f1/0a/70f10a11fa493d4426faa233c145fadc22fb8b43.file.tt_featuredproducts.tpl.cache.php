<?php /* Smarty version Smarty-3.1.19, created on 2018-11-08 12:34:31
         compiled from "modules/tt_featuredproducts/views/templates/hook/tt_featuredproducts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14221004745be41f47a817e5-85312838%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70f10a11fa493d4426faa233c145fadc22fb8b43' => 
    array (
      0 => 'modules/tt_featuredproducts/views/templates/hook/tt_featuredproducts.tpl',
      1 => 1524814884,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14221004745be41f47a817e5-85312838',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
    'tt_total' => 0,
    'tt_cnt' => 0,
    'product' => 0,
    'allProductsLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be41f47a93349_08867772',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be41f47a93349_08867772')) {function content_5be41f47a93349_08867772($_smarty_tpl) {?>
<?php $_smarty_tpl->tpl_vars["tt_cnt"] = new Smarty_variable("1", null, 0);?>
<?php $_smarty_tpl->tpl_vars["tt_total"] = new Smarty_variable("0", null, 0);?>
<?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
	<?php $_smarty_tpl->tpl_vars['tt_total'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_total']->value+1, null, 0);?>
<?php } ?>

<section class="ttfeatured-products clearfix">
    <h3 class="tab-title"><?php echo smartyTranslate(array('s'=>'Featured Products','d'=>'Modules.FeaturedProducts.Shop'),$_smarty_tpl);?>
</h3>
    <i class="material-icons left ">&#xE314;</i>
    <div class="ttfeatured-content products">
        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
			<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
				<!-- Start TemplateTrip 2 product slide code -->
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2!=0) {?>
				<ul>
					<li class="featureli">
						<ul>
						<li>
				<?php }?>
			<?php }?>
				<!-- End TemplateTrip 2 product slide code -->

				<?php echo $_smarty_tpl->getSubTemplate ("catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>


				<!-- Start TemplateTrip 2 product slide code -->
			<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
						</li>
						</ul>
					</li>
					</ul>
				<?php }?>
				<?php }?>

				<?php $_smarty_tpl->tpl_vars['tt_cnt'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_cnt']->value+1, null, 0);?>
				<!-- End TemplateTrip 2 product slide code -->
        <?php } ?>
		<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=8) {?>
			<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
					</li>
					</ul>
				</li>
				</ul>
			<?php }?>
		<?php }?>
    </div>

    <i class="material-icons right ">&#xE315;</i>
	<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>4) {?>
	<div class="customNavigation">
		<a class="btn prev ttfeature_prev"><?php echo smartyTranslate(array('s'=>'Prev','d'=>'Modules.FeaturedProducts.Shop'),$_smarty_tpl);?>
</a>
		<a class="btn next ttfeature_next"><?php echo smartyTranslate(array('s'=>'Next','d'=>'Modules.FeaturedProducts.Shop'),$_smarty_tpl);?>
</a>
	</div>
	<?php }?>
    <div class="allproduct"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['allProductsLink']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'Ver colección','d'=>'Modules.FeaturedProducts.Shop'),$_smarty_tpl);?>
</a></div>
</section>
<?php }} ?>
