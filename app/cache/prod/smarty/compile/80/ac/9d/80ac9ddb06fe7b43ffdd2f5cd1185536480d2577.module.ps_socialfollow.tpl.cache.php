<?php /* Smarty version Smarty-3.1.19, created on 2018-11-08 12:34:31
         compiled from "module:ps_socialfollow/ps_socialfollow.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14922091655be41f47cc31b7-71240932%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '80ac9ddb06fe7b43ffdd2f5cd1185536480d2577' => 
    array (
      0 => 'module:ps_socialfollow/ps_socialfollow.tpl',
      1 => 1524225330,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '14922091655be41f47cc31b7-71240932',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'social_links' => 0,
    'social_link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be41f47cc8685_76971426',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be41f47cc8685_76971426')) {function content_5be41f47cc8685_76971426($_smarty_tpl) {?>


  <div class="block-social links col-sm-3 sr-desktop">
    <h4 class="hidden-sm-down"><?php echo smartyTranslate(array('s'=>'Follow US','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>
</h4>
	<div  class="title clearfix hidden-md-up" data-toggle="collapse" data-target="#social-footer">
  	<!--span class="h3"><?php echo smartyTranslate(array('s'=>'social','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>
</span>
	<span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
	</span-->
  </div>
    <ul id="social-footer" class="collapse in">
      <?php  $_smarty_tpl->tpl_vars['social_link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['social_link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['social_links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['social_link']->key => $_smarty_tpl->tpl_vars['social_link']->value) {
$_smarty_tpl->tpl_vars['social_link']->_loop = true;
?>
        <li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['social_link']->value['class'], ENT_QUOTES, 'UTF-8');?>
"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['social_link']->value['url'], ENT_QUOTES, 'UTF-8');?>
" target="_blank"><P><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['social_link']->value['label'], ENT_QUOTES, 'UTF-8');?>
</P></a></li>
      <?php } ?>
    </ul>
  </div>

<?php }} ?>
