<?php /* Smarty version Smarty-3.1.19, created on 2018-11-08 12:34:31
         compiled from "module:tt_specials/views/templates/hook/tt_specials.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19846309885be41f476829a1-02010776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e6dbda1ddf326fc35e22f1cc40cfd1117be2f17' => 
    array (
      0 => 'module:tt_specials/views/templates/hook/tt_specials.tpl',
      1 => 1523439482,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '19846309885be41f476829a1-02010776',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
    'tt_total' => 0,
    'tt_cnt' => 0,
    'product' => 0,
    'allSpecialProductsLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be41f47699892_77236186',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be41f47699892_77236186')) {function content_5be41f47699892_77236186($_smarty_tpl) {?>
<?php $_smarty_tpl->tpl_vars["tt_cnt"] = new Smarty_variable("1", null, 0);?>
<?php $_smarty_tpl->tpl_vars["tt_total"] = new Smarty_variable("0", null, 0);?>
<?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
	<?php $_smarty_tpl->tpl_vars['tt_total'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_total']->value+1, null, 0);?>
<?php } ?>

<section class="ttspecial-products clearfix row">
   <div class="block_content container">
  <h3 class="tt-title"><?php echo smartyTranslate(array('s'=>'Specials','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</h3>
  <div class="customNavigation">
<a class="btn prev ttspecial_prev"><?php echo smartyTranslate(array('s'=>'Prev','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</a>
<a class="btn next ttspecial_next"><?php echo smartyTranslate(array('s'=>'Next','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</a>
</div>
   <span class="ttspecial-subtitle">special items in this season</span>
  <div class="ttspecial-list container">
  <div class="row">
  <div class="products">
    <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
	<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=6) {?>
				<!-- Start TemplateTrip 2 product slide code -->
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2!=0) {?>
				<ul>
					<li class="specialli">
						<ul>
						<li>
				<?php }?>
			<?php }?>
				<!-- End TemplateTrip 2 product slide code -->
      <?php echo $_smarty_tpl->getSubTemplate ("catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

	  <!-- Start TemplateTrip 2 product slide code -->
			<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=6) {?>
				<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
						</li>
						</ul>
					</li>
					</ul>
				<?php }?>
				<?php }?>

				<?php $_smarty_tpl->tpl_vars['tt_cnt'] = new Smarty_variable($_smarty_tpl->tpl_vars['tt_cnt']->value+1, null, 0);?>
		<!-- End TemplateTrip 2 product slide code -->
    <?php } ?>
	<?php if ($_smarty_tpl->tpl_vars['tt_total']->value>=6) {?>
			<?php if ($_smarty_tpl->tpl_vars['tt_cnt']->value%2==0) {?>
					</li>
					</ul>
				</li>
				</ul>
			<?php }?>
		<?php }?>
  </div>
  </div>
  </div>
  <?php if ($_smarty_tpl->tpl_vars['tt_total']->value>4) {?>
	<div class="customNavigation">
		<a class="btn prev ttspecial_prev"><?php echo smartyTranslate(array('s'=>'Prev','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</a>
		<a class="btn next ttspecial_next"><?php echo smartyTranslate(array('s'=>'Next','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</a>
	</div>
	<?php }?>
   <div class="allproduct"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['allSpecialProductsLink']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'All sale products','d'=>'Modules.Specials.Shop'),$_smarty_tpl);?>
</a></div>
   </div>
</section>
<?php }} ?>
