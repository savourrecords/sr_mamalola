<?php /*%%SmartyHeaderCode:12021810335be42426ec86c0-28337433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39d31a599d73c039735add7bd5dc7a2a3a72c0ba' => 
    array (
      0 => 'module:ps_categoryproducts/views/templates/hook/ps_categoryproducts.tpl',
      1 => 1524498857,
      2 => 'module',
    ),
    '420277693c3c133872a8e96e87bcb181160ebea1' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1531297349,
      2 => 'file',
    ),
    '48879e23ae4f9c39f9c8bf3a6740893522ccbaa0' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/customize/button-cart.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    'ff8dc98f35c359d2bd5b65221f50004c63fd0234' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/variant-links.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12021810335be42426ec86c0-28337433',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be4560be678c4_42076677',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be4560be678c4_42076677')) {function content_5be4560be678c4_42076677($_smarty_tpl) {?>																

               
<section class="category-products clearfix mt-3">
  
  <h2 class="tt-title">
          16 otros productos en la misma categoría:
      </h2>
  <i class="material-icons left">&#xE314;</i>
  <div class="products row">
                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="33" data-id-product-attribute="238" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/196-home_default2/bugui-plata.jpg"
					  alt = "BUGUI PLATA"
					  data-full-size-image-url = "https://mamalolashoes.com/196-home_default2/bugui-plata.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/197-home_default2/bugui-plata.jpg" data-full-size-image-url="https://mamalolashoes.com/197-large_default/bugui-plata.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="33" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sneakers</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36">BUGUI PLATA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">85,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="22" data-id-product-attribute="172" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/22-172-leucade-beige.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/49-home_default2/leucade-beige.jpg"
					  alt = "LEUCADE BEIGE"
					  data-full-size-image-url = "https://mamalolashoes.com/49-home_default2/leucade-beige.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/48-home_default2/leucade-beige.jpg" data-full-size-image-url="https://mamalolashoes.com/48-large_default/leucade-beige.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="22" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/22-172-leucade-beige.html#/26-shoes_size-36">LEUCADE BEIGE</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/22-172-leucade-beige.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">31,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">63,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="21" data-id-product-attribute="166" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/21-166-leucade-rosa.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/51-home_default2/leucade-rosa.jpg"
					  alt = "LEUCADE ROSA"
					  data-full-size-image-url = "https://mamalolashoes.com/51-home_default2/leucade-rosa.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/52-home_default2/leucade-rosa.jpg" data-full-size-image-url="https://mamalolashoes.com/52-large_default/leucade-rosa.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="21" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/21-166-leucade-rosa.html#/26-shoes_size-36">LEUCADE ROSA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/21-166-leucade-rosa.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">31,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">63,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="13" data-id-product-attribute="118" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					  alt = "COMINO BLANCO"
					  data-full-size-image-url = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/74-home_default2/comino-blanco.jpg" data-full-size-image-url="https://mamalolashoes.com/74-large_default/comino-blanco.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="13" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">COMINO BLANCO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="39" data-id-product-attribute="420" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/211-home_default2/mitte-serraje-negro.jpg"
					  alt = "MITTE NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/211-home_default2/mitte-serraje-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/212-home_default2/mitte-serraje-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/212-large_default/mitte-serraje-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="39" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36">MITTE NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="18" data-id-product-attribute="148" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/58-home_default2/naxos-gris.jpg"
					  alt = "NAXOS GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/58-home_default2/naxos-gris.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/60-home_default2/naxos-gris.jpg" data-full-size-image-url="https://mamalolashoes.com/60-large_default/naxos-gris.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="18" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36">NAXOS GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="44" data-id-product-attribute="400" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/181-home_default2/born-brown.jpg"
					  alt = "BORN GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/181-home_default2/born-brown.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/182-home_default2/born-brown.jpg" data-full-size-image-url="https://mamalolashoes.com/182-large_default/born-brown.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="44" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36">BORN GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">79,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="19" data-id-product-attribute="154" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/59-home_default2/naxos-taupe.jpg"
					  alt = "NAXOS TAUPE"
					  data-full-size-image-url = "https://mamalolashoes.com/59-home_default2/naxos-taupe.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/57-home_default2/naxos-taupe.jpg" data-full-size-image-url="https://mamalolashoes.com/57-large_default/naxos-taupe.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="19" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36">NAXOS TAUPE</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="3" data-id-product-attribute="58" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					  alt = "ITACA CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/107-home_default2/itaca-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/107-large_default/itaca-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="3" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">ITACA CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="17" data-id-product-attribute="142" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/63-home_default2/mallorca-cuero.jpg"
					  alt = "MALLORCA CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/63-home_default2/mallorca-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/64-home_default2/mallorca-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/64-large_default/mallorca-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="17" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36">MALLORCA CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="26" data-id-product-attribute="196" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/37-home_default2/bora-bora-fuxia.jpg"
					  alt = "BORA BORA FUXIA"
					  data-full-size-image-url = "https://mamalolashoes.com/37-home_default2/bora-bora-fuxia.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/36-home_default2/bora-bora-fuxia.jpg" data-full-size-image-url="https://mamalolashoes.com/36-large_default/bora-bora-fuxia.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="26" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36">BORA BORA FUXIA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="30" data-id-product-attribute="220" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sneakers/30-220-brac-plata.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/205-home_default2/brac-plata.jpg"
					  alt = "BRAC PLATA"
					  data-full-size-image-url = "https://mamalolashoes.com/205-home_default2/brac-plata.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/206-home_default2/brac-plata.jpg" data-full-size-image-url="https://mamalolashoes.com/206-large_default/brac-plata.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="30" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sneakers</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sneakers/30-220-brac-plata.html#/26-shoes_size-36">BRAC PLATA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sneakers/30-220-brac-plata.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="11" data-id-product-attribute="106" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/79-home_default2/comino-cuero.jpg"
					  alt = "COMINO CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/79-home_default2/comino-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/80-home_default2/comino-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/80-large_default/comino-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="11" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36">COMINO CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="16" data-id-product-attribute="136" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/67-home_default2/mallorca-negro.jpg"
					  alt = "MALLORCA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/67-home_default2/mallorca-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/66-home_default2/mallorca-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/66-large_default/mallorca-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="16" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36">MALLORCA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="47" data-id-product-attribute="383" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					  alt = "BRERA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/185-home_default2/brera-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/185-large_default/brera-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="47" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">BRERA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="20" data-id-product-attribute="160" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/20-160-leucade-leopardo.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/150-home_default2/leucade-leopardo.jpg"
					  alt = "LEUCADE LEOPARDO"
					  data-full-size-image-url = "https://mamalolashoes.com/150-home_default2/leucade-leopardo.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/151-home_default2/leucade-leopardo.jpg" data-full-size-image-url="https://mamalolashoes.com/151-large_default/leucade-leopardo.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="20" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/20-160-leucade-leopardo.html#/26-shoes_size-36">LEUCADE LEOPARDO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/20-160-leucade-leopardo.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">32,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">65,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

        </div>
  <i class="material-icons right">&#xE315;</i>
   	<div class="customNavigation">
		<a class="btn prev Categoryproducts_prev">Prev</a>
		<a class="btn next Categoryproducts_next">Next</a>
	</div>
  
</section>

<?php }} ?>
