<?php /*%%SmartyHeaderCode:12021810335be42426ec86c0-28337433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39d31a599d73c039735add7bd5dc7a2a3a72c0ba' => 
    array (
      0 => 'module:ps_categoryproducts/views/templates/hook/ps_categoryproducts.tpl',
      1 => 1524498857,
      2 => 'module',
    ),
    '420277693c3c133872a8e96e87bcb181160ebea1' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1531297349,
      2 => 'file',
    ),
    '48879e23ae4f9c39f9c8bf3a6740893522ccbaa0' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/customize/button-cart.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    'ff8dc98f35c359d2bd5b65221f50004c63fd0234' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/variant-links.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12021810335be42426ec86c0-28337433',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be4842f085154_19262395',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be4842f085154_19262395')) {function content_5be4842f085154_19262395($_smarty_tpl) {?>								

               
<section class="category-products clearfix mt-3">
  
  <h2 class="tt-title">
          8 otros productos en la misma categoría:
      </h2>
  <i class="material-icons left">&#xE314;</i>
  <div class="products row">
                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="40" data-id-product-attribute="415" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/172-home_default2/mitte-marron.jpg"
					  alt = "MITTE GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/172-home_default2/mitte-marron.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/173-home_default2/mitte-marron.jpg" data-full-size-image-url="https://mamalolashoes.com/173-large_default/mitte-marron.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="40" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36">MITTE GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="44" data-id-product-attribute="400" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/181-home_default2/born-brown.jpg"
					  alt = "BORN GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/181-home_default2/born-brown.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/182-home_default2/born-brown.jpg" data-full-size-image-url="https://mamalolashoes.com/182-large_default/born-brown.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="44" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36">BORN GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/44-400-born-brown.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">79,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="45" data-id-product-attribute="394" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/156-home_default2/camden.jpg"
					  alt = "CAMDEN PIEL NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/156-home_default2/camden.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/157-home_default2/camden.jpg" data-full-size-image-url="https://mamalolashoes.com/157-large_default/camden.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="45" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36">CAMDEN PIEL NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="48" data-id-product-attribute="378" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/48-378-brera-marron.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/187-home_default2/brera-marron.jpg"
					  alt = "BRERA VISON"
					  data-full-size-image-url = "https://mamalolashoes.com/187-home_default2/brera-marron.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/188-home_default2/brera-marron.jpg" data-full-size-image-url="https://mamalolashoes.com/188-large_default/brera-marron.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="48" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/48-378-brera-marron.html#/26-shoes_size-36">BRERA VISON</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/48-378-brera-marron.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="47" data-id-product-attribute="383" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					  alt = "BRERA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/185-home_default2/brera-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/185-large_default/brera-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="47" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">BRERA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="42" data-id-product-attribute="405" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/178-home_default2/born-negro.jpg"
					  alt = "BORN NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/178-home_default2/born-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/179-home_default2/born-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/179-large_default/born-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="42" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36">BORN NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">79,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="39" data-id-product-attribute="420" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/211-home_default2/mitte-serraje-negro.jpg"
					  alt = "MITTE NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/211-home_default2/mitte-serraje-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/212-home_default2/mitte-serraje-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/212-large_default/mitte-serraje-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="39" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36">MITTE NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/39-420-mitte-serraje-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="46" data-id-product-attribute="388" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/46-388-malasana.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/159-home_default2/malasana.jpg"
					  alt = "MALASAÑA PIEL NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/159-home_default2/malasana.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/160-home_default2/malasana.jpg" data-full-size-image-url="https://mamalolashoes.com/160-large_default/malasana.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="46" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/46-388-malasana.html#/26-shoes_size-36">MALASAÑA PIEL NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/46-388-malasana.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

        </div>
  <i class="material-icons right">&#xE315;</i>
   	<div class="customNavigation">
		<a class="btn prev Categoryproducts_prev">Prev</a>
		<a class="btn next Categoryproducts_next">Next</a>
	</div>
  
</section>

<?php }} ?>
