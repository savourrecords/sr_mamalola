<?php /*%%SmartyHeaderCode:12021810335be42426ec86c0-28337433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39d31a599d73c039735add7bd5dc7a2a3a72c0ba' => 
    array (
      0 => 'module:ps_categoryproducts/views/templates/hook/ps_categoryproducts.tpl',
      1 => 1524498857,
      2 => 'module',
    ),
    '420277693c3c133872a8e96e87bcb181160ebea1' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1531297349,
      2 => 'file',
    ),
    '48879e23ae4f9c39f9c8bf3a6740893522ccbaa0' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/customize/button-cart.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    'ff8dc98f35c359d2bd5b65221f50004c63fd0234' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/variant-links.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12021810335be42426ec86c0-28337433',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be447341c5772_30304678',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be447341c5772_30304678')) {function content_5be447341c5772_30304678($_smarty_tpl) {?>																

               
<section class="category-products clearfix mt-3">
  
  <h2 class="tt-title">
          16 otros productos en la misma categoría:
      </h2>
  <i class="material-icons left">&#xE314;</i>
  <div class="products row">
                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="33" data-id-product-attribute="238" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/196-home_default2/bugui-plata.jpg"
					  alt = "BUGUI PLATA"
					  data-full-size-image-url = "https://mamalolashoes.com/196-home_default2/bugui-plata.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/197-home_default2/bugui-plata.jpg" data-full-size-image-url="https://mamalolashoes.com/197-large_default/bugui-plata.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="33" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sneakers</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36">BUGUI PLATA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sneakers/33-238-bugui-plata.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">85,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="42" data-id-product-attribute="405" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/178-home_default2/born-negro.jpg"
					  alt = "BORN NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/178-home_default2/born-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/179-home_default2/born-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/179-large_default/born-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="42" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36">BORN NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/42-405-born-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">79,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="28" data-id-product-attribute="208" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/28-208-yap-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/121-home_default2/yap-cuero.jpg"
					  alt = "YAP CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/121-home_default2/yap-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/120-home_default2/yap-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/120-large_default/yap-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="28" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/28-208-yap-cuero.html#/26-shoes_size-36">YAP CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/28-208-yap-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">27,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">54,99 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="1" data-id-product-attribute="46" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/99-home_default2/itaca-negro.jpg"
					  alt = "ITACA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/99-home_default2/itaca-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/97-home_default2/itaca-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/97-large_default/itaca-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="1" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36">ITACA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="3" data-id-product-attribute="58" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					  alt = "ITACA CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/107-home_default2/itaca-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/107-large_default/itaca-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="3" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">ITACA CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="13" data-id-product-attribute="118" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					  alt = "COMINO BLANCO"
					  data-full-size-image-url = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/74-home_default2/comino-blanco.jpg" data-full-size-image-url="https://mamalolashoes.com/74-large_default/comino-blanco.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="13" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">COMINO BLANCO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="40" data-id-product-attribute="415" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/172-home_default2/mitte-marron.jpg"
					  alt = "MITTE GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/172-home_default2/mitte-marron.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/173-home_default2/mitte-marron.jpg" data-full-size-image-url="https://mamalolashoes.com/173-large_default/mitte-marron.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="40" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36">MITTE GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/40-415-mitte-marron.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="27" data-id-product-attribute="202" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/27-202-yap-rosa.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/34-home_default2/yap-rosa.jpg"
					  alt = "YAP ROSA"
					  data-full-size-image-url = "https://mamalolashoes.com/34-home_default2/yap-rosa.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/33-home_default2/yap-rosa.jpg" data-full-size-image-url="https://mamalolashoes.com/33-large_default/yap-rosa.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="27" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/27-202-yap-rosa.html#/26-shoes_size-36">YAP ROSA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/27-202-yap-rosa.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">27,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">54,99 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="45" data-id-product-attribute="394" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/156-home_default2/camden.jpg"
					  alt = "CAMDEN PIEL NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/156-home_default2/camden.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/157-home_default2/camden.jpg" data-full-size-image-url="https://mamalolashoes.com/157-large_default/camden.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="45" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36">CAMDEN PIEL NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/45-394-camden.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="26" data-id-product-attribute="196" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/37-home_default2/bora-bora-fuxia.jpg"
					  alt = "BORA BORA FUXIA"
					  data-full-size-image-url = "https://mamalolashoes.com/37-home_default2/bora-bora-fuxia.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/36-home_default2/bora-bora-fuxia.jpg" data-full-size-image-url="https://mamalolashoes.com/36-large_default/bora-bora-fuxia.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="26" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36">BORA BORA FUXIA</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/26-196-bora-bora-fuxia.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="47" data-id-product-attribute="383" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					  alt = "BRERA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/184-home_default2/brera-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/185-home_default2/brera-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/185-large_default/brera-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="47" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">botines</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">BRERA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/botines/47-383-brera-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,00 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="2" data-id-product-attribute="52" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/101-home_default2/itaca-champan.jpg"
					  alt = "ITACA CHAMPÁN"
					  data-full-size-image-url = "https://mamalolashoes.com/101-home_default2/itaca-champan.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/98-home_default2/itaca-champan.jpg" data-full-size-image-url="https://mamalolashoes.com/98-large_default/itaca-champan.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="2" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36">ITACA CHAMPÁN</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="17" data-id-product-attribute="142" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/63-home_default2/mallorca-cuero.jpg"
					  alt = "MALLORCA CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/63-home_default2/mallorca-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/64-home_default2/mallorca-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/64-large_default/mallorca-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="17" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36">MALLORCA CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/17-142-mallorca-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="41" data-id-product-attribute="410" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/destacados/41-410-mitte-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/175-home_default2/mitte-negro.jpg"
					  alt = "MITTE PIEL NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/175-home_default2/mitte-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/176-home_default2/mitte-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/176-large_default/mitte-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="41" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">destacados</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/destacados/41-410-mitte-negro.html#/26-shoes_size-36">MITTE PIEL NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/destacados/41-410-mitte-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">99,90 €</span>
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="24" data-id-product-attribute="184" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/24-184-molokai-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/144-home_default2/molokai-cuero.jpg"
					  alt = "MOLOKAI CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/144-home_default2/molokai-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/145-home_default2/molokai-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/145-large_default/molokai-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="24" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/24-184-molokai-cuero.html#/26-shoes_size-36">MOLOKAI CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/24-184-molokai-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">24,75 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">49,50 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

                

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="16" data-id-product-attribute="136" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/67-home_default2/mallorca-negro.jpg"
					  alt = "MALLORCA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/67-home_default2/mallorca-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/66-home_default2/mallorca-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/66-large_default/mallorca-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="16" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36">MALLORCA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/16-136-mallorca-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">29,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">59,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

        </div>
  <i class="material-icons right">&#xE315;</i>
   	<div class="customNavigation">
		<a class="btn prev Categoryproducts_prev">Prev</a>
		<a class="btn next Categoryproducts_next">Next</a>
	</div>
  
</section>

<?php }} ?>
