<?php /*%%SmartyHeaderCode:19846309885be41f476829a1-02010776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4e6dbda1ddf326fc35e22f1cc40cfd1117be2f17' => 
    array (
      0 => 'module:tt_specials/views/templates/hook/tt_specials.tpl',
      1 => 1523439482,
      2 => 'module',
    ),
    '420277693c3c133872a8e96e87bcb181160ebea1' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1531297349,
      2 => 'file',
    ),
    '48879e23ae4f9c39f9c8bf3a6740893522ccbaa0' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/customize/button-cart.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
    'ff8dc98f35c359d2bd5b65221f50004c63fd0234' => 
    array (
      0 => '/home/mamalolamz/www/themes/PRS01/templates/catalog/_partials/variant-links.tpl',
      1 => 1523439483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19846309885be41f476829a1-02010776',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5be94742391b00_42061036',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5be94742391b00_42061036')) {function content_5be94742391b00_42061036($_smarty_tpl) {?>								
<section class="ttspecial-products clearfix row">
   <div class="block_content container">
  <h3 class="tt-title">Specials</h3>
  <div class="customNavigation">
<a class="btn prev ttspecial_prev">Prev</a>
<a class="btn next ttspecial_next">Next</a>
</div>
   <span class="ttspecial-subtitle">special items in this season</span>
  <div class="ttspecial-list container">
  <div class="row">
  <div class="products">
    					<!-- Start TemplateTrip 2 product slide code -->
								<ul>
					<li class="specialli">
						<ul>
						<li>
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="11" data-id-product-attribute="106" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/79-home_default2/comino-cuero.jpg"
					  alt = "COMINO CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/79-home_default2/comino-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/80-home_default2/comino-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/80-large_default/comino-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="11" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36">COMINO CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/11-106-comino-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
											
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="12" data-id-product-attribute="112" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/12-112-comino-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/76-home_default2/comino-negro.jpg"
					  alt = "COMINO NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/76-home_default2/comino-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/77-home_default2/comino-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/77-large_default/comino-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="12" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/12-112-comino-negro.html#/26-shoes_size-36">COMINO NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/12-112-comino-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
													</li>
						</ul>
					</li>
					</ul>
								
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
								<ul>
					<li class="specialli">
						<ul>
						<li>
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="13" data-id-product-attribute="118" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					  alt = "COMINO BLANCO"
					  data-full-size-image-url = "https://mamalolashoes.com/75-home_default2/comino-blanco.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/74-home_default2/comino-blanco.jpg" data-full-size-image-url="https://mamalolashoes.com/74-large_default/comino-blanco.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="13" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">COMINO BLANCO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/13-118-comino-blanco.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,95 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,90 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
											
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="1" data-id-product-attribute="46" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/99-home_default2/itaca-negro.jpg"
					  alt = "ITACA NEGRO"
					  data-full-size-image-url = "https://mamalolashoes.com/99-home_default2/itaca-negro.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/97-home_default2/itaca-negro.jpg" data-full-size-image-url="https://mamalolashoes.com/97-large_default/itaca-negro.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="1" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36">ITACA NEGRO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/1-46-itaca-negro.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
													</li>
						</ul>
					</li>
					</ul>
								
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
								<ul>
					<li class="specialli">
						<ul>
						<li>
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="2" data-id-product-attribute="52" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/101-home_default2/itaca-champan.jpg"
					  alt = "ITACA CHAMPÁN"
					  data-full-size-image-url = "https://mamalolashoes.com/101-home_default2/itaca-champan.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/98-home_default2/itaca-champan.jpg" data-full-size-image-url="https://mamalolashoes.com/98-large_default/itaca-champan.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="2" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36">ITACA CHAMPÁN</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/2-52-itaca-champan.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
											
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="3" data-id-product-attribute="58" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					  alt = "ITACA CUERO"
					  data-full-size-image-url = "https://mamalolashoes.com/103-home_default2/itaca-cuero.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/107-home_default2/itaca-cuero.jpg" data-full-size-image-url="https://mamalolashoes.com/107-large_default/itaca-cuero.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="3" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">ITACA CUERO</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/3-58-itaca-cuero.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
													</li>
						</ul>
					</li>
					</ul>
								
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
								<ul>
					<li class="specialli">
						<ul>
						<li>
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="18" data-id-product-attribute="148" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/58-home_default2/naxos-gris.jpg"
					  alt = "NAXOS GRIS"
					  data-full-size-image-url = "https://mamalolashoes.com/58-home_default2/naxos-gris.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/60-home_default2/naxos-gris.jpg" data-full-size-image-url="https://mamalolashoes.com/60-large_default/naxos-gris.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="18" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart btn-default" data-button-action="add-to-cart" title="Add to cart">
					<span>Añadir al carrito</span>
				</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">inicio</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36">NAXOS GRIS</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/inicio/18-148-naxos-gris.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
											
						<!-- End TemplateTrip 2 product slide code -->
    					<!-- Start TemplateTrip 2 product slide code -->
											<!-- End TemplateTrip 2 product slide code -->
      

<article class="product-miniature js-product-miniature col-sm-4" data-id-product="19" data-id-product-attribute="154" itemscope itemtype="https://schema.org/Product">
	<div class="thumbnail-container">
		<div class="ttproduct-image">
			
			  <a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36" class="thumbnail product-thumbnail">
				<img
					  class="ttproduct-img1"
					  src = "https://mamalolashoes.com/59-home_default2/naxos-taupe.jpg"
					  alt = "NAXOS TAUPE"
					  data-full-size-image-url = "https://mamalolashoes.com/59-home_default2/naxos-taupe.jpg"
					>
					
	<img class="second_image img-responsive" src="https://mamalolashoes.com/57-home_default2/naxos-taupe.jpg" data-full-size-image-url="https://mamalolashoes.com/57-large_default/naxos-taupe.jpg" alt="" />
	
	
			  </a>
			
			
				<ul class="product-flags">
											<li class="online-only">¡Disponible sólo en Internet!</li>
											<li class="discount">Precio rebajado</li>
									</ul>
			
			<div class="ttproducthover">
			<div class="tt-button-container">
				
<div class="product-add-to-cart">
			<form action="https://mamalolashoes.com/carrito" method="post" class="add-to-cart-or-refresh">
			<div class="product-quantity" style="display:none;">
				<input type="number" name="id_product" value="19" class="product_page_product_id">
				<input type="number" name="id_customization" value="0" class="product_customization_id">
				<input type="number" name="qty" class="quantity_wanted input-group" value="1" min="1"/>
			</div>
							<a href="javascript:void(0);" class="button ajax_add_to_cart_button add-to-cart-disable btn-default" title="Out of stock">
				<span>out of stock</span>
			</a>
					</form>
	</div>

			</div>
			
			<div class="quick-view-block">
				<a href="#" class="quick-view" data-link-action="quickview" title="Quick view">
					<i class="material-icons search">&#xE8B6;</i> Vista rápida
				</a>
			</div>
			
			</div>
		</div>
		
		<div class="ttproduct-desc">
			<div class="product-description">
							 <h5 class="cat-name">sandalias</h5>

				
					<span class="h3 product-title" itemprop="name"><a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36">NAXOS TAUPE</a></span>
					<span class="sr-hover-add-cart" itemprop="name"><a href="https://mamalolashoes.com/sandalias/19-154-naxos-taupe.html#/26-shoes_size-36">A&ntilde;adir a la cesta &gt;</a></span>
				
				
					<div class="product-desc-short" itemprop="description"></div>
				
		
				
											<div class="product-price-and-shipping">
							<span itemprop="price" class="price">34,50 €</span>
															
								<span class="sr-only">Precio base</span>
																	<span class="discount-percentage">-50%</span>
																<span class="regular-price">69,00 €</span>
								
														
						  <span class="sr-only">Precio</span>
							
							
						</div>
									
				
				
			  
			  
				<div class="highlighted-informations no-variants hidden-sm-down">
				
									
			</div>
			</div>
			
			
		</div>

	</div>
</article>
	

	  <!-- Start TemplateTrip 2 product slide code -->
													</li>
						</ul>
					</li>
					</ul>
								
						<!-- End TemplateTrip 2 product slide code -->
    						  </div>
  </div>
  </div>
  	<div class="customNavigation">
		<a class="btn prev ttspecial_prev">Prev</a>
		<a class="btn next ttspecial_next">Next</a>
	</div>
	   <div class="allproduct"><a href="https://mamalolashoes.com/productos-rebajados">Todas los productos en oferta</a></div>
   </div>
</section>
<?php }} ?>
