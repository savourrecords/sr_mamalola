<?php
class Tools extends ToolsCore
{
    /*
    * module: disableaddonsapi
    * date: 2018-10-15 12:02:03
    * version: 1.0.0
    */
    public static function addonsRequest($request, $params = array())
    {
        if (Module::isEnabled('disableaddonsapi') && Configuration::get('DISABLEADDONSAPI_ENABLE')) {
            return false;
        } else {
            return parent::addonsRequest($request, $params);
        }
    }
}