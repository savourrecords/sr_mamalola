<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cenote
 */

?>




<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="sr-post-item">
		<?php cenote_post_thumbnail(); ?>
		<header class="entry-header sr-entry-header">
			<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" class="capital-letters sr-post-item-title" rel="bookmark">', '</a></h2>' ); ?>
		</header>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

