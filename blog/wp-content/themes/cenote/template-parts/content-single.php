<?php
/**
 * Template part for displaying single post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cenote
 */

$content_orders = get_theme_mod( 'cenote_single_order_layout', array( 'thumbnail', 'categories', 'title', 'meta', 'content', 'footer' ) );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	foreach ( $content_orders as $key => $content_order ) :
		if ( 'thumbnail' === $content_order ) :
			global $dynamic_featured_image;
			$featured_images = $dynamic_featured_image->get_featured_images( get_the_ID() );

			//You can now loop through the image to display them as required
			//Mostramos como imagen la primera que aparece
			//.get_the_permalink().
			if(isset($featured_images[0])) {
			echo "<div class='slide'>";
			echo "<span> <img src='".$featured_images[0]['full']."' class='img-slide' /> </span>";
			echo "</div>";

			};
		/*
		elseif ( 'categories' === $content_order ) :
				if(isset($featured_images[0])) {
					echo "<div class='tg-top-cat element-blog'>";
				} else {
					echo "<div class='tg-top-cat'>";
				}
			?>
				<?php cenote_post_categories(); ?>
			</div>
			<?php
		*/
		elseif ( 'title' === $content_order ) :
				if(isset($featured_images[0])) {
					echo "<header class='entry-header element-blog'>";
				} else {
					echo "<header class='entry-header sr-margen-10-arriba'>";
				}
			?>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
			<?php

		elseif ( 'meta' === $content_order && 'post' === get_post_type() ) :
				if(isset($featured_images[0])) {
					echo "<div class='entry-meta element-blog'>";
				} else {
					echo "<div class='entry-meta '>";
				}
				cenote_posted_on();
			?>
			</div><!-- .entry-meta -->
			<?php
		elseif ( 'content' === $content_order ) :
				if(isset($featured_images[0])) {
					echo "<div class='entry-content element-blog'>";
				} else {
					echo "<div class='entry-content'>";
				}
			?>
				<?php the_content(); ?>
			</div><!-- .entry-content -->
			<?php
			/*
				elseif ( 'footer' === $content_order ) :
					if(isset($featured_images[0])) {
						echo "<footer class='entry-footer element-blog'>";
					} else {
						echo "<footer class='entry-footer'>";
					}
					?>
					
				<?php cenote_entry_footer(); ?>
			</footer><!-- .entry-footer -->
			<?php
			*/
		endif;
	endforeach;

	// Show author box if enabled.
	if ( true === get_theme_mod( 'cenote_single_enable_author_box', true ) ) {
		get_template_part( 'template-parts/author/author', 'box' );
	}
	?>
</article><!-- #post-<?php the_ID(); ?> -->
