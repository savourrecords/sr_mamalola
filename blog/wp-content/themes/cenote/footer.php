<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-par	als
 *
 * @package cenote
 */

?>
		</div><!-- .tg-container -->
	</div><!-- #content -->

	<?php
	// Show related post if enabled.
	if ( true === get_theme_mod( 'cenote_single_enable_related_post', true ) && is_single() ) {
		get_template_part( 'template-parts/related/related', 'post' );
	}
	//wp_enqueue_script( '/wp_content/themes/cenote/assets/js/cookies.js' );
	?>
	<footer id="footer" class="">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div id="newslatter" class="col-sm-12">
						<div class="block_newsletter">
							<div class="row">
								<div class="tt-content">
									<h4 class="tt-title sr-nl-title">Suscribete a nuestra newsletter para ser la primera en recibir nuestras últimas noticias, las promociones más exclusivas y cositas divertidas</h4>
								</div>
							</div>
							<div class="row">
								<div class="block_content">
									<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
										<div class="row">
											<div class="ttinput_newsletter">
												<!--input style="display:none;" class="btn btn-primary sr-btn-newsletter float-xs-right hidden-xs-down" name="submitNewsletter" type="submit" value=">"-->
												<div class="input-wrapper">
													<input style="border-bottom:0px;" name="EMAIL" type="text" value="" placeholder="Escribe tu mail" aria-labelledby="block-newsletter-label">
													<img id="img-sr-arrow" class="sr-arrow" width="50px;" src="<?php echo str_replace('/blog','',get_site_url()); ?>/img/sr-img/PB_ICON_ARROW.svg">
												</div>
												<input type="hidden" name="action" value="0">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="footer-container">
			<div class="container">
				<div class="row">
					<div class="sr-social-mobile sr-mobile social-top">
						<ul id="social-footer" class="collapse in">
							<li class="facebook"><a href="https://www.facebook.com/Mamalola-847480102123765/" target="_blank"><i class="tg-icon-facebook"></i></a></li>
							<li class="instagram"><a href="https://www.instagram.com/mamalolashoes/" target="_blank"><i class="tg-icon-instagram"></i></a></li>
						</ul>
						<div style="clear:both"></div>
					</div>
					<div class="col-md-6 linklist links">
						<div class="col-md-6 wrapper">
							<ul class="collapse in">
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/6-guia-de-tallas">Guía de tallas</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/9-envios-y-devoluciones">Envíos y devoluciones</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/mi-cuenta">Mi cuenta</a></li>
							</ul>
						</div>
						<div class="col-md-6 wrapper">
							<ul class="collapse in">
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/13-sneakers">Sneakers</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/12-sandalias">Sandalias</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/">Toda la colección</a></li>
							</ul>
						</div>
						<div class="col-md-6 wrapper">
							<ul class="collapse in">
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/3-terminos-y-condiciones-de-uso">Términos y condiciones</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/7-politica-de-privacidad">Política y privacidad</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/10-politica-de-cookies">Política de cookies</a></li>
							</ul>
						</div>
						<div class="col-md-6 wrapper">
							<ul class="collapse in">
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/4-sobre-nosotros">Sobre nosotros</a></li>
								<li><a class="custom-page-link"href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/8-contacto">Contacto</a></li>
								<li><a class="custom-page-link" href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/11-seguimiento-del-pedido">Seguimiento del pedido</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 social-bottom">
						<div>
							<ul id="social-footer" class="collapse in">
								<li class="facebook"><a href="https://www.facebook.com/Mamalola-847480102123765/" target="_blank"><i class="tg-icon-facebook"></i></a></li>
								<li class="instagram"><a href="https://www.instagram.com/mamalolashoes/" target="_blank"><i class="tg-icon-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">    
				<div class="row">
					<div class="col-md-12">
						<p class="text-sm-center">
							<?php get_template_part( 'template-parts/footer/footer', 'info' ); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div id="cookieBlock" class="my_cookies" style="display: none;">
			<div class="sr-alert sr-alert-info sr-alert-dismissible">
				<p class="coockies-text">Utilizamos cookies propias y de terceros para mejorar su experiencia de usuario. Si continua navegando consideramos que acepta su uso.<a href="<?php echo str_replace('/blog','',get_site_url()); ?>/content/10-politica-de-cookies"> Leer más</a> <span id="btn-close-cookie">OK</span></p>
			</div>
		</div>
	</footer>

</div>
<?php
do_action( 'cenote_after_footer' );
wp_footer();
?>

</body>
</html>
