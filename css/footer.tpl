{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 <div class="footer-top">
<div class="container">
  <div class="row">
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
  </div>
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="row">

      <div class="sr-social-mobile sr-mobile"></div>
      {block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
  </div>
</div>
<div class="bottom-footer">
	<div class="container">    
	 <div class="row">
      <div class="col-md-12">
        <p class="text-sm-center">
          {block name='copyright_link'}
            <a class="_blank" href="http://www.prestashop.com" target="_blank">
              {l s='%copyright% %year% - MAMALOLA' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}
            </a>
          {/block}
        </p>
      </div>
	  </div>
	  </div>
</div>
<div class="backtotop-img">
	<a href="#" class="goToTop show" title="Back to top"> <i class="material-icons arrow-up"></i></a>
	</div>
<a class="goToTop ttbox-img show" href="#"> </a>

<div id="cookieBlock" class="my_cookies" style="display:none">
  <div class="sr-alert sr-alert-info sr-alert-dismissible" >
	<p class="coockies-text">{l s="Utilizamos cookies propias y de terceros para mejorar su experiencia de usuario. Si continua navegando consideramos que acepta su uso."}<a href="http://mamalolashoes.com/content/10-politica-de-cookies"> {l s="Leer más"}</a> <span id="btn-close-cookie">{l s="OK"}</span></p>
  </div>
</div>

<div style="display:none;">
	<a href="#popup-nl" class="nl-fancybox" >link</a>
	<table id="popup-nl" class="table-newsletter-popup">
		<tr>
			<td class="nl-img-left content-table-newsletter-popup">
				<span class="title-table-newsletter-popup">
          SUSCRÍBETE A LA <strong>NEWSLETTER</strong>,<br>DISFRUTA DE TU <strong style="color:#000;">10% DTO</strong><br>Y SÉ LO MÁS.
        </span>
				<div class="NW_FRM_Modal"><form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post"><input type="text" name="NAME" placeholder="Nombre" class="input-name-content-newsletter-popup"><br><input type="text" name="EMAIL" placeholder="Correo electrónico" class="input-email-content-newsletter-popup"><br><input type="submit" value="SER #MAMALOLAGIRL >" class="input-submit-content-newsletter-popup"></form><br><br><span class="link-content-newsletter-popup"><a href="http://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">Más información sobre política de privacidad</a></span></div>
			</td>
		</tr>
	</table>
</div>

{literal}
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117775943-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117775943-1');


  function showNewsletterPopup(){
		$(".nl-fancybox").fancybox({  maxWidth  : 590,
		   maxHeight : 440,
		   fitToView : false,
		   width     : '100%',
		   height    : '60%',
		   afterShow: function() {
			  var imagen = $('#fotoDisplay');
			  var maxHeightLong = 330;
			  var maxWidthLong = 570;
			  var ratio = 0;
			  var width = imagen.width();
			  var height = imagen.height();
	 
				if(width > maxWidthLong){
					ratio = maxWidthLong / width;   // get ratio for scaling image
					$('#fotoDisplay').css("width", maxWidthLong); // Set new width
					$('#fotoDisplay').css("height", height * ratio);  // Scale height based on ratio
					height = height * ratio;    // Reset height to match scaled image
				}

				var width = $(this).width();    // Current image width
				var height = $(this).height();  // Current image height

			 if(height > maxHeightLong){
				ratio = maxHeightLong / height; // get ratio for scaling image
				$('#fotoDisplay').css("height", maxHeightLong);   // Set new height
				$('#fotoDisplay').css("width", width * ratio);    // Scale width based on ratio
				width = width * ratio;				
			 }
		  },		  
			autoSize  : false
		}).trigger('click');


		/*setTimeout(function(){
				jQuery('.sr-content-modal-fancy').prepend('<img src="http://mamalolashoes.com/img/GIF_POP-UP.gif" class="img-rayos-newsletter">');
				jQuery('.sr-content-modal-fancy').prepend('<img src="http://mamalolashoes.com/img/POP_UP_WEB-X2_.png" class="newsletter-close-button" onclick="$.fancybox.close(true);">');
				jQuery('.sr-content-modal-fancy').prepend('<div class="NW_FRM_Modal"><form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post"><input type="text" name="NAME" placeholder="Nombre" class="input-name-content-newsletter-popup"><br><input type="text" name="EMAIL" placeholder="Correo electrónico" class="input-email-content-newsletter-popup"><br><input type="submit" value="SER #MAMALOLAGIRL >" class="input-submit-content-newsletter-popup"></form><br><br><span class="link-content-newsletter-popup"><a href="http://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">Más información sobre política de privacidad</a></span></div>');
				jQuery('.fancybox-close').addClass('close-buttom-newsletter-popup-oculto');
		},3000);*/

		setCookie("showNewsletterPopup", 'true', 1);
	}
	
	function checkCookieNewsletter() {
		var lastShow = getCookie("showNewsletterPopup");
		var registredNewsletter = getCookie("registredNewsletter");

		if (lastShow == undefined && registredNewsletter == undefined) {
			(function(){
			  var newscript = document.createElement('script');
				 newscript.type = 'text/javascript';
				 newscript.async = true;
				 newscript.src = 'http://mamalolashoes.com/js/jquery/plugins/fancybox/jquery.fancybox.js';
			  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(newscript);
			})();
			setTimeout(function(){ showNewsletterPopup(); }, 4000);
		}
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	} 
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
	}

	jQuery(document).ready(function(){
		/*checkCookieNewsletter();*/
	});

</script>

<style>
  .fancybox-overlay {
      z-index: 1000000 !important;
  }
</style>

<!-- Google Code para etiquetas de remarketing -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información personal identificable o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 804972302;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/804972302/?guid=ON&amp;script=0"/>
</div>
</noscript>

{/literal}


