<?php

	$user = 'mamalolamzsavour';
	$pass = 'MlRecords15';
	$db = new mysqli('mamalolamzsavour.mysql.db',$user,$pass,'mamalolamzsavour');

	$idShop = 1;
	
	if ($db->connect_error) {
		die("");
	} 
	
	$db->set_charset("utf8");

	$sql = "SELECT DISTINCT p.id_product as 'ID', pl.name as 'Nombre', pl.description as 'Descripcion', 		
		concat('https://mamalolashoes.com','/',p.id_product ,'-',pl.link_rewrite ,'.html') as 'Link',
		concat('https://mamalolashoes.com/',pi1.id_image,'-home_default2/',pl.link_rewrite,'.jpg') as 'Img1',
		concat('https://mamalolashoes.com/',pi2.id_image,'-home_default2/',pl.link_rewrite,'.jpg') as 'Img2',
		concat(ROUND(((p.price*0.21)+p.price),2),' EUR') as 'Precio', '' as 'sale_price', 
		s.quantity as 'Disponibilidad', 'Nuevo' as 'Estado',
		'Adult' as 'Edad', 'Mamalola' as 'Marca', p.reference as 'MPN', 'Ropa y accesorios, Calzado' as 'Categoria google producto', CASE WHEN ROUND(((p.price*0.21)+p.price),2)>=150 THEN 'ES::0.00 EUR' ELSE 'ES::0.00 EUR' END as 'Envio', 
		'Mujer' as 'Sexo',
		replace(replace(pl.description_short,'<p>',''),'</p>','') as 'COLOR', 'PIEL' as 'Material',
		(case  
		   when sp.reduction_type like 'amount' then concat(ROUND((p.price*1.21)-(sp.reduction),2),' EUR')
		   when sp.reduction_type like 'percentage' then concat(ROUND((p.price*1.21)-((p.price*1.21)*sp.reduction),2),' EUR')
		 end ) as 'reduction', '35-40' as 'Talla'
		FROM ps_product p 
		LEFT JOIN ps_product_attribute pa ON (p.id_product = pa.id_product) 
		LEFT JOIN ps_stock_available pq ON (p.id_product = pq.id_product AND pa.id_product_attribute = pq.id_product_attribute) 
		LEFT JOIN ps_product_lang pl ON (p.id_product = pl.id_product) 
		LEFT JOIN ps_product_attribute_combination pac ON (pa.id_product_attribute = pac.id_product_attribute)
		LEFT JOIN ps_attribute_lang pal ON (pac.id_attribute = pal.id_attribute)
		LEFT JOIN ps_category_product cp ON (p.id_product = cp.id_product)
		LEFT JOIN ps_category_lang cl ON (cp.id_category = cl.id_category)
		LEFT JOIN ps_category c ON (cp.id_category = c.id_category)
		LEFT JOIN ps_product_shop ps ON (ps.id_product = p.id_product)
		LEFT JOIN ps_specific_price sp ON (sp.id_product = p.id_product and ((sp.from<= now() and sp.to>= now()) or (sp.from = '0000-00-00 00:00:00' and sp.to= '0000-00-00 00:00:00')))
		LEFT JOIN ps_stock_available s ON (p.id_product = s.id_product)
		LEFT JOIN ps_image pi1 on p.id_product = pi1.id_product and pi1.position = 1 
		LEFT JOIN ps_image pi2 on p.id_product = pi2.id_product and pi2.position = 2
		WHERE (pq.id_shop = " . $idShop . " and pl.id_shop = " . $idShop . " and cl.id_shop = " . $idShop . " and ps.id_shop = " . $idShop . " and s.id_shop = " . $idShop . ") and pl.id_lang = 1 and ps.active = 1 AND pal.id_lang = 1 and s.quantity > 0 AND p.active=1 AND concat('https://mamalolashoes.com/',pi1.id_image,'-home_default2/',pl.link_rewrite,'.jpg') <>'' AND concat('https://mamalolashoes.com/',pi2.id_image,'-home_default2/',pl.link_rewrite,'.jpg')<>''
		GROUP BY p.id_product,pl.name, pl.description
		ORDER BY p.id_product
		";


	if ($result=mysqli_query($db,$sql))
	{
		$db->close();
		$nsUrl = 'http://base.google.com/ns/1.0';

		$doc = new DOMDocument('1.0', 'UTF-8');

		$rootNode = $doc->appendChild($doc->createElement('rss'));
		$rootNode->setAttribute('version', '2.0');
		$rootNode->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:g', $nsUrl);

		$channelNode = $rootNode->appendChild($doc->createElement('channel'));
		$channelNode->appendChild($doc->createElement('title', 'Mamalola'));
		$channelNode->appendChild($doc->createElement('description', ''));
		$channelNode->appendChild($doc->createElement('link', 'https://mamalolashoes.com'));

		while ($row=mysqli_fetch_row($result)){
			$description = strip_tags ($row[2]);
			$titulo = strtolower($row[1]);
			
			$itemNode = $channelNode->appendChild($doc->createElement('item'));
			$itemNode->appendChild($doc->createElement('title'))->appendChild($doc->createTextNode(ucwords($titulo)));
			$itemNode->appendChild($doc->createElement('description'))->appendChild($doc->createTextNode($description));
			$itemNode->appendChild($doc->createElement('link'))->appendChild($doc->createTextNode($row[3]));
			$itemNode->appendChild($doc->createElement('g:id'))->appendChild($doc->createTextNode($row[0]));
			$itemNode->appendChild($doc->createElement('g:price'))->appendChild($doc->createTextNode($row[6]));
		
			$reduction = $row[18];
			$price = $row[20];
			$finalSalePrice = $price;
			
			/*if($reduction !== null && $reduction !== '' && $reduction !== '0'){
				$finalSalePrice = number_format(floatval($price * (1-$reduction)),2);
				$finalSalePrice = $finalSalePrice.' EUR';
				
				if($finalSalePrice !== $row[6]){
					$itemNode->appendChild($doc->createElement('g:sale_price'))->appendChild($doc->createTextNode($finalSalePrice));
				}
			}*/
			
				$itemNode->appendChild($doc->createElement('g:sale_price'))->appendChild($doc->createTextNode($row[18]));
		
			$itemNode->appendChild($doc->createElement('g:brand'))->appendChild($doc->createTextNode('Mamalola'));
			$itemNode->appendChild($doc->createElement('g:condition'))->appendChild($doc->createTextNode('new'));
			$itemNode->appendChild($doc->createElement('g:image_link'))->appendChild($doc->createTextNode($row[4]));
			$itemNode->appendChild($doc->createElement('g:additional_image_link'))->appendChild($doc->createTextNode($row[5]));
			$itemNode->appendChild($doc->createElement('g:color'))->appendChild($doc->createTextNode($row[16]));
			$itemNode->appendChild($doc->createElement('g:mpn'))->appendChild($doc->createTextNode($row[12]));
			$itemNode->appendChild($doc->createElement('g:gender'))->appendChild($doc->createTextNode($row[15]));
			$itemNode->appendChild($doc->createElement('g:age_group'))->appendChild($doc->createTextNode($row[10]));
			$itemNode->appendChild($doc->createElement('g:adult'))->appendChild($doc->createTextNode("no"));
			$itemNode->appendChild($doc->createElement('g:size'))->appendChild($doc->createTextNode($row[19]));	
			$itemNode->appendChild($doc->createElement('g:material'))->appendChild($doc->createTextNode($row[17]));
			$itemNode->appendChild($doc->createElement('g:shipping'))->appendChild($doc->createTextNode($row[14]));				
			$itemNode->appendChild($doc->createElement('g:availability'))->appendChild($doc->createTextNode('in stock'));					

		}
		
		header("Content-type: text/xml; charset=utf-8");
		echo $doc->saveXML();
		exit();

	} else {
		$db->close();
	}

?>


	
	
