$(function() {
    "use strict";
    $(function() {
            $(".preloader").fadeOut();
        }),

        jQuery(document).on("click", ".mega-dropdown", function(i) {
            i.stopPropagation();
        });


    var i = function() {
        (window.innerWidth > 0 ? window.innerWidth : this.screen.width) < 1170 ? ($("body").addClass("mini-sidebar"),
            $(".navbar-brand span").hide(), $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible"),
            $(".sidebartoggler i").addClass("ti-menu")) : ($("body").removeClass("mini-sidebar"),
            $(".navbar-brand span").show());
        var i = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 1;
        (i -= 70) < 1 && (i = 1), i > 70 && $(".page-wrapper").css("min-height", i + "px");
    };


    $(window).ready(i), $(window).on("resize", i), $(".sidebartoggler").on("click", function() {
            $("body").hasClass("mini-sidebar") ? ($("body").trigger("resize"), $(".scroll-sidebar, .slimScrollDiv").css("overflow", "hidden").parent().css("overflow", "visible"),
                $("body").removeClass("mini-sidebar"), $(".navbar-brand span").show()) : ($("body").trigger("resize"),
                $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible"),
                $("body").addClass("mini-sidebar"), $(".navbar-brand span").hide());
        }),



        $(".fix-header .header").stick_in_parent({}), $(".nav-toggler").click(function() {
            $("body").toggleClass("show-sidebar"), $(".nav-toggler i").toggleClass("mdi mdi-menu"),
                $(".nav-toggler i").addClass("mdi mdi-close");
        }),



        $(".search-box a, .search-box .app-search .srh-btn").on("click", function() {
            $(".app-search").slideToggle(200);
        }),



        $(".floating-labels .form-control").on("focus blur", function(i) {
            $(this).parents(".form-group").toggleClass("focused", "focus" === i.type || this.value.length > 0);
        }).trigger("blur"), $(function() {
            for (var i = window.location, o = $("ul#sidebarnav a").filter(function() {
                    return this.href == i;
                }).addClass("active").parent().addClass("active");;) {
                if (!o.is("li")) break;
                o = o.parent().addClass("in").parent().addClass("active");
            }
        }),

        $(function() {
            $("#sidebarnav").metisMenu();
        }),

        $(".scroll-sidebar").slimScroll({
            position: "left",
            size: "5px",
            height: "100%",
            color: "#dcdcdc"
        }),

        $(".message-center").slimScroll({
            position: "right",
            size: "5px",
            color: "#dcdcdc"
        }),

        $(".aboutscroll").slimScroll({
            position: "right",
            size: "5px",
            height: "80",
            color: "#dcdcdc"
        }),

        $(".message-scroll").slimScroll({
            position: "right",
            size: "5px",
            height: "570",
            color: "#dcdcdc"
        }),

        $(".chat-box").slimScroll({
            position: "right",
            size: "5px",
            height: "470",
            color: "#dcdcdc"
        }),

        $(".slimscrollright").slimScroll({
            height: "100%",
            position: "right",
            size: "5px",
            color: "#dcdcdc"
        }),



        $("body").trigger("resize"), $(".list-task li label").click(function() {
            $(this).toggleClass("task-done");
        }),



        $("#to-recover").on("click", function() {
            $("#loginform").slideUp(), $("#recoverform").fadeIn();
        }),



        $('a[data-action="collapse"]').on("click", function(i) {
            i.preventDefault(), $(this).closest(".card").find('[data-action="collapse"] i').toggleClass("ti-minus ti-plus"),
                $(this).closest(".card").children(".card-body").collapse("toggle");
        }),



        $('a[data-action="expand"]').on("click", function(i) {
            i.preventDefault(), $(this).closest(".card").find('[data-action="expand"] i').toggleClass("mdi-arrow-expand mdi-arrow-compress"),
                $(this).closest(".card").toggleClass("card-fullscreen");
        }),



        $('a[data-action="close"]').on("click", function() {
            $(this).closest(".card").removeClass().slideUp("fast");
        });
});

jQuery(document).ready(function(){
    jQuery('.btn-stop-process').off('click').on('click',function(){
        window.location.href = './index.php?action=status&subAction=stop';
    });
    jQuery('.btn-start-process').off('click').on('click',function(){
        window.location.href = './index.php?action=status&subAction=start';
    });
    jQuery('.btn-sync').off('click').on('click',function(){
        window.location.href = './index.php?action=sync&subAction=sync';
    });
    jQuery('.btn-sync-error').off('click').on('click',function(){
        ErrorSincronizacion();
    });
    jQuery('.btn-lanzar').off('click').on('click',function(){
        window.location.href = './index.php?action=run&subAction=run&step=1';
    });
    jQuery('.btn-lanzar-error').off('click').on('click',function(){
        ErrorLanzadera();
    });
    jQuery('.btn-lanzar-debug').off('click').on('click',function(){
        window.location.href = './index.php?action=run&subAction=debug';
    });
    jQuery('.btn-cancel-run').off('click').on('click',function(){
        window.location.href = './index.php';
    });
    jQuery('.btn-continue-run').off('click').on('click',function(){
        window.location.href = './index.php?action=run&subAction=run&step=2';
    });
    jQuery('#themeName').keyup(function(){
        ReplaceSpaces();
    });
    jQuery('#themeContent').focusout(function(){
        CompruebaComillasSimples('#themeContent');
    });
    jQuery('#themeItems').focusout(function(){
        CompruebaComillasSimples('#themeItems');
    });
    jQuery('#themeContent').focusin(function(){
        jQuery(idElemento).css('border','1px solid #666666');
    });
    jQuery('#themeItems').focusin(function(){
        jQuery(idElemento).css('border','1px solid #666666');
    });
    jQuery('#btn-submit').off('click').on('click',function(e){
        if(!CompruebaComillasSimples('#themeContent'))
            e.preventDefault();
        else
            if(!CompruebaComillasSimples('#themeItems'))
                e.preventDefault();
    });
    jQuery('#urlImagen').focusout(function(){
        jQuery('#previa-img').prop('src',jQuery('#urlImagen').val());
    });
    jQuery('#btn-menu-mobile').off('click').on('click',function(){
        if(parseInt(jQuery('.left-sidebar').css('left'))<0)
            jQuery('.left-sidebar').css('left','0px');
        else
            jQuery('.left-sidebar').css('left','-240px');
    });
    jQuery('#btn-test-connection').off('click').on('click',function(){
        TestConnection();
    });
    jQuery('#btn-init-session').off('click').on('click',function(){
        window.location.href='./';
    });
});

function ErrorLanzadera(){
    jQuery('#msg-error-lanzadera').slideDown();
    setTimeout(function(){
        jQuery('#msg-error-lanzadera').slideUp();
    }, 6000)
}

function ErrorSincronizacion(){
    jQuery('#msg-error-sincronizacion').slideDown();
    setTimeout(function(){
        jQuery('#msg-error-sincronizacion').slideUp();
    }, 6000)
}

function muestraTablaOculta(idIcono, idTabla){
    /*Mostramos/Ocultamos la tabla*/
    if(jQuery('#table_'+idTabla).css('display') == 'table'){
        jQuery('#table_'+idTabla).slideUp('slow');
        jQuery('#'+idIcono).css('transform','rotate(0deg)');
    }else{
        jQuery('#table_'+idTabla).slideDown('slow');
        jQuery('#'+idIcono).css('transform','rotate(180deg)');
    }
}

function ReplaceSpaces(){
    var themeName = jQuery('#themeName').val();
    themeName = themeName.replace(" ","_");
    jQuery('#themeName').val(themeName);
}

function ViewPrevia(idPlantilla){
    var url = "http://mamalolashoes.com/savour/carritos/html/preview.php?idPlantilla=" + idPlantilla;
    jQuery('#frame-prev').prop('src', url);
}

function EditHtml(idPlantilla){
    jQuery('#fileName').val(idPlantilla);
    jQuery('#frm-edit').submit();
}

function DeleteTheme(idPlantilla){
    jQuery('#fileNameD').val(idPlantilla);
    jQuery('#frm-delete').submit();
}

function CompruebaComillasSimples(idElemento){
    var texto = jQuery(idElemento).val();
    if(texto.indexOf("'")>-1){
        jQuery(idElemento).css('border','1px dotted #990000');
        alert("El contenido HTML no puede contener comillas simples < ' >.");
        return false;
    }else{
        return true;
    }
}

function lanzaajax(tipo,url,parametros,capa){
    if (capa==null) capa="";
    var respuesta='';
     
     $.ajax({
      url: url,
      type: tipo,
      data: parametros,
      dataType:"html",
      async:false,
      errror:muestraError,
      success: function(html){
                       if(capa!="")
                         document.getElementById(capa).innerHTML=html;
                         respuesta=html;
              }
    });
    return respuesta;
        
}

function muestraError(html){
    alert(html);
}

function TestConnection(){
    var server = jQuery('#bbdd_Server').val();
    var user = jQuery('#bbdd_User').val();
    var pass = jQuery('#bbdd_Password').val();
    var db = jQuery('#bbdd_Name').val();
    var parametros = "server=" + server + "&user=" + user + "&pass=" + pass + "&db=" + db;
    var rs = lanzaajax("POST","./install/test-connection.php",parametros,null);
    if(rs == 'KO'){
        jQuery('#error-conexion').slideDown('slow');
        setTimeout(function(){jQuery('#error-conexion').slideUp('slow');},3000);
    }else{
        jQuery('#ok-conexion').slideDown('slow');
        setTimeout(function(){jQuery('#ok-conexion').slideUp('slow');},3000);
    }
}