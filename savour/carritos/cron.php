<?php    
    include_once 'PHPMailer/Exception.php';
    include_once 'PHPMailer/PHPMailer.php';
    include_once 'PHPMailer/SMTP.php';

    include_once('config/config-db.php');
    include_once('config/config-connection.php');
    include_once('config/config-general.php');
    include_once("./actions/get-dashboard-info.php");

    $fechaIni = date("d/m/Y");
    $horaIni = date("H:i:s");
    $fechaFin = date("d/m/Y");
    $horaFin = date("H:i:s");
    $log = "";

    function GeneraAmigable($texto){
        $textoAmigable = $texto;
        $textoAmigable = mb_strtolower($textoAmigable);
        $textoAmigable = str_replace("á","a",$textoAmigable);
        $textoAmigable = str_replace("é","e",$textoAmigable);
        $textoAmigable = str_replace("í","i",$textoAmigable);
        $textoAmigable = str_replace("ó","o",$textoAmigable);
        $textoAmigable = str_replace("ú","u",$textoAmigable);
        $textoAmigable = str_replace("à","a",$textoAmigable);
        $textoAmigable = str_replace("è","e",$textoAmigable);
        $textoAmigable = str_replace("è","i",$textoAmigable);
        $textoAmigable = str_replace("ò","o",$textoAmigable);
        $textoAmigable = str_replace("ù","u",$textoAmigable);
        $textoAmigable = str_replace("ñ","n",$textoAmigable);
        $textoAmigable = str_replace(" - ","-",$textoAmigable);
        $textoAmigable = str_replace(" ","-",$textoAmigable);
        return $textoAmigable;
    }

    if($RW_DashboardInfo['status_process']!="PARADO"){

        $log .= "[" . date("d-m-Y") . "] [" . date("H:i") . "] - Sincronizando datos de los carritos.";
        $SyncSQL = "CALL SP_INSERTA_CARRITOS_ENVIO();";
        if(@mysqli_query($link,$SyncSQL)){

            $linkSyncAuto = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
            $SqlUpdate = "UPDATE ps_config_carritos_abandonados SET last_sync_date=DATE_FORMAT(NOW(),'%d/%m/%Y'), last_sync_hour=TIME_FORMAT(NOW(),'%H:%i'), last_sync_type='Automático';";
            @mysqli_query($linkSyncAuto,$SqlUpdate);

            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando proceso de recuperación de datos.";
            
            if(isset($_GET["idCustomer"]) && $_GET["idCustomer"]!=""){
                $SyncDataCarrito = "CALL SP_OBTENER_CARRITOS_ENVIO_POR_ID_CARRITO(" . $_GET["idCustomer"] . ");";
            }else{
                $SyncDataCarrito = "CALL SP_OBTENER_CARRITOS_ENVIO();";
            }
            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Recuperando datos del carrito.";
            $link1 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
            if($ResultInfoCarrito = @mysqli_query($link1,$SyncDataCarrito)){
                if(@mysqli_num_rows($ResultInfoCarrito)>0){
                    while($RowInfoCarrito = @mysqli_fetch_array($ResultInfoCarrito)){
                        $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando proceso de recuperación de datos del usuario y productos.";
                        $link2 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                        $SqlProducto = "CALL SP_OBTENER_PRODUCTOS_CARRITO(" . $RowInfoCarrito['idCarrito'] . ", 'N');"; 
                        $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Obteniendo datos del usuario y productos.";
                        $SqlQueryProducto = @mysqli_query($link2, $SqlProducto) or die('Error al intentar lanzar la query ' . $SqlProducto);
                        if(@mysqli_num_rows($SqlQueryProducto)>0){
                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Datos obtenidos correctamente.";
                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Obteniendo plantilla del emal.";
                            $linkContent = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                            $SqlEmailContent = "SELECT contentHtml FROM ps_themes_emails_carritos WHERE activo='S';";
                            if($RS_SqlEmailContent = @mysqli_query($linkContent, $SqlEmailContent)){
                                $RowEmailContent = @mysqli_fetch_array($RS_SqlEmailContent);
                                $contentEmail = $RowEmailContent['contentHtml'];
                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Conformando contenido del email.";
                                $bodyEmail = "";
                                $emailCustomer = "";
                                while($RowProducto = @mysqli_fetch_array($SqlQueryProducto)){
                                    $urlImagen = "http://mamalolashoes.com/" . $RowProducto["idImagen"] . "-large_default/" . GeneraAmigable(utf8_encode($RowProducto["nombreProducto"])). ".jpg";
                                    $linkItem = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                    $SqlEmailItem = "SELECT contentItem FROM ps_themes_emails_carritos WHERE activo='S';";
                                    $RS_SqlEmailItem = @mysqli_query($linkItem, $SqlEmailItem);
                                    $RowEmailItem = @mysqli_fetch_array($RS_SqlEmailItem);
                                    $linea = $RowEmailItem['contentItem'];
                                    $linea = str_replace("@UrlImagen@",$urlImagen,$linea);
                                    $linea = str_replace("@NombreProducto@",utf8_encode($RowProducto["nombreProducto"]),$linea);
                                    $bodyEmail .= $linea;
                                    if($emailCustomer=="")
                                        $emailCustomer = $RowProducto["emailCustomer"];
                                }
                                $contentEmail = str_replace("@BodyEmail@",$bodyEmail,$contentEmail);
                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Email conformado correctamente.";
                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando envío del email.";

                                $to = $emailCustomer;
                                //$to = "josemacia@savourrecords.com";
                                try{
                                    //$mail = new PHPMailer(true);
                                    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                                    //Server settings
                                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                                    $mail->isSMTP();                                      // Set mailer to use SMTP
                                    $mail->Host = 'smtp.gmail.com';  				  // Specify main and backup SMTP servers
                                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                    $mail->Username = 'hola@mamalolashoes.com';   // SMTP username
                                    $mail->Password = 'hajeitafnmttnsxt';                    // SMTP password
                                    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                                    $mail->Port = 587;                                    // TCP port to connect to
                                    //Recipients
                                    $mail->setFrom('hola@mamalolashoes.com', 'Mamalola Shoes');
                                    $mail->addAddress($to, '');
                                    //$mail->addCC('anarubio@savourrecords.com');
                                    
                                    //Content
                                    $mail->isHTML(true);                                  	// Set email format to HTML
                                    $mail->Subject = 'Inevitable, inolvidable...';
                                    $mail->Body  = $contentEmail;
                                    $mail->send();
                                    $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Email enviado con éxito.";
                                    
                                    $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=1, observaciones='" . $log . "' WHERE idEnvio=" . $RowInfoCarrito["idEnvio"];
                                } catch (Exception $e) {
                                    $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar enviar el email. " . $mail->ErrorInfo;
                                    $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=0, observaciones='" . $log . "' WHERE idEnvio=" . $RowInfoCarrito["idEnvio"];
                                }
                            }else{
                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar recuperar la plantilla " . $nameTheme . ".";
                                $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=0, observaciones='" . $log . "' WHERE idEnvio=" . $RowInfoCarrito["idEnvio"];
                            }
                        }else{
                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Error al intentar obtener la información del usuario y/o productos.";        
                        }
                        $linkUPD = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                        @mysqli_query($linkUPD,$SqlUPD);
                    }
                }else{
                    $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - No existen datos para carritos en este momento para procesar.";
                }
                $type="Manual";
                if(isset($_GET['job']) && $_GET['job']=='S')
                    $type="Automático";
                $link3 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                $SqlUpdate = "UPDATE ps_config_carritos_abandonados SET last_update_date=DATE_FORMAT(NOW(),'%d/%m/%Y'), last_update_hour=TIME_FORMAT(NOW(),'%H:%i'), last_sync_type='" . $type . "';";
                @mysqli_query($link3,$SqlUpdate);
            }else{
                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar obtener la información del carrito.";
            }

            echo $log;

            $fechaFin = date("d/m/Y");
            $horaFin = date("H:i:s");

            /*Enviamos el email para informar de que el proceso ha finalizado*/
            try{
                //$mail = new PHPMailer(true);
                $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                //Server settings
                $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'SSL0.OVH.NET';  				  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'hola@mamalolashoes.com';   // SMTP username
                $mail->Password = 'mamalola17*!';                    // SMTP password
                $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;                                    // TCP port to connect to
                //Recipients
                $mail->setFrom('hola@mamalolashoes.com', 'Mamalola Shoes');
                $mail->addAddress('josemacia@savourrecords.com');
                $mail->addCC('josegilabert@savourrecords.com');
                
                //Content
                $mail->isHTML(true);                                  	// Set email format to HTML
                $mail->Subject = utf8_decode('MAMALOLA SHOES - Proceso automático de carritos abandonados.');
                $mail->Body  = "El proceso de envío de carritos a finalizado con el siguiente log: <br><br><br>" . $log . "<br><br><br>FECHA INICIO: " . $fechaIni . "<br>HORA INICIO: " . $horaIni . "<br>FECHA FINALIZACIÓN: " . $fechaFin . "<br>HORA FINALIZACIÓN: " . $horaFin;
                $mail->send();
            } catch (Exception $e) {
                echo "<br><br>Error al enviar el email de confirmación a Savour.";
            }


        }else{
            die("Error al intentar sincronizar.");
        }
    }else{
        die('El proceso está parado. Inicielo para que puedan enviarse los emails de carritos.');
    }

?>