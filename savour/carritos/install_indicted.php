<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="logotipo-savour.jpeg">
    <title>GCA - Gestión de Carritos Abandonados v1.0</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    
    <div id="error-conexion" class="alert alert-danger" style="display:none; position:fixed; width:100%;">
        <strong>ERROR DE CONEXIÓN!</strong> No se pudo establecer la conexión con la base de datos.
    </div>

    <div id="ok-conexion" class="alert alert-success" style="display:none; position:fixed; width:100%;">
        <strong>PERFECTO!</strong> La conexión con la base de datos se estableció correctamente.
    </div>

    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">

        <?php
            function RunSqlFile($file, $bbdd_Server, $bbdd_User, $bbdd_password, $bbdd_Name){
                $status = false;
                $sqlFileToExecute = $file;
                $hostname = $bbdd_Server;
                $db_user = $bbdd_User;
                $db_password = $bbdd_password;
                $db_Name = $bbdd_Name;

                $link = @mysqli_connect($hostname, $db_user, $db_password, $db_Name) or die('Error al conectar con ' . $hostname);
                
                
                // read the sql file
                $f = fopen($sqlFileToExecute,"r+");
                $sqlFile = fread($f, filesize($sqlFileToExecute));
                $sqlArray = explode(';',$sqlFile);
                foreach ($sqlArray as $stmt) {
                if (strlen($stmt)>3 && substr(ltrim($stmt),0,2)!='/*') {
                    $result = mysql_query($stmt);
                    if (!$result) {
                    $sqlErrorCode = mysql_errno();
                    $sqlErrorText = mysql_error();
                    $sqlStmt = $stmt;
                    break;
                    }
                }
                }
                if ($sqlErrorCode == 0) {
                    $status = true;
                    echo "Script ejecutado correctamente.";
                } else {
                    echo "Hubo errores al ejecutar el script SQL:<br/>";
                    echo "\tError code: $sqlErrorCode<br/>";
                    echo "\tError text: $sqlErrorText<br/>";
                    echo "\tStatement:<br/> $sqlStmt<br/>";
                }

                return $status;
            }
        ?>

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <h1>Instalación</h1>
                            </div>
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                    <?php
                                        if(!isset($_POST['bbdd_Server']) && $_POST['bbdd_Server']==""){
                                    ?>
                                        <h4 class="card-title">Conexión a Base de Datos</h4>
                                        <form class="form-horizontal p-t-20" method="POST" action="install_indicted.php">
                                            <div class="form-group row">
                                                <label for="uname" class="col-sm-3 control-label">Servidor</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bbdd_Server" name="bbdd_Server" placeholder="Servidor">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email2" class="col-sm-3 control-label">Usuario</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bbdd_User" name="bbdd_User" placeholder="Usuario">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="web1" class="col-sm-3 control-label">Contraseña</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="bbdd_Password" name="bbdd_Password" placeholder="Contraseña">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="margin-bottom:10%;">
                                                <label for="pass3" class="col-sm-3 control-label">Nombre BBDD</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="bbdd_Name" name="bbdd_Name" placeholder="Nombre BBDD">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="card-title">Datos de LogIn</h4>
                                            <div class="form-group row">
                                                <label for="uname" class="col-sm-3 control-label">Usuario</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="user_Name" name="user_Name" placeholder="Usuario">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="margin-bottom:10%;">
                                                <label for="uname" class="col-sm-3 control-label">Contraseña</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="user_Password" name="user_Password" placeholder="Contraseña">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row m-b-0">
                                                <div class="offset-sm-3 col-sm-9 ">
                                                    <button type="button" class="btn btn-success waves-effect waves-light" id="btn-test-connection">Probar Conexión</button>
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Comenzar Instalación &raquo;</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                        }else{
                                            echo 'Generando fichero de configuración.<br>';
                                            $db_file_name = "./config/config-db.php";
                                            $login_file_name = "./config/config-login.php";
                                            $table_file_name = "./install/install-tables.sql";
                                            $procedure_file_name = "./install/install-procedures.sql";

                                            $content_db_file = '
                                                <?php
                                                    /*Datos de Conexión con la BBDD*/
                                                    define("_SERVER_DB_","' . $_POST['bbdd_Server'] . '");
                                                    define("_USER_DB_","' . $_POST['bbdd_User'] . '");
                                                    define("_PASSWORD_DB_","' . $_POST['bbdd_Password'] . '");
                                                    define("_NAME_DB_","' . $_POST['bbdd_Name'] . '");
                                                ?>
                                            ';
                                            $content_login_file = '
                                                <?php
                                                    $user_val = "' . $_POST['user_Name'] . '";
                                                    $pass_val = "' . $_POST['user_Password'] . '";
                                                ?>
                                            ';
                                            if(file_exists($db_file_name))
                                                @unlink($db_file_name);
                                            if(file_exists($login_file_name))
                                                @unlink($login_file_name);

                                            $f = fopen($db_file_name, 'a+');
                                            fputs($f, $content_db_file);
                                            fclose($f);

                                            $f2 = fopen($login_file_name, 'a+');
                                            fputs($f2, $content_login_file);
                                            fclose($f2);

                                            echo 'Fichero de configuración generado.<br>';
                                            echo 'Recuperando información de tablas.<br>';
                                            if(file_exists($table_file_name)){
                                                echo 'Información de tablas obtenida correctamente.<br>';
                                                echo 'Generando tablas.<br>';    
                                                if(RunSqlFile($table_file_name, $_POST['bbdd_Server'], $_POST['bbdd_User'], $_POST['bbdd_Password'], $_POST['bbdd_Name'])){
                                                    echo 'Recuperando información de procedimientos.<br>';
                                                    if(file_exists($procedure_file_name)){
                                                        echo 'Generando procedimientos.<br>';    
                                                        if(RunSqlFile($procedure_file_name, $_POST['bbdd_Server'], $_POST['bbdd_User'], $_POST['bbdd_Password'], $_POST['bbdd_Name'])){
                                                            echo 'Renombrando fichero de instalación';
                                                            if(@rename('./install.php','./install_indicted.php')){
                                                                echo 'Fichero renombrado correctamente.<br>';
                                                            }else{
                                                                echo 'Error al renombrar el fichero. Renombre el fichero <i>install.php</i> manualmente.<br>';
                                                            }
                                                            echo '<button type="button" class="btn btn-success waves-effect waves-light" id="btn-init-session">Iniciar Sesión</button>';
                                                        }
                                                    }else{
                                                        echo 'Error al recuperar información de los procedimientos.';        
                                                    }
                                                }
                                            }else{
                                                echo 'Error al recuperar información de las tablas.';
                                            }
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/scripts.js"></script>

</body>

</html>