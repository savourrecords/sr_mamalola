<?php
    include_once 'PHPMailer/Exception.php';
    include_once 'PHPMailer/PHPMailer.php';
    include_once 'PHPMailer/SMTP.php';

    include_once('config/config-db.php');
    include_once('config/config-connection.php');
    include_once('config/config-general.php');
    include_once("actions/get-dashboard-info.php");
    

    $imagen = "https://palomabarcelo.com/1597-large_default/azucena.jpg";
    $nombre = "AZUCENA";
    $referencia = "124-001-209";
    $precio = "208,60";

    $SqlPlantilla = "SELECT asuntoPlantilla,contentHtml,contentItem FROM ps_themes_emails_carritos WHERE idPlantilla=" . $_GET['idPlantilla'];
    if($RS_SqlPlantilla = @mysqli_query($link, $SqlPlantilla)){
        if(@mysqli_num_rows($RS_SqlPlantilla)>0){
            $RowPlantilla = @mysqli_fetch_array($RS_SqlPlantilla);
            
            $item = $RowPlantilla['contentItem'];
            $item = str_replace("@UrlImagen@",$imagen,$item);
            $item = str_replace("@NombreProducto@",$nombre,$item);
            $item = str_replace("@ReferenciaProducto@",$referencia,$item);
            $item = str_replace("@PrecioProducto@",$precio,$item);
            $content = $RowPlantilla['contentHtml'];
            $content = str_replace("@BodyEmail@",$item,$content);

            $emails = $_GET['emailTest'];
            if(strpos(';',$emails) > 0){
                $emails = explode(';',$emails);
                $to = $emails[0];
            }else{
                $to = $_GET['emailTest'];
            }

            $asuntoPlantilla = "Test de Envios";
            if($RowPlantilla['asuntoPlantilla']!='' && $RowPlantilla['asuntoPlantilla']!=null)
                $asuntoPlantilla = $RowPlantilla['asuntoPlantilla'];

            try{
                //$mail = new PHPMailer(true);
                $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                //Server settings
                $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';  				  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'josegilabert@savourrecords.com';   // SMTP username
                $mail->Password = 'gilabertsimon88';                    // SMTP password
                $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;                                    // TCP port to connect to
                //Recipients
                $mail->setFrom('josemacia@savourrecords.com', utf8_decode('Paloma Barceló (Test)'));
                $mail->addAddress($to, '');

                if(sizeof($emails) > 1){
                    for($i=1; $i<sizeof($emails); $i++)
                        $mail->addCC($emails[$i]);
                }
                
                //Content
                $mail->isHTML(true);                                  	// Set email format to HTML
                $mail->Subject = utf8_decode($asuntoPlantilla);
                $mail->Body  = $content;
                $mail->send();
                
                echo 'OK';
            } catch (Exception $e) {
                echo 'ERROR: ' . $e;
            }
        }
    }else{
        echo 'ERROR: Error al recuperar la plantilla.';
    }
?>