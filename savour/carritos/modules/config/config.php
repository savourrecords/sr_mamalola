<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Configuración</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Configuración</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Configuración</h1><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">
                                    <div class="basic-form">
                                        <form action="./index.php?action=config&subAction=save" method="POST">
                                            <div class="form-group">
                                                <p class="text-muted m-b-15 f-s-12">Logotipo</p>
                                                <input id="urlImagen" type="text" name="urlImagen" class="form-control input-default " placeholder="Url Logotipo" value="<?php echo $urlLogotipo; ?>">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <img id="previa-img" src="<?php echo $urlLogotipo; ?>" class="img-responsive"><br>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top:5%;">
                                                <p class="text-muted m-b-15 f-s-12">Plantilla de envío</p>
                                                    <select name="nameTheme" class="form-control">
                                                        <?php
                                                            $SqlThemes = "SELECT idPlantilla, nombrePlantilla, activo FROM ps_themes_emails_carritos ORDER BY nombrePlantilla ASC;";
                                                            if($RS_SqlThemes = @mysqli_query($link,$SqlThemes)){
                                                                if(@mysqli_num_rows($RS_SqlThemes)){
                                                                    while($RowThemes = @mysqli_fetch_array($RS_SqlThemes)){
                                                                        $activo = "";
                                                                        if($RowThemes['activo']=='S')
                                                                            $activo = "selected";
                                                        ?>
                                                                        <option <?php echo $selected; ?> value="<?php echo $RowThemes['idPlantilla']; ?>"><?php echo $RowThemes['nombrePlantilla']; ?></option>
                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    </select>
                                            </div>
                                            <div class="form-group" style="margin-top:5%;">
                                                <p class="text-muted m-b-15 f-s-12">Directorio Base</p>
                                                <input type="text" name="dirBase" class="form-control input-default " placeholder="Directorio base" value="<?php echo $dirBase; ?>">
                                            </div>
                                            <div class="form-group" style="margin-top:5%;">
                                                <p class="text-muted m-b-15 f-s-12">Dominio Web</p>
                                                <input type="text" name="dominioWeb" class="form-control input-default " placeholder="Dominio web" value="<?php echo $dominioWeb; ?>">
                                            </div>
                                            <div class="form-group" style="margin-top:5%;">
                                                <button style="float:right;" type="submit" class="btn btn-success m-b-10 m-l-5">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>