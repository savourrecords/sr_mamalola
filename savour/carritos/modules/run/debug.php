<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Procesos</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Procesos</li>
                        <li class="breadcrumb-item active">Lanzar (Debug)</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Run (Debug)</h1><br>
                <span style="color:#990000;">Recuerde que el total de productos que aparece en cada carrito es el total acumulativo. En las tablas de muestreo aparecerán los productos agregados al carrito sin repeticiones con lo que puede no cuadrar el total de elementos de las tablas con el total de productos.</span><br><br><br><br>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            function GeneraAmigable($texto){
                                $textoAmigable = $texto;
                                $textoAmigable = mb_strtolower($textoAmigable);
                                $textoAmigable = str_replace("á","a",$textoAmigable);
                                $textoAmigable = str_replace("é","e",$textoAmigable);
                                $textoAmigable = str_replace("í","i",$textoAmigable);
                                $textoAmigable = str_replace("ó","o",$textoAmigable);
                                $textoAmigable = str_replace("ú","u",$textoAmigable);
                                $textoAmigable = str_replace("à","a",$textoAmigable);
                                $textoAmigable = str_replace("è","e",$textoAmigable);
                                $textoAmigable = str_replace("è","i",$textoAmigable);
                                $textoAmigable = str_replace("ò","o",$textoAmigable);
                                $textoAmigable = str_replace("ù","u",$textoAmigable);
                                $textoAmigable = str_replace("ñ","n",$textoAmigable);
                                $textoAmigable = str_replace(" - ","-",$textoAmigable);
                                $textoAmigable = str_replace(" ","-",$textoAmigable);
                                return $textoAmigable;
                            }

                            $SyncSQL = "CALL SP_OBTENER_CARRITOS_ENVIO();";
                            if($ResultSetSync = @mysqli_query($link,$SyncSQL)){
                                if(@mysqli_num_rows($ResultSetSync)>0){
                                    while($RowSetSync = @mysqli_fetch_array($ResultSetSync)){
                                        $link2 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                        $SqlProducto = "CALL SP_OBTENER_PRODUCTOS_CARRITO(" . $RowSetSync['idCarrito'] . ", 'N');";
                                        $SqlQueryProducto = @mysqli_query($link2, $SqlProducto) or die('Error al intentar lanzar la query ' . $SqlProducto);
                                        echo "<h3>Carrito (" . $RowSetSync["idCarrito"] . ") Productos (" . @mysqli_num_rows($SqlQueryProducto) . ") <img id='dropdown_" . $RowSetSync["idCarrito"] . "' style='max-width:14px;' src='./icons/drop-down-arrow.png' align='absmiddle' onClick='muestraTablaOculta(this.id," . $RowSetSync["idCarrito"] . ");' /></h3>";
                                        if(@mysqli_num_rows($SqlQueryProducto)>0){
                        ?>
                                                    <table class="table table-bordered" style="display:none;" id="table_<?php echo $RowSetSync["idCarrito"]; ?>" style="margin-bottom:5%;">
                                                    <thead>
                                                        <tr style="background:#E5E5E5;">
                                                            <th>ID</th>
                                                            <th>ID Customer</th>
                                                            <th>Customer</th>
                                                            <th>Email</th>
                                                            <th>ID Producto</th>
                                                            <th>Referencia</th>
                                                            <th>Producto</th>
                                                            <th>Precio</th>
                                                            <th style="text-align:left;">URL Imagen</th>
                                                            <th style="text-align:left;">Lanzadera</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                        <?php
                                            $LastIdProduct = 0;
                                            while($ResultSetProducto = @mysqli_fetch_array($SqlQueryProducto)){
                                                if($ResultSetProducto["idProducto"] != $LastIdProduct){
                        ?>
                                                        <tr>
                                                            <th scope="row"><?php echo $ResultSetProducto["idCarrito"]; ?></th>
                                                            <td><?php echo $ResultSetProducto["idCustomer"]; ?></td>
                                                            <td><?php echo utf8_encode($ResultSetProducto["nombreCustomer"]); ?></td>
                                                            <td><?php echo $ResultSetProducto["emailCustomer"]; ?></td>
                                                            <td><?php echo $ResultSetProducto["idProducto"]; ?></td>
                                                            <td><?php echo $ResultSetProducto["referenciaProducto"]; ?></td>
                                                            <td><?php echo utf8_encode($ResultSetProducto["nombreProducto"]); ?></td>
                                                            <td><?php echo $ResultSetProducto["precioProducto"]; ?>€</td>
                                                            <?php $urlImagen = "http://mamalolashoes.com/" . $ResultSetProducto["idImagen"] . "-large_default/" . GeneraAmigable(utf8_encode($ResultSetProducto["nombreProducto"])). ".jpg"; ?>
                                                            <td style="text-align:left;"><button type="button" onclick="window.open('<?php echo $urlImagen; ?>');" class="btn btn-success btn-sm m-b-10 m-l-5">Ver</button> <?php echo $urlImagen; ?></td>
                                                            <td style="text-align:left;"><button type="button" onclick="window.location.href='./index.php?action=run&subAction=run&step=2&idCustomer=<?php echo $ResultSetProducto["idCarrito"]; ?>';" class="btn btn-warning btn-sm m-b-10 m-l-5">Lanzar</button></td>
                                                        </tr>
                        <?php
                                                    $LastIdProduct = $ResultSetProducto["idProducto"];
                                                }
                                            }
                        ?>
                                                    </tbody>
                                                </table>
                        <?php
                                        }
                                    }
                                }
                                $type="Manual";
                                if(isset($_GET['job']) && $_GET['job']=='S')
                                    $type="Automático";
                                $link3 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                $SqlUpdate = "UPDATE ps_config_carritos_abandonados SET last_update_date=DATE_FORMAT(NOW(),'%d/%m/%Y'), last_update_hour=TIME_FORMAT(NOW(),'%H:%i'), last_sync_type='" . $type . "';";
                                //@mysqli_query($link3,$SqlUpdate);
                            }else{
                                echo "Hubo un error al intentar parar el proceso.";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>