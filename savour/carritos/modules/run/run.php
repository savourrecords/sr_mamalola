<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Procesos</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Procesos</li>
                        <li class="breadcrumb-item active">Lanzar (Manual)</li>
                    </ol>
                </div>
            </div>

<?php if(isset($_GET["step"]) && $_GET["step"]!=""){ ?>
    <?php if($_GET["step"]==1){ ?>
        <div class="container-fluid">    
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Run (Manual)</h1><br>
                        Atención, si continúa con el proceso se enviarán los emails de carritos a todos los usuarios correspondientes. En caso de querer lanzarlo para un usuario concreto puede hacerlo desde la tabla de muestreo que aparece al lanzar el debug.<br><br>
                        ¿Está seguro de querer continuar lanzando el proceso?<br><br><br><br>
                        <button type="button" class="btn-continue-run btn btn-success m-b-10 m-l-5">Continuar</button>
                        <button type="button" class="btn-cancel-run btn btn-danger m-b-10 m-l-5">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    <?php }else{ ?>

<div class="container-fluid">
    
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h1>Run (Manual)</h1><br>
                    <div class="row">
                        <div class="col-md-12">
                                <?php
                                    function GeneraAmigable($texto){
                                        $textoAmigable = $texto;
                                        $textoAmigable = mb_strtolower($textoAmigable);
                                        $textoAmigable = str_replace("á","a",$textoAmigable);
                                        $textoAmigable = str_replace("é","e",$textoAmigable);
                                        $textoAmigable = str_replace("í","i",$textoAmigable);
                                        $textoAmigable = str_replace("ó","o",$textoAmigable);
                                        $textoAmigable = str_replace("ú","u",$textoAmigable);
                                        $textoAmigable = str_replace("à","a",$textoAmigable);
                                        $textoAmigable = str_replace("è","e",$textoAmigable);
                                        $textoAmigable = str_replace("è","i",$textoAmigable);
                                        $textoAmigable = str_replace("ò","o",$textoAmigable);
                                        $textoAmigable = str_replace("ù","u",$textoAmigable);
                                        $textoAmigable = str_replace("ñ","n",$textoAmigable);
                                        $textoAmigable = str_replace(" - ","-",$textoAmigable);
                                        $textoAmigable = str_replace(" ","-",$textoAmigable);
                                        return $textoAmigable;
                                    }
                                    if($RW_DashboardInfo['status_process']!="PARADO"){

                                        $log = "[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando proceso de recuperación de datos.";
                                        
                                        if(isset($_GET["idCustomer"]) && $_GET["idCustomer"]!=""){
                                            $SyncSQL = "CALL SP_OBTENER_CARRITOS_ENVIO_POR_ID_CARRITO(" . $_GET["idCustomer"] . ");";
                                        }else{
                                            $SyncSQL = "CALL SP_OBTENER_CARRITOS_ENVIO();";
                                        }
                                        $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Recuperando datos del carrito.";
                                        if($ResultSetSync = @mysqli_query($link,$SyncSQL)){
                                            if(@mysqli_num_rows($ResultSetSync)>0){
                                                while($RowSetSync = @mysqli_fetch_array($ResultSetSync)){
                                                    $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando proceso de recuperación de datos del usuario y productos.";
                                                    $link2 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                                    $SqlProducto = "CALL SP_OBTENER_PRODUCTOS_CARRITO(" . $RowSetSync['idCarrito'] . ", 'N');";
                                                    $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Obteniendo datos del usuario y productos.";
                                                    $SqlQueryProducto = @mysqli_query($link2, $SqlProducto) or die('Error al intentar lanzar la query ' . $SqlProducto);
                                                    if(@mysqli_num_rows($SqlQueryProducto)>0){
                                                        $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Datos obtenidos correctamente.";
                                                        $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Obteniendo plantilla del emal.";
                                                        $file = $dirBase . "html/" . $nameTheme;
                                                        if(file_exists($file)){
                                                            $contentEmail = file_get_contents($file);
                                                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Conformando contenido del email.";
                                                            $bodyEmail = "";
                                                            $emailCustomer = "";
                                                            while($RowProducto = @mysqli_fetch_array($SqlQueryProducto)){
                                                                $urlImagen = "http://mamalolashoes.com/" . $RowProducto["idImagen"] . "-large_default/" . GeneraAmigable(utf8_encode($RowProducto["nombreProducto"])). ".jpg";
                                                                $linea = file_get_contents($dirBase . "html/" . $nameTheme . "_LP.php");
                                                                $linea = str_replace("@UrlImagen@",$urlImagen,$linea);
                                                                $linea = str_replace("@NombreProducto@",utf8_encode($RowProducto["nombreProducto"]),$linea);
                                                                $bodyEmail .= $linea;
                                                                if($emailCustomer=="")
                                                                    $emailCustomer = $RowProducto["emailCustomer"];
                                                            }
                                                            $contentEmail = str_replace("@BodyEmail@",$bodyEmail,$contentEmail);
                                                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Email conformado correctamente.";
                                                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Iniciando envío del email.";

                                                            $to = $emailCustomer;
                                                            //$to = "josemacia@savourrecords.com";
                                                            try{
                                                                //$mail = new PHPMailer(true);
                                                                $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                                                                //Server settings
                                                                $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                                                                $mail->isSMTP();                                      // Set mailer to use SMTP
                                                                $mail->Host = 'SSL0.OVH.NET';  				  // Specify main and backup SMTP servers
                                                                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                                                $mail->Username = 'hola@mamalolashoes.com';   // SMTP username
                                                                $mail->Password = 'mamalola17*!';                    // SMTP password
                                                                $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
                                                                $mail->Port = 465;                                    // TCP port to connect to
                                                                //Recipients
                                                                $mail->setFrom('hola@mamalolashoes.com', 'Mamalola Shoes');
                                                                $mail->addAddress($to, '');
                                                                //$mail->addCC('anarubio@savourrecords.com');
                                                                
                                                                //Content
                                                                $mail->isHTML(true);                                  	// Set email format to HTML
                                                                $mail->Subject = 'Inevitable, inolvidable...';
                                                                $mail->Body  = $contentEmail;
                                                                $mail->send();
                                                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Email enviado con éxito.";
                                                                
                                                                $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=1, observaciones='" . $log . "' WHERE idEnvio=" . $RowSetSync["idEnvio"];
                                                            } catch (Exception $e) {
                                                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar enviar el email. " . $mail->ErrorInfo;
                                                                $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=0, observaciones='" . $log . "' WHERE idEnvio=" . $RowSetSync["idEnvio"];
                                                            }
                                                        }else{
                                                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar recuperar la plantilla " . $nameTheme . " desde la ruta " . $file . ".";
                                                            $SqlUPD = "UPDATE ps_env_envios SET procesado=1, estadoEnvio=0, observaciones='" . $log . "' WHERE idEnvio=" . $RowSetSync["idEnvio"];
                                                        }
                                                    }
                                                    $linkUPD = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                                    @mysqli_query($linkUPD,$SqlUPD);
                                                }
                                            }else{
                                                $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar recuperar la información del usuario y productos.";
                                            }
                                            $type="Manual";
                                            if(isset($_GET['job']) && $_GET['job']=='S')
                                                $type="Automático";
                                            $link3 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                            $SqlUpdate = "UPDATE ps_config_carritos_abandonados SET last_update_date=DATE_FORMAT(NOW(),'%d/%m/%Y'), last_update_hour=TIME_FORMAT(NOW(),'%H:%i'), last_sync_type='" . $type . "';";
                                            @mysqli_query($link3,$SqlUpdate);
                                        }else{
                                            $log .= "<br>[" . date("d-m-Y") . "] [" . date("H:i") . "] - Hubo un error al intentar obtener la información del carrito.";
                                        }

                                    }else{
                                        echo "El proceso está parado y no pueden hacerse envíos de carritos.";
                                    }
                                ?>
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home5" role="tab" aria-controls="home5" aria-expanded="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Log</span></a>                                            </li>
                                    </ul>
                                    <div class="tab-content tabcontent-border p-20" id="myTabContent">
                                        <div role="tabpanel" class="tab-pane fade show active" id="home5" aria-labelledby="home-tab">
                                            <?php echo $log; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php } ?>
<?php } ?>