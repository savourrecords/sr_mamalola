<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Procesos</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Procesos</li>
                        <li class="breadcrumb-item active">Sincronizar</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Synchronize</h1><br>
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            $SyncSQL = "CALL SP_INSERTA_CARRITOS_ENVIO();";
                            if($ResultSetSync = @mysqli_query($link,$SyncSQL)){
                                if(@mysqli_num_rows($ResultSetSync)>0){
                                    while($RowSetSync = @mysqli_fetch_array($ResultSetSync)){
                        ?>
                                   <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>TOTAL</th>
                                                    <th>NUEVOS</th>
                                                    <th>PROCESADOS</th>
                                                    <th>PENDIENTES</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $RowSetSync['TotalRegistros']; ?></td>
                                                    <td><?php echo $RowSetSync['NuevosRegistros']; ?></td>
                                                    <td><?php echo $RowSetSync['TotalRegistrosProcesados']; ?></td>
                                                    <td><?php echo $RowSetSync['TotalRegistrosSinProcesar']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 
                        <?php
                                    }
                                }
                                
                                $type="Manual";
                                if(isset($_GET['job']) && $_GET['job']=='S')
                                    $type="Automático";

                                $link2 = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);
                                $SqlUpdate = "UPDATE ps_config_carritos_abandonados SET last_sync_date=DATE_FORMAT(NOW(),'%d/%m/%Y'), last_sync_hour=TIME_FORMAT(NOW(),'%H:%i'), last_sync_type='" . $type . "';";
                                @mysqli_query($link2,$SqlUpdate);

                            }else{
                                echo "Hubo un error al intentar parar el proceso.";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>