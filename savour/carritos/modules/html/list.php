<form method="POST" id="frm-edit" action="index.php?action=html&subAction=new" style="display:none;">
    <input type="hidden" id="fileName" name="fileName">
</form>
<form method="POST" id="frm-delete" action="index.php?action=html&subAction=delete" style="display:none;">
    <input type="hidden" id="fileNameD" name="fileNameD">
</form>

<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Plantillas</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Plantillas</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Theme List</h1><br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card-body">
                                <?php
                                    $SqlThemes = "SELECT idPlantilla, nombrePlantilla, activo, lastUpdate FROM ps_themes_emails_carritos ORDER BY activo DESC, nombrePlantilla ASC;";
                                    if($RS_SqlThemes = @mysqli_query($link, $SqlThemes)){
                                        if(@mysqli_num_rows($RS_SqlThemes)>0){
                                            while($RowThemes = @mysqli_fetch_array($RS_SqlThemes)){
                                                $activo = '';
                                                if($RowThemes['activo'] == "S")
                                                    $activo = '<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>';
                                                echo "<h3 style='line-height:15px;'>" . $RowThemes['nombrePlantilla'] . " " . $activo . "<br><span style='font-size:12px !important;'><strong>Last Update: </strong>" . $RowThemes['lastUpdate'] . "</span></h3>";
                                ?>
                                                    <button type="button" class="btn btn-info btn-sm m-b-10 m-l-5" onclick="ViewPrevia(<?php echo $RowThemes['idPlantilla']; ?>);">Previsualizar</button>
                                                    <button type="button" class="btn btn-warning btn-sm m-b-10 m-l-5" onclick="EditHtml(<?php echo $RowThemes['idPlantilla']; ?>);">Editar</button>
                                                    <button type="button" class="btn btn-danger btn-sm m-b-10 m-l-5" onclick="DeleteTheme(<?php echo $RowThemes['idPlantilla']; ?>);">Eliminar</button>
                                <?php
                                            }
                                        }
                                    }
                                ?>        
                            </div>
                        </div>
                        <div class="col-md-8" style="background:#F5F5F5;">
                            <div class="card-body">
                                <iframe id="frame-prev" style="border:0px !important; width:100%; height:1700px;" src=""></iframe>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>