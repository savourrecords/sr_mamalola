<?php
    $nombrePlantilla = "";
    $contenidoPlantilla = "";
    $contenidoPlantillaItem = "";
    $idPlantilla = "";

    if(isset($_POST['fileName']) && $_POST['fileName']!= ""){
        $SqlPlantilla = "SELECT * FROM ps_themes_emails_carritos WHERE idPlantilla=" . $_POST['fileName'];
        if($RS_SqlPlantilla = @mysqli_query($link, $SqlPlantilla)){
            if(@mysqli_num_rows($RS_SqlPlantilla)>0){
                $RowSqlPlantilla = @mysqli_fetch_array($RS_SqlPlantilla);
                $nombrePlantilla = $RowSqlPlantilla['nombrePlantilla'];
                $contenidoPlantilla = $RowSqlPlantilla['contentHtml'];
                $contenidoPlantillaItem = $RowSqlPlantilla['contentItem'];
                $idPlantilla = $_POST['fileName'];
            }
        }
    }
?>

<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Plantillas</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Plantillas</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>New Theme</h1><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">
                                    <div class="basic-form">
                                        <form action="./index.php?action=html&subAction=save" method="POST">
                                            <input type="hidden" name="idPlantilla" value="<?php echo $idPlantilla; ?>">
                                            <div class="form-group">
                                                <p class="text-muted m-b-15 f-s-12">Nombre de Plantilla</p>
                                                <input type="text" id="themeName" name="themeName" class="form-control input-default" placeholder="Nombre de plantilla" value="<?php echo $nombrePlantilla; ?>">
                                            </div>
                                            <div class="form-group">
                                                <p class="text-muted m-b-15 f-s-12">HTML Plantilla (recuerde que el contenido dinámico debe informarse en la plantilla como <i>@BodyEmail@</i> y no se permite el uso de comillas smples < ' >)</p>
                                                <textarea id="themeContent" class="col-md-12 input-default" style="height:500px;" name="themeContent"><?php echo $contenidoPlantilla; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <p class="text-muted m-b-15 f-s-12">HTML Items (recuerde que los parámetros dinamicos son <i>@NombreProducto@</i> y <i>@UrlImagen@</i>  y no se permite el uso de comillas smples < ' >)</p>
                                                <textarea id="themeItems" class="col-md-12 input-default" style="height:500px;" name="themeContentItem"><?php echo $contenidoPlantillaItem; ?></textarea>
                                            </div>
                                            <div class="form-group" style="margin-top:5%;">
                                                <button id="btn-submit" style="float:right;" type="submit" class="btn btn-success m-b-10 m-l-5">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>