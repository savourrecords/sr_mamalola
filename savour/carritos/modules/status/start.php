<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Status</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Status</li>
                        <li class="breadcrumb-item active">Iniciar</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Start Process</h1><br>
                    <div class="row">
                        <div class="col-md-12">
                        <?php
                            $StopProcessSql = "UPDATE ps_config_carritos_abandonados SET status_process='INICIADO';";
                            if(@mysqli_query($link,$StopProcessSql)){
                                echo "El proceso ha sido iniciado.";
                                echo "<script>setTimeout(function(){ window.location.href='index.php'; },3000);</script>";
                            }else{
                                echo "Hubo un error al intentar iniciar el proceso.";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>