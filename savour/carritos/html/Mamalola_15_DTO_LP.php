<div style="background-color:transparent;">
        <div style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid mixed-two-up ">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td style="background-color:transparent;" align="center">
                            <table cellpadding="0" cellspacing="0" border="0" style="width: 650px;">
                                <tr class="layout-full-width" style="background-color:#FFFFFF;">
                                    <![endif]-->
                                    <!--[if (mso)|(IE)]>
                                    <td align="center" width="217" style=" width:217px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                        <![endif]-->
                                        <div class="col num4" style="display: table-cell;vertical-align: top;max-width: 320px;min-width: 216px;">
                                            <div style="background-color: transparent; width: 100% !important;">
                                                <!--[if (!mso)&(!IE)]><!-->
                                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                    <!--<![endif]-->
                                                    <div align="center" class="img-container center  autowidth  fullwidth " style="padding-right: 0px;  padding-left: 0px;">
                                                        <!--[if mso]>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr style="line-height:0px;line-height:0px;">
                                                                <td style="padding-right: 0px; padding-left: 0px;" align="center">
                                                                    <![endif]-->
                                                                    <img class="center  autowidth  fullwidth" align="center" border="0" src="@UrlImagen@" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 216.666666666667px" width="216.666666666667">
                                                                    <!--[if mso]>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </div>
                                                    <!--[if (!mso)&(!IE)]><!-->
                                                </div>
                                                <!--<![endif]-->
                                            </div>
                                        </div>
                                        <!--[if (mso)|(IE)]>
                                    </td>
                                    <td align="center" width="433" style=" width:433px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top">
                                        <![endif]-->
                                        <div class="col num8" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 432px;">
                                            <div style="background-color: transparent; width: 100% !important;">
                                                <!--[if (!mso)&(!IE)]><!-->
                                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                    <!--<![endif]-->
                                                    <div class="">
                                                        <!--[if mso]>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                                    <![endif]-->
                                                                    <div style="color:#555555;line-height:120%;font-family:Ubuntu, Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                                        <div style="font-size:12px;line-height:14px;color:#555555;font-family:Ubuntu, Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 14px;line-height: 17px"><span style="font-size: 14px; line-height: 16px;" id="_mce_caret" data-mce-bogus="1"></span><span style="font-size: 26px; line-height: 31px;">@NombreProducto@</span></p>
                                                                        </div>
                                                                    </div>
                                                                    <!--[if mso]>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </div>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="divider " style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                        <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td class="divider_inner" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 10px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                    <table class="divider_content" height="0px" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px solid #FFFFFF;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                        <tbody>
                                                                            <tr style="vertical-align: top">
                                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                                                    <span> </span>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if (!mso)&(!IE)]><!-->
                                                </div>
                                                <!--<![endif]-->
                                            </div>
                                        </div>
                                        <!--[if (mso)|(IE)]>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <![endif]-->
            </div>
        </div>
    </div>