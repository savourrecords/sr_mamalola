<?php
    include_once('../config/config-db.php');
    include_once('../config/config-connection.php');

    $imagen = "http://mamalolashoes.com/103-large_default/itaca-cuero.jpg";
    $nombre = "ITACA CUERO";

    $SqlPlantilla = "SELECT contentHtml,contentItem FROM ps_themes_emails_carritos WHERE idPlantilla=" . $_GET['idPlantilla'];
    if($RS_SqlPlantilla = @mysqli_query($link, $SqlPlantilla)){
        if(@mysqli_num_rows($RS_SqlPlantilla)>0){
            $RowPlantilla = @mysqli_fetch_array($RS_SqlPlantilla);
            
            $item = $RowPlantilla['contentItem'];
            $item = str_replace("@UrlImagen@",$imagen,$item);
            $item = str_replace("@NombreProducto@",$nombre,$item);
            $content = $RowPlantilla['contentHtml'];
            $content = str_replace("@BodyEmail@",$item,$content);
            echo utf8_decode($content);
        }
    }else{
        die('No se pudo recuperar la información de esta plantilla.');
    }
?>