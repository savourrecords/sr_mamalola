-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: mamalolamzsavour.mysql.db
-- Tiempo de generación: 11-07-2018 a las 11:13:58
-- Versión del servidor: 5.6.39-log
-- Versión de PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mamalolamzsavour`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ACTUALIZA_ENVIO_CARRITOS`$$
CREATE DEFINER=`mamalolamzsavour`@`%` PROCEDURE `SP_ACTUALIZA_ENVIO_CARRITOS` (IN `idEnvio` INT, IN `idCarrito` INT, IN `estadoEnvio` INT, IN `observaciones` VARCHAR(8000), IN `debug` BIT)  BEGIN
        
        
        
        IF(debug) THEN
			SELECT CONCAT("UPDATE ps_env_envios SET procesado=1, estadoEnvio=",estadoEnvio,", observaciones='",observaciones,"' WHERE idEnvio=",idEnvio) as QueryDymaic;
        ELSE
			SET @Query = CONCAT("UPDATE ps_env_envios SET procesado=1, estadoEnvio=",estadoEnvio,", observaciones='",observaciones,"' WHERE idEnvio=",idEnvio);
			PREPARE statement FROM @Query;
			EXECUTE statement;
			DEALLOCATE PREPARE statement;
        END IF;
        
END$$

DROP PROCEDURE IF EXISTS `SP_INSERTA_CARRITOS_ENVIO`$$
CREATE DEFINER=`mamalolamzsavour`@`%` PROCEDURE `SP_INSERTA_CARRITOS_ENVIO` ()  BEGIN

		DECLARE NuevosRegistros INT;
        DECLARE TotalRegistros INT;
        DECLARE TotalRegistrosProcesados INT;
        DECLARE TotalRegistrosSinProcesar INT;
        
        SET NuevosRegistros = 0;
        SET TotalRegistros = 0;
        SET TotalRegistrosProcesados = 0;
        SET TotalRegistrosSinProcesar = 0;

		CREATE TABLE `_TMP_ps_env_envios` (
		  `idEnvio` int(11) NOT NULL AUTO_INCREMENT,
		  `fechaEnvio` datetime NOT NULL,
		  `emailFrom` varchar(150) DEFAULT NULL,
		  `idCustomer` int(11) NOT NULL,
		  `emailTo` varchar(150) NOT NULL,
		  `asunto` varchar(255) DEFAULT NULL,
		  `idCarrito` int(11) NOT NULL,
		  `procesado` bit(1) NOT NULL DEFAULT b'0',
		  `estadoEnvio` bit(1) DEFAULT NULL,
		  `observaciones` varchar(8000) DEFAULT NULL,
		  PRIMARY KEY (`idEnvio`),
		  UNIQUE KEY `idEnvio_UNIQUE` (`idEnvio`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
        INSERT INTO
			_TMP_ps_env_envios(fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones)
        SELECT
				NOW() as fechaEnvio,
				NULL as emailFrom,
				CLI.id_customer as idCustomer,
				CLI.email as emailTo,
				CONCAT(CLI.firstname,", un carrito precisa de tu atencion...") as asunto,
				CRR.id_cart as idCarrito,
				false as procesado,
				NULL as estadoEnvio,
				NULL as obsercaciones
			FROM
				ps_cart CRR INNER JOIN
				ps_customer CLI ON CLI.id_customer=CRR.id_customer 
			WHERE
				id_cart NOT IN(
					SELECT DISTINCT id_cart FROM ps_orders
				) AND
				id_cart IN(
					SELECT DISTINCT id_cart FROM ps_cart_product
				) AND
				TIMEDIFF(NOW(),CRR.date_add)>1 AND
				DATEDIFF(NOW(),CRR.date_add)>1
			ORDER BY
				CRR.id_cart ASC;
        
			SET NuevosRegistros = (SELECT COUNT(*) FROM _TMP_ps_env_envios WHERE idEnvio NOT IN( SELECT idEnvio FROM ps_env_envios));
        
			
			INSERT INTO
				ps_env_envios(fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones)
            SELECT
				fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones
			FROM
				_TMP_ps_env_envios
			WHERE
				idEnvio NOT IN(
					SELECT idEnvio FROM ps_env_envios
				);
            
            DROP TABLE _TMP_ps_env_envios;
            
            CREATE TABLE `_TMP_ps_env_envios` (
			  `idEnvio` int(11) NOT NULL AUTO_INCREMENT,
			  `fechaEnvio` datetime NOT NULL,
			  `emailFrom` varchar(150) DEFAULT NULL,
			  `idCustomer` int(11) NOT NULL,
			  `emailTo` varchar(150) NOT NULL,
			  `asunto` varchar(255) DEFAULT NULL,
			  `idCarrito` int(11) NOT NULL,
			  `procesado` bit(1) NOT NULL DEFAULT b'0',
			  `estadoEnvio` bit(1) DEFAULT NULL,
			  `observaciones` varchar(8000) DEFAULT NULL,
			  PRIMARY KEY (`idEnvio`),
			  UNIQUE KEY `idEnvio_UNIQUE` (`idEnvio`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        
			INSERT INTO
				_TMP_ps_env_envios(fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones)
            SELECT
				fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones
			FROM
				ps_env_envios;
                
			TRUNCATE TABLE ps_env_envios;
            
            INSERT INTO
				ps_env_envios(fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones)
            SELECT
				fechaEnvio, emailFrom, idCustomer, emailTo, asunto,idCarrito, procesado, estadoEnvio, observaciones
			FROM
				_TMP_ps_env_envios;
			
            DROP TABLE _TMP_ps_env_envios;
           
           SET TotalRegistros = (SELECT COUNT(*) FROM ps_env_envios);
           SET TotalRegistrosProcesados = (SELECT COUNT(*) FROM ps_env_envios WHERE procesado=1);
           SET TotalRegistrosSinProcesar =  (SELECT COUNT(*) FROM ps_env_envios WHERE procesado=0);
           
           SELECT TotalRegistros, NuevosRegistros, TotalRegistrosProcesados, TotalRegistrosSinProcesar;
			
END$$

DROP PROCEDURE IF EXISTS `SP_OBTENER_CARRITOS_ENVIO`$$
CREATE DEFINER=`mamalolamzsavour`@`%` PROCEDURE `SP_OBTENER_CARRITOS_ENVIO` ()  BEGIN

		SELECT idEnvio,idCarrito FROM ps_env_envios WHERE procesado!=1 ORDER BY idCarrito DESC;
        
END$$

DROP PROCEDURE IF EXISTS `SP_OBTENER_CARRITOS_ENVIO_POR_ID_CARRITO`$$
CREATE DEFINER=`mamalolamzsavour`@`%` PROCEDURE `SP_OBTENER_CARRITOS_ENVIO_POR_ID_CARRITO` (IN `idCarritoFilter` INT)  BEGIN

		SELECT idEnvio,idCarrito FROM ps_env_envios WHERE procesado!=1 AND idCarrito=idCarritoFilter  ORDER BY idCarrito DESC;
        
END$$

DROP PROCEDURE IF EXISTS `SP_OBTENER_PRODUCTOS_CARRITO`$$
CREATE DEFINER=`mamalolamzsavour`@`%` PROCEDURE `SP_OBTENER_PRODUCTOS_CARRITO` (IN `idCarrito` INT, IN `debug` CHAR(1))  BEGIN

	DECLARE idCarritoFiltro INT;
        DECLARE debugQuery CHAR(1);
        
        SET idCarritoFiltro = idCarrito;
        
        IF(debug=NULL) THEN
			SET debugQuery = "N";
		ELSE
			SET debugQuery = debug;
		END IF;
        
        IF(debugQuery="S") THEN
			SELECT CONCAT("SELECT DISTINCT CRR.id_cart as idCarrito, CLI.id_customer as idCustomer, (CASE WHEN CLI.lastname=NULL THEN CLI.firstname ELSE  CONCAT(CLI.firstname,' ',CLI.lastname) END) as nombreCustomer, CLI.email as emailCustomer, PROD.id_product as idProducto, PROD.reference as referenciaProducto, LPROD.name as nombreProducto, ROUND(PROD.price*1.21, 2) as precioProducto, IMG.id_image as idImagen FROM ps_cart as CRR INNER JOIN ps_customer CLI ON CLI.id_customer=CRR.id_customer INNER JOIN ps_cart_product CPROD ON CPROD.id_cart=CRR.id_cart INNER JOIN ps_product PROD ON PROD.id_product=CPROD.id_product INNER JOIN ps_product_lang AS LPROD ON LPROD.id_product=PROD.id_product INNER JOIN ps_image IMG ON (IMG.id_product=PROD.id_product AND IMG.cover=1) WHERE CRR.id_cart=",idCarritoFiltro,";") as QueryDymaic;
        ELSE
			SET @Query = CONCAT("SELECT DISTINCT CRR.id_cart as idCarrito, CLI.id_customer as idCustomer, (CASE WHEN CLI.lastname=NULL THEN CLI.firstname ELSE  CONCAT(CLI.firstname,' ',CLI.lastname) END) as nombreCustomer, CLI.email as emailCustomer, PROD.id_product as idProducto, PROD.reference as referenciaProducto, LPROD.name as nombreProducto, ROUND(PROD.price*1.21, 2) as precioProducto, IMG.id_image as idImagen FROM ps_cart as CRR INNER JOIN ps_customer CLI ON CLI.id_customer=CRR.id_customer INNER JOIN ps_cart_product CPROD ON CPROD.id_cart=CRR.id_cart INNER JOIN ps_product PROD ON PROD.id_product=CPROD.id_product INNER JOIN ps_product_lang AS LPROD ON LPROD.id_product=PROD.id_product INNER JOIN ps_image IMG ON (IMG.id_product=PROD.id_product AND IMG.cover=1) WHERE CRR.id_cart=",idCarritoFiltro,";");
			PREPARE statement FROM @Query;
			EXECUTE statement;
			DEALLOCATE PREPARE statement;
        END IF;
        
END$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
