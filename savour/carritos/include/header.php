<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="logotipo-savour.jpeg">
    <title>GCA - Gestión de Carritos Abandonados v1.0</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">

    <div id="msg-error-lanzadera" style="display:none; position:fixed; top:0%; left:0%; width:100%; z-index:9999;" class="alert alert-danger">No se pude lanzar el proceso estándo parado.<br/>Pulse el botón de "Iniciar" para poder lanzar el proceso.</div>
    <div id="msg-error-sincronizacion" style="display:none; position:fixed; top:0%; left:0%; width:100%; z-index:9999;" class="alert alert-danger">No se pude sincronizar el proceso estándo parado.<br/>Pulse el botón de "Iniciar" para poder sincronizar el proceso.</div>

    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <img id="btn-menu-mobile" src="./icons/mobile-menu.png" class="img-responsive" style="display:none;position: absolute;width: 30px;height: 30px;left: 1%;top: 25%;">
                    <a class="navbar-brand" href="index.php">
                        <b><img src="<?php echo $urlLogotipo; ?>" alt="homepage" class="dark-logo" /></b>
                        <span><img src="<?php echo $urlLogotipo; ?>" alt="homepage" class="dark-logo" style="width:50%;"/></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
                        <li style="display:none;" id="btn-menu" class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="images/logotipo-savour.jpeg" alt="Savour Records" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="actions/logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="index.php" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="nav-label">App</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-server"></i><span class="hide-menu">Procesos</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a <?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'href="#" class="btn-sync-error"'; else echo 'href="index.php?action=sync&subAction=sync"'; ?>>Sincronizar</a></li>
                                <li><a <?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'href="#" class="btn-lanzar-error"'; else echo 'href="index.php?action=run&subAction=debug"'; ?>>Lanzar (Debug)</a></li>
                                <li><a <?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'href="#" class="btn-lanzar-error"'; else echo 'href="index.php?action=run&subAction=run&step=1"'; ?>>Lanzar (Manual)</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-globe"></i><span class="hide-menu">Estado</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="index.php?action=status&subAction=start">Iniciar</a></li>
                                <li><a href="index.php?action=status&subAction=stop">Parar</a></li>
                            </ul>
                        </li>
                        <li> <a class="" href="index.php?action=config&subAction=config" aria-expanded="false"><i class="fa fa-wpforms"></i><span class="hide-menu">Configuración</span></a></li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-columns"></i><span class="hide-menu">Plantillas</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="index.php?action=html&subAction=new">Nueva</a></li>
                                <li><a href="index.php?action=html&subAction=list">Listar</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->