<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>

<div class="container-fluid">
    
    <!--Calendario-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="year-calendar"></div>
            </div>
        </div>
    </div>

    <!--Status-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Status</h1><br>
                <div class="row" style="background:#dee2e6; padding-top:10px; padding-bottom:10px;">
                    <div class="col-md-12">
                        El estado actual es
                        <strong>
                            <?php 
                                if($RW_DashboardInfo['status_process']=="INICIADO")
                                    echo "INICIADO";
                                else
                                    echo "PARADO"; 
                            ?>
                        </strong>
                    </div>
                </div>
                <?php if($RW_DashboardInfo['status_process']=="INICIADO"){ ?>
                    <button type="button" class="btn-stop-process btn btn-danger m-b-10 m-l-5" style="float:right; margin-top:5%;">Detener</button>
                <?php }else{ ?>
                    <button type="button" class="btn-start-process btn btn-success m-b-10 m-l-5" style="float:right; margin-top:5%;">Iniciar</button>
                <?php } ?>
            </div>
        </div>
    </div>

    <!--LastSync-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Last Sync</h1><br>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:33%;">FECHA</th>
                                    <th style="width:33%;">HORA</th>
                                    <th style="width:33%; text-align:left;">TIPO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:33%;"><?php echo $RW_DashboardInfo['last_sync_date']; ?></td>
                                    <td style="width:33%;"><?php echo $RW_DashboardInfo['last_sync_hour']; ?>h</td>
                                    <td style="width:33%; text-align:left;"><?php echo utf8_encode($RW_DashboardInfo['last_sync_type']); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button type="button" class="<?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'btn-sync-error'; else echo 'btn-sync'; ?> btn btn-info m-b-10 m-l-5" style="float:right; margin-top:5%;">Sincronizar</button>
            </div>
        </div>
    </div>

    <!--LastRun-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h1>Last Execution</h1><br>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:33%;">FECHA</th>
                                    <th style="width:33%;">HORA</th>
                                    <th style="width:33%; text-align:left;">TIPO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:33%;"><?php echo $RW_DashboardInfo['last_update_date']; ?></td>
                                    <td style="width:33%;"><?php echo $RW_DashboardInfo['last_update_hour']; ?>h</td>
                                    <td style="width:33%; text-align:left;"><?php echo utf8_encode($RW_DashboardInfo['last_update_type']); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button type="button" class="<?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'btn-lanzar-error'; else echo 'btn-lanzar'; ?> btn btn-primary m-b-10 m-l-5" style="float:right; margin-top:5%;">Lanzar</button>
                <button type="button" class="<?php if($RW_DashboardInfo['status_process']=="PARADO") echo 'btn-lanzar-error'; else echo 'btn-lanzar-debug'; ?> btn btn-warning m-b-10 m-l-5" style="float:right; margin-top:5%;">Debug</button>
            </div>
        </div>
    </div>

</div>