<?php
    $link = @mysqli_connect(_SERVER_DB_, _USER_DB_, _PASSWORD_DB_,_NAME_DB_) or die('Error al conectar con ' . _SERVER_DB_);


    function RunQuery($query){
        return @mysqli_query($link, $query) or die('Error al lanzar la consulta.');
    }

    function GetNumRows($resultSet){
        return @mysqli_num_rows($resultSet);
    }

    function GetRow($resultSet){
        return @mysqli_fetch_array($resultSet);
    }
?>