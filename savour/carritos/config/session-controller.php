<?php
    session_start();

    if(!isset($_SESSION['uconn']) || $_SESSION['uconn']!="Records15"){
        if(file_exists("install.php"))
            header("Location:install.php");
        else
            if(!isset($_GET['force']) || $_GET['force']!="S")
                header("Location:login.php");
    }
?>