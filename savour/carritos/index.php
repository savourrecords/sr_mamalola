<?php    
    //use PHPMailer\PHPMailer\{PHPMailer, Exception};
    include_once 'PHPMailer/Exception.php';
    include_once 'PHPMailer/PHPMailer.php';
    include_once 'PHPMailer/SMTP.php';
    include_once('config/session-controller.php');
    include_once('config/config-db.php');
    include_once('config/config-connection.php');
    include_once('config/config-general.php');
    include_once("./actions/get-dashboard-info.php");
    include_once('include/header.php');
    

    $fileInclude = './include/white-container.php';
    if(isset($_GET['action']) && $_GET['action']!="" && isset($_GET['subAction']) && $_GET['subAction']!=""){
        $fileInclude = './modules/' . $_GET['action'] . '/' . $_GET['subAction'] . '.php';
    }
    include_once($fileInclude);


    include_once('include/footer.php');
?>