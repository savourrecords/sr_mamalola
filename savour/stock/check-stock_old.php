<?php
	$mysqli = new mysqli("mamalolamzsavour.mysql.db", "mamalolamzsavour", "MlRecords15", "mamalolamzsavour");
	if (mysqli_connect_errno()) {
		exit();
	}
	
	$id_cart = filter_var($_GET['id_cart'], FILTER_SANITIZE_NUMBER_INT);
	$id_cart = intVal($id_cart);
	/* crear una sentencia preparada */
	$sqlQuery = "select cp.id_product, cp.id_product_attribute ,sa.quantity as Disponible,cp.quantity as Pedida from ps_stock_available sa
	inner join ps_cart_product cp on cp.id_product_attribute = sa.id_product_attribute
	where cp.id_cart = ? and sa.quantity < cp.quantity";

	$result = []; 

	if ($stmt = $mysqli->prepare($sqlQuery)) {

		/* ligar parámetros para marcadores */
		$stmt->bind_param("i", $id_cart);

		/* ejecutar la consulta */
		$stmt->execute();

		//$sentencia->bind_result($idProd, $idProdAttr,$quantity,$availableQuantity);

		$meta = $stmt->result_metadata(); 
		while ($field = $meta->fetch_field()) 
		{ 
			$params[] = &$row[$field->name]; 
		} 

		call_user_func_array(array($stmt, 'bind_result'), $params); 

		while ($stmt->fetch()) { 
			foreach($row as $key => $val) 
			{ 
				$c[$key] = $val; 
			} 
			$result[] = $c; 
		} 

		echo json_encode($result);

		/* cerrar sentencia */
		$stmt->close();
	}

	/* cerrar conexión */
	$mysqli->close();
?>	