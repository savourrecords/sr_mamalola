<?php

	$mysqli = new mysqli("mamalolamzsavour.mysql.db", "mamalolamzsavour", "MlRecords15", "mamalolamzsavour");

	if (mysqli_connect_errno()) {
		exit();
	}

	if (!isset($_POST['items'])){
	
		exit();
	}

	/* Tratamiento de datos */

	$args = array(
		'pid'   => FILTER_SANITIZE_ENCODED,
		'attrid'   => FILTER_SANITIZE_ENCODED,
		'qty'   => FILTER_SANITIZE_ENCODED
	);
	
	/*	$items = filter_var_array($_POST['items'], $args);*/
	$items = $_POST['items'];
 
	//Recopilación ID products
	$idProducts = [];
	foreach($items as $item){
		array_push($idProducts, $item['pid']);
	}
	
	$strIdProduct = implode(",", $idProducts);
	
	/* Crear una sentencia preparada */
	$sqlQuery = "select sa.id_product, sa.id_product_attribute ,sa.quantity as Disponible
        from ps_stock_available sa
		where sa.id_product in ({$strIdProduct})";



	if ($stmt = $mysqli->prepare($sqlQuery)) {

		/* ejecutar la consulta */
		$stmt->execute();

		//$sentencia->bind_result($idProd, $idProdAttr,$quantity,$availableQuantity);

		$meta = $stmt->result_metadata(); 
		while ($field = $meta->fetch_field()) 
		{ 
			$params[] = &$row[$field->name]; 
		} 

		call_user_func_array(array($stmt, 'bind_result'), $params); 

		while ($stmt->fetch()) { 
			foreach($row as $key => $val) 
			{ 
				$c[$key] = $val; 
			}
			foreach($items as $item){
				if ($row['id_product'] == $item['pid'] && $row['id_product_attribute'] == $item['attrid']) {
					$c['qtyOrder'] = $item['qty']; 
					$result[] = $c;
				}
			}
		} 
		
		echo json_encode($result);

		/* cerrar sentencia */
		$stmt->close();
	}

	/* cerrar conexión */
	$mysqli->close();
?>	