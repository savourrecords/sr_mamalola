var iterator = 0;
			jQuery(document).ready(function(){
				// Get the modal
				var modal = document.getElementById('LaunchNewsletterModal');

				// Get the button that opens the modal
				var btn = document.getElementById("btn_LaunchNewsletterModal");

				// Get the <span> element that closes the modal
				var span = document.getElementsByClassName("close")[0];

				// When the user clicks the button, open the modal 
				btn.onclick = function() {
					modal.style.display = "block";
				}

				// When the user clicks on <span> (x), close the modal
				span.onclick = function() {
					modal.style.display = "none";
					jQuery('._modal_FORM_INPUT').val('');
					jQuery('._modal_CHEKBOX_GDPR').prop('checked', false);
				}

				// When the user clicks anywhere outside of the modal, close it
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
						jQuery('._modal_FORM_INPUT').val('');
						jQuery('._modal_CHEKBOX_GDPR').prop('checked', false);
					}
				}

				jQuery('._modal_FORM_SUBMIT').off('click').on('click',function(e){
					var obj = jQuery('#GDPR');
					if(!obj.is(':checked')){
						e.preventDefault();
						jQuery('._modal_TEXTO_GDPR').css('color','#990000');
						jQuery('._modal_TEXTO_GDPR a').css('color','#990000');
						setTimeout(function(){
							jQuery('._modal_TEXTO_GDPR').css('color','#FFF');
							jQuery('._modal_TEXTO_GDPR a').css('color','#FFF');
						},200);
						setTimeout(function(){
							jQuery('._modal_TEXTO_GDPR').css('color','#990000');
							jQuery('._modal_TEXTO_GDPR a').css('color','#990000');
						},400);
						setTimeout(function(){
							jQuery('._modal_TEXTO_GDPR').css('color','#FFF');
							jQuery('._modal_TEXTO_GDPR a').css('color','#FFF');
						},600);
						setTimeout(function(){
							jQuery('._modal_TEXTO_GDPR').css('color','#990000');
							jQuery('._modal_TEXTO_GDPR a').css('color','#990000');
						},800);
						setTimeout(function(){
							jQuery('._modal_TEXTO_GDPR').css('color','#FFF');
							jQuery('._modal_TEXTO_GDPR a').css('color','#FFF');
						},1000);
					}else{
						jQuery('body').css('overflow','auto');
					}
				});

				jQuery('.close').attr('title','Cerrar/Close');

				jQuery('#LaunchNewsletterModal').off('click').on('click',function(){
					jQuery('body').css('overflow','auto');
				});

				jQuery('.close').off('click').on('click',function(){
					jQuery('body').css('overflow','auto');
					jQuery('._modal_FORM_INPUT').val('');
					jQuery('._modal_CHEKBOX_GDPR').prop('checked', false);
				});

				jQuery('._modal_FORM_SUBMIT_MOBILE').off('click').on('click',function(){
					jQuery('._modal_FORM_SUBMIT').click();
				});
			});