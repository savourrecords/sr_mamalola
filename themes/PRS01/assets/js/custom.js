/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
 
/* --------------------------- TmplateTrip JS ------------------------------ */

/* ----------- Start Page-loader ----------- */
		$(window).load(function() 
		{ 
			$(".ttloading-bg").fadeOut("slow");
		})
/* ----------- End Page-loader ----------- */
	

$(document).ready(function(){
				   
if($(window).width()<=992){
	$('.sr-cart-black').attr('src','https://mamalolashoes.com/img/cesta-blanca.svg');
}

/* Go to Top JS START */
		if ($('.goToTop').length) {
			var scrollTrigger = 100, // px
				backToTop = function () {
					var scrollTop = $(window).scrollTop();
					if (scrollTop > scrollTrigger) {
						$('.goToTop').addClass('show');
					} else {
						$('.goToTop').removeClass('show');
					}
				};
			backToTop();
			$(window).on('scroll', function () {
				backToTop();
			});
			$('.goToTop').on('click', function (e) {
				e.preventDefault();
				$('html,body').animate({
					scrollTop: 0
				}, 700);
			});
		}
	/* Go to Top JS END */
	
	var simplebar = new Nanobar();
	simplebar.go(100);
	
	/*---------------- Search ---------------- */

	$(".top-nav .ttsearch_button").click(function() {
	
	$('#page').toggleClass('ttsearch-fixed');
	$('#header .header-nav .right-nav').toggleClass('container');
	$('.top-nav .ttsearchtoggle').parent().toggleClass('active');
	$('.top-nav .ttsearchtoggle').toggle('fast', function() {
	});
	$('.top-nav #search_query_top').attr('autofocus', 'autofocus').focus();
	});
	
	/*---------------- End Search ---------------- */
	
	/* ----------- Start Templatetrip User-info ----------- */
	
	$('.ttuserheading').click(function(event){
	$(this).toggleClass('active');
	event.stopPropagation();
	$(".user-info").slideToggle("fast");
	});
	$(".user-info").on("click", function (event) {
	event.stopPropagation();
	});
	/* ----------- End Templatetrip User-info ----------- */
	
	/* -------------- Start Homepage Tab ------------------- */

$("#hometab").prepend("<div class='tabs'><ul class='nav nav-tabs'></ul></div>");
$("#hometab .ttfeatured-products .tab-title").wrap("<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ttfeatured-content'></a></li>");
$("#hometab .ttbestseller-products .tab-title").wrap("<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ttbestseller-content'></a></li>");
$("#hometab .ttnew-products .tab-title").wrap("<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ttnew-content'></a></li>");
$("#hometab .tabs ul.nav-tabs").append($("#hometab > section li.nav-item"));

$("#hometab > section.ttfeatured-products").wrap("<div class='tab-pane row fade' id='ttfeatured-content'>");
$("#hometab > section.ttbestseller-products").wrap("<div class='tab-pane row fade' id='ttbestseller-content'>");
$("#hometab > section.ttnew-products").wrap("<div class='tab-pane row fade' id='ttnew-content'>");
$("#hometab > .tab-pane").wrapAll("<div class='home-tab-content' id='home-tab-content' />");
$("#hometab").append($("#hometab > .home-tab-content"));

$('#hometab .tabs ul.nav-tabs > li:first-child a').addClass('active');
$('#hometab #home-tab-content .tab-pane:first-child').addClass('in active');

/* -------------- End Homepage Tab ------------------- */



	/* ------------ Start Add Product Bootsrap class JS --------------- */
	
	colsCarousel = $('#right-column, #left-column').length;
	if (colsCarousel == 2) {
		ci=2;
	} else if (colsCarousel == 1) {
		ci=3;
	} else {
		ci=3;
	}

	
		var cols_count = $('#right-column, #left-column').length;
		if (cols_count == 2) {
			$('#content .products .product-miniature, #content-wrapper .products .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols_count == 1) {
			$('#content .products .product-miniature, #content-wrapper .products .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .products .product-miniature, #content-wrapper .products .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}
		
	/* ------------ End Add Product Bootsrap class JS --------------- */
	
	
	/* ----------- carousel For FeatureProduct ----------- */
	
	 var ttfeature = $(".ttfeatured-products .products");
      ttfeature.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".ttfeature_next").click(function(){
        ttfeature.trigger('owl.next');
      })

      $(".ttfeature_prev").click(function(){
        ttfeature.trigger('owl.prev');
      })
	  
	  
	 /* ----------- carousel For ttnew-products ----------- */
	 
	 var ttnew = $(".ttnew-products .products");
      ttnew.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".ttnew_next").click(function(){
        ttnew.trigger('owl.next');
      })

      $(".ttnew_prev").click(function(){
        ttnew.trigger('owl.prev');
      })
	  
	  
	 /* ----------- carousel For bestseller ----------- */
	 
	 var ttbestseller = $(".ttbestseller-products .products");
      ttbestseller.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".ttbestseller_next").click(function(){
        ttbestseller.trigger('owl.next');
      })

      $(".ttbestseller_prev").click(function(){
        ttbestseller.trigger('owl.prev');
      })
	  
	  
	 /* ----------- carousel For ttspecial ----------- */
	 
	 var ttspecial = $(".ttspecial-products .products");
      ttspecial.owlCarousel({
     	 items : 3, //10 items above 1000px browser width
     	 itemsDesktop : [1200,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".ttspecial_next").click(function(){
        ttspecial.trigger('owl.next');
      })

      $(".ttspecial_prev").click(function(){
        ttspecial.trigger('owl.prev');
      })
	  
	  
	 /* ----------- carousel For viewproduct ----------- */
	 
	 var viewproduct = $(".view-product .products");
      viewproduct.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".viewproduct_next").click(function(){
        viewproduct.trigger('owl.next');
      })

      $(".viewproduct_prev").click(function(){
        viewproduct.trigger('owl.prev');
      })
	  
	  
	/* ----------- carousel For Crossselling ----------- */
	
	 var Crossselling = $(".crossselling-product .products");
      Crossselling.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".Crossselling_next").click(function(){
        Crossselling.trigger('owl.next');
      })

      $(".Crossselling_prev").click(function(){
        Crossselling.trigger('owl.prev');
      })
	  
	  
	  /* ----------- carousel For Categoryproducts ----------- */
	  
	  var Categoryproducts = $(".category-products .products");
      Categoryproducts.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".Categoryproducts_next").click(function(){
        Categoryproducts.trigger('owl.next');
      })

      $(".Categoryproducts_prev").click(function(){
        Categoryproducts.trigger('owl.prev');
      })
	  
	  
	  /* ----------- carousel For accessories ----------- */
	  
	  var accessories = $(".product-accessories .products");
      accessories.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1200,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".accessories_next").click(function(){
        accessories.trigger('owl.next');
      })

      $(".accessories_prev").click(function(){
        accessories.trigger('owl.prev');
      })
	  
	  
	/* ----------- Start Carousel For Productpage Thumbs ----------- */
	
		$("#ttproduct-thumbs").owlCarousel({
		navigation:true,
		navigationText: [
			"<i class='material-icons'>&#xE5CB;</i>",
			"<i class='material-icons'>&#xE5CC;</i>"],
		items: 4, //10 items above 1000px browser width
		itemsDesktop : [1200,4], 
		itemsDesktopSmall : [991,3], 
		itemsTablet: [767,2], 
		itemsMobile : [480,1] 
	});
	
	
 	/* -----------Start carousel For TT- brand logo ----------- */
 
	 var ttbrandlogo = $("#ttbrandlogo-carousel");
		  ttbrandlogo.owlCarousel({
			navigation:true,
			navigationText: [
					"<i class='material-icons'>&#xE5CB;</i>",
					"<i class='material-icons'>&#xE5CC;</i>"],
		  autoPlay : true,
			 items :5, //10 items above 1000px browser width
			 itemsDesktop : [1200,5], 
			 itemsDesktopSmall : [991,4], 
			 itemsTablet: [767,3], 
			 itemsMobile : [480,2] 
		  });
	
	/* -----------End carousel For TT brand logo ----------- */
	
	
	 /* -----------Start carousel For Footer Catalog ----------- */
	  
	var ttcatalog = $("#ttcatalog-carousel");
      ttcatalog.owlCarousel({
		 autoPlay : true,
     	 items : 1, //10 items above 1000px browser width
     	 itemsDesktop : [1200,1], 
     	 itemsDesktopSmall : [991,1], 
     	 itemsTablet: [767,1], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events

      $(".ttcatalog_next").click(function(){
        ttcatalog.trigger('owl.next');
      })
      $(".ttcatalog_prev").click(function(){
        ttcatalog.trigger('owl.prev');
      })
	
	 /* -----------End carousel For Footer Catalog ----------- */
 
 	 /* ----------- carousel For CMS Gallery ----------- */

  
var ttcmsgallery = $("#ttcmsgallery-carousel");
  ttcmsgallery.owlCarousel({
 autoPlay : true,
     	 items : 3, //10 items above 1000px browser width
     	 itemsDesktop : [1200,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events

      $(".ttcmsgallery_next").click(function(){
        ttcmsgallery.trigger('owl.next');
      })
      $(".ttcmsgallery_prev").click(function(){
        ttcmsgallery.trigger('owl.prev');
      })
 
  /* ------------ Start TemplateTrip Parallax JS ------------ */
	 
	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
	if(!isMobile) {
	if($(".parallex").length){  $(".parallex").sitManParallex({  invert: false });};    
	}else{
	$(".parallex").sitManParallex({  invert: true });
	}	

/* ------------ End TemplateTrip Parallax JS ------------ */  
 
 /* ----------- Start Templatetrip AddToCart Button ----------- */
 
$( ".tt-button-container .add-to-cart" ).mousedown(function() {
  var form_className = $(this).parent().attr('class');
  $(this).parent().attr('id',form_className);

  var hidden_page_className = $(this).parent().find('.product-quantity .product_page_product_id').attr('class');
  $(this).parent().find('.product-quantity .product_page_product_id').attr('id',hidden_page_className);

  var customization_className = $(this).parent().find('.product-quantity .product_customization_id').attr('class');
  $(this).parent().find('.product-quantity .product_customization_id').attr('id',customization_className);

  var quantity_className = $(this).parent().find('.product-quantity .quantity_wanted').attr('class');
  $(this).parent().find('.product-quantity .quantity_wanted').attr('id',quantity_className);
});

$( ".tt-button-container .add-to-cart" ).mouseup(function() {
  $(this).parent().removeAttr('id');
  $(this).parent().find('.product-quantity > input').removeAttr('id');
});

/* ----------- Start Templatetrip Homeslider ----------- */

$('.carousel-item:odd').addClass('odd');
 	$('.carousel-item:even').addClass('even');

/* ----------- End Templatetrip Homeslider ----------- */

/* -----------Start carousel For TT- cms Testimonial ----------- */
 var tttestimonial = $("#tttestimonial-carousel");
      tttestimonial.owlCarousel({
		pagination : true,
         paginationNumbers : true,
  	  autoPlay : true,
     	 items : 1, //10 items above 1000px browser width
     	 itemsDesktop : [1200,1], 
     	 itemsDesktopSmall : [991,1], 
     	 itemsTablet: [767,1], 
     	 itemsMobile : [480,1] 
      });

      // Custom Navigation Events
      $(".tttestimonial_next").click(function(){
        tttestimonial.trigger('owl.next');
      })

      $(".tttestimonial_prev").click(function(){
        tttestimonial.trigger('owl.prev');
      })
  
/* -----------End carousel For TT- cms Testimonial ----------- */


/* ----------- End Templatetrip AddToCart Button ----------- */
	
});	  

function bindGrid()
{
	var view = localStorage.getItem('display');
	if (view == 'list')
		display(view);
	else
		$('.display').find('#ttgrid').addClass('active');
	//Grid	
	$(document).on('click', '#ttgrid', function(e){
		e.preventDefault();
		display('grid');
	});
	//List
	$(document).on('click', '#ttlist', function(e){
		e.preventDefault();
		display('list');		
	});	
}

$("#products .product-list .thumbnail-container .ttproduct-image .ttproducthover").each(function(){
    $(this).appendTo($(this).parent().parent().find(".ttproduct-desc"));
});
$("#products .product-grid .thumbnail-container .ttproduct-desc .ttproducthover").each(function(){
    $(this).appendTo($(this).parent().parent().parent().find(".ttproduct-image"));
});
$(".ttspecial-products .thumbnail-container .ttproducthover").each(function(){
    $(this).appendTo($(this).parent().parent().parent().find(".ttproduct-desc"));
});



function display(view)
{
	if (view == 'list')
	{
		$('#ttgrid').removeClass('active');
		$('#ttlist').addClass('active');
		$('#content-wrapper .products.product-thumbs .product-miniature').attr('class', 'product-miniature js-product-miniature product-list col-xs-12');
		$('#content-wrapper .products.product-thumbs .product-miniature .ttproduct-image').attr('class', 'ttproduct-image col-xs-5 col-sm-5 col-md-4');
		$('#content-wrapper .products.product-thumbs .product-miniature .ttproduct-desc').attr('class', 'ttproduct-desc col-xs-7 col-sm-7 col-md-8');
		$("#products .product-list .thumbnail-container .ttproduct-image .ttproducthover").each(function(){
			$(this).appendTo($(this).parent().parent().find(".ttproduct-desc"));
		});

		$('#ttlist').addClass('active');
		$('.grid-list').find('#ttlist').addClass('selected');
		$('.grid-list').find('#ttgrid').removeAttr('class');
		localStorage.setItem('display', 'list');
	}
	else
	{
		$('#ttlist').removeClass('active');
		$('#ttgrid').addClass('active');

		var cols_count = $('#right-column, #left-column').length;
		if (cols_count == 2) {
			$('#js-product-list .products.product-thumbs .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols_count == 1) {
			$('#js-product-list .products.product-thumbs .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#js-product-list .products.product-thumbs .product-miniature').attr('class', 'product-miniature js-product-miniature product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}
		$("#products .product-grid .thumbnail-container .ttproduct-desc .ttproducthover").each(function(){
			$(this).appendTo($(this).parent().parent().parent().find(".ttproduct-image"));
		});

		$('#content-wrapper .products.product-thumbs .product-miniature .ttproduct-image').attr('class', 'ttproduct-image');
		$('#content-wrapper .products.product-thumbs .product-miniature .ttproduct-desc').attr('class', 'ttproduct-desc');
		
		$('.grid-list').find('#ttgrid').addClass('selected');
		$('.grid-list').find('#ttlist').removeAttr('class');
		localStorage.setItem('display', 'grid');
	}
}
$(document).ready(function(){
	bindGrid();
	if($('.header-top .img-responsive').attr('src') == 'https://mamalolashoes.com/img/logo2.svg' && !isHompage) {		
		$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');		
		$('.top-menu li a').css('color','black');
	} else {		
		if(isHompage){			
			$('.top-menu li a').css('color','white');	
			$('.right-nav').css('color','white');	
			$('.right-nav a').css('color','white');
				$('.sr-cart-black').show();
				$('.sr-cart-white').hide();		
		} else {			
			$('.top-menu li a').css('color','black');			
			$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');		
				$('.sr-cart-black').show();
				$('.sr-cart-white').hide();	
		}	
	} 	

	if($(window).width() <= 991 && $(window).width() >= 543) {
			$('.sr-filter').addClass('sr-margin-80')
	}	

	$(window).scroll(function() {		
		var scrollTop = $(window).scrollTop();		
		var srLogoUrl = $('.header-top .img-responsive').attr('src');	

		if($(window).width() >= 1025) {
			if(srLogoUrl != undefined) {
				if(scrollTop > 40){
					if (srLogoUrl.indexOf("logo1.svg") >= 0){				
						$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');				
						$('.header-top').addClass('sr-menu-fixed');				
						$('.top-menu li a').css('color','black');				
						$('.right-nav').addClass('sr-right-nav-fixed');	
						$('.right-nav').css('color','black');
						$('.right-nav a').css('color','black');
						$('#header .header-nav').css('z-index','999999');
					}		
					
					/*Tmp sr*/
					if($('#header').hasClass('sr-header-white')){
						$('#header .header-nav').css('height','0px');
						$('#header .header-nav').css('padding','0px 0px 0px');
					}

						$('.sr-cart-black').show();
						$('.sr-cart-white').hide();
						$('.sr-right-nav-fixed').css('z-index','999900');
				} else {
					if (srLogoUrl.indexOf("logo2.svg") >= 0 && isHompage){				
						$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');				
						$('.header-top').removeClass('sr-menu-fixed');				
						$('.right-nav').removeClass('sr-right-nav-fixed');				
						$('.top-menu li a').css('color','black');	
						$('.right-nav').css('color','black');
						$('.right-nav a').css('color','black');
						$('.sr-cart-black').show();
						$('.sr-cart-white').hide();	
						
						if($('#header').hasClass('sr-header-white')){
							$('#header .header-nav').css('height','0px');
							$('#header .header-nav').css('padding','0px 0px 0px');
						} else {
							$('#header .header-nav').css('height','');
							$('#header .header-nav').css('padding','');
						}
					} 		
				}	
			}else{
				$('#header .header-nav').css('z-index','1');
			}
		}

		/* fijar filtros categoria movil  */
		if($(window).width() <= 991) {
			var catbanner = $('#category .sr-banner-flex img').height();
			var cintillo = $('sr-cintillo').height();
			if(scrollTop >= (catbanner+cintillo+30)) {
				$('.sr-filter').addClass('sr-filter-fixed');
				$('.sr-filter').removeClass('sr-margin-80');
				
				
			} else {
				$('.sr-filter').removeClass('sr-filter-fixed');
				$('.sr-filter').addClass('sr-margin-80')
				
			}

			if(scrollTop > 0) { 
				$('#mobile_top_menu_wrapper').addClass('sr-menu-options-fixed');

			} else {
				$('#mobile_top_menu_wrapper').removeClass('sr-menu-options-fixed');
			}

		}

		
		
		/* end fijar filtros categoria */
	});
	
});


/* ------------ End Grid List JS --------------- */



/* select shoes size */
function printSelect() {
	$('.product-variants #group_4 option').each(function() {
		if($(this).attr('selected') == "selected") {
			var value = $(this).attr('value');
			$('.sr-shoes-size span').each(function() {
				$(this).removeClass('shoes-size-selected');
				if($(this).data('value') == value) {
					$(this).addClass('shoes-size-selected');
				}

			});
		}
		
	});
}

$(document).ready(function(){
	printSelect();

	$('.sr-shoes-size li').on('click', function() {

		var value = $(this).find('span').data('value');
			
		$('.sr-shoes-size span').each(function() {
			$(this).removeClass('shoes-size-selected');
			if($(this).data('value') == value) {
				$(this).addClass('shoes-size-selected');
			}

		});

	
		$('.product-variants #group_4 option').each(function() {
			$(this).prop('selected','');
			if($(this).attr('value') == value) {
				$(this).prop('selected', 'selected');	
				
			}

		});
		$('.product-variants #group_4').trigger('change');

	});

	$('.product-variants #group_4').change(function() {
		console.log("refresco");
	});

	$('#category #left-column .facet ul li').each(function() {
		var size = $(this).find('a').text().split('(')[0];
		$('#category .sr-category-tallas ul').each(function() {
			$(this).append('<li><span>'+size+'</span></li>');
		})
	})

	$('#category .sr-category-tallas ul li').click(function() {
		var size = $(this).find('span')
		var aux = 0;
		$('#category #left-column .facet ul li').each(function() {
			if($(this).find('a').text().split('(')[0] == size.text()) {
				aux=1;
				$(this).find('a').trigger('click');
			}
		})
		if($(this).find('span').hasClass('sr-shoe-size-selected')) {
			$(this).find('span').removeClass('sr-shoe-size-selected');
		} else {
			$(this).find('span').addClass('sr-shoe-size-selected');
		}
	})

	$('#category #js-product-list-top .dropdown-menu a').each(function() {
		if($(this).hasClass('num-4')) {
			$('#category .sr-sort ul').append('<li><span class="sr-asc" > Ascendente</span></li>');		
		} else if($(this).hasClass('num-5')) {
			$('#category .sr-sort ul').append('<li><span class="sr-desc" >Descendente /</span></li>');		
		}
	});

	$('#category .sr-sort li').click(function() {
		var aux = $(this).find('span').attr('class');

		if($(this).find('span').hasClass('sr-desc')) {
			$('.dropdown-menu .num-5').trigger('click');
		} else if($(this).find('span').hasClass('sr-asc')) {
			$(' .dropdown-menu .num-4').trigger('click');
		}

		$('#category .sr-sort li').each(function() {
			if($(this).find('span').hasClass(aux)) {
				$(this).find('span').addClass('sr-sort-select');
			} else {
				$(this).find('span').removeClass('sr-sort-select');
			}
		})
	});
	

	$('body').on('srModalClose',function(ev){
		setTimeout(printSelect, 1000 );
	});


	var title = $('#category .title-category').text();
	$('#category .sr-bottom-category span').text('#'+title);


	$('#product .right').click(function(){	
		$('#product .images-container #tt-jqzoom li').first().find('img').trigger('click');
	})

	$('#product .left').click(function(){	
		$('#product .images-container #tt-jqzoom li').first().find('img').trigger('click');
	})


	if($(window).width() <= 991) {
		var contact = $('#header .right-nav div').first().html();
		var account = $('#header #_mobile_user_info a').first().html();
		var hrefAccount = $('#header #_mobile_user_info a').attr('href');

		$('.header-top #top-menu').append('<li class="sr-mobile-contact sr-mobile">'+contact+'</li>');
		$('.header-top #top-menu').append('<li class="sr-mobile-account sr-mobile"><a href="'+hrefAccount+'">'+account+'</a></li>');
		
		var linkSocial = $('#footer #social-footer');
		$('#footer .sr-social-mobile').append(linkSocial);
		$('#footer .sr-social-mobile').append('<div style="clear:both"></div>');
	
		if(!isHompage){			
			$('.sr-cart-black').show();
			$('.sr-cart-white').hide();		
		}
	}	

	$('#product .category-products .left').click(function() {
		$('#product .category-products .customNavigation .prev').trigger('click');
	})
	$('#product .category-products .right').click(function() {
		$('#product .category-products .customNavigation .next').trigger('click');
	})
	
	//check cart
	//srCheckStock(false, null);
	
	/*$('.cart-summary .btn-primary').click(function(ev){
		srCheckStock(true, ev);
	});*/
});
/* end select shoes size*/


/*Sr function check-stock*/
function srCheckStock(srClick, ev){
	if($('#sr-hidden-block').size()>0){
		if(srClick){
			ev.preventDefault();
		}
		
		var param = [];
		$('#sr-hidden-block div').each(function(){
		   var pid = $(this).data('pid');
		   var attrid = $(this).data('pattr');
		   var qty = $(this).data('qty');
		   var partialResult = {"pid":pid,"attrid":attrid,"qty":qty};
		   param.push(partialResult);
		});
		
		var url = "https://mamalolashoes.com/savour/stock/check-stock.php";
		var paramText = JSON.stringify({"items": param});
		showBtn = true;
		$.ajax({url: url, 
				data: {"items": param},
				type: 'POST', 
				dataType: "json",
			    success: function (response) {
					$.each(response,function(k,v){
						$('.cart-item').each(function(k2,v2){
							if($(this).data('prodid')==v['id_product'] && $(this).data('attrid')==v['id_product_attribute'] && parseInt(v['qtyOrder']) > v['Disponible']){
								$(this).css('background-color','rgb(255, 192, 192)');
								$('.sr-warning-stock-cart').show();
								showBtn = false;
							}
						});
					});
					
					if(srClick){
						if(showBtn){
							//lanzar el siguiente paso
							window.location.href = $('.cart-summary .btn-primary').attr('href');
						} else {
							$('.sr-warning-stock-cart').show();
						}

					} else {
						if(showBtn){
							$('.cart-summary .btn-primary').show();
							$('.sr-warning-stock-cart').hide();
						} else {
							$('.cart-summary .btn-primary').hide();
							$('.sr-warning-stock-cart').show();
						}
					}
			   }
		});
	}
}
/*End Sr function check-stock*/

/*Cookies and facebook events*/

$(document).ready(function(){
	$('#cart .cart-grid-right .cart-detailed-actions .btn-primary').click(function(){
		fbq('track', 'InitiateCheckout');
	});
	$('#checkout .js-address-form .btn-primary').click(function(){
		fbq('track', 'AddPaymentInfo');
	});
	
	checkCookie();
    $('#btn-close-cookie').click(function(ev){
      document.getElementById('cookieBlock').style.display = 'none';
    });
    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function checkCookie() {
      var user = getCookie("userCookie");
      if (user == "") {
        document.getElementById('cookieBlock').style.display = 'block';
        setCookie("userCookie", 'true', 365);
      }
    }
	
	/*Triger */
	$('.ttfeatured-products .right').click(function(){
		$('.ttfeatured-products .customNavigation .ttfeature_next').trigger('click');
	});
	$('.ttfeatured-products .left').click(function(){
		$('.ttfeatured-products .customNavigation .ttfeature_prev').trigger('click');
	});
	
	$('.sr-title-category li:first span').append(' /');
	
	/*if($(window).width() > 991 && isHompage) {
		$('.header-nav').on('mouseenter','*',function(){
			if($(window).scrollTop() <= 39){
				$('.header-top').css('top','24px');
				$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');			
				$('.right-nav').css('margin-top','29px');		
				$('.header-top').addClass('sr-menu-fixed');				
				$('.top-menu li a').css('color','black');				
				$('.right-nav').addClass('sr-right-nav-fixed');	
				$('.right-nav').css('color','black');
				$('.right-nav a').css('color','black');
				$('.right-nav').css('z-index','900000');
				$('.sr-cart-black').show();
				$('.sr-cart-white').hide();	
			}
		});
		
		$('#carousel').mouseenter(function(){
			if($(window).scrollTop() <= 39){
				$('.header-top').css('top','0px');
				$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo1.svg');			
				$('.right-nav').css('margin-top','0px');		
				$('.header-top').removeClass('sr-menu-fixed');				
				$('.top-menu li a').css('color','white');				
				$('.right-nav').removeClass('sr-right-nav-fixed');	
				$('.right-nav').css('color','white');
				$('.right-nav a').css('color','white');
				$('.right-nav').css('z-index','900000');
				$('.sr-cart-black').hide();
				$('.sr-cart-white').show();	
			}
		});
	}*/
	
	if($(window).width() > 991 && isHompage) {
		$('.header-nav, .top-menu').on('mouseenter','*',function(){
			if($(window).scrollTop() <= 39 && !$('#header').hasClass('sr-header-white')){
				$('#header').addClass('sr-header-white');
				$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');	
				$('.sr-cart-black').show();
				$('.sr-cart-white').hide();
				$('#header .header-nav').css('z-index','1');
			}
		});
		
		$('#carousel').mouseenter(function(){
			if($(window).scrollTop() <= 39){
				$('#header').removeClass('sr-header-white');
				$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');
				$('.sr-cart-black').show();
				$('.sr-cart-white').hide();	
				/*$('#header .header-nav').css('height','');
				$('#header .header-nav').css('padding','');*/
			}
		});
	}
	
	/*Load fancybox script*/
	/*var srFancyboxScript = "https://mamalolashoes.com/js/jquery/plugins/fancybox/jquery.fancybox.js";
	$.getScript(srFancyboxScript, function() {
		$('.sr-guia-tallas a').click(function(ev){
			var srSizeUrl = $(this).attr('href').trim();

			ev.preventDefault();
			$.get(srSizeUrl,function(data){
				var srContentSize = $(data).find('#main').html();
				$.fancybox({'content':srContentSize,'width':400,'autosize':false});
			});
			
		});
	});*/
	
	$('#img-sr-arrow').click(function(ev){
		$(this).closest('form').submit();
	});
	console.log('Savour');

	/*var url = window.location.href;
	if(jQuery('.container-lookbook') && url.indexOf('15-campaigns')>0){
		jQuery('.page-header').remove();
		jQuery('.page-content.page-cms').css('border','0px !important');
		jQuery('#wrapper > .container').css('max-width',window.outerWidth);
		jQuery('.page-content.page-cms').css('border','0px !important');
		var urlFrame = '<iframe src="http://mamalolashoes.com/savour/lookbook/index.html" style="width:100%;height:600px;border:0px !important;margin-top: -117px;"></iframe>';
		if(window.outerWidth<=1000)
			urlFrame = '<iframe src="http://mamalolashoes.com/savour/lookbook/index.html?versionMovil=S" style="width:100%;height:319px;border:0px !important;margin-top: -117px;"></iframe>';
		jQuery('#content').html(urlFrame);
	}*/
	
	//tmp banner newcollection sprinng summer
		$('.sr-cart-white').hide();	
	$('.sr-cart-black').show();
	$('.header-top .img-responsive').attr('src','https://mamalolashoes.com/img/logo2.svg');				
	$('.top-menu li a').css('color','black');				
	$('.right-nav').addClass('sr-right-nav-fixed');	
	$('.right-nav').css('color','black');
	$('.right-nav a').css('color','black');

});

$(window).resize(function(){
	if($(window).width()<=992){
		$('.sr-cart-black').attr('src','https://mamalolashoes.com/img/cesta-blanca.svg');
	}
});

function resizeModal(){
	if($(window).height() < 715){
		$('.fancybox-inner').css('height','461px !important');
	}
}