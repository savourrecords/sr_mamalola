{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="_desktop_user_info">
{if $logged}
<a href="{$my_account_url}">{$customerName}</a>	
	<div class="user-info">
		<li>
			<a class="logout hidden-sm-down" href="{$logout_url}" rel="nofollow">
				<i class="material-icons user">&#xE7FF;</i>
				<span class="hidden-sm-down">{l s='Sign out' d='Shop.Theme.Actions'}</span>
			</a>
		</li>
	</div>
</div>
    {else}
<a href="{$my_account_url}">{l s="Mi cuenta"}</a>	
  <div class="user-info">
  </div>
</div>
    {/if}

