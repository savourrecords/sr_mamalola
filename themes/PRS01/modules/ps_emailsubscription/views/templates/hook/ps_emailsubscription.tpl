{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="newslatter" class="col-sm-12">
<div class="block_newsletter">
  <div class="row">
	<div class="tt-content col-sm-6">
    <h4 class="tt-title sr-nl-title">{l s='Suscribete a nuestra newsletter para ser la primera en recibir nuestras últimas noticias, las promociones más exclusivas y cositas divertidas' d='Shop.Theme.Global'}</h4>
	</div>

    <div class="block_content col-sm-5">
      <form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&id=c0482f22a7" method="post">
        <div class="row">
		          <div class="ttinput_newsletter">
		  	<!--i class="material-icons">&#xE163;</i-->
            <input style="display:none;"
              class="btn btn-primary sr-btn-newsletter float-xs-right hidden-xs-down"
              name="submitNewsletter"
              type="submit"
              value=">"
            >
            <input
              class="btn btn-primary float-xs-right hidden-sm-up"
              name="submitNewsletter"
              type="submit"
              value="{l s='OK' d='Shop.Theme.Actions'}"
            >
			  <div class="input-wrapper" style="border-bottom:1px solid white;">
              <input style="border-bottom:0px;"
                name="EMAIL"
                type="text"
                value="{$value}"
                placeholder="{l s='Escribe tu mail' d='Shop.Forms.Labels'}"
                aria-labelledby="block-newsletter-label"
              >
			  <img id="img-sr-arrow" style="float:right; margin-top: 5px; z-index: 999; width: 5%; color: white;cursor:pointer;" width="30px;" src="https://mamalolashoes.com/img/sr-img/PB_ICON_ARROW.svg" />
			</div>
            <input type="hidden" name="action" value="0">
            <div class="clearfix"></div>
          </div>
          <div class="col-xs-12">
              {if $conditions}
                <p class="newsletter-desc">{$conditions}</p>
              {/if}
              {if $msg}
                <p class="alert {if $nw_error}alert-danger{else}alert-success{/if}">
                  {$msg}
                </p>
              {/if}
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>