{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}
{if $page.page_name == 'index'}<div class="sr-orange-category-block">

	<!-- New Banner mobile-->
	<a class="sr-mobile" href="https://mamalolashoes.com/20-new-collection"><img class="sr-mobile" width="100%" src="{$base_dir_ssl}img/sr-img/banner/BENNER_WEB_MOVIL_FW.jpg" /></a>
	
	<a  class="sr-orange-category-block-item sr-orange-category-block-item-left" href="https://mamalolashoes.com/18-botines">		{l s="#Botines"}<br/><br/>		<span class="sr-btn-white">{l s="Ver todas"}</span>	</a>		<a class="sr-orange-category-block-item sr-orange-category-block-item-right" href="https://mamalolashoes.com/13-sneakers">		{l s="#Sneakers"}<br/><br/>		<span class="sr-btn-white">{l s="Ver todas"}</span>	</a></div>{/if}
  <section id="main">

    {block name='page_header_container'}
      {block name='page_title' hide}
        <header class="page-header">
          <h1 class="tt-innerpagetitle">{$smarty.block.child}</h1>
        </header>
      {/block}
    {/block}

    {block name='page_content_container'}
      <section id="content" class="page-content card card-block">
        {block name='page_content_top'}{/block}
        {block name='page_content'}
          <!-- Page content -->
        {/block}
      </section>
    {/block}
	{if $page.page_name == 'index'}	<div style="display:none;" class="sr-block-home-image-bottom">		<img src="https://mamalolashoes.com/img/img-bottom-left.jpg" class="sr-block-home-image-bottom-left sr-desktop" />		<img src="https://mamalolashoes.com/img/img-bottom-right.jpg" class="sr-block-home-image-bottom-right sr-desktop" />	
    <div class="sr-block-home-image-bottom-top sr-mobile"><img src="https://mamalolashoes.com/img/sr-img/M_BANNERLOOKBOOK_MOBILE1.jpg" /> </div>

      <img src="https://mamalolashoes.com/img/sr-img/M_BANNERLOOKBOOK_MOBILE2.jpg" class="sr-block-home-image-bottom-bottom sr-mobile" />
      

    <div style="clear: both;"></div>
  </div>	{/if}
  <div style="clear: both;"></div>

  {if $page.page_name == 'index'}
    <p style="display:none;" class="sr-link-home-bottom">{l s="Lookbook Primavera / Verano 18 "}<a href="https://mamalolashoes.com/content/15-campaigns">{l s="Ver Lookbook >"}</a></p>
  {/if}


    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}

  </section>

{/block}
