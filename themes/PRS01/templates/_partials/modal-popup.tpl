
        <script src="{$base_dir}/savour/jquery.colorbox.js"></script>
        <script src="{$base_dir}/savour/modal-popup.js"></script>
        <link rel="stylesheet" type="text/css" href="{$base_dir}/savour/modal-popup.css" />

        <!-- Trigger/Open The Modal -->
		<button id="btn_LaunchNewsletterModal" style="display:none;">Open Modal</button>
		<!-- The Modal -->
		<div id="LaunchNewsletterModal" class="modal">
		<!-- Modal content -->
		<div class="modal-content">
			<span class="close">&times;</span>
			<div class="_modal_CONTENT _modal_HEADER _modal_BORDER_RADIUS_TOP" style="padding-top:40px !important;">SUSCRÍBETE A LA <strong>NEWSLETTER</strong>,</div>
			<div class="_modal_CONTENT _modal_HEADER">DISFRUTA DE TU <span class="_modal_BACK_TEXT">10% DTO</span></div>
			<div class="_modal_CONTENT _modal_HEADER" style="padding-bottom:20px !important;">Y SÉ LO MÁS.</div>
			<div id="_modal_FORM" class="_modal_CONTENT">
				<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
					<input type="text" name="NAME" class="_modal_FORM_INPUT" placeholder="Nombre">
					<input type="text" name="EMAIL" class="_modal_FORM_INPUT" placeholder="Correo electrónico"><br>
					<input class="_modal_CHEKBOX_GDPR" type="checkbox" name="GDPR" id="GDPR"> <span class="_modal_TEXTO_GDPR">Acepto los <a class="_modal_LEGAL" href="http://mamalolashoes.com/content/3-terminos-y-condiciones-de-uso" target="_blank">términos legales</a> y <a class="_modal_LEGAL" href="http://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">política de privacidad</a>.</span><br><br>
					<input type="submit" class="_modal_FORM_SUBMIT" value="SER #MAMALOLAGIRL >">
				</form>
			</div>
			<div id="_modal_LEGAL" class="_modal_CONTENT _modal_LEGAL _modal_BORDER_RADIUS_BOTTOM" style="padding-bottom:40px !important;">
				<a href="http://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">Más información sobre política de privacidad</a>
			</div>
		</div>
		</div>