{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 <div class="footer-top">
<div class="container">
  <div class="row">
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
  </div>
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="row">

      <div class="sr-social-mobile sr-mobile"></div>
      {block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
  </div>
</div>
<div class="bottom-footer">
	<div class="container">    
	 <div class="row">
      <div class="col-md-12">
        <p class="text-sm-center">
          {block name='copyright_link'}
              {l s='%copyright% %year% - MAMALOLA' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}
          {/block}
        </p>
      </div>
	  </div>
	  </div>
</div>
<div class="backtotop-img">
	<a href="#" class="goToTop show" title="Back to top"> <i class="material-icons arrow-up"></i></a>
	</div>
<a class="goToTop ttbox-img show" href="#"> </a>

<div id="cookieBlock" class="my_cookies" style="display:none">
  <div class="sr-alert sr-alert-info sr-alert-dismissible" >
	<p class="coockies-text">{l s="Utilizamos cookies propias y de terceros para mejorar su experiencia de usuario. Si continua navegando consideramos que acepta su uso."}<a href="https://mamalolashoes.com/content/10-politica-de-cookies"> {l s="Leer más"}</a> <span id="btn-close-cookie">{l s="OK"}</span></p>
  </div>
</div>

<!--TMP MODAL POPUP-->
<script src="{$base_dir_ssl}/savour/jquery.colorbox.js"></script>
<script src="{$base_dir_ssl}/savour/modal-popup.js"></script>
<link rel="stylesheet" type="text/css" href="{$base_dir_ssl}/savour/modal-popup.css?a={1|rand:1000}" />

<!-- Trigger/Open The Modal -->
<div id="modal-movil" style="display:none;">
		<button id="btn_LaunchNewsletterModal" style="display:none;">Open Modal</button>
		<!-- The Modal -->
		<div id="LaunchNewsletterModal" class="modal">
		<!-- Layer Rayos -->
		<!--<div class="LayerRayos"><img src="https://mamalolashoes.com/savour/GIF_POP-UP.gif"></div>-->
		<div class="LayerFormTop _modal_BORDER_RADIUS_BOTTOM" style="top:62% !important;">
			<div id="_modal_FORM" class="_modal_CONTENT">
				<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
					<input type="text" name="NAME" class="_modal_FORM_INPUT" placeholder="Nombre">
					<input type="text" name="EMAIL" class="_modal_FORM_INPUT" placeholder="Correo electrónico"><br>
					<input class="_modal_CHEKBOX_GDPR" type="checkbox" name="GDPR" id="GDPR"> <span class="_modal_TEXTO_GDPR"> <input type="checkbox" required style="width:12px;height:12px;opacity:1;display:none;" /> He leído y acepto las <a href="https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui" target="_blank" style="color:#000 !important; font-weight:bold;">bases legales del sorteo</a> y la <a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank" style="color:#000 !important; font-weight:bold;">política de privacidad</a> de Mamalola..</span><br><br>
					<input type="submit" class="_modal_FORM_SUBMIT" value="APUNTARME >">
					<div class="_modal_FORM_SUBMIT_MOBILE">></div>
				</form>
			</div>
			<div id="_modal_LEGAL" class="_modal_CONTENT _modal_LEGAL _modal_BORDER_RADIUS_BOTTOM" style="padding-bottom:40px !important;display:none;">
				<a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">Más información sobre política de privacidad</a>
			</div>
		</div>
		<!-- Modal content -->
		<div class="modal-content moda-popup popup-background-mobile">
			<span class="close">&times;</span>
			<br/>
			<div class="_modal_CONTENT _modal_HEADER _modal_BORDER_RADIUS_TOP" style="margin-bottom:5%; font-size:1.4em !important;">¿Unas Mamalola por la face? </div><br/>
			<!--div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;"><strong>¿Unas Mamalola por la face? </strong></div-->
			<div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase; line-height:1em !important; margin-top:-30px;">Suscríbete a nuestra newsletter, participa y gana nuestras Bugui.</div>
			<div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;">¡En el color que tú elijas!</div>
			<!--div class="_modal_CONTENT _modal_HEADER" style="text-transform:uppercase;">{l s="modelo más top"}.</div-->
			<div class="_modal_RESERVED_CONTENT"></div>
		</div>
		</div>
</div>
</div>

<!--<div style="display:none;">
	<a href="#popup-nl" class="nl-fancybox" >link</a>
	<table id="popup-nl" class="table-newsletter-popup">
		<tr>
			<td class="nl-img-left content-table-newsletter-popup">
				<h3>¿Quieres unas Mamalola<br>por la face?</h3>
				<p class="title-table-newsletter-popup">
					Suscríbete a nuestra <strong>Newsletter</stong>,<br>
					participa y gana nuestras Bugui.<br>
					¡En el color que tú elijas!
				</p>
				<div class="NW_FRM_Modal">	
					<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post">
						<input type="text" name="NAME" placeholder="Nombre" class="input-name-content-newsletter-popup"><br>
						<input type="text" name="EMAIL" placeholder="Correo electrónico" class="input-email-content-newsletter-popup"><br>
						<input type="submit" value="No me lo pierdo >" class="input-submit-content-newsletter-popup">
					</form>
					<br><br>
					<p class="link-content-newsletter-popup">
						He leído y acepto las <a href="https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui" target="_blank">bases legales del sorteo</a> y la <a href="https://mamalolashoes.com/content/7-politica-de-privacidad" target="_blank">política de privacidad</a> de Mamalola.
					</p>
				</div>
			</td>
		</tr>
	</table>
</div>-->

<div style="display:none;">
	<a href="#popup-nl" class="nl-fancybox" >link</a>
	
	<table id="popup-nl">
		<tr>
			<td style="width:2%;"></td>
			<td style="width:39%;text-align:center;background:#FFFFFF;-webkit-background-size: cover;background-size: cover; color:#000 !important;">
				<!--p class="nl-title" style="color:black;text-transform:uppercase;margin-top:0%; font-size:2.2em; letter-spacing:5px; padding: 0 40px;margin-bottom:40px;"><strong>10% dto.</strong></p-->
				<h1 style="margin-top:0px; text-align: left; font-size: 27px;">¿Quieres unas Mamalola<br>por la face?</h1>
				<p style="color:#000; font-size:21px; line-height:1.5em;">
					Suscríbete a nuestra newsletter,<br>participa y gana nuestras Bugui.<br>
					¡En el color que tú elijas!
				</p>
				<form action="https://mamalolashoes.us18.list-manage.com/subscribe/post?u=04260b23cdb8865c0e9aec462&amp;id=c0482f22a7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
					<div id="mc_embed_signup_scroll">
					<img src="https://mamalolashoes.com/img/ICONO_SOBRE.png" width="42px" style="display:none;" class="nl-img-left">
					<div class="mc-field-group" style="margin-top: 10px;">
						<input type="text" required value="" name="FNAME" class="required email" id="mce-FNAME" placeholder='{l s="Nombre"}' style="border-bottom:1px solid #000; width:70%;">
					</div>
					<div class="indicates-required hidden" style="display:none;"><span class="asterisk">*</span> {l s="indicates required"}</div>

						<input type="email" placeholder='{l s="Correo electrónico"}' value="" name="EMAIL" class="required email" id="mce-EMAIL" style="border-bottom:1px solid #000;margin-bottom:1px; width:70%; margin-bottom:4% !important;">
					</div>
					<div class="mc-field-group hidden" style="display:none;">
						<label for="mce-LNAME">Last Name </label>
						<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div> 
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9f55b42ec392a5921c498348a_815046a7f3" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value='{l s="No me lo pierdo >"}' name="subscribe" id="mc-embedded-subscribe" class="button" style="background-color: #000000 !important; color:#FFF !important; width: 70%; padding: 14px !important; margin-bottom:9%; margin-top:20px;"></div>
					<p style="font-size:9px;color:#000;margin-bottom:0px;width:98%; margin-left:2%; line-height:0px; color:#000 !important; text-align:left;"><input type="checkbox" required style="font-weight:bold; width:12px;height:12px;opacity:1;" /> He leído y acepto <a target="_blank" style="font-weight:bold; color:#000 !important; text-decoration:underline;" href='https://mamalolashoes.com/content/18-bases-legales-sorteazo-bugui'>bases legales del sorteo</a> {l s='y la'} <a target="_blank" style="font-weight:bold; color:#000 !important; text-decoration:underline;" href='https://mamalolashoes.com/content/7-politica-de-privacidad'>{l s='política de privacidad'}</a> {l s='de MAMALOLA'}.</p>
				</form>
			</td>
			<td style="width:59%; text-align:right !important;;" class="nl-img-left">
				<img src="https://mamalolashoes.com/img/sr-img/banner3/COMPLETO.png" style="width:100% !important;" id="fotoDisplay"/>
			</td>	
		</tr>
	</table>
</div>

{literal}
<script src="./savour/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117775943-1"></script>-->
<script>
  /*window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117775943-1');*/


	function ShowNewsletterModal(){
		jQuery('body').css('overflow','hidden');
		jQuery('#btn_LaunchNewsletterModal').click();
		setCookie('showNewsletterPopup', 'true', 1);
	}

  function showNewsletterPopup(){
		setCookie('showNewsletterPopup', 'true', 1);
		$(".nl-fancybox").fancybox({  maxWidth  : 823,
		   maxHeight : 449,
		   fitToView : false,
		   width     : '100%',
		   height    : '70%',
		   afterShow: function() {
			  var imagen = $('#fotoDisplay');
			  var maxHeightLong = 823;
			  var maxWidthLong = 449;
			  var ratio = 0;
			  var width = imagen.width();
			  var height = imagen.height();
	 
				if(width > maxWidthLong){
					ratio = maxWidthLong / width;   // get ratio for scaling image
					$('#fotoDisplay').css("width", maxWidthLong); // Set new width
					$('#fotoDisplay').css("height", height * ratio);  // Scale height based on ratio
					height = height * ratio;    // Reset height to match scaled image
				}

				var width = $(this).width();    // Current image width
				var height = $(this).height();  // Current image height

			 if(height > maxHeightLong){
				ratio = maxHeightLong / height; // get ratio for scaling image
				$('#fotoDisplay').css("height", maxHeightLong);   // Set new height
				$('#fotoDisplay').css("width", width * ratio);    // Scale width based on ratio
				width = width * ratio;				
			 }
			resizeModal();
		  },		  
			autoSize  : false
		}).trigger('click');
	}
	
	function checkCookieNewsletter() {
		var lastShow = getCookie("showNewsletterPopup");
		var registredNewsletter = getCookie("registredNewsletter");

		if (lastShow == undefined && registredNewsletter == undefined) {
			
			setTimeout(function(){ 
				if(window.outerWidth>991){
					showNewsletterPopup()
				}else{
					jQuery('#modal-movil').css('display','block');
					ShowNewsletterModal();
				}
			}, 4000);
		}
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	} 
	
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
	}

	jQuery(document).ready(function(){
		checkCookieNewsletter();

		var dominio = window.location.href;
		var urlBlog = dominio.substring(0,dominio.indexOf(".com/")+4) + "/blog";
		jQuery('#footer_sub_menu_13236').append('<li style="display:none;"><a href="' + urlBlog + '">Magazine</a></li>');

		var urlNow = window.location.href;
		/*if(urlNow == 'https://mamalolashoes.com/')
			loadBouncingImages(numeroAleatorio(1,5));*/

		//var LinkRebajas = '<li class="category menuRebajas" id="category-16"><a class="dropdown-item enlaceRebajas" href="https://mamalolashoes.com/16-rebajas" data-depth="0">#REBAJAS &#39;18</a></li>';
		//jQuery('.top-menu').append(LinkRebajas);

	});

</script>

<style>
  .fancybox-overlay {
      z-index: 1000000 !important;
  }
</style>

<!--JS para lookboo-->
<script language="javascript" src="https://mamalolashoes.com/savour/jquery.slides.min.js"></script>

<!-- Google Code para etiquetas de remarketing -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información personal identificable o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte https://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 804972302;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/804972302/?guid=ON&amp;script=0"/>
</div>
</noscript>

{/literal}


<div id="field-required" class="DIV_contentModal">
	<div class="DIV_textRequired">
		Debe aceptar los términos y condiciones para continuar con el proceso.<br>
		<button class="btn-aceptar-modal btn btn-primary center-block">Aceptar</buton><br>
	</div>
</div>