{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}
  <section id="main">

    {block name='product_list_header'}
      <h2 class="h2 tt-innerpagetitle">{$listing.label}</h2>
    {/block}

    <section id="products">
      {if $listing.products|count}

        <div id="">
          {block name='product_list_top'}
            {include file='catalog/_partials/products-top.tpl' listing=$listing}
          {/block}
        </div>

        {block name='product_list_active_filters'}
          <div id="" class="hidden-sm-down">
            {$listing.rendered_active_filters nofilter}
          </div>
        {/block}

        <div id="">
          {block name='product_list'}
            {include file='catalog/_partials/products.tpl' listing=$listing}
          {/block}
        </div>





        <div id="js-product-list-bottom">
          {block name='product_list_bottom'}
            {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
          {/block}
        </div>



<!-- tmp bug -->
        <div class="sr-bottom-category-show">
          <span>
            {if $category.name == 'Sandalias'}
              {l s="#Sneakers"}
               </span>
              <br/>
              <a href="http://mamalolashoes.com/13-sneakers">{l s="Ver colección"}  ></a>
            {elseif $category.name == 'New collection'}
			  {l s="#Outlet"}
               </span>
              <br/>
              <a href="http://mamalolashoes.com/19-outlet">{l s="Ver colección"}  ></a>
            {elseif $category.name == 'Outlet'}
			  {l s="#New collection"}
               </span>
              <br/>
              <a href="https://mamalolashoes.com/20-new-collection">{l s="Ver colección"}  ></a>
			{elseif $category.name == 'Botines'}
			  {l s="#Sneakers"}
               </span>
              <br/>
              <a href="http://mamalolashoes.com/13-sneakers">{l s="Ver colección"}  ></a>
			{else}
              {l s="#Botines"}
              </span>
              <br/>
              <a href="http://mamalolashoes.com/18-botines">{l s="Ver colección"}  ></a>
            {/if}
        </div>

        <div class="sr-bottom-category">
          <span></span>
          <br/>
          <a>{l s="Ver todas las "} {$category.name} ></a>
        </div>
<!-- end tmp bug -->

      {else}

        {include file='errors/not-found.tpl'}

      {/if}
    </section>

  </section>
{/block}
