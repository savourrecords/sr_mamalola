<?php

$this->_html .= "!function(o){o.fn.showModalDialog=function(n){var i=o.extend({},o.fn.showModalDialog.defaults,n),t=o(\"<iframe />\");"
        . "t.attr({src:i.url,scrolling:i.scrolling}),t.css({padding:0,margin:0,\"padding-bottom\":10});"
        . "var l=t.dialog({autoOpen:!0,modal:!0,width:i.width,height:i.height,resizable:i.resizable,position:i.position,overlay:{opacity:.5,background:\"black\"},close:function(){i.returnValue=t[0].contentWindow.window.returnValue,i.onClose()},resizeStop:function(){t.css(\"width\",\"100%\")}});"
        . "return t.css(\"width\",\"100%\"),t[0].contentWindow.window.dialogArguments=i.dialogArguments,t[0].contentWindow.window.close=function(){l.dialog(\"close\")},t.load(function(){if(l){var n=o(this).contents().find(\"title\").html();"
        . "n.length>50&&(n=n.substring(0,50)+\"...\"),l.dialog(\"option\",\"title\",n)}}),"
        . "null},o.fn.showModalDialog.defaults={url:null,dialogArguments:null,"
        . "height:\"auto\",width:\"auto\",position:\"center\","
        . "resizable:!0,scrolling:\"yes\",onClose:function(){},returnValue:null}}(jQuery),"
        . "jQuery.showModalDialog=function(o){\$().showModalDialog(o)};";

?>