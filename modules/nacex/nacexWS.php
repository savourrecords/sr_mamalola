<?php

/*
 * mexpositop 2017
 */
//session_start();

include_once("nacexDTO.php");
include_once("nacexDAO.php");
include_once("AdminConfig.php");
include_once("nacexutils.php");
//include_once("nacexView");
//include_once("nacextabMasivo");

class nacexWS {

    public static function ws_getEstadoExpedicion($expe_codigo, $errorplain = false) {
        $nacexWS = new nacexWS();
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI ws_getEstadoExpedicion :: expe_codigo: " . $expe_codigo);

        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";
        $getEstadoExpedicionResponse = array();

        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');
        $metodo = "getEstadoExpedicion";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
			<soapenv:Header/>
			<soapenv:Body>
			      <typ:getEstadoExpedicion>
					<String_1>' . $nacexWSusername . '</String_1>
					<String_2>' . $nacexWSpassword . '</String_2>
			         	<String_3>' . $expe_codigo . '</String_3>
			         	<String_4></String_4>
			         	<String_5></String_5>
			         	<String_6></String_6>
			      </typ:getEstadoExpedicion>
			</soapenv:Body>
		</soapenv:Envelope>';

        nacexutils::writeNacexLog("ws_getEstadoExpedicion :: XML: " . $XML);

        $postResult = $nacexWS->requestWS($URL, $XML, $metodo);
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $result = $nacexWS->treatmentXML($postResult, $metodo);
        if ($result[0] == "ERROR") {
            nacexutils::writeNacexLog("getEstadoExpedicion :: error al obtener estado la expedicion: " . $result[1]);
            return $result[1];
        } else   if ($result[4] == "ANULADA") {
            nacexutils::writeNacexLog("getEstadoExpedicion :: error al obtener estado la expedicion: " . $result[1]);
            return $result[1];
        } else{
            $getEstadoExpedicionResponse["expe_codigo"] = $result[0];
            $getEstadoExpedicionResponse["fecha"] = $result[1];
            $getEstadoExpedicionResponse["hora"] = $result[2];
            $getEstadoExpedicionResponse["observaciones"] = $result[3];
            $getEstadoExpedicionResponse["estado"] = $result[4];
            $getEstadoExpedicionResponse["estado_code"] = $result[5];
            $getEstadoExpedicionResponse["origen"] = $result[6];
            $getEstadoExpedicionResponse["albaran"] = $result[7];
            $getEstadoExpedicionResponse["exps_rels "] = $result[8];
            nacexutils::writeNacexLog("getEstadoExpedicion :: expe_codigo:" . $result[0] . "|fecha:" . $result[1] . "|hora:" . $result[2] . "|observaciones:" . $result[3] . "|estado:" . $result[4] . "|estado_code:" . $result[5] . "|origen:" . $result[6] . "|albaran:" . $result[7] . "|exps_rels:" . $result[8]);
        }

        nacexutils::writeNacexLog("FIN ws_getEstadoExpedicion :: expe_codigo: " . $expe_codigo);
        nacexutils::writeNacexLog("----");

        return $getEstadoExpedicionResponse;
    }

    public static function ws_getDatosWSExpedicion($expe_codigo, $errorplain = false) {

        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI ws_getDatosWSExpedicion :: expe_codigo: " . $expe_codigo);
        $nacexWS = new nacexWS();

        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";
        $getDatosWSExpedicion = array();

        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');

        $metodo = "getInfoEnvio";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
			<soapenv:Header/>
			<soapenv:Body>
			      <typ:getInfoEnvio>
					<String_1>' . $nacexWSusername . '</String_1>
					<String_2>' . $nacexWSpassword . '</String_2>
			        <String_3>E</String_3>
			        <String_4>' . $expe_codigo . '</String_4>
			        <String_5></String_5>
			        <String_6></String_6>
			      </typ:getInfoEnvio>
			</soapenv:Body>
		</soapenv:Envelope>';

        nacexutils::writeNacexLog("ws_getDatosWSExpedicion :: XML: " . $XML);

        $postResult = $nacexWS->requestWS($URL, $XML, $metodo, "width:auto;", $errorplain);
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $resultado= $nacexWS->treatmentXML($postResult, $metodo);
        if ($resultado[0] == "ERROR") {
            nacexutils::writeNacexLog("ws_getDatosWSExpedicion :: error al obtener estado la expedicion: " . $resultado[1]);
            return $resultado[1];
        } else {
            $getDatosWSExpedicion["expe_cod"] = $resultado[0];
            $getDatosWSExpedicion["ag"] = $resultado[1];
            $getDatosWSExpedicion["num"] = $resultado[2];
            $getDatosWSExpedicion["serv_cod"] = $resultado[7];
            $array_refs = explode(";", $resultado[14]);
            $getDatosWSExpedicion["ref"] = $array_refs[0];
            nacexutils::writeNacexLog("ws_getDatosWSExpedicion :: expe_cod:" . $resultado[0] . "|ag:" . $resultado[1] . "|num:" . $resultado[2] . "|serv_cod:" . $resultado[7] . "|ref:" . $array_refs[0]);
        }

        nacexutils::writeNacexLog("FIN ws_getDatosWSExpedicion :: expe_codigo: " . $expe_codigo);
        nacexutils::writeNacexLog("----");
        return $getDatosWSExpedicion;
    }

    /*
      public static function errorComunicacionWS($metodo, $ch, $style="text-align:left;width:396px;", $returnString=false, $errorplain=false){
      //$info = curl_getinfo($ch);
      if($errorplain){
      return	'[Error cURL ' . curl_errno($ch) . ']: ' . curl_error($ch);
      }
      $str = '<br>
      <div style="' . $style . '" class="alert error ncx_network_error">
      <h1>Error de comunicación con el Web Service</h1>
      <div class="metodo" align="center">' . $metodo . '</div>
      <div class="descripcion">[Error cURL ' . curl_errno($ch) . ']: ' . curl_error($ch) . '</div>
      </div>';
      nacexutils::writeNacexLog($metodo." :: [Error cURL ".curl_errno($ch)." ]: ".curl_error($ch));
      if($returnString){
      return $str;
      }else{
      echo $str;
      }
      }
     */

    public static function ws_getValoracion($cp_ent, $tip_ser, $kil) {

        $nacexWS = new nacexWS();
        $cp_rec = Configuration::get('NACEX_CP_REC');
        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";

        //$getValoracionResponse = array();
        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = strtoupper(md5(Configuration::get('NACEX_WSPASSWORD_ORIGINAL')));


        $tip_env = Configuration::get('NACEX_TIP_ENV');
        $tip_env_int = Configuration::get('NACEX_TIP_ENV_INT');

        $ag_cli = Configuration::get('NACEX_AGCLI');


        $array_agclis = explode(",", $ag_cli);
        //Cogemos la primera pareja
        $firstagcli = trim($array_agclis[0]);
        $arr_ag_cli = explode("/", $firstagcli);

        $del_cli = trim($arr_ag_cli[0]);
        $num_cli = trim($arr_ag_cli[1]);

        //El motor no esta preparado para valorar pesos < 1Kg, si el peso es menor a 1Kg lo modificamos.
        if (floatval($kil) < 1) {
            $kil = 1;
        }

        $metodo = "getValoracion";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
			<soapenv:Header/>
			<soapenv:Body>
			      <typ:getValoracion>
					 <String_1>' . $nacexWSusername . '</String_1>
					 <String_2>' . $nacexWSpassword . '</String_2>
					 <arrayOfString_3>cp_rec=' . $cp_rec . '</arrayOfString_3>
					 <arrayOfString_3>cp_ent=' . $cp_ent . '</arrayOfString_3>
					 <arrayOfString_3>tip_ser=' . $tip_ser . '</arrayOfString_3>         
					 <arrayOfString_3>tip_env=' . $tip_env . '</arrayOfString_3>         
					 <arrayOfString_3>kil=' . $kil . '</arrayOfString_3>         					    
					 <arrayOfString_3>del_cli=' . $del_cli . '</arrayOfString_3>
					 <arrayOfString_3>num_cli=' . $num_cli . '</arrayOfString_3>
			      </typ:getValoracion>
			</soapenv:Body>
		</soapenv:Envelope>';

        $postResult = $nacexWS->requestWS($URL, $XML, $metodo, $style = "width:auto;");
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $resultado =  $nacexWS->treatmentXML($postResult, $metodo);

        return $resultado;
    }

    public static function cancelExpedicion($id_pedido, $cod_exp) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI cancelExpedicion :: id_pedido: " . $id_pedido . " |cod_exp: " . $cod_exp);

        $nacexDTO = new nacexDTO();
        $nacexWS = new nacexWS();
                
        $id_order = $id_pedido;

        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";

        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');

        $metodo = "cancelExpedicion";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
					<soapenv:Header/>
					<soapenv:Body>
						<typ:cancelExpedicion>
							<String_1>' . $nacexWSusername . '</String_1>
							<String_2>' . $nacexWSpassword . '</String_2>
							<arrayOfString_3>' . $cod_exp . '</arrayOfString_3>
							<arrayOfString_3></arrayOfString_3>
							<arrayOfString_3></arrayOfString_3>
							<arrayOfString_3></arrayOfString_3>
						</typ:cancelExpedicion>
					</soapenv:Body>
				</soapenv:Envelope>';

        nacexutils::writeNacexLog("cancelExpedicion :: XML: " . $XML);

        $postResult = $nacexWS->requestWS($URL, $XML, $metodo, $style = "width:auto;");
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $resultado = $nacexWS->treatmentXML($postResult, $metodo);

        if ($resultado[0] == "ERROR") {
            echo '<br><div class="alert error" style="width:396px">';
            foreach ($resultado as $res) {
                echo '<p>' . $res . '</p>';
                nacexutils::writeNacexLog("cancelExpedicion :: " . $res);
            }
            echo '</div>';
            //return utf8_encode($this->_html);(mexpositop 20171127)
            return;
        } else {
            echo '<br><div class="conf confirm" style="width:396px">';
            echo '<img src="' . $nacexDTO->getPath() . 'logo.gif" />';
            foreach ($resultado as $res) {
                echo '<p>' . $res . '</p>';
                nacexutils::writeNacexLog("cancelExpedicion :: " . $res);
            }
            echo '</div>';
            echo '<script>
					jQuery("#ncx_boxinfo").hide();
				  </script>';

            $datospedido = nacexDAO::getDatosPedido($id_order);
            nacexutils::writeNacexLog("cancelExpedicion :: obtenemos los datos del pedido id_order:" . $id_order);

            echo nacexVIEW::showExpedicionForm($id_order, $datospedido, null, false,"");

            nacexDAO::cancelarExpedicion($id_pedido);

            nacexutils::writeNacexLog("FIN cancelExpedicion :: id_pedido: " . $id_pedido . " |cod_exp: " . $cod_exp);
            nacexutils::writeNacexLog("----");
        }
    }

    public static function putExpedicion($id_pedido, $datospedido, $datosexp, $isnacexcambio,  $referenciaNacex) {

        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI putExpedicion :: id_order: " . $datospedido[0]['id_order'] . "| module: " . $datospedido[0]['module'] . "| total_paid_real: " . $datospedido[0]['total_paid_real'] . "| email: " . $datospedido[0]['email'] . "| firstname: " . $datospedido[0]['firstname'] . "| lastname: " . $datospedido[0]['lastname'] . "| address1: " . $datospedido[0]['address1'] . "| postcode: " . $datospedido[0]['postcode'] . "| city: " . $datospedido[0]['city'] . "| phone: " . $datospedido[0]['phone'] . "| phone_mobile: " . $datospedido[0]['phone_mobile'] . "| iso_code: " . $datospedido[0]['iso_code'] . "| ncx: " . $datospedido[0]['ncx'] . "| id_carrier: " . $datospedido[0]['id_carrier']);
        $nacexDTO = new nacexDTO();
        $nacexWS = new nacexWS();
                
        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";

        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');

        $nacex_agcli = Tools::getValue('nacex_agcli');
        //$nacex_agcli_array = array();		
        $nacex_agcli_array = explode("/", $nacex_agcli);
        $nacexCodigoAgencia = $nacex_agcli_array[0];
        $nacexCodigoCliente = $nacex_agcli_array[1];

        $nacexTipSer = Tools::getValue('nacex_tip_ser');
        $xml_NacexFrecuencia = $nacexTipSer == '09' ? "<arrayOfString_3>frec_codigo=" . Tools::getValue('nacex_frecuencia') . "</arrayOfString_3>" : "";

        $nacexReferencia = $isnacexcambio ? $datosexp["ref"] : 
            Configuration::get('NACEX_REF_PERS_PREFIJO')!='$reference'? nacexutils::getReferenciaGeneral(). $id_pedido :$referenciaNacex;

        $nacexTipCob = Tools::getValue('nacex_tip_cob');
        $nacexTipRee = Tools::getValue('nacex_tip_ree');
        $nacexTipEnv = Tools::getValue('nacex_tip_env');
        //$nacexInsAdi_qr = Configuration::get('NACEX_INS_ADI_Q_R');
        $nacexRet = Tools::getValue('nacex_ret');

        $nacexTipSeg = Tools::getValue('nacex_tip_seg');
        $nacexImpSeg = Tools::getValue('nacex_imp_seg');

        $nacexPerEnt = Tools::getValue('nacex_per_ent');

        $bul = Tools::getValue('nacex_bul');

        $tip_pre1 = Tools::getValue('nacex_tip_pre1');
        $pre1 = Tools::getValue('nacex_pre1');
        $mod_pre1 = Tools::getValue('nacex_mod_pre1');
        $prep1lus = Tools::getValue('nacex_pre1_plus');

        $q_r = Tools::getValue('inst_adi');
        $obs1 = Tools::getValue('nacex_obs1');
        $obs2 = Tools::getValue('nacex_obs2');

        // Intrernacional
        $ImpDeclarado = Tools::getValue('nacex_impDeclarado');
        $Contenido = Tools::getValue('nacex_contenido');

        //Datos del pedido
        $datos = Db::getInstance()->ExecuteS(
                'SELECT o.id_order,o.module,u.email,a.firstname,
				a.lastname,a.address1,a.address2,a.postcode,a.city,a.phone,a.phone_mobile,z.iso_code,
				case when o.total_paid_real > 0 
					then o.total_paid_real
				else
					o.total_paid
				end as total_paid_real
				FROM ' . _DB_PREFIX_ . 'orders AS o
				JOIN ' . _DB_PREFIX_ . 'customer AS u
				JOIN ' . _DB_PREFIX_ . 'address a
				JOIN ' . _DB_PREFIX_ . 'country AS z
				WHERE o.id_order = "' . $id_pedido . '"
				AND u.id_customer = o.id_customer
				AND a.id_address = o.id_address_delivery
				AND a.id_country = z.id_country'
        );
        nacexutils::writeNacexLog("putExpedicion :: obtenidos datos pedido BD id_order: " . $datos[0]['id_order'] . " | module: " . $datos[0]['module'] . " | total_paid_real: " . $datos[0]['total_paid_real'] . " | email: " . $datos[0]['email'] . " | firstname: " . $datos[0]['firstname'] . " | lastname " . $datos[0]['lastname'] . " | address1: " . $datos[0]['address1'] . " | postcode: " . $datos[0]['postcode'] . " | city: " . $datos[0]['city'] . " | phone: " . $datos[0]['phone'] . " | phone_mobile: " . $datos[0]['phone_mobile'] . " | iso_code: " . $datos[0]['iso_code']);

        //Obtenemos más detalles del pedido, como el peso y las referencias
        $productospedido = Db::getInstance()->ExecuteS('SELECT product_quantity, product_weight, product_reference FROM ' . _DB_PREFIX_ . 'order_detail where id_order = "' . $id_pedido . '"');
        $peso = 0;
        $bultos = 0;
        foreach ($productospedido as $producto) {
            $peso += floatval($producto['product_quantity'] * $producto['product_weight']);
            $bultos += $producto['product_quantity'];
            //$prodref = str_replace(";", ",", $producto['product_reference']); 
        }

        //Informarmos de las instrucciones adicionales
        $xml_insadi_qr = "";
        
        $array_insadi_qr = nacexutils::cutupString($q_r, 40, 15);
        for ($i = 0; $i < count($array_insadi_qr); $i++) {
             if ($array_insadi_qr [$i] != ""){
                 $xml_insadi_qr .=  "<arrayOfString_3>ins_adi";
                 $xml_insadi_qr .=   $i+1;
                 $xml_insadi_qr .=   "=";
                 $xml_insadi_qr .=  $array_insadi_qr [$i];
                 $xml_insadi_qr .=  "</arrayOfString_3>";
                 }
             }        

        $xml_insadi_qr .= ($xml_insadi_qr != '') ? "<arrayOfString_3>ins_adi=S</arrayOfString_3>" : "";

        //Miramos si hay que informar del retorno
        $xml_ret = (isset($nacexRet) && $nacexRet == "SI") ? "<arrayOfString_3>ret=S</arrayOfString_3>" : "";

        $xml_seg = "";
        //Miramos si hay que informar del seguro
        if ((isset($nacexImpSeg) && $nacexImpSeg != "")) {
            $xml_seg = "<arrayOfString_3>tip_seg=" . $nacexTipSeg . "</arrayOfString_3>";
            $xml_seg.= "<arrayOfString_3>seg=" . number_format(str_replace(",", ".", str_replace(".", "", $nacexImpSeg)), 2, ".", ",") . "</arrayOfString_3>";
        }

        //Miramos si obtenemos el peso fijo o de los artículos de la cesta
        if (Configuration::get("NACEX_PESO") == "F") {
            $peso = str_replace(",", ".", str_replace(".", "", Configuration::get("NACEX_PESO_NUMERO")));
        } else {
            $peso = $peso < 1 ? 1 : $peso;
        }

        $bultos = $bultos < 1 ? 1 : $bultos;

        $nacex_peso_origen = $peso;
        // Obtenemos el num de pedido
        $nacex_referencia = sprintf('%06d', $datos[0]['id_order']);
        //Obtenemos el importe total del pedido
        $nacex_importe_servicio = $datos[0]['total_paid_real'];

        //Datos del comprador
        $nacex_nombre_destinatario = $datos[0]['firstname'] . ' ' . $datos[0]['lastname'];
        $nacex_nombre_via_destinatario = $datos[0]['address1'] . ' ' . $datos[0]['address2'];
        $nacex_poblacion_destinatario = $datos[0]['city'];
        $nacex_CP_destinatario = $datos[0]['postcode'];
        //$nacex_cod_provincia_destinatario= $dir_pedido->getRegion();
        $tlf = "";
        if (isset($datos[0]['phone']) && $datos[0]['phone'] != "") {
            $tlf = $datos[0]['phone'];
            if (isset($datos[0]['phone_mobile']) && $datos[0]['phone_mobile'] != "") {
                $tlf = $tlf . "/";
            }
        }
        if (isset($datos[0]['phone_mobile'])) {
            $tlf = $tlf . $datos[0]['phone_mobile'];
        }
        $nacex_telefono_destinatario = $tlf;
        $nacex_email_destinatario = $datos[0]['email'];
        $nacex_pais = $datos[0]['iso_code'];

        //PAGO CONTRA REEMBOLSO --------------------------------------------------------------------------------------------------		
        //Nombres módulos pago contra reembolso indicados en Configuración
        $array_modsree = array();
        $array_modsree = explode(",", Configuration::get('NACEX_MODULOS_REEMBOLSO'));
        //nacexutils::writeNacexLog("putExpedicion :: configurado modulo de reembolso [".Configuration::get('NACEX_MODULOS_REEMBOLSO')."]");	
        $nacex_reembolso = null;
        $metodo_pago = strtolower($datos[0]['module']);
        //if($metodo_pago == 'cashondelivery'){
        if (  in_array($metodo_pago, $array_modsree) || 
              strpos($metodo_pago, 'cashondelivery') !== false ||
                intval(Tools::getValue('nacex_imp_ree')) > 0) {
            
            $nacex_reembolso = floatval($nacex_importe_servicio)==null? Tools::getValue('nacex_imp_ree'):floatval($nacex_importe_servicio);
           
            nacexutils::writeNacexLog("putExpedicion :: detectado modo de pago [" . $metodo_pago . "] expedicion y reembolso de " . $nacex_reembolso);

            //Si estamos documentanto un nacex cambio, debemos comprobar si el cliente ha informado de la cantidad de reembolso
            if ($isnacexcambio) {
                $nacex_reembolso = floatval(str_replace(",", ".", Tools::getValue('nacex_imp_ree')));
                if ($nacex_reembolso == 0) {
                    $nacex_reembolso = null;
                }
            }
        }
        //------------------------------------------------------------------------------------------------------------------------

        $xml_ree = "";
        if (isset($nacex_reembolso)) {
            $xml_ree = "<arrayOfString_3>tip_ree=" . $nacexTipRee . "</arrayOfString_3>";
            $xml_ree .= "<arrayOfString_3>ree=" . $nacex_reembolso . "</arrayOfString_3>";
        }
        $xml_pre = "";

        if ($tip_pre1 != "N") {
            $xml_pre = "<arrayOfString_3>tip_pre1=" . $tip_pre1 . "</arrayOfString_3>";
            if ($mod_pre1 == "S" || $mod_pre1 == "P" || $mod_pre1 == "R" || $mod_pre1 == "E") {
                $xml_pre .= "<arrayOfString_3>mod_pre1=" . $mod_pre1 . "</arrayOfString_3>";
            } else {
                $xml_pre .= "<arrayOfString_3>mod_pre1=S</arrayOfString_3>";
            }
            $xml_pre .= "<arrayOfString_3>pre1=" . substr($pre1, 0, 50) . "</arrayOfString_3>";
            if (($mod_pre1 == "P" || $mod_pre1 == "E") && ($prep1lus && strlen($prep1lus) > 0)) {
                $xml_pre .= "<arrayOfString_3>msg1=" . substr($prep1lus, 0, 719) . "</arrayOfString_3>";
            }
        }

        $xml_obs1 = isset($obs1) && $obs1 != "" ? "<arrayOfString_3>obs1=" . $obs1 . "</arrayOfString_3>" : "";
        $xml_obs2 = isset($obs2) && $obs2 != "" ? "<arrayOfString_3>obs2=" . $obs2 . "</arrayOfString_3>" : "";
        $xml_per_ent = isset($nacexPerEnt) && $nacexPerEnt != "" ? "<arrayOfString_3>per_ent=" . $nacexPerEnt . "</arrayOfString_3>" : "";

        //Variables NacexShop
        $ncxshop = null;
        $array_shop_data = array();

        $shop_codigo = "";
        $shop_alias = "";
        $shop_nombre = "";
        $shop_direccion = "";
        $shop_cp = "";
        $shop_poblacion = "";
        $shop_provincia = "";
        $shop_telefono = "";

        if ($datospedido[0]['ncx'] != "1" && nacexDTO::isNacexShopCarrier($datospedido[0]['id_carrier'])) {
            $array_shop_data = explode("|", $datospedido[0]['ncx']);
            if (isset($array_shop_data)) {
                $shop_codigo = isset($array_shop_data[0]) ? $array_shop_data[0] : "";
                $shop_alias = isset($array_shop_data[1]) ? $array_shop_data[1] : "";
                $shop_nombre = isset($array_shop_data[2]) ? $array_shop_data[2] : "";
                $shop_direccion = isset($array_shop_data[3]) ? $array_shop_data[3] : "";
                $shop_cp = isset($array_shop_data[4]) ? $array_shop_data[4] : "";
                $shop_poblacion = isset($array_shop_data[5]) ? $array_shop_data[5] : "";
                $shop_provincia = isset($array_shop_data[6]) ? $array_shop_data[6] : "";
                $shop_telefono = isset($array_shop_data[7]) ? $array_shop_data[7] : "";
            }
            $ncxshop = "<arrayOfString_3>shop_codigo=" . $shop_codigo . "</arrayOfString_3>";
        }

        //DEPARTAMENTOS DEL CLIENTE ----------------------------------------------------------------------------
        $dpto = Tools::getValue('nacex_departamentos', "");
        $xml_dpto = isset($dpto) && $dpto != '' ? "<arrayOfString_3>dep_cli=" . $dpto . "</arrayOfString_3>" : "";
        //DEPARTAMENTOS DEL CLIENTE ----------------------------------------------------------------------------
        //Internacional ----------------------------------------------------------------------------------------
        $xml_NacexImpDeclarado = "";
        $xml_NacexContenido = "";

        if ($nacex_pais != "ES" && $nacex_pais != "PT" && $nacex_pais != "AD") {
            if (isset($ImpDeclarado)) {
                $xml_NacexImpDeclarado = "<arrayOfString_3>val_dec=" . $ImpDeclarado . "</arrayOfString_3>";
            } else {
                $xml_NacexImpDeclarado = "<arrayOfString_3>val_dec=" . $valor . "</arrayOfString_3>";
            }
            if (isset($Contenido)) {
                $xml_NacexContenido = "<arrayOfString_3>con=" . $Contenido . "</arrayOfString_3>";
            }
        }
        //Internacional ----------------------------------------------------------------------------------------
        $metodo = "putExpedicion";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
			<soapenv:Header/>
			<soapenv:Body>
				<typ:putExpedicion>
					<String_1>' . $nacexWSusername . '</String_1>
					<String_2>' . $nacexWSpassword . '</String_2>
					<arrayOfString_3>del_cli=' . $nacexCodigoAgencia . '</arrayOfString_3>
					<arrayOfString_3>num_cli=' . $nacexCodigoCliente . '</arrayOfString_3>
					<arrayOfString_3>fec=' . date("d/m/Y") . '</arrayOfString_3>' . 
                $xml_dpto . '
					<arrayOfString_3>tip_ser=' . $nacexTipSer . '</arrayOfString_3>
					<arrayOfString_3>tip_cob=' . $nacexTipCob . '</arrayOfString_3>
					<arrayOfString_3>tip_env=' . $nacexTipEnv . '</arrayOfString_3>' .
                $xml_ree . '' .
                $xml_pre . '' .
                $xml_obs1 . '' .
                $xml_obs2 . '' .
                $xml_per_ent . '
					<arrayOfString_3>ref_cli=' . $nacexReferencia . '</arrayOfString_3>					
					<arrayOfString_3>bul=' . $bul . '</arrayOfString_3>
					<arrayOfString_3>kil=' . $peso . '</arrayOfString_3>
					<arrayOfString_3>nom_ent=' . substr($nacex_nombre_destinatario, 0, 50) . '</arrayOfString_3>
					<arrayOfString_3>dir_ent=' . substr($nacex_nombre_via_destinatario, 0, 45) . '</arrayOfString_3>
					<arrayOfString_3>pais_ent=' . $nacex_pais . '</arrayOfString_3>					
					<arrayOfString_3>cp_ent=' . substr($nacex_CP_destinatario, 0, 15) . '</arrayOfString_3>
					<arrayOfString_3>pob_ent=' . substr($nacex_poblacion_destinatario, 0, 40) . '</arrayOfString_3>
					<arrayOfString_3>tel_ent=' . substr($nacex_telefono_destinatario, 0, 20) . '</arrayOfString_3>' .
                $ncxshop . '' .
                $xml_insadi_qr . '' .
                $xml_ret . '' .
                $xml_seg . '' .
                $xml_NacexImpDeclarado . '' .
                $xml_NacexContenido . '' .
                $xml_NacexFrecuencia . '</typ:putExpedicion>
			</soapenv:Body>
		</soapenv:Envelope>';


        nacexutils::writeNacexLog("putExpedicion :: XML: " . $XML);
$errorplain= false;
        $postResult = $nacexWS->requestWS($URL, $XML, $metodo, $style = "width:auto;",  $errorplain);
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $resultado =  $nacexWS->treatmentXML($postResult, $metodo);


        if ($resultado[0] == "ERROR") {
            echo '<br><div style="text-align:left;width:396px" class="alert error"><img src="' . $nacexDTO->getPath() . 'logo.gif" />';
            foreach ($resultado as $res) {
                echo '<p>' . $res . '</p>';
                nacexutils::writeNacexLog("putExpedicion :: ERROR =>" . $res);
            }
            echo '</div>';
            nacexutils::writeNacexLog("FIN putExpedicion :: id_pedido: " . $id_pedido);
            nacexutils::writeNacexLog("----");
            //return $this->_html;(mexpositop 20171127)
            return false;
        } else {

            $putExpedicionResponse = array();
            $putExpedicionResponse["exp_cod"] = $resultado[0];
            $putExpedicionResponse["ag_cod_num_exp"] = $resultado[1];
            $putExpedicionResponse["color"] = $resultado[2];
            $putExpedicionResponse["ent_ruta"] = $resultado[3];
            $putExpedicionResponse["ent_cod"] = $resultado[4];
            $putExpedicionResponse["ent_nom"] = $resultado[5];
            $putExpedicionResponse["ent_tlf"] = $resultado[6];
            $putExpedicionResponse["serv"] = $resultado[7];
            $putExpedicionResponse["hora_entrega"] = $resultado[8];
            $putExpedicionResponse["barcode"] = $resultado[9];
            $putExpedicionResponse["fecha_objetivo"] = $resultado[10];
            $putExpedicionResponse["cambios"] = $resultado[11];

            $putExpedicionResponse["shop_codigo"] = $shop_codigo;
            $putExpedicionResponse["shop_alias"] = $shop_alias;
            $putExpedicionResponse["shop_nombre"] = $shop_nombre;
            $putExpedicionResponse["shop_direccion"] = $shop_direccion;

            $putExpedicionResponse["ret"] = $nacexRet;

            $putExpedicionResponse["ref"] = $nacexReferencia;
            
            nacexutils::writeNacexLog("putExpedicion :: recibido putExpedicionResponse: exp_cod: " . $putExpedicionResponse['exp_cod'] . "| ag_cod_num_exp: " . $putExpedicionResponse['ag_cod_num_exp'] . "| color: " . $putExpedicionResponse['color'] . "| ent_ruta: " . $putExpedicionResponse['ent_ruta'] . "| ent_cod: " . $putExpedicionResponse['ent_cod'] . "| ent_nom: " . $putExpedicionResponse['ent_nom'] . "| ent_tlf: " . $putExpedicionResponse['ent_tlf'] . "| serv: " . $putExpedicionResponse['serv'] . "| hora_entrega: " . $putExpedicionResponse['hora_entrega'] . "| barcode: " . $putExpedicionResponse['barcode'] . "| fecha_objetivo: " . $putExpedicionResponse['fecha_objetivo'] . "| cambios: " . $putExpedicionResponse['cambios'] . "| shop_codigo: " . $putExpedicionResponse['shop_codigo'] . "| shop_alias: " . $putExpedicionResponse['shop_alias'] . "| shop_nombre: " . $putExpedicionResponse['shop_nombre'] . "| shop_direccion: " . $putExpedicionResponse['shop_direccion'] . "| ret: " . $putExpedicionResponse['ret']);
            nacexutils::writeNacexLog("putExpedicion :: guardamos expedicion en BD prestashop");
            
            nacexDAO::guardarExpedicion($nacex_agcli, $id_pedido, $putExpedicionResponse, $bul, $array_shop_data, $nacexRet, $nacexTipSer, $nacex_reembolso);
            nacexView::showExpedicionBoxInfo($putExpedicionResponse, $id_pedido);

            nacexDAO::actualizarTrackingExpedicion($id_pedido, $resultado[1]);

            nacexutils::writeNacexLog("FIN putExpedicion :: id_pedido: " . $id_pedido);
            nacexutils::writeNacexLog("----");
        }
         return true;
    }

    
    public static function putExpedicionMasivo($id_pedido, $data = array()) {

        $datospedido = nacexDAO::getDatosPedido($id_pedido);
        $nacexWS = new nacexWS();
        
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI putExpedicionMasivo :: id_order: " . $datospedido[0]['id_order'] . "| module: " . $datospedido[0]['module'] . "| total_paid_real: " . $datospedido[0]['total_paid_real'] . "| email: " . $datospedido[0]['email'] . "| firstname: " . $datospedido[0]['firstname'] . "| lastname: " . $datospedido[0]['lastname'] . "| address1: " . $datospedido[0]['address1'] . "| postcode: " . $datospedido[0]['postcode'] . "| city: " . $datospedido[0]['city'] . "| phone: " . $datospedido[0]['phone'] . "| phone_mobile: " . $datospedido[0]['phone_mobile'] . "| iso_code: " . $datospedido[0]['iso_code'] . "| ncx: " . $datospedido[0]['ncx'] . "| id_carrier: " . $datospedido[0]['id_carrier']);

        $urlNacex = Configuration::get('NACEX_WS_URL');
        $URL = $urlNacex . "/soap";
        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');

        $nacexInsAdi_qr = Configuration::get('NACEX_INS_ADI_Q_R');

        $nacexCodigoAgencia = $data["nacex_agencia"];
        $nacexCodigoCliente = $data["nacex_cliente"];
        $nacexTipSer = $data["nacex_tip_ser"];
        $xml_NacexFrecuencia = $nacexTipSer == '09' ? "<arrayOfString_3>frec_codigo=" . Tools::getValue('nacex_frecuencia') . "</arrayOfString_3>" : "";
        $nacexReferencia = nacexutils::getReferenciaGeneral() . $id_pedido;
        $nacexTipCob = $data["nacex_tip_cob"];
        $nacexTipRee = $data["nacex_tip_ree"];
        $nacexTipEnv = $data["nacex_tip_env"];
        $nacexRet = $data["nacex_ret"];
        $nacexTipSeg = $data["nacex_tip_seg"];
        $nacexImpSeg = $data["nacex_imp_seg"];
        $bul = $data["nacex_bul"];
        $tip_pre1 = $data["nacex_tip_pre1"];
        //$nacexPerEnt = Tools::getValue('nacex_per_ent');				
        $pre1 = Tools::getValue('nacex_pre1');
        $mod_pre1 = $data["nacex_mod_pre1"];
        $prep1lus = $data["nacex_pre1_plus"];
        $obs1 = $data["obs1"];
        $obs2 = $data["obs2"];

        // Internacional
        $ImpDeclarado = Tools::getValue('nacex_impDeclarado');
        $Contenido = Tools::getValue('nacex_contenido');

        //Datos del pedido
        $datos = nacexDAO::getDetallepedido($id_pedido);

        nacexutils::writeNacexLog("putExpedicionMasivo :: obtenidos datos pedido BD id_order: " . $datos[0]['id_order'] . " | module: " . $datos[0]['module'] . " | total_paid_real: " . $datos[0]['total_paid_real'] . " | email: " . $datos[0]['email'] . " | firstname: " . $datos[0]['firstname'] . " | lastname " . $datos[0]['lastname'] . " | address1: " . $datos[0]['address1'] . " | postcode: " . $datos[0]['postcode'] . " | city: " . $datos[0]['city'] . " | phone: " . $datos[0]['phone'] . " | phone_mobile: " . $datos[0]['phone_mobile'] . " | iso_code: " . $datos[0]['iso_code']);


        //INSTRUCCIONES ADICIONALES Y REFERENCIAS ------------------------------------------------------------------------------
        $instrucciones = "";

        //Obtenemos m�s detalles del pedido, como el peso y las referencias
        $productospedido = Db::getInstance()->ExecuteS('SELECT product_quantity, product_weight, product_reference, total_price_tax_incl 
																										FROM ' . _DB_PREFIX_ . 'order_detail
																										where id_order = "' . $id_pedido . '"');
        $peso = 0;
        $bultos = 0;
        $valor = 0;
        $q_r = "";

        foreach ($productospedido as $producto) {
            $peso += floatval($producto['product_quantity'] * $producto['product_weight']);
            $bultos += $producto['product_quantity'];
            $prodref = str_replace(";", ",", $producto['product_reference']);
            $valor+= floatval($producto['total_price_tax_incl']);
            $q_r .= " ** " . $producto['product_quantity'] . " # " . $prodref;
        }
        $xml_insadi_qr = "";
        //Miramos si hay que informar cantidad y referencia en Instrucciones Adicionales:
        if (isset($nacexInsAdi_qr) && $nacexInsAdi_qr == "SI") {
            $instrucciones .= $q_r . " " . $data["inst_adi"];
        } else {
            $instrucciones .= $data["inst_adi"];
        }

        $array_insadi_qr = nacexutils::cutupString($instrucciones, 40, 15);
        $boolinsadiqr = false;
        $iqr = 1;
        foreach ($array_insadi_qr as $qr) {
            if ($qr) {
                $xml_insadi_qr .= "<arrayOfString_3>ins_adi" . $iqr . "=" . $qr . "</arrayOfString_3>";
                $boolinsadiqr = true;
                $iqr++;
            }
        }

        if ($boolinsadiqr) {
            $xml_insadi_qr .= "<arrayOfString_3>ins_adi=S</arrayOfString_3>";
        }
        //INSTRUCCIONES ADICIONALES Y REFERENCIAS ------------------------------------------------------------------------------


        if ($bultos < 1) {
            $bultos = 1;
        }

        //ENVIO CON RETORNO ----------------------------------------------------------------------------------------------------
        $xml_ret = "";
        //Miramos si hay que informar del retorno
        if ((isset($nacexRet) && $nacexRet == "SI")) {
            $xml_ret = "<arrayOfString_3>ret=S</arrayOfString_3>";
        }
        //ENVIO CON RETORNO ----------------------------------------------------------------------------------------------------
        //ENVIO CON SEGURO -----------------------------------------------------------------------------------------------------		
        $xml_seg = "";
        //Miramos si hay que informar del seguro
        if ((isset($nacexImpSeg) && $nacexImpSeg != "")) {
            $xml_seg = "<arrayOfString_3>tip_seg=" . $nacexTipSeg . "</arrayOfString_3>";
            $nacexImpSeg = str_replace(".", "", $nacexImpSeg);
            $nacexImpSeg = str_replace(",", ".", $nacexImpSeg);
            $importe = number_format($nacexImpSeg, 2, ".", ",");
            $xml_seg.= "<arrayOfString_3>seg=" . $importe . "</arrayOfString_3>";
        }
        //ENVIO CON SEGURO -----------------------------------------------------------------------------------------------------
        //KG -------------------------------------------------------------------------------------------------------------------
        if (Configuration::get("NACEX_PESO") == "F") {
            $peso = Configuration::get("NACEX_PESO_NUMERO");
            $peso = str_replace(".", "", $peso);
            $peso = str_replace(",", ".", $peso);
        } else {
            if ($peso < 1) {
                $peso = 1;
            }
        }
        //KG -------------------------------------------------------------------------------------------------------------------	
        //Datos del comprador
        $nacex_nombre_destinatario = $datos[0]['firstname'] . ' ' . $datos[0]['lastname'];
        $nacex_nombre_via_destinatario = $datos[0]['address1'];
        $nacex_poblacion_destinatario = $datos[0]['city'];
        $nacex_CP_destinatario = $datos[0]['postcode'];

        $tlf = "";
        if (isset($datos[0]['phone']) && $datos[0]['phone'] != "") {
            $tlf = $datos[0]['phone'];
            if (isset($datos[0]['phone_mobile']) && $datos[0]['phone_mobile'] != "") {
                $tlf = $tlf . "/";
            }
        }
        if (isset($datos[0]['phone_mobile'])) {
            $tlf = $tlf . $datos[0]['phone_mobile'];
        }

        $nacex_telefono_destinatario = $tlf;
        $nacex_email_destinatario = $datos[0]['email'];
        $nacex_pais = $datos[0]['iso_code'];

        //PAGO CONTRA REEMBOLSO --------------------------------------------------------------------------------------------------		
        //Nombres m�dulos pago contra reembolso indicados en Configuraci�n
        $array_modsree = array();
        $array_modsree = explode(",", Configuration::get('NACEX_MODULOS_REEMBOLSO'));

        //Obtenemos el importe total del pedido
        $nacex_importe_servicio = $datos[0]['total_paid_real'];
        $nacex_reembolso = null;

        $metodo_pago = strtolower($datos[0]['module']);
        if (in_array($metodo_pago, $array_modsree) || strpos($metodo_pago, 'cashondelivery') !== false) {
            $nacex_reembolso = floatval($nacex_importe_servicio);
            nacexutils::writeNacexLog("putExpedicionMasivo :: detectado modo de pago [" . $metodo_pago . "] expedicion y reembolso de " . $nacex_reembolso);
        }
        $xml_ree = "";
        if (isset($nacex_reembolso)) {
            $xml_ree = "<arrayOfString_3>tip_ree=" . $nacexTipRee . "</arrayOfString_3>";
            $xml_ree .= "<arrayOfString_3>ree=" . $nacex_reembolso . "</arrayOfString_3>";
        }
        //PAGO CONTRA REEMBOLSO --------------------------------------------------------------------------------------------------
        //PREALERTA --------------------------------------------------------------------------------------------------------------
        $xml_pre = "";

        if ($tip_pre1 != "N") {

            if ($tip_pre1 == "S") {
                !empty($datos[0]['phone_mobile']) ? $pre1 = $datos[0]['phone_mobile'] : $pre1 = $datos[0]['phone'];
            } else if ($tip_pre1 == "E") {
                $pre1 = $datos[0]['email'];
            }

            $xml_pre = "<arrayOfString_3>tip_pre1=" . $tip_pre1 . "</arrayOfString_3>";
            if ($mod_pre1 == "S" || $mod_pre1 == "P" || $mod_pre1 == "R" || $mod_pre1 == "E") {
                $xml_pre .= "<arrayOfString_3>mod_pre1=" . $mod_pre1 . "</arrayOfString_3>";
            } else {
                $xml_pre .= "<arrayOfString_3>mod_pre1=S</arrayOfString_3>";
            }
            $xml_pre .= "<arrayOfString_3>pre1=" . substr($pre1, 0, 50) . "</arrayOfString_3>";
            if (($mod_pre1 == "P" || $mod_pre1 == "E") && ($prep1lus && strlen($prep1lus) > 0)) {
                $xml_pre .= "<arrayOfString_3>msg1=" . substr($prep1lus, 0, 719) . "</arrayOfString_3>";
            }
        }
        //PREALERTA --------------------------------------------------------------------------------------------------------------
        //OBSERVACIONES ----------------------------------------------------------------------------------------------------------
        $xml_obs1 = "";
        if (isset($obs1) && $obs1 != "") {
            $xml_obs1 = "<arrayOfString_3>obs1=" . $obs1 . "</arrayOfString_3>";
        }
        $xml_obs2 = "";
        if (isset($obs2) && $obs2 != "") {
            $xml_obs2 = "<arrayOfString_3>obs2=" . $obs2 . "</arrayOfString_3>";
        }
        //OBSERVACIONES ----------------------------------------------------------------------------------------------------------

        $xml_per_ent = "";
        if (isset($nacexPerEnt) && $nacexPerEnt != "") {
            $xml_per_ent = "<arrayOfString_3>per_ent=" . $nacexPerEnt . "</arrayOfString_3>";
        }

        //DEPARTAMENTOS DEL CLIENTE ----------------------------------------------------------------------------
        $xml_dpto = "";
        $dpto = Tools::getValue('nacex_departamentos');
        if (isset($dpto) && $dpto != false) {
            $xml_dpto = "<arrayOfString_3>dep_cli=" . $dpto . "</arrayOfString_3>";
        }
        //DEPARTAMENTOS DEL CLIENTE ----------------------------------------------------------------------------
        //Internacional ----------------------------------------------------------------------------------------
        $xml_NacexImpDeclarado = "";
        $xml_NacexContenido = "";
        if ($nacex_pais != "ES" && $nacex_pais != "PT" && $nacex_pais != "AD") {

            if (isset($ImpDeclarado) && $ImpDeclarado != false) {
                $xml_NacexImpDeclarado = "<arrayOfString_3>val_dec=" . $ImpDeclarado . "</arrayOfString_3>";
            } else {
                $xml_NacexImpDeclarado = "<arrayOfString_3>val_dec=" . $valor . "</arrayOfString_3>";
            }

            if (isset($Contenido) && $Contenido != false) {
                $xml_NacexContenido = "<arrayOfString_3>con=" . $Contenido . "</arrayOfString_3>";
            } else {
                $xml_NacexContenido = "<arrayOfString_3>con=OTROS</arrayOfString_3>";
            }
        }
        //Internacional ----------------------------------------------------------------------------------------
        $metodo = "putExpedicion";
        $XML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="urn:soap/types">
			<soapenv:Header/>
			<soapenv:Body>
			<typ:putExpedicion>
			<String_1>' . $nacexWSusername . '</String_1>
			<String_2>' . $nacexWSpassword . '</String_2>
			<arrayOfString_3>del_cli=' . $nacexCodigoAgencia . '</arrayOfString_3>
			<arrayOfString_3>num_cli=' . $nacexCodigoCliente . '</arrayOfString_3>
			<arrayOfString_3>fec=' . date("d/m/Y") . '</arrayOfString_3>
			<arrayOfString_3>tip_ser=' . $nacexTipSer . '</arrayOfString_3>
			<arrayOfString_3>tip_cob=' . $nacexTipCob . '</arrayOfString_3>
			<arrayOfString_3>tip_env=' . $nacexTipEnv . '</arrayOfString_3>
			' . $xml_ree . '
			' . $xml_pre . '
			' . $xml_obs1 . '
			' . $xml_obs2 . '
			' . $xml_per_ent . '
			<arrayOfString_3>ref_cli=' . $nacexReferencia . '</arrayOfString_3>					
			<arrayOfString_3>bul=' . $bul . '</arrayOfString_3>
			<arrayOfString_3>kil=' . $peso . '</arrayOfString_3>
			<arrayOfString_3>nom_ent=' . substr($nacex_nombre_destinatario, 0, 50) . '</arrayOfString_3>
			<arrayOfString_3>dir_ent=' . substr($nacex_nombre_via_destinatario, 0, 45) . '</arrayOfString_3>
			<arrayOfString_3>pais_ent=' . $nacex_pais . '</arrayOfString_3>					
			<arrayOfString_3>cp_ent=' . substr($nacex_CP_destinatario, 0, 15) . '</arrayOfString_3>
			<arrayOfString_3>pob_ent=' . substr($nacex_poblacion_destinatario, 0, 40) . '</arrayOfString_3>
			<arrayOfString_3>tel_ent=' . substr($nacex_telefono_destinatario, 0, 20) . '</arrayOfString_3>
			' . $xml_insadi_qr . '
			' . $xml_ret . '
			' . $xml_seg . '
			' . $xml_NacexImpDeclarado . '
			' . $xml_NacexContenido . ' 										
			' . $xml_NacexFrecuencia . ' 
			</typ:putExpedicion>
			</soapenv:Body>
			</soapenv:Envelope>';


        nacexutils::writeNacexLog("putExpedicionMasivo :: XML: " . $XML);
/*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=utf-8"));

        $postResult = curl_exec($ch);


        if (curl_errno($ch)) {
            $nacex_response = array('tipo' => 'ERROR',
                'msg' => $this->l('Error de conexión con WebService de Nacex.'));
            return $nacex_response;
        }

	$xml = new  SimpleXMLElement($postResult);// mexpositop 20171128
               //simplexml_load_string($postResult, NULL, NULL, "http://www.w3.org/2003/05/soap-envelope");
        $xml->registerXPathNamespace("ns0", "urn:soap/types");
        $resultado = $xml->xpath('//ns0:putExpedicionResponse/result');
*/
$errorplain= false;
        $postResult = $nacexWS->requestWS($URL, $XML, $metodo, $style = "width:auto;", $errorplain);
        if ($postResult[0] == "ERROR") {
            return $postResult;
        }
        $resultado =  $nacexWS->treatmentXML($postResult, $metodo);
        


        if ($resultado[0] == "ERROR") {
            $nacex_response = array('tipo' => 'ERROR',
                'msg' => '[Pedido ' . $id_pedido . ']: Error al generar la expedición: - ' . $resultado[1]);
        } else {

            $putExpedicionResponse = array();
            $putExpedicionResponse["exp_cod"] = $resultado[0];
            $putExpedicionResponse["ag_cod_num_exp"] = $resultado[1];
            $putExpedicionResponse["color"] = $resultado[2];
            $putExpedicionResponse["ent_ruta"] = $resultado[3];
            $putExpedicionResponse["ent_cod"] = $resultado[4];
            $putExpedicionResponse["ent_nom"] = $resultado[5];
            $putExpedicionResponse["ent_tlf"] = $resultado[6];
            $putExpedicionResponse["serv"] = $resultado[7];
            $putExpedicionResponse["hora_entrega"] = $resultado[8];
            $putExpedicionResponse["barcode"] = $resultado[9];
            $putExpedicionResponse["fecha_objetivo"] = $resultado[10];
            $putExpedicionResponse["cambios"] = $resultado[11];
            $putExpedicionResponse["shop_codigo"] = "";
            $putExpedicionResponse["shop_alias"] = "";
            $putExpedicionResponse["shop_nombre"] = "";
            $putExpedicionResponse["shop_direccion"] = "";
            $putExpedicionResponse["ret"] = $nacexRet;
            
            $putExpedicionResponse["ref"] = $nacexReferencia;
            
            nacexutils::writeNacexLog("putExpedicionMasivo :: recibido putExpedicionResponse: exp_cod: " . $putExpedicionResponse['exp_cod'] . "| ag_cod_num_exp: " . $putExpedicionResponse['ag_cod_num_exp'] . "| color: " . $putExpedicionResponse['color'] . "| ent_ruta: " . $putExpedicionResponse['ent_ruta'] . "| ent_cod: " . $putExpedicionResponse['ent_cod'] . "| ent_nom: " . $putExpedicionResponse['ent_nom'] . "| ent_tlf: " . $putExpedicionResponse['ent_tlf'] . "| serv: " . $putExpedicionResponse['serv'] . "| hora_entrega: " . $putExpedicionResponse['hora_entrega'] . "| barcode: " . $putExpedicionResponse['barcode'] . "| fecha_objetivo: " . $putExpedicionResponse['fecha_objetivo'] . "| cambios: " . $putExpedicionResponse['cambios'] . "| ret: " . $putExpedicionResponse['ret']);
            nacexutils::writeNacexLog("putExpedicionMasivo :: guardamos expedicion en BD prestashop");

            $nacex_agcli = $nacexCodigoAgencia . "/" . $nacexCodigoCliente;
            $resultDB = nacexDAO::guardarExpedicion($nacex_agcli, $id_pedido, $putExpedicionResponse, $bul, null, $nacexRet, $nacexTipSer, $nacex_reembolso);

            if (!$resultDB) {
                $nacex_response = array('tipo' => 'ERROR',
                    'msg' => '[Pedido ' . $id_pedido . ']: Error al guardar la expedición: - ' . $resultado[1]);
            } else {
                nacexDAO::actualizarTrackingExpedicion($id_pedido, $resultado[1]);
                $nacex_response = array('tipo' => 'SUCCESS',
                    'msg' => '[Pedido ' . $id_pedido . ']: Expedición generada correctamente: - ' . $resultado[1]);
            }
        }

        nacexutils::writeNacexLog("FIN putExpedicionMasivo :: id_pedido: " . $id_pedido);
        nacexutils::writeNacexLog("----");

        return $nacex_response;
    }

    private function treatmentXML($postResult, $metodo) {
        $xml = new SimpleXMLElement($postResult);
        $xml->registerXPathNamespace("ns0", "urn:soap/types");
        $result = $xml->xpath('//ns0:' . $metodo . 'Response');
        return (array) $result[0]->result;
    }

    private function requestWS($URL, $XML, $metodo, $style = "text-align:left;width:396px;", $errorplain = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $XML);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml; charset=utf-8"));

          /**parche mexpositop 20171222**/
      //  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $postResult = curl_exec($ch);

        if (curl_errno($ch)) {
            $str = $errorplain ? '[Error cURL ' . curl_errno($ch) . ']: ' . curl_error($ch) :
                    '<br>
			<div style="' . $style . '" class="alert error ncx_network_error">
				<h1>Error de comunicación con el Web Service</h1>
				<div class="metodo" align="center">' . $metodo . '</div>
				<div class="descripcion">[Error cURL ' . curl_errno($ch) . ']: ' . curl_error($ch) . '</div>
			</div>';
            nacexutils::writeNacexLog($metodo . " :: [Error cURL " . curl_errno($ch) . " ]: " . curl_error($ch));
            return array("ERROR", $str);
        }

        return $postResult;
    }
/*
    public function xml2array($xmlObject, $out = array()) {
        foreach ((array) $xmlObject as $index => $node){
           // $out[$index] = ( is_object($node) ) ? xml2array($node) : $node;
             $out[$index] =  $node;
        }
        return $out;
    }
*/
}

?>
