<?php

//include_once("ConsultasBD.php");

/*
 * Config
 */


class nacexutils {
const nacexVersion = '2.2.0.8';
const _moduleName = 'nacex';

    public static function getModuleName() {
            return self::_moduleName;
    }

    public static function getDefValue($array, $index, $def) {
        if (isset($array[$index])) {
            return $array[$index];
        } else {
            return $def;
        }
    }

    public static function normalizarDecimales($val, $numdecs, $sepdec, $sepmil, $noprintifzero, $noprintifcerodec) {

        if ($noprintifzero && (!isset($val) || $val == "0")) {
            return "";
        } else {
            if ($noprintifcerodec){
                return $val;
            }else{
                return number_format($val, $numdecs, $sepdec, $sepmil);
            }
        }
    }

    public static function getDatosListado($id_pedido) {

        $datoslistado = array();

        $datoslistado["id_order"] = $id_pedido;
        $datoslistado["nom_ent"] = null;
        $datoslistado["tel_ent"] = null;
        $datoslistado["dir_ent"] = null;
        $datoslistado["pob_ent"] = null;
        $datoslistado["cp_ent"] = null;
        $datoslistado["tel_ent"] = null;
        $datoslistado["email_ent"] = null;
        $datoslistado["pais_ent"] = null;

        $datoslistado["peso"] = null;
        $datoslistado["bultos"] = null;

        $datoslistado["importe"] = null;
        $datoslistado["ree"] = null;


        //Datos relacionados al pedido
        $datos = Db::getInstance()->ExecuteS(
                'SELECT o.id_order,o.module,u.email,a.firstname,
				a.lastname,a.address1,a.postcode,a.city,a.phone,a.phone_mobile,z.iso_code,
				case when o.total_paid_real > 0 
					then o.total_paid_real
				else
					o.total_paid
				end as total_paid_real
				FROM ' . _DB_PREFIX_ . 'orders AS o
				JOIN ' . _DB_PREFIX_ . 'customer AS u
				JOIN ' . _DB_PREFIX_ . 'address a
				JOIN ' . _DB_PREFIX_ . 'country AS z
				WHERE o.id_order = "' . $id_pedido . '"
				AND u.id_customer = o.id_customer
				AND a.id_address = o.id_address_delivery
				AND a.id_country = z.id_country'
        );

        //Detalles del pedido
        $productospedido = Db::getInstance()->ExecuteS(
                'SELECT product_quantity, product_weight FROM ' . _DB_PREFIX_ . 'order_detail
				where id_order = "' . $id_pedido . '"');

        //Detalle de la expedicion
        $datosexpedicion = nacexDAO::getDatosExpedicion($id_pedido);

        foreach ($productospedido as $producto) {
            $datoslistado["peso"] += floatval($producto['product_quantity'] * $producto['product_weight']);
            $datoslistado["bultos"] += $producto['product_quantity'];
        }
        if ($datoslistado["peso"] < 1) {
            $datoslistado["peso"] = 1;
        }
        if ($datoslistado["bultos"] < 1) {
            $datoslistado["bultos"] = 1;
        }


        //Obtenemos el importe total del pedido
        $datoslistado["importe"] = $datos[0]['total_paid_real'];

        //Datos del comprador (entrega)
        $datoslistado["nom_ent"] = $datos[0]['firstname'] . ' ' . $datos[0]['lastname'];
        $datoslistado["dir_ent"] = $datos[0]['address1'];
        $datoslistado["pob_ent"] = $datos[0]['city'];
        $datoslistado["cp_ent"] = $datos[0]['postcode'];

        //$nacex_cod_provincia_destinatario= $dir_pedido->getRegion();
        $tlf = "";
        if (isset($datos[0]['phone']) && $datos[0]['phone'] != "") {
            $tlf = $datos[0]['phone'];
            if (isset($datos[0]['phone_mobile']) && $datos[0]['phone_mobile'] != "") {
                $tlf = $tlf . "/";
            }
        }
        if (isset($datos[0]['phone_mobile'])) {
            $tlf = $tlf . $datos[0]['phone_mobile'];
        }
        $datoslistado["tel_ent"] = $tlf;
        $datoslistado["email_ent"] = $datos[0]['email'];
        $datoslistado["pais_ent"] = $datos[0]['iso_code'];


        //HAY QUE CONTROLAR SI EL COMPRADOR A ELEGIDO CONTRA REEMBOLSO
       // $array_modsree = array();
        $array_modsree = explode(",", Configuration::get('NACEX_MODULOS_REEMBOLSO'));

        $metodo_pago = strtolower($datos[0]['module']);

        if (in_array($metodo_pago, $array_modsree) || strpos($metodo_pago, 'cashondelivery') !== false) {
            if (isset($datosexpedicion) && $datosexpedicion[0]['imp_ree'] != 0) {
                $datoslistado["ree"] = floatval($datosexpedicion[0]['imp_ree']);
            } else {
                $datoslistado["ree"] = floatval($datos[0]['total_paid_real']);
            }
        }

        return $datoslistado;
    }

    public static function markSelectedOption($option_field_name, $config_name, $val) {
        if ($config_name != null && $config_name != '') {
            if (Tools::getValue($option_field_name, Configuration::get($config_name)) == $val) {
                return "selected=\"selected\"";
            }
        }
        return "";
    }

    public static function markSelectedFrontendOption($option_field_name, $valuef, $val) {
        if ($valuef != null && $valuef != '') {
            if (Tools::getValue($option_field_name, $valuef) == $val) {
                return "selected=\"selected\"";
            }
        }
        return "";
    }

    public static function markCheckedOption($option_field_name, $config_name, $val) {
        if ($config_name != null && $config_name != '') {
            if (Tools::getValue($option_field_name, Configuration::get($config_name)) == $val) {
                return "checked=\"checked\"";
            }
        }
        return "";
    }

    public static function markSelectedMultiOption($array_multi_option_field_name, $config_name, $sep, $val) {

        $results = Tools::getValue($array_multi_option_field_name, NULL);
        if (isset($results)) {
            
        } else {
            $results = explode($sep, Configuration::get($config_name));
        }
        for ($i = 0; $i < count($results); $i++) {
            if ($results[$i] == $val){
                return "selected=\"selected\"";
            }
        }
        return "";
    }

    public static function markCheckedCheckBoxes($array_multi_option_field_name, $config_name, $sep, $val) {

        $results = Tools::getValue($array_multi_option_field_name, NULL);
        if (isset($results)) {
            
        } else {
            $results = explode($sep, Configuration::get($config_name));
        }
        for ($i = 0; $i < count($results); $i++) {
            if ($results[$i] == $val)
                return "checked";
        }
        return "";
    }

    private static function selectValMultiInputText($array_multi_option_field_name, $config_name, $sep, $val) {

        //echo "<font color='red'>".$val."</font><br>";
        $results = Tools::getValue($array_multi_option_field_name, NULL);
        //var_dump($results);

        if (isset($results)) {
            
        } else {
            $results = explode($sep, Configuration::get($config_name));
        }
        for ($i = 0; $i < count($results); $i++) {
            if ($results[$i] == $val)
                return $val;
        }
        return $val;
    }

    public static function isNacexGenericCarrier($id_carrier) {
        $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier = "' . $id_carrier . '"');

        if (isset($datoscarrier) && isset($datoscarrier[0])) {
            nacexutils::writeNacexLog("isNacexGenericCarrier :: [" . $id_carrier . "] => " . (($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] == "NACEX") || ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] == "NACEXSHOP")));
            return (
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] == "NACEX") ||
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] == "NACEXSHOP")
                    );
        } else {
            nacexutils::writeNacexLog("isNacexGenericCarrier :: [" . $id_carrier . "] => false");
            return false;
        }
    }

    public static function printEtiqueta($id_pedido) {
        $urlPrint = Configuration::get('NACEX_PRINT_URL');
        $modelPrint = Configuration::get('NACEX_PRINT_MODEL');
        $etPrint = Configuration::get('NACEX_PRINT_ET');
        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');
        $parametros = "user=" . $nacexWSusername . "&pass=" . $nacexWSpassword . "&model=" . $modelPrint . "&et=" . $etPrint . "&ref=" . getReferenciaGeneral() . $id_pedido;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlPrint . "?" . $parametros);
          /**parche mexpositop 20171222**/
     //  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $postResult = curl_exec($ch);
        curl_close($ch);
    }

    public static function getMapCambios($cambios) {

        // "key1=value1|key2=value2|...|keyN=valueN"

        $mapa = array();

        $pares = explode("|", $cambios);

        for ($i = 0; $i < count($pares); $i++) {
            $par = $pares[$i];
            if (strlen($par)) {
                $kv = explode("=", $par);
                $k = $kv[0];
                $v = $kv[1];
                $mapa[$k] = $v;
            }
        }
        return $mapa;
    }

    public static function cutupString($str, $cutLength, $numCuts) {

        $text = preg_replace("/[\r\n]+/", "", $str);

        $ret = array();
       // $lastIndex = 0;

        for ($i = 0; $i < $numCuts; $i++) {
            $ret[$i] = substr($text, 0, $cutLength);
            $text = substr($text, $cutLength, strlen($text));
        }
        return $ret;
    }

    public static function getReferenciaGeneral() {
        $nacexDTO= new nacexDTO();
        if (Configuration::get('NACEX_REF_PERS') == "SI" ) {
            return Configuration::get('NACEX_REF_PERS_PREFIJO');
        } else {
            return $nacexDTO->PREFIJO_REFERENCIA;
        }
    }

    public static function showAppletImpresion() {
        $html = "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>
						<script type='text/javascript'>
								 if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){
									document.write(\"<OBJECT \",
				                   			\"tabindex='-1'\",			   
												   			\"codeBase='http://java.sun.com/products/plugin/autodl/jinstall-1_5_0-windows-i586.cab#Version=1,5,0,0'\",
				 								   			\"classid='clsid:8AD9C840-044E-11D1-B3E9-00805F499D93'\",
												   			\"id='ImpEtiqueta'\",
												   			\"width='1'\",
				                   			\"height='1'>\",
				                				\"<PARAM name='code' value='ImpEtiqueta.class'>\",
				                				\"<PARAM name='java_archive' value='NacexImpresion_signed.jar'/>\",
				                				\"<PARAM name='java_type' value='application/x-java-applet;version=1.5.0'/>\",
				                				\"<PARAM name='java_codebase' value='" . Configuration::get('NACEX_PRINT_URL') . "'/>\",
				                				\"<PARAM name='initial_focus' value='false'/>\",
				                				\"<PARAM NAME='MAYSCRIPT' VALUE='true'/>\",
											       		\"<PARAM NAME='SHOW_PRINTER_DIALOG' VALUE='true'/>\",
												   			\"<PARAM NAME='SINDATOS' VALUE='No hay resultados'/>\",
				                   			\"</OBJECT>\");
								 }else{
									document.write(\"<embed code='ImpEtiqueta.class' id='ImpEtiqueta'\",
												   			 \"codebase='" . Configuration::get('NACEX_PRINT_URL') . "'\",
												   			 \"width='0'\",
												   			 \"height='0'\",
												   			 \"archive='NacexImpresion_signed.jar'\",
												   			 \"type='application/x-java-applet;version=1.5'\",
												   			 \"initial_focus='false'\",
												   			 \"MAYSCRIPT='true'\",
												   			 \"SHOW_PRINTER_DIALOG='true'\",
												   			 \"SINDATOS='No hay resultados' >\");
								}
							</script>";

        return $html;
    }

    function getToken($tab) {
        global $cookie;
        return Tools::getAdminToken($tab . (int) Tab::getIdFromClassName($tab) . (int) $cookie->id_employee);
    }

    public static function changeAddressIfNacexShop($datospedido) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI changeAddressIfNacexShop :: id_order:" . $datospedido["id_order"]);

        $array_address_invoice = nacexDAO::getAddressInvoiceByOrder($datospedido["id_order"]);

        if (nacexDTO::isNacexShopCarrier($datospedido['id_carrier'])) {
            echo "<script>				
					$(function() { 
						var fieldset = $('a[href^=\'?tab=AdminAddresses&id_address\']:eq(0)').closest('fieldset');						
						$(fieldset).find('form>div').css('display', 'none');
						var legend = $(fieldset).find('legend');																		
						$(legend).html('Dirección de Entrega Nacex!Shop');
						$(fieldset).addClass('bg_nacexshop').prepend(legend);	
						$(fieldset).append('<hr/><span><i>Att: " . $array_address_invoice[0]["firstname"] . " " . $array_address_invoice[0]["lastname"] . "</i></span>');
						$('a[href^=\'?tab=AdminAddresses&id_address\']:eq(0)').css('display', 'none');						
					});
			      </script>";
            nacexutils::writeNacexLog("changeAddressIfNacexShop :: insertada informacion de direccion nacexshop");
        }
        nacexutils::writeNacexLog("FIN changeAddressIfNacexShop :: id_order:" . $datospedido["id_order"]);
        nacexutils::writeNacexLog("----");
    }

    public static function explodeShopData($shop_data) {
        $ret = array();
        
        $array_shop_data = isset($shop_data)? explode("|", $shop_data):null;
        if (isset($array_shop_data)) {
                $ret['shop_codigo']=    isset($array_shop_data[0])?$array_shop_data[0]:null;
                $ret['shop_alias']=     isset($array_shop_data[1])?$array_shop_data[1]:null;
                $ret['shop_nombre']=    isset($array_shop_data[2])?$array_shop_data[2]:null;
                $ret['shop_direccion']= isset($array_shop_data[3])?$array_shop_data[3]:null;
                $ret['shop_cp']=        isset($array_shop_data[4])?$array_shop_data[4]:null;
                $ret['shop_poblacion']= isset($array_shop_data[5])?$array_shop_data[5]:null;
                $ret['shop_provincia']= isset($array_shop_data[6])?$array_shop_data[6]:null;	
        }
        return $ret;
    }

    public static function writeNacexLog($txt) {
       // global $nacexVersion;
        if (Configuration::get('NACEX_SAVE_LOG') == "SI") {
            $logname = $_SERVER["DOCUMENT_ROOT"] . __PS_BASE_URI__ . "modules/nacex/log/" . "nacex_" . date("Ymd") . ".log";
            $logfile = fopen($logname, "a");
            fwrite($logfile, "[" . date('Y-m-d H:i:s') . "] <PS:" . _PS_VERSION_ . " - NCX:" . self::nacexVersion . "> " . $txt . "\n");
        }
    }

    public static function arrayFlatten($array) { 
        $flattern = array(); 
        foreach ($array as $key => $value){ 
            $new_key = array_keys($value); 
            $flattern[] = $value[$new_key[0]]; 
        } 
        return $flattern; 
} 
}

