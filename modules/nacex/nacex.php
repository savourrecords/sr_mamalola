<?php

header('Content-Type: text/html; charset=utf-8');
//session_start(); mexpositop 20180201
include_once("nacexWS.php");
include_once("nacexVIEW.php");
include_once("AdminConfig.php");
include_once("nacexutils.php");
include_once("nacexDAO.php");

/*
 *  2012 Nacex PrestaShop
 *  2017 mexpositop Adapted for PS1.7
 */

if (Configuration::get('NACEX_SHOW_ERRORS') == "SI") {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
} else {
    error_reporting(0);
    ini_set('display_errors', '0');
}

if (!defined('_PS_VERSION_')) {
    exit;
}

class nacex extends CarrierModule {

    private $_html = '';
    private $_postErrors = array();
    private $_confErrors = array();
    private $_moduleName = 'nacex';
    public $id_carrier;
    public $tabs = array(
        array(
            'name' => 'Nacex',
            'class_name' => 'nacextab',
            'visible' => true,
            'parent_class_name' => 'AdminParentOrders'
        )
        , array(
            'name' => 'Nacex Masivo',
            'class_name' => 'nacextabMasivo',
            'visible' => true,
            'parent_class_name' => 'AdminParentOrders'
        )
    );

    public function install() {

        nacexDAO::createTablesIfNotExists();

        if (!parent::install() ||
                !$this->registerHook('updateCarrier') ||
                !$this->registerHook('adminOrder') ||
                !$this->registerHook('beforeCarrier') ||
                !$this->registerHook('processCarrier') ||
                !$this->registerHook('orderConfirmation') ||
                !$this->registerHook('orderDetailDisplayed') ||
                !$this->registerHook('PDFInvoice')) {
            return false;
        }
        return true;
    }

    public function uninstall() {
        //Borramos TAB NACEX
        Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'tab WHERE module = "' . $this->_moduleName . '"');
        parent::uninstall();
    }

    public function __construct() {

        $this->name = 'nacex';
        $this->tab = 'shipping_logistics';
        $this->version = nacexutils::nacexVersion;
        $this->author = 'Nacex';
        parent::__construct();
        $this->displayName = 'Módulo Nacex';
        $this->description = 'Genere expediciones e imprima etiquetas enviando información a Nacex con un solo clic.';
    }

    /*
     * * Hook update carrier
     * *
     */

    public function hookupdateCarrier($params) {
        nacexutils::writeNacexLog("---");
        nacexutils::writeNacexLog("INI hookupdateCarrier :: id_carrier: " . $params['id_carrier']);

        //Actualiza ID de transportista en Configuración cuando éstos han sido actualizados.
        //Al modificarse, éstos aumentan su ID. Este algoritmo sólo guardará la nueva ID si sólo difiere en -1	

        $id_carrier = $params['id_carrier'];
        if ($id_carrier == Configuration::get('TRANSPORTISTA_NACEX')) {
            Configuration::updateValue('TRANSPORTISTA_NACEX', $params['carrier']->id);
        }
        if ($id_carrier == Configuration::get('TRANSPORTISTA_NACEXSHOP')) {
            Configuration::updateValue('TRANSPORTISTA_NACEXSHOP', $params['carrier']->id);
        }

        $transportistas_f_piped = Configuration::get("NACEX_ID_TRANSPORTISTAS_F");
        $array_transportistas_f = explode("|", $transportistas_f_piped);

        if (in_array($id_carrier, $array_transportistas_f)) {
            $transportistas_f_piped = str_replace($id_carrier, $params['carrier']->id, $transportistas_f_piped);
            Configuration::updateValue('NACEX_ID_TRANSPORTISTAS_F', $transportistas_f_piped);
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET ncx="nacex" WHERE id_carrier = "' . $params['carrier']->id . '"');
            nacexutils::writeNacexLog("hookupdateCarrier :: actualizado campo [ncx = nacex] del carrier");
        }

        $transportistas_nxshop_f_piped = Configuration::get("NACEX_ID_TRANSPORTISTAS_NXSHOP_F");
        $array_transportistas_nxshop_f = explode("|", $transportistas_nxshop_f_piped);
        if (in_array($id_carrier, $array_transportistas_nxshop_f)) {
            $transportistas_nxshop_f_piped = str_replace($id_carrier, $params['carrier']->id, $transportistas_nxshop_f_piped);
            Configuration::updateValue('NACEX_ID_TRANSPORTISTAS_NXSHOP_F', $transportistas_nxshop_f_piped);
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET ncx="nacexshop" WHERE id_carrier = "' . $params['carrier']->id . '"');
            nacexutils::writeNacexLog("hookupdateCarrier :: actualizado campo [ncx = nacexshop] del carrier");
        }

        //Por último, actualizamos el campo de tip_serv apra no perder el servicio al que hace referencia el carrier
        $tip_serv_ant = Db::getInstance()->ExecuteS('SELECT tip_serv,ncx FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier = "' . $id_carrier . '"');
        nacexutils::writeNacexLog("hookupdateCarrier :: realizando " . 'UPDATE ' . _DB_PREFIX_ . 'carrier SET tip_serv="' . $tip_serv_ant[0]["tip_serv"] . '",ncx="' . $tip_serv_ant[0]["ncx"] . '" WHERE id_carrier = "' . $params['carrier']->id . '"');
        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET tip_serv="' . $tip_serv_ant[0]["tip_serv"] . '",ncx="' . $tip_serv_ant[0]["ncx"] . '" WHERE id_carrier = "' . $params['carrier']->id . '"');

        nacexutils::writeNacexLog("FIN hookupdateCarrier :: id_carrier: " . $params['id_carrier']);
        nacexutils::writeNacexLog("---");
    }

    //No muestra nada... ?
    public function hookOrderDetail($params) {
        
    }

    public function hookOrderDetailDisplayed($params) {
        include_once("nacexWS.php");
        nacexutils::writeNacexLog("---");
        nacexutils::writeNacexLog("INI hookOrderDetailDisplayed :: id_order: " . $params['order']->id);
        //$this->_html .= "<font color='red'>>>>>>>>> hookOrderDetailDisplayed <<<<<<<</font><br>"; 		 		

        $nacexDTO = new nacexDTO();

        $id_order = $params['order']->id;
        $id_carrier = (int) $params['order']->id_carrier;
        $id_cart = (int) $params['order']->id_cart;
        $datosexpedicion = nacexDAO::getDatosExpedicionNacex($id_order);
        $expe_codigo = "";

        echo "<link type='text/css' rel='stylesheet' href='" . $_SERVER['PHP_SELF'] . "/../modules/nacex/css/nacex.css' />";


        if ($nacexDTO->isNacexShopCarrier($id_carrier)) {
            //Codigo|Alias|Nombre|Direccion|CP|Poblacion|Provincia|Telefono 			 			
            nacexutils::writeNacexLog("hookOrderDetailDisplayed :: obteniendo datos nacex shop");
            $datosnacexshop = nacexDAO::getDatosCartNacexShop($id_cart);
            $textoentreganacexshop = $this->l("Entrega Nacex!Shop");
            $this->_html .= " 
 				<script> 	
 					$('ul.address:eq(1)').addClass('bg_nacexshop');
 					$('li.address_title:eq(1)').html('" . $textoentreganacexshop . " <span style=\"float: right; font-size: smaller; margin-right: 3px;\">" . @$datosnacexshop['shop_codigo'] . "</span>'); 					
 					$('ul.address:eq(1)').append('<li><span><i>Att: ' + $('span.address_firstname:eq(0)').text() + ' ' + $('span.address_lastname:eq(0)').text() + '</i></span></li>'); 					 					 					
 				</script>			 			 			
 			";
        }

        $var_html = "";
        $ver_estado = Configuration::get('NACEX_SHOW_F_EXPE_STATE') == "SI";

        if ($ver_estado) {
            $var_html .= "<div class='row'>";
            if ((($nacexDTO->isNacexShopCarrier($id_carrier) || $nacexDTO->isNacexShopCarrier($id_carrier))) && ((!empty($datosexpedicion)) && (isset($datosexpedicion[0]["exp_cod"])))) {
                //if ((!empty($datosexpedicion)) && (isset($datosexpedicion[0]["exp_cod"]))){
                //echo "EXPE_CODIGO: " . $datosexpedicion[0]["exp_cod"] . "<br>";	 			
                // $respuestaGetEstadoExpedicion = null;
                $respuestaGetEstadoExpedicion = nacexWS::ws_getEstadoExpedicion($datosexpedicion[0]["exp_cod"], true);

                //var_dump($respuestaGetEstadoExpedicion);


                if (is_array($respuestaGetEstadoExpedicion)) {
                    if (isset($respuestaGetEstadoExpedicion)) {

                        if (isset($respuestaGetEstadoExpedicion[0]) && $respuestaGetEstadoExpedicion[0] == "ERROR") {
                            if (isset($respuestaGetEstadoExpedicion[2]) && $respuestaGetEstadoExpedicion[2] == "5611") {
                                $var_html .= "<ul align='center' class='address estado_expedicion'><h3 class='ncx_pending'>Estado de la Expedición</h3><p align='center'><b><i>Pendiente de integración</i></b></p></ul>";
                            } else {
                                $var_html .= utf8_encode("<ul align='center' class='address estado_expedicion'><h3 class='ncx_error'>Estado de la Expedición</h3><p>") . $respuestaGetEstadoExpedicion[0] . " " . $respuestaGetEstadoExpedicion[2] . "</p></ul>";
                            }
                        } else {
                            $var_html .= utf8_encode("<ul align='center' class='address estado_expedicion'><h3 class='ncx_checked'>Estado de la Expedición</h3><p><b>") . $this->l('Fecha') . ":</b> " . $respuestaGetEstadoExpedicion["fecha"] . "</p><p><b>" . $this->l('Hora') . ":</b> " . $respuestaGetEstadoExpedicion["hora"] . "</p><p><b>" . $this->l('Obs.') . ":</b> " . $respuestaGetEstadoExpedicion["observaciones"] . "</p><p><b>Estado:</b> " . $respuestaGetEstadoExpedicion["estado"] . "</p></ul>";
                        }
                    } else {
                        $var_html .= utf8_encode("<ul align='center' class='address estado_expedicion'><h3 class='ncx_question'>Estado de la Expedición</h3><p align='center'><b><i>") . $this->l('Sin datos') . "</i></b></p></ul>";
                    }
                } else {
                    $var_html .= utf8_encode("<ul align='center' class='address estado_expedicion'><h3 class='ncx_error'>Estado de la Expedición</h3>") . $respuestaGetEstadoExpedicion . "</ul>";
                }
            } else if (($nacexDTO->isNacexCarrier($id_carrier) || $nacexDTO->isNacexShopCarrier($id_carrier))) {
                $var_html .= "<ul align='center' class='address estado_expedicion'><h3 class='ncx_warning'>Estado de la Expedición</h3><i>Expedición pendiente de documentar</i></ul>";
            }
            $var_html .= "</div>";
        }



        $this->_html .= '<script>
                                    $(function() { $("div#block-order-detail div.adresses_bloc").append("' . $var_html . '");});			
				</script>';
        return $this->_html;
    }

    /* Catch product returns and substract loyalty points */

    public function hookOrderReturn($params) {
        
    }

    /* Hook display on shopping cart summary */

    public function hookShoppingCart($params) {
        
    }

    /* Hook called when a new order is created */

    public function hookNewOrder($params) {
        
    }

    /* Hook called when an order change its status */

    public function hookUpdateOrderStatus($params) {
        
    }

    public function hookPDFInvoice($params) {
        
    }

    public function hookPDFInvoice_old($params) {

        $pdf = $params['pdf'];
        $id_order = $params['id_order'];
        $datospedido = getDatosPedido($id_order);
        $nacexDTO = new nacexDTO();

        if ($nacexDTO->isNacexShopCarrier($datospedido[0]['id_carrier'])) {

            $datosnacexshop = nacexutils::explodeShopData($datospedido[0]['ncx']);

            $shopdirpdf = "";
            $shopdirpdf .= isset($datosnacexshop['shop_alias']) ? $datosnacexshop['shop_alias'] : " " . " " . isset($datosnacexshop['shop_nombre']) ? $datosnacexshop['shop_nombre'] : " \n";
            $shopdirpdf .= isset($datosnacexshop['shop_direccion']) ? $datosnacexshop['shop_direccion'] : " \n";
            $shopdirpdf .= isset($datosnacexshop['shop_cp']) ? $datosnacexshop['shop_cp'] : " " . " " . isset($datosnacexshop['shop_poblacion']) ? $datosnacexshop['shop_poblacion'] : " \n";
            $shopdirpdf .= isset($datosnacexshop['shop_provincia']) ? $datosnacexshop['shop_provincia'] : " \n";
            $shopdirpdf .= isset($datosnacexshop['shop_telefono']) ? $datosnacexshop['shop_telefono'] : " ";

            $width = 100;
            $pdf->SetX(10);
            $pdf->SetY(25);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell($width, 10, $this->l('Entrega Nacex!Shop'), 0, 'L');

            $pdf->SetX(10);
            $pdf->SetY(35);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetFillColor(255, 255, 255, 0);
            $pdf->Rect(10, 35, 100, 30, "F");
            $pdf->MultiCell($width, 6.0, $shopdirpdf, 0, 'L', 0);
            $pdf->Ln(5);
        }
    }

    public function hookBeforeCarrier($params) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI hookBeforeCarrier ::");
        $nacexDTO = new nacexDTO();

        $pagina = $_SERVER["REQUEST_URI"];
        $datosdireccion = "";
        $cp = "";
        //$att = $params['cookie']->customer_firstname . " " . $params['cookie']->customer_lastname;		
        //	Datos de la dirección del paso anterior si no es OPC

        if (!strpos($pagina, 'order-opc')) {

            nacexutils::writeNacexLog("hookBeforeCarrier :: Obteniendo datos direccion en modalidad 5 pasos");

            if ($params['cart']->id_address_delivery) {
                $tools_id_address = $params['cart']->id_address_delivery;

                try {
                    $datosdireccion = Db::getInstance()->ExecuteS(
                            "SELECT a.firstname,a.lastname,a.address1,a.postcode,a.city,a.phone,a.phone_mobile
							FROM " . _DB_PREFIX_ . "address a
							WHERE a.id_address = " . $tools_id_address
                    );
                    $cp = $datosdireccion[0]['postcode'];
                    nacexutils::writeNacexLog("hookBeforeCarrier :: Capturado CP del cliente (" . $cp . ")");
                } catch (Exception $e) {
                    
                }
            }
        }



//** mexpositop 20171115
        $array_nxshop_id_carriers = array();
        $array_id_carriers = array();

        $cart = $params['cart'];
        $carriersList = $cart->getDeliveryOptionList();
        $carriers = array_keys($carriersList [array_keys($carriersList)[0]]);


        foreach ($carriers as $key => $value) {
            if ($nacexDTO->isNacexShopCarrier(str_replace(",", "", $value))) {
                array_push($array_nxshop_id_carriers, intval($value));
            } else if ($nacexDTO->isNacexCarrier(str_replace(",", "", $value))) {
                array_push($array_id_carriers, intval($value));
            }
        }
        /*
          for ($i = 0; $i < count($carriers); $i++) {
          //$id_carrier = $carriers[$i]['id_carrier'];
          //$id_carrier = substr(Cart::desintifier($id_carrier, ","), 0, -1);
          if ($nacexDTO->isNacexShopCarrier($carriers[i])) {
          array_push($array_nxshop_id_carriers, $carriers[i]);
          } else if ($nacexDTO->isNacexCarrier($carriers[i])) {
          array_push($array_id_carriers, $carriers[i]);
          }
          } */
        //$array_nxshop_id_carriers = explode("|", Configuration::get("NACEX_ID_TRANSPORTISTAS_NXSHOP_F"));
        //$array_id_carriers = explode("|", Configuration::get("NACEX_ID_TRANSPORTISTAS_F"));
        $num_nxshops = count($array_nxshop_id_carriers);
        $num_nxs = count($array_id_carriers);


        $id_cart = $cart->id;
        $id_carrier = $cart->id_carrier;
        $id_ncx_carrier = $nacexDTO->getNacexIdCarrier();
        $id_ncxshop_carrier = $nacexDTO->getNacexShopIdCarrier();
        $isNacexShopCarrier = $nacexDTO->isNacexShopCarrier($id_carrier);

        $logoservswidth = Configuration::get("NACEX_LOGOSERVS_WIDTH");
        $logoservswidth .= isset($logoservswidth) && $logoservswidth != null && $logoservswidth != "" ? "px" : "auto";


        //echo ">>>>>>>>>>>>" . $logoservswidth;     

        /* No pinta el JS en el HTML este métod aunque existe
          if (method_exists($this->context->controller, 'addJquery')) {
          $this->context->controller->addJquery();
          $this->context->controller->addJS($this->_path . '../modules/nacex/js/nacex.js');
          }else{                          }
         * 
         */
        $this->_html .= "<script src='../modules/nacex/js/jquery-1.11.0.min.js' type='text/javascript'></script>";
        $this->_html .= "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>";


        /* Campo oculto para informar si el transportista es Nacex */
        $this->_html .= "<input type='hidden' id='nacex_carrier' name='nacex_carrier' />";
        /* Campo oculto para informar si el transportista es NacexShop y para recuperar datos nacex shop */
        $this->_html .= "<input type='hidden' id='shop_datos' name='shop_datos' />";

        $tipoOrder = strpos($this->context->controller->php_self, "order-opc") == false ? "true" : "false";
        $isGuest = $cart->isGuestCartByCartId($cart->id) ? 'true' : 'false';

        $this->_html .= "
		<script src='" . Tools::getHttpHost(true) . __PS_BASE_URI__ . "/modules/nacex/js/jquery.showModalDialog.js' type='text/javascript'></script>
<script>	
                                
var id_cart = '" . $id_cart . "';
				
var isOPC = " . $tipoOrder . ";
var isOPC2 = $(location).attr('pathname').indexOf('/onepagecheckout/') !== -1 ? true : false; //  /onepagecheckout/
var isGuest =" . $isGuest . ";
var href_opc = '';
                                
var agencias_clientes  = '" . Configuration::get("NACEX_AGCLI") . "';                                    

var num_nxs = " . $num_nxs . "
var ncx_carrier = '" . $id_ncx_carrier . "';                                    
var nxids = new Array();
";
        for ($i = 0; $i < $num_nxs; $i++) {
            $this->_html .= "nxids['" . $array_id_carriers[$i] . "']=1;";
        }
        $this->_html .= "							
var num_nxshops = " . $num_nxshops . "
var ncxshop_carrier = '" . $id_ncxshop_carrier . "';	
var nxshopids = new Array(); 
";
        for ($i = 0; $i < $num_nxshops; $i++) {
            $this->_html .= "nxshopids['" . $array_nxshop_id_carriers[$i] . "']=1;";
        }
        $this->_html .= "
            
	$(function() {
	    $('input[name^=\"delivery_option\"]').each(function() {
	        var num = $(this).val().match(/\d+/);
	        $(this).click(function() {
	            nxshopids[num] ? GetShop(num) : ClearShop();
	        });
	    });
	    codigo_postal_entrega = prestashop.customer.addresses[prestashop.cart.id_address_delivery].postcode;
	});

	function GetShop(carrier_sel) {
	    $('#order').fadeTo('fast', 0.4);
	    modalWin('" . $nacexDTO->getPath() . "nxShop.php?host=www.nacex.es&cp=' + codigo_postal_entrega + '&clientes=' + agencias_clientes);
	    $('#order').fadeTo('fast', 1);
	    return false;
	}

	function unsetDatosSession() {
	    var msg = '';
	    $.ajax({
	        type: 'POST',
	        url: '" . $nacexDTO->getPath() . "CPuntoNacexShop.php',
	        data: 'metodo_nacex=unsetSession',
	        async: false,
	        success: function(msg) {
	            //alert('unsetDatosSession: '+msg);
	        }
	    });
	}

	function setDatosSession(txt) {
	    var msg = '';
	    $.ajax({
	        type: 'POST',
	        url: '" . $nacexDTO->getPath() . "CPuntoNacexShop.php',
	        data: 'txt=' + txt + '&metodo_nacex=setSession',
	        async: false,
	        success: function(msg) {
	            //alert('setDatosSession: '+msg);
	        }
	    });
	}

	function getDatosSession() {
	    var msg = '';
	    $.ajax({
	        type: 'POST',
	        url: '" . $nacexDTO->getPath() . "CPuntoNacexShop.php',
	        data: 'metodo_nacex=getSession',
	        async: false,
	        success: function(msg) {
	            //alert('getDatosSession: '+msg);
	            rellenarNacexShop(msg);
	        }
	    });
	}

	function seleccionadoNacexShop(tipo, txt) {
            rellenarNacexShop(txt);
	    setDatosSession(txt);
	    
            var shopc = $('#nxshop_codigo').val();
	    var shopa = $('#nxshop_alias').val();
	    var shopn = $('#nxshop_nombre').val();
	    var shopd = $('#nxshop_direccion').val();
	    var shopcp= $('#nxshop_cp').val();
	    var shopp = $('#nxshop_poblacion').val();
	    var shoppr= $('#nxshop_provincia').val();
	    var shopt = $('#nxshop_telefono').val();
            
	    var msg = 'Ha elegido el siguiente punto de entrega Nacex Shop:\\n' + shopn + '(' + shopc + ') ubicado en ' + shopd + ' (' + shopcp + ') de ' + shopp + ' (' + shoppr + ')\\n\\n* Pulse Aceptar si desea recoger su pedido en este punto de entrega\\n';
	    if (confirm(msg)) {
                document.getElementById('shop_datos').value = shopc + '|' + shopa + '|' + shopn + '|' + shopd + '|' + shopcp + '|' + shopp + '|' + shoppr + '|' + shopt;
                document.cookie = 'opc_shop_datos=' +  shopc + '|' + shopa + '|' + shopn + '|' + shopd + '|' + shopcp+ '|' + shopp + '|' + shoppr + '|' + shopt;
		document.cookie = 'opc_id_cart=' + id_cart ;
              }
	        /*
                if (isOPC) {
	            document.cookie = 'opc_shop_datos=' + shopc + '|' + shopa + '|' + shopn + '|' + shopd + '|' + shopcp + '|' + shopp + '|' + shoppr + '|' + shopt;
                    document.cookie = 'opc_id_cart=' + id_cart;
	            unsetDatosSession();
	            return true;
	        } else {
	            document.getElementById('nxshop_codigo').value = '';
	            ClearShop();
	            $('#order').fadeTo('fast', 1);
	            return false;
	        }
            */
	    // if(isOPC){ updateCarrierSelectionAndGift();} NOT FOR 1.7 versions

	}

	function ClearShop() {
	    $('[id^=nxshop').val('')
	    unsetDatosSession();
	    // if(isOPC){ updateCarrierSelectionAndGift();} NOT FOR 1.7 versions
	}

	function rellenarNacexShop(txt) {
	    //1085|0831-03|LIBRERÍA OPERA|Major 7|08870|SITGES|BARCELONA|938942143
	    if (txt == null || txt.length <= 0 || txt.indexOf(\"|\")==-1){
	            return false
	        }
	        var datos = txt.split('|'); $('#nxshop_codigo').val(datos[0]); $('#nxshop_alias').val(datos[1]); $('#nxshop_nombre').val(datos[2]); $('#nxshop_direccion').val(datos[3]); $('#nxshop_cp').val(datos[4]); $('#nxshop_poblacion').val(datos[5]); $('#nxshop_provincia').val(datos[6]); $('#nxshop_telefono').val(datos[7]);
	    }

	    function modalWin(url) {
	        LeftPosition = (screen.width) ? (screen.width - 700) / 2 : 0;
	        TopPosition = (screen.height) ? (screen.height - 500) / 2 : 0;
	        ventana = window.open(url, '', 'height=550,width=820,top=' + (TopPosition - 10) + ',left=' + LeftPosition + ',toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,location=no,modal=yes');
	    }
            
    </script>	
			<style>
				td.carrier_name label img, td.delivery_option_logo img{
					width: " . $logoservswidth . ";
				}							
			</style>
			
			<div style='display:none'>
				<span id='nacex_module_version'>" . nacexutils::nacexVersion . "</span>
				<table>
				<!--1085|0831-03|LIBRERÍA OPERA|Major 7|08870|SITGES|BARCELONA|938942143-->
				<tr><td>Código</td><td><input id='nxshop_codigo' type='text'/></td></tr>
				<tr><td>Alias</td><td><input id='nxshop_alias' type='text'/></td></tr>
				<tr><td>Nombre</td><td><input id='nxshop_nombre' type='text'/></td></tr>
				<tr><td>Dirección</td><td><input id='nxshop_direccion' type='text'/></td></tr>
				<tr><td>CP</td><td><input id='nxshop_cp' type='text'/></td></tr>
				<tr><td>Población</td><td><input id='nxshop_poblacion' type='text'/></td></tr>
				<tr><td>Provincia</td><td><input id='nxshop_provincia' type='text'/></td></tr>
				<tr><td>Telefono</td><td><input id='nxshop_telefono' type='text'/></td></tr>
				</table>
			</div>";

        nacexutils::writeNacexLog("FIN hookBeforeCarrier ::");
        nacexutils::writeNacexLog("----");
        return utf8_encode($this->_html);
    }

    /* After the carrier is selected llegamos a "Elija su modo de pago". 
      Este Hook no se corresponde con ninguna pantalla, pero se ejecuta en segundo plano, sólo en código
      Necesario para modalidad 4pasos */

    public function hookProcessCarrier($params) {

        $id_cart = $params['cart']->id;
        $carrier = $params['cart']->id_carrier;
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI hookProcessCarrier :: id_cart: " . $id_cart . " | carrier: " . $carrier);
        $shop_datos = Tools::getValue('shop_datos', null);

        if ($shop_datos != null) {
            $nacexDTO = new nacexDTO();
            if ($nacexDTO->isNacexCarrier($carrier)) {
                Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'cart SET ncx="1"	WHERE id_cart = "' . $id_cart . '"');
                nacexutils::writeNacexLog("UPDATE hookProcessCarrier :: UPDATE " . _DB_PREFIX_ . "cart SET ncx=1 WHERE id_cart = " . $id_cart);
            } else if ($nacexDTO->isNacexShopCarrier($carrier) && $shop_datos) {
                Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'cart SET ncx="' . $shop_datos . '" WHERE id_cart = "' . $id_cart . '"');
                nacexutils::writeNacexLog("UPDATE hookProcessCarrier :: UPDATE " . _DB_PREFIX_ . "cart SET ncx=" . $shop_datos . " WHERE id_cart = " . $id_cart);
                //Reset cookies
                setcookie('opc_shop_datos', '', time() - 3600);
                setcookie('opc_id_cart', '', time() - 3600);
            }
        }
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("FIN hookProcessCarrier");
    }

    /* Hook que surge después de que el cliente haya confirmado el pedido. 
      Necesario para modalidad OPC */

    public function hookOrderConfirmation($params) {
        nacexutils::writeNacexLog("----");
        isset($params['order']->id) ?
                        nacexutils::writeNacexLog("INI hookOrderConfirmation :: id_order: " . $params['order']->id) :
                        nacexutils::writeNacexLog("INI hookOrderConfirmation :: id_order: NULL !!");

        //    global $cookie;
        $nacexDTO = new nacexDTO();

        $id_order = isset($params['order']->id) ? $params['order']->id : null;
        $id_cart = isset($params['order']->id_cart) ? $params['order']->id_cart : null;
        $carrier = isset($params['order']->id_carrier) ? $params['order']->id_carrier : null;
        $id_address_delivery = isset($params['order']->id_address_delivery) ? $params['order']->id_address_delivery : null;
        //   $nacex_carrier = null;
        $shop_datos = 0;
        if ($nacexDTO->isNacexCarrier($carrier)) {
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'cart SET ncx="1"	WHERE id_cart = "' . $id_cart . '"');
            nacexutils::writeNacexLog("hookOrderConfirmation :: actualizado campo nacex [ncx=1] de la tabla cart.");
        } else if ($nacexDTO->isNacexShopCarrier($carrier)) {
            nacexutils::writeNacexLog("hookOrderConfirmation :: isNacexShopCarrier");
            $shop_datos = "";

            //Modalidad OPC													
            if (isset($_COOKIE['opc_id_cart']) && isset($_COOKIE['opc_shop_datos']) && $_COOKIE['opc_id_cart'] == $id_cart) {
                $shop_datos = $_COOKIE['opc_shop_datos'];
                //Borramos las dos cookies estableciendo fecha de caducidad en el pasado
                setcookie('opc_shop_datos', '', time() - 3600);
                setcookie('opc_id_cart', '', time() - 3600);

                Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'cart SET ncx="' . $shop_datos . '" WHERE id_cart = "' . $id_cart . '"');
                nacexutils::writeNacexLog("hookOrderConfirmation :: actualizado campo nacex [ncx=" . $shop_datos . "] de la tabla cart.");
            } else {
                $ret = Db::getInstance()->ExecuteS('SELECT ncx FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = "' . $id_cart . '"');
                $shop_datos = $ret[0]['ncx'];
                nacexutils::writeNacexLog("hookOrderConfirmation :: shop_datos obtenidos -" . $shop_datos);
            }

            //--------------------------------------------------------------------------------------------
            nacexDAO::setNacexShopAddressinBD($id_order, $id_cart, $id_address_delivery, $shop_datos);
            //--------------------------------------------------------------------------------------------					
        }

        isset($params['order']->id) ?
                        nacexutils::writeNacexLog("FIN hookOrderConfirmation :: id_order: " . $params['order']->id) :
                        nacexutils::writeNacexLog("FIN hookOrderConfirmation :: id_order: NULL!!");

        nacexutils::writeNacexLog("----");
    }

    public function hookAdminOrder($params) {
        include_once("nacexWS.php");
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI hookAdminOrder :: id_order: " . $params['id_order']);

        $id_order = (int) $params['id_order'];
        $datospedido = nacexDAO::getDatosPedido($id_order);

        //El transportista es de Nacex o por configuración ha elegido forzar mostrar formulario Generar Expedición
        if (( isset($datospedido[0]['ncx']) && $datospedido[0]['ncx'] != "" ) ||
                ( isset($datospedido[0]['id_carrier']) && $datospedido[0]['id_carrier'] == nacexDTO::getNacexIdCarrier() ) ||
                nacexDTO::isNcxCarrier($datospedido[0]['id_carrier']) ||
                Configuration::get('NACEX_FORCE_GENFORM') == "SI") {

            echo "<link type='text/css' rel='stylesheet' href='../modules/nacex/css/nacex.css' />";
            echo "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>";

            nacexutils::changeAddressIfNacexShop($datospedido[0]);
            $exp = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'nacex_expediciones where fecha_baja IS NULL AND id_envio_order = "' . $id_order . '"');
            // echo "DEBUG: " . dump($exp);

            $referenciaNacex = (Configuration::get("NACEX_REF_PERS_PREFIJO") != "" ? Configuration::get("NACEX_REF_PERS_PREFIJO") : "Pedido ") . $id_order;  // $reference
            if (strpos($referenciaNacex, '$reference') !== false) {
                $referenciaNacex = $datospedido[0]['reference'];
            }
            //Miramos si existe y no está dada de baja
            if (!isset($exp[0]['id_envio_order']) || trim($exp[0]['id_envio_order']) === '') {

                nacexutils::writeNacexLog("hookAdminOrder :: expedicion NO encontrada en BD, mostramos showExpedicionForm, showExpedicionForm 1");
                if (Tools::isSubmit('submitputexpedicion')) {
                    nacexutils::writeNacexLog("hookAdminOrder :: submitputexpedicion, creamos expedicion");

                    if (!nacexWS::putExpedicion($id_order, $datospedido, null, Tools::isSubmit('submitputexpedicion'), $referenciaNacex))
                        $this->_html = nacexVIEW::showExpedicionForm($id_order, $datospedido, null, Tools::isSubmit('submitcambioexpedicion'), $referenciaNacex);

                    nacexutils::writeNacexLog("FIN hookAdminOrder :: id_order: " . $params['id_order'] . " showExpedicionForm 1");
                    nacexutils::writeNacexLog("----");
                    return $this->_html;
                }
                $this->_html = nacexVIEW::showExpedicionForm($id_order, $datospedido, null, Tools::isSubmit('submitcambioexpedicion'), $referenciaNacex);
                return $this->_html;
            }

            nacexutils::writeNacexLog("hookAdminOrder :: expedicion encontrada y activa en BD, datos recuperados");

            if (Tools::isSubmit('submitcambioexpedicion')) {
                nacexutils::writeNacexLog("hookAdminOrder :: submitcambioexpedicion, showExpedicionForm 2");

                $this->_html = $this->showExpedicionForm($id_order, $datospedido, $exp[0], Tools::isSubmit('submitcambioexpedicion'));

                nacexutils::writeNacexLog("FIN hookAdminOrder :: id_order: " . $params['id_order'] . " showExpedicionForm 2");
                nacexutils::writeNacexLog("----");
                return $this->_html;
            }
            /*
              if (Tools::isSubmit('submitputexpedicion')) {
              nacexutils::writeNacexLog("hookAdminOrder :: expedicion encontrada en BD, lanzamos putExpedicion");

              nacexWS::putExpedicion($id_order, $datospedido, $exp[0], Tools::isSubmit('submitputexpedicion'));

              $this->_html = nacexVIEW::showExpedicionForm($id_order, $datospedido, $exp[0], Tools::isSubmit('submitputexpedicion'));

              nacexutils::writeNacexLog("FIN hookAdminOrder :: id_order: " . $params['id_order']." showExpedicionForm 3");
              nacexutils::writeNacexLog("----");
              return $this->_html;

              //Si existe la Expedición mostramos el box con la info
              }
             */
            nacexVIEW::showExpedicionBoxInfo($exp[0], $id_order);

            if (!empty($_POST) && Tools::isSubmit('submitcancelexpedicion')) {
                return nacexWS::cancelExpedicion($id_order, $exp[0]["exp_cod"]);
            }
        }
        return $this->_html;
    }

    /**
     * Instalación / Configuración
     * El simple hecho de añadir esta función provoca la aparición de un enlace
     * "Configurar" en la instalación realizada del módulo.
     */
    public function getContent() {

        $this->_html .= '<h2>' . $this->l('NACEX') . '</h2>';
        if (!empty($_POST) AND Tools::isSubmit('submitSave')) {
            $this->_postValidation();
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else
                foreach ($this->_postErrors AS $err)
                    $this->_html .= '<div class="alert error"><img src="' . _PS_IMG_ . 'admin/forbbiden.gif" alt="nok" />&nbsp;' . $err . '</div>';
        }
        $this->_displayForm();
        return $this->_html;
    }

    private function _displayForm() {
        $this->_html = getFormularioConfiguracion($this);
    }

    public function getErroresConfiguracion() {
        return $this->_confErrors;
    }

    private function _postValidation() {

        $this->_confErrors = validarFormularioConfiguracion($this);
        // Check configuration values
        /* if(Tools::getValue('nacex_agcli') == '' &&
          Tools::getValue('nacex_wspassword') == '' &&
          Tools::getValue('nacex_wsusername') == '' &&
          Tools::getValue('nacex_print_url') == '' &&
          Tools::getValue('nacex_print_model') == '' &&
          Tools::getValue('nacex_print_et') == '' &&
          Tools::getValue('nacex_ws_url') == '') */
        if (!empty($this->_confErrors) && count($this->_confErrors) > 0) {
            $this->_postErrors[] = $this->l(utf8_encode('Error al guardar la configuraci&oacute;n del m&oacute;dulo.'));
        }
    }

    private function _postProcess() {

        // Saving new configurations
        //RECONSTRUIMOS TABs NACEX EN CADA CONFIGURACIÓN
        Db::getInstance()->execute('DELETE FROM ' . _DB_PREFIX_ . 'tab WHERE module = "' . $this->_moduleName . '"');

        $tab = new Tab();
        $tab->class_name = 'nacextab';
        $tab->id_parent = Tab::getIdFromClassName("AdminParentOrders");
        $tab->position = 99;
        $tab->module = $this->_moduleName;
        $tab->name[(int) (Configuration::get('PS_LANG_DEFAULT'))] = 'Nacex';
        $tab2 = new Tab();
        $tab2->class_name = 'nacextabMasivo';
        $tab2->id_parent = Tab::getIdFromClassName("AdminParentOrders");
        $tab2->position = 99;
        $tab2->module = $this->_moduleName;
        $tab2->name[(int) (Configuration::get('PS_LANG_DEFAULT'))] = 'Nacex Masivo';

        $tab->save();
        $tab2->save();


        $result = guardarConfiguracion();

        if ($result) {

            nacexDAO::setTransportistasBackend();
            nacexDAO::setTransportistasFrontend();
            Configuration::get('NACEX_SERV_BACK_OR_FRONT') == 'B' ?
                            nacexDAO::activarServicios("backend") :
                            nacexDAO::activarServicios("frontend");
            $this->_html .= $this->displayConfirmation('Configuración actualizada');
        } else {
            $this->_html .= $this->displayErrors('Configuración fallida');
        }
    }

    //Métodos abstractos dependientes de la clase padre CarrierModule
    // CART
    /**
     * @deprecated 1.5.0, use Cart->getPackageShippingCost() ????
     */
    //Implementación métodos abstractos heredados

    public function getOrderShippingCost($params, $shipping_cost) {
        // metodo deprecado y que no devuelve nada en 1.7, tal como está
        global $cookie;
        $nacexDTO = new nacexDTO();
        $id_carrier = $this->id_carrier; // no valido en 1.7 revisar _carriers

        $total = $params->getOrderTotal(true, Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);

        $carrier = nacexDAO::getCarrierById($id_carrier);
        $aplicar_gastos_manipulacion = Configuration::get("NACEX_GASTOS_MANIPULACION") == "SI";
        $gastos_manipulacion_val = Configuration::get("NACEX_GASTOS_MANIPULACION_VAL");

        $importe_fijo_nacex = Configuration::get('NACEX_IMP_FIJO');
        $importe_fijo_nacex_valor = floatval(str_replace(",", ".", Configuration::get('NACEX_IMP_FIJO_VAL')));
        $importe_fijo_nacexshop = Configuration::get('NACEXSHOP_IMP_FIJO');
        $importe_fijo_nacexshop_valor = floatval(str_replace(",", ".", Configuration::get('NACEXSHOP_IMP_FIJO_VAL')));
        $calcular_ws_importe = Configuration::get('NACEX_WS_IMPORTE');

        $nacex_importe_min_grat = Configuration::get('NACEX_IMP_MIN_GRAT') == "SI";
        $nacex_importe_min_grat_val = Configuration::get('NACEX_IMP_MIN_GRAT_VAL');
        $nacexshop_importe_min_grat = Configuration::get('NACEXSHOP_IMP_MIN_GRAT') == "SI";
        $nacexshop_importe_min_grat_val = Configuration::get('NACEXSHOP_IMP_MIN_GRAT_VAL');

        $nacex_mostrar_coste_0 = Configuration::get('NACEX_MOSTRAR_COSTE_0') == "SI";

        //PARA SERVICIOS NACEX ESTANDARD
        if ($nacexDTO->isNacexCarrier($id_carrier)) {

            //Miramos si tiene definido un importe fijo
            if ($importe_fijo_nacex == "SI") {
                $shipping_cost = number_format($importe_fijo_nacex_valor, 2, ".", "");
                nacexutils::writeNacexLog("getOrderShippingCost :: Transportista con importe fijo (Carrier:" . $id_carrier . " => " . $shipping_cost . " euros");

                //Si no tiene importe fijo, miramos si hay que calcular el precio mediante WS
            } else {

                //Miramos si el transportista es gratuito dado el 'importe minimo gratiuto'
                if ($nacex_importe_min_grat && $total >= $nacex_importe_min_grat_val) {
                    $shipping_cost = 0;
                    $calcular_ws_importe = "NO";
                    nacexutils::writeNacexLog("getOrderShippingCost :: Transportista gratuito dado el importe min. grat.(Carrier:" . $id_carrier . " => 0 euros");
                }
                //Miramos si hay que calcular el precio mediante WS
                if ($calcular_ws_importe == "SI") {
                    $shipping_cost = nacexDAO::getImporteWebservice($params, $shipping_cost, $id_carrier);
                    //Sólo en el caso de calcular el importe por webservice añadimos gastos de manipulacion (si es necesario)
                    if ($aplicar_gastos_manipulacion) {
                        $shipping_cost += $gastos_manipulacion_val;
                        nacexutils::writeNacexLog("getOrderShippingCost :: Aplicamos gastos de manipulacion (Carrier:" . $id_carrier . " => +" . $gastos_manipulacion_val . " euros ");
                    }
                }
            }
        }

        //PARA SERVICIOS NACEXSHOP
        else {
            if ($nacexDTO->isNacexShopCarrier($id_carrier)) {

                //Miramos si tiene definido un importe fijo
                if ($importe_fijo_nacexshop == "SI") {
                    $shipping_cost = number_format($importe_fijo_nacexshop_valor, 2, ".", "");
                    nacexutils::writeNacexLog("getOrderShippingCost :: Transportista con importe fijo (Carrier:" . $id_carrier . " => " . $shipping_cost . " euros ");

                    //Si no tiene importe fijo, miramos si hay que calcular el precio mediante WS
                } else {

                    //Miramos si el transportista es gratuito dado el 'importe minimo gratiuto'
                    if ($nacexshop_importe_min_grat && $total >= $nacexshop_importe_min_grat_val) {
                        $shipping_cost = 0;
                        $calcular_ws_importe = "NO";
                        nacexutils::writeNacexLog("getOrderShippingCost :: Transportista gratuito dado el importe min. grat.(Carrier:" . $id_carrier . " => 0 euros ");
                    }
                    //Miramos si hay que calcular el precio mediante WS			
                    if ($calcular_ws_importe == "SI") {
                        $shipping_cost = nacexDAO::getImporteWebservice($params, $shipping_cost, $id_carrier);
                        //Sólo en el caso de calcular el importe por webservice añadimos gastos de manipulacion (si es necesario)
                        if ($aplicar_gastos_manipulacion) {
                            $shipping_cost += $gastos_manipulacion_val;
                            nacexutils::writeNacexLog("getOrderShippingCost :: Aplicamos gastos de manipulacion (Carrier:" . $id_carrier . " => +" . $gastos_manipulacion_val . " euros ");
                        }
                    }
                }
            }
        }

        //Comprobamos si hay que mostrar los transportistas con coste 0€
        if (!$nacex_mostrar_coste_0 && $shipping_cost <= 0) {
            nacexutils::writeNacexLog("getOrderShippingCost :: Carrier descartado debido coste 0 Euros(Carrier:" . $id_carrier . " => " . $shipping_cost . " Euros");
            return false;
        } else {
            return $shipping_cost;
        }
    }

    public function getOrderShippingCostExternal($params) {
        return calculateShippingCostForCarrierId($this->id_carrier);
    }

}
