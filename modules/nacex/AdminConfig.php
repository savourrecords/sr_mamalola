<?php
	
	function getFormularioConfiguracion($obj){
                                   
			$nacex = new nacex();
			$nacexDTO = new nacexDTO();
                        
                        $errores = $obj->getErroresConfiguracion();
                        $divpayment = "";
			$paymentModules = Module::getPaymentModules();
															
			
			if($paymentModules){
				$titulo = "Se ha detectado los siguientes módulos de pago:";
				$mensaje = "";
				foreach ($paymentModules as $module){
					$mensaje .= " - ".$module["name"];
				}
				$divpayment = showDivInfo("info_nacex_modulos_reembolso", $titulo,$mensaje);
			}
			
			
			$link_log = "";
			$config_pdf = __PS_BASE_URI__."modules/nacex/docs/Nacex_Prestashop_Configuracion.pdf";
			$archivo_log = $_SERVER["DOCUMENT_ROOT"].__PS_BASE_URI__."modules/nacex/log/"."nacex_".date("Ymd").".log";
	    $url_log = __PS_BASE_URI__."modules/nacex/log/"."nacex_".date("Ymd").".log";
			if (file_exists($archivo_log)) {
	    	$link_log = "<a href=\"$url_log\" target=\"_blank\"><img src=\"".$nacexDTO->getPath()."/img/lupa.gif\" title=\"Ver archivo de log\"  alt=\"Ver archivo de log\" style=\"right: 18px; position: relative;\"></a>";	
	    }
			
			$html = "<link type='text/css' rel='stylesheet' href='../modules/nacex/css/nacex.css' />";
			$html .= "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>";
			
			$html .=  '<script type="text/javascript">
									$(document).ready(function(){
										$("#configForm").on("keypress", function(e) {
							  				var code = e.keyCode || e.which; 
							  				if (code  == 13) {               
							    				e.preventDefault();
							    				return false;
							  				}
										});
									});
									function disableValor(obj_name){
										$(\'input[name="\'+obj_name+\'"]\').val("");
										$(\'input[name="\'+obj_name+\'"]\').prop("disabled", true);
									}
									function enableValor(obj_name){
										$(\'input[name="\'+obj_name+\'"]\').prop("disabled", false);
										$(\'input[name="\'+obj_name+\'"]\').focus();
									}
									function disablePrealerta(){
										$(\'input[name="nacex_preal_plus_txt"]\').val("");
										$(\'input[name="nacex_preal_plus_txt"]\').prop("disabled", true);	
										$(\'input[name="nacex_mod_preal"]\').each(function(i) {
											if($(this).val()=="S"){
												$(this).prop("checked", true);
											}	
											$(this).prop("disabled", true);	
										});
									}
									function enablePrealerta(){
										$(\'input[name="nacex_mod_preal"]\').each(function(i) {
												$(this).prop("disabled", false);	
										});
									}
									function disableImpMinGrat(obj1, obj2){
										$(\'input[name="\'+obj2+\'"]\').val("");	
										$(\'input[name="\'+obj1+\'"][value="NO"]\').prop("checked",true);
										$(\'input[name="\'+obj1+\'"]\').prop("disabled", true);
										$(\'input[name="\'+obj2+\'"]\').prop("disabled", true);
									}
									function enableImpMinGrat(obj1, obj2){
										$(\'input[name="\'+obj1+\'"][value="NO"]\').prop("checked",true);
										$(\'input[name="\'+obj1+\'"]\').prop("disabled", false);										
									}
								 </script>';
			
			if (Tools::isSubmit('submitSave')) {
				if (!count($errores)){
					$html .= '<div class="bootstrap" style="margin-top:10px"><div class="alert alert-success conf" style="width:auto">';	
					$html .= '<strong>Módulo Nacex configurado y online!</strong>';
					$html .= '</div></div>';
				}else{
					$keys = array_keys($errores);
					$html .= '<div class="bootstrap" style="margin-top:10px"><div class="alert alert-danger error" style="width:auto">';
					$html .= '<strong>Módulo Nacex no está configurado aún!</strong><br>';
					$html .= 'Por favor, revise los posibles errores y vuelva a guardar la configuración.';
					$html .= '</div></div>';	
					
					//Posiciona el scroll y el foco en el primer campo erróneo
					$html .= '<script type="text/javascript">
											$(document).ready(function(){
												$("html, body").animate({scrollTop: ($("#'.$keys[0].'").offset().top)-100},600);
												$("input[name=\"'.$keys[0].'\"]").focus();												
											});
										</script>';											
				}
			}

			//Mediante este código validamos que la URL de impresión por defecto sea http://www.nacex.es/applets
			$url_imp = Tools::getValue('nacex_print_url');
			if(!$url_imp){
				Configuration::updateValue('NACEX_PRINT_URL', $nacexDTO->getURL_PRO_Applets());				
			}

                        
			// Selección servicios desde el Frontend o desde el Backend
			$serv_back_or_front_B = "checked=\"checked\"";
			$serv_back_or_front_F = "";
			if(Tools::getValue('nacex_serv_back_or_front', Configuration::get('NACEX_SERV_BACK_OR_FRONT')) == "B") {
				$serv_back_or_front_B = "checked=\"checked\"";
				$serv_back_or_front_F = "";
			}else if(Tools::getValue('nacex_serv_back_or_front', Configuration::get('NACEX_SERV_BACK_OR_FRONT')) == "F") {
				$serv_back_or_front_F = "checked=\"checked\"";
				$serv_back_or_front_B = "";
			}	
			
			// Mostrar estado en la visualización del pedido por parte del cliente desde el Frontend
			$show_f_expe_state_no = "";
			$show_f_expe_state_si = "checked=\"checked\"";
			if(Tools::getValue('nacex_show_f_expe_state', Configuration::get('NACEX_SHOW_F_EXPE_STATE')) == "SI") {
				$show_f_expe_state_si = "checked=\"checked\"";
				$show_f_expe_state_no = "";
			}else if(Tools::getValue('nacex_show_f_expe_state', Configuration::get('NACEX_SHOW_F_EXPE_STATE')) == "NO") {
				$show_f_expe_state_no = "checked=\"checked\"";
				$show_f_expe_state_si = "";
			}
			
			// Actualizar atuomáticamente el tracking en el pedido
			$act_tracking_no = "checked=\"checked\"";
			$act_tracking_si = "";
			if(Tools::getValue('nacex_act_tracking', Configuration::get('NACEX_ACT_TRACKING')) == "SI") {
				$act_tracking_si = "checked=\"checked\"";
				$act_tracking_no = "";
			}else if(Tools::getValue('nacex_act_tracking', Configuration::get('NACEX_ACT_TRACKING')) == "NO") {
				$act_tracking_no = "checked=\"checked\"";
				$act_tracking_si = "";
			}
			
			//Importe fijo servicios Nacex
			$nacex_importe_fijo_no = "checked=\"checked\"";
			$nacex_importe_fijo_si = "";
			$nacex_importe_fijo_DIS = "";
			$nacex_importe_min_radio_DIS = "";
			if(Tools::getValue('nacex_importe_fijo', Configuration::get('NACEX_IMP_FIJO')) == "SI") {
				$nacex_importe_fijo_si = "checked=\"checked\"";
				$nacex_importe_fijo_no = "";
				$nacex_importe_fijo_DIS = "";
				$nacex_importe_min_radio_DIS = "disabled='true'";
				$nacex_importe_min_grat_DIS = "disabled='true'";
			}else if(Tools::getValue('nacex_importe_fijo', Configuration::get('NACEX_IMP_FIJO')) == "NO") {
				$nacex_importe_fijo_no = "checked=\"checked\"";
				$nacex_importe_fijo_si = "";
				$nacex_importe_fijo_DIS = "disabled='true'";
			}else{
				$nacex_importe_fijo_no = "checked=\"checked\"";
				$nacex_importe_fijo_si = "";
				$nacex_importe_fijo_DIS = "disabled='true'";
			}
			
			$divInfoImpFijo = showDivInfo("info_nacex_importe_fijo",
                        	$obj->l("Importe fijo Nacex:"),
                                $obj->l("El importe fijo será el importe final del envío. No se tendrán en cuenta los gastos de manipulación y/o posibles descuentos."));
			
			//Importe minimo gratuito servicios Nacex
			$nacex_importe_min_grat_no = "checked=\"checked\"";
			$nacex_importe_min_grat_si = "";
			$nacex_importe_min_grat_DIS = "";
			if(Tools::getValue('nacex_importe_min_grat', Configuration::get('NACEX_IMP_MIN_GRAT')) == "SI") {
				$nacex_importe_min_grat_si = "checked=\"checked\"";
				$nacex_importe_min_grat_no = "";
				$nacex_importe_min_grat_DIS = "";
			}else if(Tools::getValue('nacex_importe_min_grat', Configuration::get('NACEX_IMP_MIN_GRAT')) == "NO") {
				$nacex_importe_min_grat_no = "checked=\"checked\"";
				$nacex_importe_min_grat_si = "";
				$nacex_importe_min_grat_DIS = "disabled='true'";
			}else{
				$nacex_importe_min_grat_no = "checked=\"checked\"";
				$nacex_importe_min_grat_si = "";
				$nacex_importe_min_grat_DIS = "disabled='true'";
			}
			$divInfoImpMinGrat = showDivInfo("info_nacex_importe_min_grat",
																			 $obj->l("Importe mánimo gratuito:"),
																			 $obj->l("El importe mínimo gratuito sólo será aplicado a los transportistas NACEX y se descartará la configuración del transportista en Prestashop."));
			
			//Importe fijo servicios NacexShop
			$nacexshop_importe_fijo_no = "checked=\"checked\"";
			$nacexshop_importe_fijo_si = "";
			$nacexshop_importe_fijo_DIS = "";
			$nacexshop_importe_min_radio_DIS = "";
			if(Tools::getValue('nacexshop_importe_fijo', Configuration::get('NACEXSHOP_IMP_FIJO')) == "SI") {
				$nacexshop_importe_fijo_si = "checked=\"checked\"";
				$nacexshop_importe_fijo_no = "";
				$nacexshop_importe_fijo_DIS = "";
				$nacexshop_importe_min_grat_DIS = "disabled='true'";
				$nacexshop_importe_min_radio_DIS = "disabled='true'";
			}else if(Tools::getValue('nacexshop_importe_fijo', Configuration::get('NACEXSHOP_IMP_FIJO')) == "NO") {
				$nacexshop_importe_fijo_no = "checked=\"checked\"";
				$nacexshop_importe_fijo_si = "";
				$nacexshop_importe_fijo_DIS = "disabled='true'";				
			}else{
				$nacexshop_importe_fijo_no = "checked=\"checked\"";
				$nacexshop_importe_fijo_si = "";
				$nacexshop_importe_fijo_DIS = "disabled='true'";
			}
			$divInfoImpFijoShop = showDivInfo("info_nacexshop_importe_fijo",
			$obj->l("Importe fijo NacexShop:"),
			$obj->l("El importe fijo será el importe final del envío. No se tendrán en cuenta los gastos de manipulación y/o posibles descuentos."));

			
			//Importe minimo gratuito servicios NacexShop
			$nacexshop_importe_min_grat_no = "checked=\"checked\"";
			$nacexshop_importe_min_grat_si = "";
			$nacexshop_importe_min_grat_DIS = "";
			if(Tools::getValue('nacexshop_importe_min_grat', Configuration::get('NACEXSHOP_IMP_MIN_GRAT')) == "SI") {
				$nacexshop_importe_min_grat_si = "checked=\"checked\"";
				$nacexshop_importe_min_grat_no = "";
				$nacexshop_importe_min_grat_DIS = "";
			}else if(Tools::getValue('nacexshop_importe_min_grat', Configuration::get('NACEXSHOP_IMP_MIN_GRAT')) == "NO") {
				$nacexshop_importe_min_grat_no = "checked=\"checked\"";
				$nacexshop_importe_min_grat_si = "";
				$nacexshop_importe_min_grat_DIS = "disabled='true'";
			}else{
				$nacexshop_importe_min_grat_no = "checked=\"checked\"";
				$nacexshop_importe_min_grat_si = "";
				$nacexshop_importe_min_grat_DIS = "disabled='true'";
			}
			
			$divInfoImpMinGratShop = showDivInfo("info_nacexshop_importe_min_grat",
																			 		$obj->l("Importe mínimo gratuito:"),
																			 		$obj->l("El importe mínimo gratuito sólo será aplicado a los transportistas NACEXSHOP y se descartará la configuración del transportista en Prestashop."));																			 		
			// Tipo de cobro
			$tip_cob_01 = "checked=\"checked\"";
			$tip_cob_02 = "";
			$tip_cob_03 = "";
			if(Tools::getValue('nacex_tip_cob', Configuration::get('NACEX_TIP_COB')) == "O") {
				$tip_cob_01 = "checked=\"checked\"";
				$tip_cob_02 = "";
				$tip_cob_03 = "";			
			}else if(Tools::getValue('nacex_tip_cob', Configuration::get('NACEX_TIP_COB')) == "D") {
				$tip_cob_02 = "checked=\"checked\"";
				$tip_cob_01 = "";
				$tip_cob_03 = "";			
			}else if(Tools::getValue('nacex_tip_cob', Configuration::get('NACEX_TIP_COB')) == "T") {
				$tip_cob_03 = "checked=\"checked\"";
				$tip_cob_01 = "";
				$tip_cob_02 = "";
			}
			
			// Tipo de Reembolso
			$tip_ree_01 = "";
			$tip_ree_02 = "checked=\"checked\"";
			$tip_ree_03 = "";
			if(Tools::getValue('nacex_tip_ree', Configuration::get('NACEX_TIP_REE')) == "O") {
				$tip_ree_01 = "checked=\"checked\"";			
				$tip_ree_02 = "";
				$tip_ree_03 = "";			
			}else if(Tools::getValue('nacex_tip_ree', Configuration::get('NACEX_TIP_REE')) == "D") {
				$tip_ree_02 = "checked=\"checked\"";
				$tip_ree_01 = "";
				$tip_ree_03 = "";			
			}else if(Tools::getValue('nacex_tip_ree', Configuration::get('NACEX_TIP_REE')) == "T") {
				$tip_ree_03 = "checked=\"checked\"";
				$tip_ree_01 = "";
				$tip_ree_02 = "";			
			}		
			
			// Tipo de envase
			$tip_env_docs = "";
			$tip_env_bag = "checked=\"checked\"";
			$tip_env_paq = "";
			
			$tip_env_docs = Tools::getValue('nacex_tip_env', Configuration::get('NACEX_TIP_ENV')) == "0" ?	"checked=\"checked\"" : "";
			$tip_env_bag  =	Tools::getValue('nacex_tip_env', Configuration::get('NACEX_TIP_ENV')) == "1" ?  "checked=\"checked\"" : "";
			$tip_env_paq  = Tools::getValue('nacex_tip_env', Configuration::get('NACEX_TIP_ENV')) == "2" ?  "checked=\"checked\"" : "";

			$tip_env_mu =  Tools::getValue('nacex_tip_env_int', Configuration::get('NACEX_TIP_ENV_INT')) == "M" ?  "checked=\"checked\"" : "";
			$tip_env_do =  Tools::getValue('nacex_tip_env_int', Configuration::get('NACEX_TIP_ENV_INT')) == "D" ?  "checked=\"checked\"" : "";
			
			// Informar en intrucciones adicionales
			$ins_adi_q_r_no = "checked=\"checked\"";
			$ins_adi_q_r_si = "";
			if(Tools::getValue('nacex_ins_adi_q_r', Configuration::get('NACEX_INS_ADI_Q_R')) == "SI") {
				$ins_adi_q_r_si = "checked=\"checked\"";
				$ins_adi_q_r_no = "";
			}else if(Tools::getValue('nacex_ins_adi_q_r', Configuration::get('NACEX_INS_ADI_Q_R')) == "NO") {
				$ins_adi_q_r_no = "checked=\"checked\"";
				$ins_adi_q_r_si = "";
			}
			
			// Añadir Instrucciones Adicionales Personalizadas a la etiqueta
			$ins_adi_pers_no = "checked=\"checked\"";
			$ins_adi_pers_si = "";
			$ins_adi_pers_DIS = "disabled='true'";
			if(Tools::getValue('ins_adi_pers', Configuration::get('NACEX_INST_PERS')) == "SI") {
				$ins_adi_pers_si = "checked=\"checked\"";
				$ins_adi_pers_no = "";
				$ins_adi_pers_DIS = "";
			}else if(Tools::getValue('ins_adi_pers', Configuration::get('NACEX_INST_PERS')) == "NO") {
				$ins_adi_pers_no = "checked=\"checked\"";
				$ins_adi_pers_si = "";
				$ins_adi_pers_DIS = "disabled='true'";
			}
			
			// Envio con retorno
			$ret_no = "checked=\"checked\"";
			$ret_si = "";
			if(Tools::getValue('nacex_ret', Configuration::get('NACEX_RET')) == "SI") {
				$ret_si = "checked=\"checked\"";
				$ret_no = "";
			}else if(Tools::getValue('nacex_ret', Configuration::get('NACEX_RET')) == "NO") {
				$ret_no = "checked=\"checked\"";
				$ret_si = "";
			}
			
			//Referencia personaliada Si/NO
			$nacex_ref_pers_no = "checked=\"checked\"";
			$nacex_ref_pers_si = "";
			$nacex_ref_pers_DIS = "";
			if(Tools::getValue('nacex_ref_pers', Configuration::get('NACEX_REF_PERS')) == "SI") {
				$nacex_ref_pers_si = "checked=\"checked\"";
				$nacex_ref_pers_no = "";
				$nacex_ref_pers_DIS = "";
			}else if(Tools::getValue('nacex_ref_pers', Configuration::get('NACEX_REF_PERS')) == "NO") {
				$nacex_ref_pers_no = "checked=\"checked\"";
				$nacex_ref_pers_si = "";
				$nacex_ref_pers_DIS = "disabled='true'";
			}else{
				$nacex_ref_pers_no = "checked=\"checked\"";
				$nacex_ref_pers_si = "";
				$nacex_ref_pers_DIS = "disabled='true'";
			}
			
			//Importe seguro
			$nacex_default_imp_seg_DIS = "";
			if(Tools::getValue('nacex_default_tip_seg', Configuration::get('NACEX_DEFAULT_TIP_SEG')) == "N") {
				$nacex_default_imp_seg_DIS = "disabled='true'";
			}
			
			//Prealerta por defecto
			$nacex_tip_preal_s = "";
			$nacex_tip_preal_n = "checked=\"checked\"";
			$nacex_tip_preal_e = "";
			$nacex_tip_preal_DIS = "";
			if(Tools::getValue('nacex_tip_preal', Configuration::get('NACEX_TIP_PREAL')) == "N") {
				$nacex_tip_preal_n = "checked=\"checked\"";			
				$nacex_tip_preal_s = "";
				$nacex_tip_preal_e = "";
				$nacex_tip_preal_DIS = "disabled='true'";				
			}else if(Tools::getValue('nacex_tip_preal', Configuration::get('NACEX_TIP_PREAL')) == "S") {
				$nacex_tip_preal_n = "";
				$nacex_tip_preal_s = "checked=\"checked\"";
				$nacex_tip_preal_e = "";
				$nacex_tip_preal_DIS = "";			
			}else if(Tools::getValue('nacex_tip_preal', Configuration::get('NACEX_TIP_PREAL')) == "E") {
				$nacex_tip_preal_n = "";
				$nacex_tip_preal_s = "";
				$nacex_tip_preal_e = "checked=\"checked\"";
				$nacex_tip_preal_DIS = "";			
			}else{
				$nacex_tip_preal_n = "checked=\"checked\"";			
				$nacex_tip_preal_s = "";
				$nacex_tip_preal_e = "";
				$nacex_tip_preal_DIS = "disabled='true'";	
			}
			
			//Modo prealerta por defecto
			$nacex_mod_preal_s = "checked=\"checked\"";
			$nacex_mod_preal_p = "";
			$nacex_mod_preal_r = "";
			$nacex_mod_preal_e = "";
			if(Tools::getValue('nacex_mod_preal', Configuration::get('NACEX_MOD_PREAL')) == "S") {
				$nacex_mod_preal_s = "checked=\"checked\"";
				$nacex_mod_preal_p = "";
				$nacex_mod_preal_r = "";
				$nacex_mod_preal_e = "";			
			}else if(Tools::getValue('nacex_mod_preal', Configuration::get('NACEX_MOD_PREAL')) == "P") {
				$nacex_mod_preal_s = "";
				$nacex_mod_preal_p = "checked=\"checked\"";
				$nacex_mod_preal_r = "";
				$nacex_mod_preal_e = "";			
			}else if(Tools::getValue('nacex_mod_preal', Configuration::get('NACEX_MOD_PREAL')) == "R") {
				$nacex_mod_preal_s = "";
				$nacex_mod_preal_p = "";
				$nacex_mod_preal_r = "checked=\"checked\"";
				$nacex_mod_preal_e = "";			
			}else if(Tools::getValue('nacex_mod_preal', Configuration::get('NACEX_MOD_PREAL')) == "E") {
				$nacex_mod_preal_s = "";
				$nacex_mod_preal_p = "";
				$nacex_mod_preal_r = "";
				$nacex_mod_preal_e = "checked=\"checked\"";			
			}else{
				$nacex_mod_preal_s = "checked=\"checked\"";
				$nacex_mod_preal_p = "";
				$nacex_mod_preal_r = "";
				$nacex_mod_preal_e = "";
			}
			
			//Bultos fijos o por artículos de la cesta
			$nacex_bultos_fijo =  "";
			$nacex_bultos_cesta = "checked=\"checked\"";
			$nacex_bultos_DIS = "";
			if(Tools::getValue('nacex_bultos', Configuration::get('NACEX_BULTOS')) == "F") {
				$nacex_bultos_fijo = "checked=\"checked\"";
				$nacex_bultos_cesta = "";
				$nacex_bultos_DIS = "";
			}else if(Tools::getValue('nacex_bultos', Configuration::get('NACEX_BULTOS')) == "C") {
				$nacex_bultos_cesta = "checked=\"checked\"";
				$nacex_bultos_fijo = "";
				$nacex_bultos_DIS = "disabled='true'";
			}else{
				$nacex_bultos_cesta = "checked=\"checked\"";
				$nacex_bultos_fijo = "";
				$nacex_bultos_DIS = "disabled='true'";
			}
			
			//Peso fijo o por artículos de la cesta
			$nacex_peso_fijo = "";
			$nacex_peso_cesta = "checked=\"checked\"";
			$nacex_peso_cesta_DIS = "";
			if(Tools::getValue('nacex_peso', Configuration::get('NACEX_PESO')) == "F") {
				$nacex_peso_fijo = "checked=\"checked\"";
				$nacex_peso_cesta = "";
				$nacex_peso_cesta_DIS = "";
			}else if(Tools::getValue('nacex_peso', Configuration::get('NACEX_PESO')) == "C") {
				$nacex_peso_cesta = "checked=\"checked\"";
				$nacex_peso_fijo = "";
				$nacex_peso_cesta_DIS = "disabled='true'";
			}else{
				$nacex_peso_cesta = "checked=\"checked\"";
				$nacex_peso_fijo = "";
				$nacex_peso_cesta_DIS = "disabled='true'";
			}
			
			//Mostrar transportistas con coste 0€
			$nacex_mostrar_coste0_si = "checked=\"checked\"";
			$nacex_mostrar_coste0_no = "";
			if(Tools::getValue('nacex_mostrar_coste0', Configuration::get('NACEX_MOSTRAR_COSTE_0')) == "SI") {
				$nacex_mostrar_coste0_si = "checked=\"checked\"";
				$nacex_mostrar_coste0_no = "";
			}else if(Tools::getValue('nacex_mostrar_coste0', Configuration::get('NACEX_MOSTRAR_COSTE_0')) == "NO") {
				$nacex_mostrar_coste0_no = "checked=\"checked\"";
				$nacex_mostrar_coste0_si = "";				
			}else{
				$nacex_mostrar_coste0_si = "checked=\"checked\"";
				$nacex_mostrar_coste0_no = "";				
			}
			
			//Aplicar gastos de manipulacional coste del envio
			$nacex_gastos_manipulacion_no = "checked=\"checked\"";
			$nacex_gastos_manipulacion_si = "";
			$nacex_gastos_manipulacion_DIS = "";
			if(Tools::getValue('nacex_gastos_manipulacion', Configuration::get('NACEX_GASTOS_MANIPULACION')) == "SI") {
				$nacex_gastos_manipulacion_si = "checked=\"checked\"";
				$nacex_gastos_manipulacion_no = "";
				$nacex_gastos_manipulacion_DIS = "";
			}else if(Tools::getValue('nacex_gastos_manipulacion', Configuration::get('NACEX_GASTOS_MANIPULACION')) == "NO") {
				$nacex_gastos_manipulacion_no = "checked=\"checked\"";
				$nacex_gastos_manipulacion_si = "";
				$nacex_gastos_manipulacion_DIS = "disabled='true'";
			}else{
				$nacex_gastos_manipulacion_no = "checked=\"checked\"";
				$nacex_gastos_manipulacion_si = "";
				$nacex_gastos_manipulacion_DIS = "disabled='true'";
			}
			$divInfoGastosMani = showDivInfo("info_nacex_gastos_manipulacion",
																 		$obj->l("Gastos de manipulación:"),
																 		$obj->l("El importe de gastos de manipulació se sumará al importe de todos los transportistas NACEX y NACEXSHOP en el caso de cálculo automático mediante el Webservice de Nacex."));

			// Mostrar formulario Generar Expedición para cualquier transportista
			$force_genform_no = "";
			$force_genform_si = "checked=\"checked\"";
			if(Tools::getValue('nacex_force_genform', Configuration::get('NACEX_FORCE_GENFORM')) == "SI") {
				$force_genform_si = "checked=\"checked\"";
				$force_genform_no = "";
			}else if(Tools::getValue('nacex_force_genform', Configuration::get('NACEX_FORCE_GENFORM')) == "NO") {
				$force_genform_no = "checked=\"checked\"";
				$force_genform_si = "";
			}	
			
			// Calcular importe de los transportistas con WS
			$nacex_ws_importe_si = "checked=\"checked\"";
			$nacex_ws_importe_no = "";
			if(Tools::getValue('nacex_ws_importe', Configuration::get('NACEX_WS_IMPORTE')) == "SI") {
				$nacex_ws_importe_si = "checked=\"checked\"";
				$nacex_ws_importe_no = "";
			}else if(Tools::getValue('nacex_ws_importe', Configuration::get('NACEX_WS_IMPORTE')) == "NO") {
				$nacex_ws_importe_no = "checked=\"checked\"";
				$nacex_ws_importe_si = "";
			}
			
			// 	Activar traza de log del módulo de Nacex
			$nacex_save_log_no = "checked=\"checked\"";
			$nacex_save_log_si = "";
			if(Tools::getValue('nacex_save_log', Configuration::get('NACEX_SAVE_LOG')) == "SI") {
				$nacex_save_log_si = "checked=\"checked\"";
				$nacex_save_log_no = "";
			}else if(Tools::getValue('nacex_save_log', Configuration::get('NACEX_SAVE_LOG')) == "NO") {
				$nacex_save_log_no = "checked=\"checked\"";
				$nacex_save_log_si = "";
			}
			
			// Mostrar errores en tiempo de ejecución
			$show_errors_no = "checked=\"checked\"";
			$show_errors_si = "";
			if(Tools::getValue('nacex_show_errors', Configuration::get('NACEX_SHOW_ERRORS')) == "SI") {
				$show_errors_si = "checked=\"checked\"";
				$show_errors_no = "";
			}else if(Tools::getValue('nacex_show_errors', Configuration::get('NACEX_SHOW_ERRORS')) == "NO") {
				$show_errors_no = "checked=\"checked\"";
				$show_errors_si = "";
			}
			
                        			
			if(Tools::getValue('nacex_print_iona', Configuration::get('NACEX_PRINT_IONA')) == "") {				
				if(Tools::getValue('nacex_print_url', Configuration::get('NACEX_PRINT_URL')) == "") {
				// no configurado		
				$printMethod="";		
				}else{// applet
					$printMethod="applet";
				}
			} else { // iona
				$printMethod="iona";
			}
			
			
			$nacex_path_log= Tools::getValue('nacex_path_log', Configuration::get('NACEX_PATH_LOG')) == "" ?
						$_SERVER["DOCUMENT_ROOT"].__PS_BASE_URI__."modules/nacex/log/":
						Tools::getValue('nacex_path_log', Configuration::get('NACEX_PATH_LOG'));

		
			$html.= '<style type="text/css">
								 .tabItem { display: block; background: #FFFFF0; border: 1px solid #CCCCCC; padding: 10px; padding-top: 20px; }
								 .columna1 { width:250px; text-align:left; vertical-align:top; font-size: 0.8em; font-weight:bold;}
								 .columna2 { text-align:left;}
								 p.tip { text-align:left; color: #ABABAB; font-size: 0.8em;}
							 </style>
                                <script type="text/javascript">
				$(document).ready(function(){ 
															 		
												 		printMethod("'.$printMethod.'");

												 		$("input[name=nacex_impresion_metodo]:radio").change(function () {
												 			printMethod( $(this).val() );
														});
												 						
												 	});
												 						
													function printMethod(metodo){
												 			if (metodo==""){															 														
																$(".ionaTR").hide();	
																$(".urlTR").hide();
												 			}
												 			
												 			if (metodo=="iona"){
												 				$("input:radio[name=nacex_impresion_metodo]")[0].checked = true;	
												 				$(".urlTR").hide();
												 				$(".ionaTR").show();
																$("input:text[name=nacex_print_iona]").val("'.Tools::getValue('nacex_print_iona', Configuration::get('NACEX_PRINT_IONA')).'");	
												 				
												 			}else{
												 				$("input:radio[name=nacex_impresion_metodo]")[1].checked = true;	
												 				$(".ionaTR").hide();	
																$(".urlTR").show();
												 				$("input:text[name=nacex_print_iona]").val(""); //queda limpio para utilizar por defecto APPLET.
																}
													}
                                                                                                     function probarConnexionWebservice(){
									 					var msg="";
									 					
														$.ajax({
															type: "POST",
															url: "'. $nacexDTO->getPath() . 'TestWSConnection.php",
															async: false,
															data: {ws_url:$("[name=\"nacex_ws_url\"]").val(), usr:$("[name=\"nacex_wsusername\"]").val(), pass:$("[name=\"nacex_wspassword\"]").val()},
															beforeSend: function(){
																$("#connectionResult").html(\'<img src= \"../modules/nacex/img/loading.gif\" style=\"width:30px\">\');
															},
															success: function(msg) {
																$("#connectionResult").html(msg);
															}
														});
									 				}
													var tm=0;
													function modulosPHP(){
									 					var msg="";
									 					
														if (tm % 2 === 0){ 
															$.ajax({
																type: "POST",
																url: "'. $nacexDTO->getPath() . 'TestModulos.php",
																async: false,
																beforeSend: function(){
																	$("#modulosResult").html(\'<img src= \"../modules/nacex/img/loading.gif\" style=\"width:30px\">\');
																},
																success: function(msg) {
																	$("#modulosResult").html(msg);
																} 		})
														}else{
															$("#modulosResult").html(\'<div id="modulosResult"></div>\')
															}
														tm++;				
														}
									 				</script>
								
								<form action="index.php?tab='.Tools::getValue('tab').'&configure='.Tools::getValue('configure').'&token='.Tools::getValue('token').'&tab_module='.Tools::getValue('tab_module').'&module_name='.Tools::getValue('module_name').'&id_tab=1&section=general" method="post" id="configForm">
									<div id="tabList">
									  <div class="tabItem" style="width: 80%;">
									  	<div id="ayuda" style="width:100%;text-align:right">
									  		<table style="width:100%">
									  		<tr>
									  		<td>
									  		<a href="'.$config_pdf.'" target="_blank">
											  <b>¿Necesitas ayuda?</b>
											  <br>!Descarga aqu&iacute; el manual de configuración del m&oacute;dulo!
											  </a>
											  </td>
											  <td style="width:30px">
											  <a href="'.$config_pdf.'" target="_blank">
											  <img src="'.$nacexDTO->getPath().'img/icon_pdf.png"/>
											  </a>
											  </td>
											  </tr>
											  </table>											  
										  </div>
									  	<fieldset>
									 			<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Configuración de conexiones</legend>
									  			<table style="border: 0px;">
									   				<tr>
															<td class="columna1">'.$obj->l('URL WS').' :</td>
															<td class="columna2" id="nacex_ws_url">
																'.showError($errores, "nacex_ws_url").'	
																<input type="text" size="50" name="nacex_ws_url" value="'.Tools::getValue('nacex_ws_url', Configuration::get('NACEX_WS_URL')).'" />
																<p class="tip">'.$obj->l('URL Web Service de Nacex.'). '<br>'.$obj->l('Ej: http://[servidor_nacex_webservice]/nacex_ws'). '</p>																
															</td>
									  				</tr>
                                                                                                        									  				<tr>
															<td class="columna1">'.$obj->l('Impresión').' : </td>																	
															<td class="columna2" id="nacex_impresion"> <input type="radio" name="nacex_impresion_metodo" value="iona"> IoNa <span style="margin-left: 20px;"><input type="radio" name="nacex_impresion_metodo" value="applet"> Applet</span>
																<p class="tip">'.$obj->l('Seleccione el método de impresión que utilizará').'<br></p>
															</td>
													</tr>																		
													<tr class="ionaTR">
															<td class="columna1"><p style="margin-left: 20px;">'.$obj->l('Host Print').' : </p></td>
															<td class="columna2" id="nacex_print_iona">
																'.showError($errores, "nacex_print_iona").'	
																<input type="text" size="50" name="nacex_print_iona" value="'.Tools::getValue('nacex_print_iona', Configuration::get('NACEX_PRINT_IONA')).'" />
																<p class="tip">'.$obj->l('URL y puerto del Host Iona').'<br>'.$obj->l('Ej: http://iona.nacex.com:8000/').'</p>
															</td>
													</tr>		
									  				<tr class="urlTR">
															<td class="columna1">'.$obj->l('URL Print').' : </td>
															<td class="columna2" id="nacex_print_url">
																'.showError($errores, "nacex_print_url").'	
																<input type="text" size="50" name="nacex_print_url" value="'.Tools::getValue('nacex_print_url', Configuration::get('NACEX_PRINT_URL')).'" />
																<p class="tip">'.$obj->l('URL Applet Impresión Nacex.').'<br>'.$obj->l('Ej: http://www.nacex.es/applets').'</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">'.$obj->l('Modelo Etiquetadora').' : </td>
															<td class="columna2" id="nacex_print_model">
																'.showError($errores, "nacex_print_model").'
																<input type="text" size="50" name="nacex_print_model" value="'.Tools::getValue('nacex_print_model', Configuration::get('NACEX_PRINT_MODEL')).'" />
																<p class="tip">Modelo Etiquetadora.<br>Ej: TECSV4, TEC472, ZEBRA, LASER, LASER_X1, LASER_A5</p>
															</td>
									  				</tr>
									  				<tr>
															<td class="columna1">'.$obj->l('Impresora').' : </td>
															<td class="columna2" id="nacex_print_et">
																'.showError($errores, "nacex_print_et").'
																<input type="text" size="50" name="nacex_print_et" value="'.Tools::getValue('nacex_print_et', Configuration::get('NACEX_PRINT_ET')).'" />
																<p class="tip">Nombre impresora física instalada.<br>Ej: Kyocera FS-1350DN KX</p>
															</td>
													  </tr>									
													  <tr>
															<td class="columna1">'.$obj->l('Usuario Web Service').' : </td>
															<td class="columna2" id="nacex_wsusername">
																'.showError($errores, "nacex_wsusername").'
																<input type="text" size="50" name="nacex_wsusername" value="'.Tools::getValue('nacex_wsusername', Configuration::get('NACEX_WSUSERNAME')).'" />
																<p class="tip">'.$obj->l('Usuario Web Service.').'</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">'.$obj->l('Password Web Service').' : </td>
															<td class="columna2" id="nacex_wspassword">
																'.showError($errores, "nacex_wspassword").'
																<input type="password" size="50" name="nacex_wspassword" value="'.Tools::getValue('nacex_wspassword', Configuration::get('NACEX_WSPASSWORD_ORIGINAL')).'" />
																<p class="tip">'.$obj->l('Password de cuenta NACEX.').'</p>
															</td>
									  				</tr>
									  				<tr>
															<td colspan="2" style="height:30px;text-align:center">
															 <input style="cursor: pointer;padding: 7px;width: 250px;" onclick="javascript:probarConnexionWebservice();" type="button" name="probarWsConn" id="probarWsConn" value="Probar conexión Webservice"/>
															 <div id="connectionResult"></div>
															 <input style="cursor: pointer;padding: 7px;width: 250px;" onclick="modulosPHP();" type="button" name="modulos" id="modulos" value="Módulos PHP"/>
															<div id="modulosResult"></div>
															</td>															
									  				</tr>
									 				</table>
                                                                                                        

											</fieldset>
									
											<br/>
									
											<fieldset>
									 			<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Configuración del abonado</legend>
									 				<table style="border: 0px;">
														<tr>
															<td class="columna1">Agencias/Clientes: </td>
															<td class="columna2" id="nacex_agcli">
																'.showError($errores, "nacex_agcli").'
																<input type="text" size="50" name="nacex_agcli" value="'.Tools::getValue('nacex_agcli', Configuration::get('NACEX_AGCLI')).'" />
																<p class="tip">Códigos de agencia/clientes separados por coma<br>Ej: 1234/01234,4321/04321</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">'.$obj->l('Departamentos de cliente').' : </td>
															<td class="columna2" id="nacex_departamentos">
																'.showError($errores, "nacex_departamentos").'
																<input type="text" size="50" name="nacex_departamentos" value="'.Tools::getValue('nacex_departamentos', Configuration::get('NACEX_DEPARTAMENTOS')).'" />
																<p class="tip">Departamentos de cliente separados por coma. El primero se usará como departamento por defecto<br>Ej: DEPT1, 2DEPARTAMENTO</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">Código postal de recogida de los envíos: </td>
															<td class="columna2" id="nacex_cprec">
																'.showError($errores, "nacex_cprec").'
																<input type="text" size="50" name="nacex_cprec" value="'.Tools::getValue('nacex_cprec', Configuration::get('NACEX_CP_REC')).'" />
																<p class="tip">Código postal de recogida u origen de los envíos necesario para calcular la tarifa o valoración de un servicio para aplicarlo a los costos de envío.</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">Permitir al cliente seleción específica del servicio: </td>
															<td class="columna2" id="nacex_serv_back_or_front">
																<input type="radio" name="nacex_serv_back_or_front" value="F" '.$serv_back_or_front_F.'/>'.$obj->l('SI').'
																&nbsp;
																<input type="radio" name="nacex_serv_back_or_front" value="B" '.$serv_back_or_front_B.'/>'.$obj->l('NO').'
																<p class="tip">Permitir al cliente desde el Frontend la selección específica del servicio Nacex o NacexShop.</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">Mostrar estado de la expedición en el Frontend: </td>
															<td class="columna2" id="nacex_show_f_expe_state">
																<input type="radio" name="nacex_show_f_expe_state" value="SI" '.$show_f_expe_state_si.'/>'.$obj->l('SI').'
																&nbsp;
																<input type="radio" name="nacex_show_f_expe_state" value="NO" '.$show_f_expe_state_no.'/>'.$obj->l('NO').'
																<p class="tip">Mostrar estado de la expedición en el Frontend, cuando el cliente visualiza los detalles de su pedido.</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">Actualizar automaticamente el tracking en el pedido: </td>
															<td class="columna2" id="nacex_act_tracking">
																<input type="radio" name="nacex_act_tracking" value="SI" '.$act_tracking_si.'/>'.$obj->l('SI').'
																&nbsp;
																<input type="radio" name="nacex_act_tracking" value="NO" '.$act_tracking_no.'/>'.$obj->l('NO').'
																<p class="tip">Si se actualiza automáticamente, el cliente podrá saber el estado del envio de su pedido</p>
															</td>
													  </tr>
													</table>
											</fieldset>
									
											<br/>
									
											<fieldset>
									 			<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Nacex servicios estándard</legend>
									 				<table style="border: 0px;"> 
									  				<tr>
															<td class="columna1">Texto para el servicio genérico: </td>
															<td class="columna2" id="nacex_gen_serv_name">
																'.showError($errores, "nacex_gen_serv_name").'
																<input type="text" size="50" name="nacex_gen_serv_name" value="'.Tools::getValue('nacex_gen_serv_name', Configuration::get('NACEX_GEN_SERV_NAME')).'" />
																<p class="tip">Texto para describir el servicio Nacex Genérico.</p>
															</td>
									  				</tr> 
									  				<tr>			   
															<td class="columna1">Tipos de servicio estándard:</td>
															<td class="columna2" id="nacex_available_tip_ser">
																'.showError($errores, "nacex_available_tip_ser").'
																<select multiple="multiple"  style="width:335px;height:110px" name="nacex_available_tip_ser[]">';
																	foreach ($nacexDTO->getServiciosNacex() as $serv => $value){
																		$servname = $value["nombre"];
																		$servdesc = $value["descripcion"];								
																		$html .='<option '.nacexutils::markSelectedMultiOption("nacex_available_tip_ser", "NACEX_AVAILABLE_TIP_SER", "|", $serv) .' value="'.$serv.'">'.$serv.$nacexDTO->getServSeparador().$servname.'</option>';
																	}			   
						   									$html .= '</select>	
						   									<p class="tip">Tipos de Servicio disponibles (multiselección)</p>
															</td>								
									  				</tr>
						   							<tr>
															<td class="columna1">Tipo de Servicio por defecto: </td> 
															<td class="columna2" id="nacex_default_tip_ser">
																'.showError($errores, "nacex_default_tip_ser").'
																<select name="nacex_default_tip_ser" style="width:335px">
																	<optgroup label="Tipo de Servicio por defecto">';
																	foreach ($nacexDTO->getServiciosNacex() as $serv => $value){
																		$servname = $value["nombre"];
																		$servdesc = $value["descripcion"];								
																		$html .='<option '.nacexutils::markSelectedOption("nacex_default_tip_ser", "NACEX_DEFAULT_TIP_SER", $serv) .' value="'.$serv.'">'.$serv.$nacexDTO->getServSeparador().$servname.'</option>';
																	}
						 											$html.= '</optgroup>
																</select>
										 						<p class="tip">Tipo de Servicio por defecto</p>
															</td>
									  				</tr>
									  				<tr>
															<td class="columna1">Habilitar importe fijo: </td>
															<td class="columna2" id="nacex_importe_fijo">
																<input type="radio" onchange="javascript:enableValor(\'nacex_importe_fijo_val\');disableImpMinGrat(\'nacex_importe_min_grat\',\'nacex_importe_min_grat_val\');" name="nacex_importe_fijo" value="SI" '.$nacex_importe_fijo_si.'/>'.$obj->l('SI').'
																&nbsp;
																<input type="radio" onchange="javascript:disableValor(\'nacex_importe_fijo_val\');enableImpMinGrat(\'nacex_importe_min_grat\',\'nacex_importe_min_grat_val\');"  name="nacex_importe_fijo" value="NO" '.$nacex_importe_fijo_no.'/>'.$obj->l('NO').'
																<p class="tip">Habilita el importe fijo para un servicio Nacex estándard.</p>
															</td>
														</tr>
														<tr>
															<td class="columna1">Importe fijo del transporte: </td>
															<td class="columna2" id="nacex_importe_fijo_val">
																'.showError($errores, "nacex_importe_fijo_val").'
																<input type="text" '.$nacex_importe_fijo_DIS.' onfocus="javascript:$(\'#info_nacex_importe_fijo\').fadeIn(400);" onblur="javascript:$(\'#info_nacex_importe_fijo\').fadeOut(400);" size="50" name="nacex_importe_fijo_val" value="'.Tools::getValue('nacex_importe_fijo_val', Configuration::get('NACEX_IMP_FIJO_VAL')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
																'.$divInfoImpFijo.'
																<p class="tip">Importe fijo de gastos de envío a aplicar a un servicio Nacex estándard.</p>
															</td>
														</tr>
														<tr>
															<td class="columna1">Habilitar importe mínimo gratuito: </td>
															<td class="columna2" id="nacex_importe_min_grat">
																<input type="radio"  '.$nacex_importe_min_radio_DIS.' onchange="javascript:enableValor(\'nacex_importe_min_grat_val\')" name="nacex_importe_min_grat" value="SI" '.$nacex_importe_min_grat_si.'/>'.$obj->l('SI').'
																&nbsp;
																<input type="radio"  '.$nacex_importe_min_radio_DIS.' onchange="javascript:disableValor(\'nacex_importe_min_grat_val\')" name="nacex_importe_min_grat" value="NO" '.$nacex_importe_min_grat_no.'/>'.$obj->l('NO').'
																<p class="tip">Habilita la configuración del importe mínimo gratuito.</p>
															</td>
													  </tr>
													  <tr>
															<td class="columna1">Importe mínimo gratuito: </td>
															<td class="columna2" id="nacex_importe_min_grat_val">
																'.showError($errores, "nacex_importe_min_grat_val").'
																<input type="text" '.$nacex_importe_min_grat_DIS.' onfocus="javascript:$(\'#info_nacex_importe_min_grat\').fadeIn(400);" onblur="javascript:$(\'#info_nacex_importe_min_grat\').fadeOut(400);"  size="50" name="nacex_importe_min_grat_val" value="'.Tools::getValue('nacex_importe_min_grat_val', Configuration::get('NACEX_IMP_MIN_GRAT_VAL')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
																'.$divInfoImpMinGrat.'
																<p class="tip">Importe de pedido a partir del cual los gastos de envío serán gratuitos.</p>
															</td>
														</tr>						
													</table>
										</fieldset>
						
								    <br/>
								
										<fieldset>
								    	<legend><img src="'.$nacexDTO->getPath().'nacexshop.png" alt="" style="width:75px"/>NacexShop servicios</legend>
								    		<table style="border: 0px;"> 		   
									  			<tr>
									  				<td class="columna1">Texto para el servicio Nacex Shop genérico: </td>
														<td class="columna2" id="nacexshop_gen_serv_name">
															'.showError($errores, "nacexshop_gen_serv_name").'
															<input type="text" size="50" name="nacexshop_gen_serv_name" value="'.Tools::getValue('nacexshop_gen_serv_name', Configuration::get('NACEXSHOP_GEN_SERV_NAME')).'" />
															<p class="tip">Texto para describir el servicio NacexShop Genérico.</p>
														</td>
									  			</tr> 
									  			<tr>
									  				<td class="columna1">Servicios Nacex Shop disponibles:</td>
									  				<td class="columna2" id="nacex_available_tip_nxshop_ser">
															<select multiple="multiple"  style="width:335px" name="nacex_available_tip_nxshop_ser[]">';
																foreach ($nacexDTO->getServiciosNacexShop() as $serv => $value){
																	$servname = $value["nombre"];
																	$servdesc = $value["descripcion"];								
																	$html .= '<option '.nacexutils::markSelectedMultiOption("nacex_available_tip_nxshop_ser", "NACEX_AVAILABLE_TIP_NXSHOP_SER", "|", $serv) .' value="'.$serv.'">'.$serv. $nacexDTO->getServSeparador().$servname.'</option>';
																}			   
															$html .='</select>	
															<p class="tip">Servicios Nacex Shop disponibles (multiselección)</p>
									  				</td>								
									  			</tr>
									  			<tr>
									  				<td class="columna1">Servicio Nacex Shop por defecto:</td>
									  				<td class="columna2" id="nacex_default_tip_nxshop_ser">
									  					'.showError($errores, "nacex_default_tip_nxshop_ser").'
									  					<select name="nacex_default_tip_nxshop_ser"  style="width:335px">
									  						<optgroup label="Servicio Nacex Shop por defecto">';
													 			foreach ($nacexDTO->getServiciosNacexShop() as $serv => $value){
																	$servname = $value["nombre"];
																	$servdesc = $value["descripcion"];								
																	$html .= '<option '.nacexutils::markSelectedOption("nacex_default_tip_nxshop_ser", "NACEX_DEFAULT_TIP_NXSHOP_SER", $serv) .' value="'.$serv.'">'.$serv. $nacexDTO->getServSeparador().$servname.'</option>';
																}
						 			  						$html .= '</optgroup>
									  					</select>
									  					<p class="tip">Servicio Nacex Shop por defecto.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Habilitar importe fijo: </td>
									  				<td class="columna2" id="nacexshop_importe_fijo">
															<input type="radio" onchange="javascript:enableValor(\'nacexshop_importe_fijo_val\');disableImpMinGrat(\'nacexshop_importe_min_grat\',\'nacexshop_importe_min_grat_val\');" name="nacexshop_importe_fijo" value="SI" '.$nacexshop_importe_fijo_si.'/>'.$obj->l('SI').'
															&nbsp;
															<input type="radio" onchange="javascript:disableValor(\'nacexshop_importe_fijo_val\');enableImpMinGrat(\'nacexshop_importe_min_grat\',\'nacexshop_importe_min_grat_val\');" name="nacexshop_importe_fijo" value="NO" '.$nacexshop_importe_fijo_no.'/>'.$obj->l('NO').'
															<p class="tip">Habilita el importe fijo para un servicio NacexShop.</p>
									  				</td>
									  			</tr>
									  			<tr>
														<td class="columna1">Importe fijo del transporte: </td>
														<td class="columna2" id="nacexshop_importe_fijo_val">
									  					<input type="text" '.$nacexshop_importe_fijo_DIS.' onfocus="javascript:$(\'#info_nacexshop_importe_fijo\').fadeIn(400);" onblur="javascript:$(\'#info_nacexshop_importe_fijo\').fadeOut(400);" size="50" name="nacexshop_importe_fijo_val" value="'.Tools::getValue('nacexshop_importe_fijo_val', Configuration::get('NACEXSHOP_IMP_FIJO_VAL')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
									  					'.$divInfoImpFijoShop.'
									  					<p class="tip">Importe fijo de gastos de envío a aplicar a un servicio NacexShop.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Habilitar importe mínimo gratuito: </td>
									  				<td class="columna2" id="nacexshop_importe_min_grat">
									  					<input type="radio" '.$nacexshop_importe_min_radio_DIS.' onchange="javascript:enableValor(\'nacexshop_importe_min_grat_val\')"  name="nacexshop_importe_min_grat" value="SI" '.$nacexshop_importe_min_grat_si.'/>'.$obj->l('SI').'
									  					&nbsp;
									  					<input type="radio" '.$nacexshop_importe_min_radio_DIS.' onchange="javascript:disableValor(\'nacexshop_importe_min_grat_val\')"  name="nacexshop_importe_min_grat" value="NO" '.$nacexshop_importe_min_grat_no.'/>'.$obj->l('NO').'
									  					<p class="tip">Habilita la configuración del importe mínimo gratuito.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Importe mínimo gratuito: </td>
									  				<td class="columna2" id="nacexshop_importe_min_grat_val">
									  					<input type="text" '.$nacexshop_importe_min_grat_DIS.' onfocus="javascript:$(\'#info_nacexshop_importe_min_grat\').fadeIn(400);" onblur="javascript:$(\'#info_nacexshop_importe_min_grat\').fadeOut(400);" size="50" name="nacexshop_importe_min_grat_val" value="'.Tools::getValue('nacexshop_importe_min_grat_val', Configuration::get('NACEXSHOP_IMP_MIN_GRAT_VAL')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
									  					'.$divInfoImpMinGratShop.'
									  					<p class="tip">Importe de pedido a partir del cual los gastos de envío serán gratuitos.</p>
									  				</td>
									  			</tr>
									  		</table>
									  </fieldset>
									  							
									  <br/>
									 
									<fieldset>
											<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Formulario backend</legend>
								      	<table style="border: 0px;">
													<tr>			   
															<td class="columna1"></td>
															<td class="columna2">Para el uso de servicios internacionales debe crear un transporte para cada servicio y definir las tarifas escaladas en peso para cada pais al que desee admitir envíos.</td>								
									  				</tr>
														<tr>			   
															<td class="columna1">Tipos de servicio internacionales disponibles:</td>
															<td class="columna2" id="nacex_available_tip_ser_int">
																'.showError($errores, "nacex_available_tip_ser_int").'
																<select multiple="multiple"  style="width:335px;" name="nacex_available_tip_ser_int[]">';
																	foreach ($nacexDTO->getServiciosNacexInt() as $serv => $value){
																		$servname = $value["nombre"];
																		$servdesc = $value["descripcion"];								
																		$html .='<option '.  nacexutils::markSelectedMultiOption("nacex_available_tip_ser_int", "NACEX_AVAILABLE_TIP_SER_INT", "|", $serv) .' value="'.$serv.'">'.$serv. $nacexDTO->getServSeparador().$servname.'</option>';
																	}			   
						   									$html .= '</select>	
						   									<p class="tip">Tipos de Servicio disponibles (multiselección)</p>
															</td>								
									  				</tr>
						   							<tr>
															<td class="columna1">Tipo de Servicio Internacional por defecto: </td> 
															<td class="columna2" id="nacex_default_tip_ser_int">
																'.showError($errores, "nacex_default_tip_ser_int").'
																<select name="nacex_default_tip_ser_int" style="width:335px">
																	<optgroup label="Tipo de Servicio Internacional por defecto">';
																	foreach ($nacexDTO->getServiciosNacexInt() as $serv => $value){
																		$servname = $value["nombre"];
																		$servdesc = $value["descripcion"];								
																		$html .='<option '.  nacexutils::markSelectedOption("nacex_default_tip_ser_int", "NACEX_DEFAULT_TIP_SER_INT", $serv) .' value="'.$serv.'">'.$serv. $nacexDTO->getServSeparador().$servname.'</option>';
																	}
						 											$html.= '</optgroup>
																</select>
										 						<p class="tip">Tipo de Servicio Internacional por defecto</p>
															</td>									  				
										 		</tr>
												<tr>						
										 			<td class="columna1">
										 				<p style="margin-left:5px;">' . $obj->l('Contenido del envio por defecto') . '</p>								
										 			</td>
													<td  class="columna2">
										 				<select id="nacex_default_contenido" name="nacex_default_contenido" value="'. Tools::getValue('NACEX_DEFAULT_CONTENIDO', '') .'" style="width:375px; margin-left:15px;" length="1" maxlength="38">'; 												 		
										 				foreach ($nacexDTO->getContenidos() as $value ){ 
										 					$html.= '<option '.  nacexutils::markSelectedOption("nacex_default_contenido", "NACEX_DEFAULT_CONTENIDO", $value) .' value="'.$value.'">'.$value.'</option>'; } 
												$html.= '</select>
										 			</td>						
										 		</tr>
										 		<tr>						
										 			<td colspan="2"><hr><br>
										 			</td>						
										 		</tr>																			
									  			<tr>
									  				<td class="columna1">Tipo de Cobro por defecto:</td>
									  				<td class="columna2" id="nacex_tip_cob">
									  					<input type="radio" name="nacex_tip_cob" value="O" '.$tip_cob_01.'/>O - Origen
									  					&nbsp;
									  					<input type="radio" name="nacex_tip_cob" value="D" '.$tip_cob_02.'/>D - Destino
									  					&nbsp;
									  					<input type="radio" name="nacex_tip_cob" value="T" '.$tip_cob_03.'/>T - Tercera 
									  					<p class="tip">Tipo de Cobro.<span style="color: blue; font-weight: bold;">En envios internacionales SOLO está disponible pago en ORIGEN.</span></p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Módulos de Pago Contra Reembolso: </td>
									  				<td class="columna2" id="nacex_modulos_reembolso">
									  					'.showError($errores, "nacex_modulos_reembolso").'
									  					<input type="text" onfocus="javascript:$(\'#info_nacex_modulos_reembolso\').fadeIn(400);" onblur="javascript:$(\'#info_nacex_modulos_reembolso\').fadeOut(400);" size="50" name="nacex_modulos_reembolso" value="'.Tools::getValue('nacex_modulos_reembolso', Configuration::get('NACEX_MODULOS_REEMBOLSO')).'" />
															'.$divpayment.'
									  					<p class="tip">Nombre de los módulos de pago contra reembolso instalados y activados separados por coma.</span><br>Ej: cashondelivery,nombreotromodulodepagocontrareembolso</p>									  					
									  				</td>
									  			</tr>
									  			<tr>
														<td class="columna1">Tipo de Reembolso por defecto:</td>
														<td class="columna2" id="nacex_tip_ree">
															<input type="radio" name="nacex_tip_ree" value="O" '.$tip_ree_01.'/>O - Origen
															&nbsp;
															<input type="radio" name="nacex_tip_ree" value="D" '.$tip_ree_02.'/>D -  Destino
															&nbsp;
															<input type="radio" name="nacex_tip_ree" value="T" '.$tip_ree_03.'/>T - Tercera
															<p class="tip">Tipo de Reembolso.<span style="color: blue; font-weight: bold;">En envios internacionales NO está disponible REEMBOLSO.</span></p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Tipo de Envío por defecto: </td>
														<td class="columna2" id="nacex_tip_env">
															<input type="radio" name="nacex_tip_env" value="0" '.$tip_env_docs.'/>0 - DOCS
															&nbsp;
															<input type="radio" name="nacex_tip_env" value="1" '.$tip_env_bag.'/>1 - BAG
															&nbsp;
															<input type="radio" name="nacex_tip_env" value="2" '.$tip_env_paq.'/>2 - PAQ
															<p class="tip">Tipo de Envío España, Portugal y Andorra.</p>
															<input type="radio" name="nacex_tip_env_int" value="M" '.$tip_env_mu.'/>M - MUESTRAS
															&nbsp;
															<input type="radio" name="nacex_tip_env_int" value="D" '.$tip_env_do.'/>D - DOCUMENTOS
															<p class="tip">Resto de Europa.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Informar cantidad y referencia en Instrucciones Adicionales: </td>
														<td class="columna2" id="nacex_ins_adi_q_r">
															<input type="radio" name="nacex_ins_adi_q_r" value="SI" '.$ins_adi_q_r_si.'/>SI
															&nbsp;
															<input type="radio" name="nacex_ins_adi_q_r" value="NO" '.$ins_adi_q_r_no.'/>NO
															<p class="tip">Informar cantidad y referencia en Instrucciones Adicionales.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Añadir Instrucciones Adicionales a la etiqueta: </td>
														<td class="columna2" id="nacex_ins_pers">
															<input type="radio" onchange="javascript:enableValor(\'nacex_custom_inst_pers\')" name="nacex_ins_pers" value="SI" '.$ins_adi_pers_si.'/>SI
															&nbsp;
															<input type="radio" onchange="javascript:disableValor(\'nacex_custom_inst_pers\')" name="nacex_ins_pers" value="NO" '.$ins_adi_pers_no.'/>NO
															<p class="tip">Permite añadir comentarios al envío en forma de instrucciones adicionales.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Texto de las Instrucciones Adicionales: </td>
														<td class="columna2" id="nacex_custom_inst_pers">
															<input type="text" '.$ins_adi_pers_DIS.' size="50" name="nacex_custom_inst_pers" value="'.Tools::getValue('nacex_inst_adi', Configuration::get('NACEX_CUSTOM_INST_ADI')).'"/>
															<p class="tip">Contenido de las instrucciones adicionales que se añadirá, por defecto, al generar todos los envíos.<br> Tamaño máximo: 600 carácteres.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Envío con Retorno por defecto: </td>
														<td class="columna2" id="nacex_ret">
															<input type="radio" name="nacex_ret" value="SI" '.$ret_si.'/>SI
															&nbsp;
															<input type="radio" name="nacex_ret" value="NO" '.$ret_no.'/>NO
															<p class="tip">Envío con Retorno por defecto.<span style="color: blue; font-weight: bold;">En envios internacionales NO está disponible envios con gestión de RETORNO.</span></p>
														</td>
													</tr>
													<tr>
									  				<td class="columna1">Referencia personalizada: </td>
									  				<td class="columna2" id="nacex_ref_pers">
									  					<input type="radio" onchange="javascript:enableValor(\'nacex_ref_pers_prefijo\')" name="nacex_ref_pers" value="SI" '.$nacex_ref_pers_si.'/>SI
									  					&nbsp;
									  					<input type="radio" onchange="javascript:disableValor(\'nacex_ref_pers_prefijo\')" name="nacex_ref_pers" value="NO" '.$nacex_ref_pers_no.'/>NO
									  					<p class="tip">La referencia se compone de un prefijo personalizable y un identificador (id pedido). La referencia nunca podrá superar los 20 dígitos.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Prefijo de la referencia personalizada: </td>
									  				<td class="columna2" id="nacex_ref_pers_prefijo">
									  					'.showError($errores, "nacex_ref_pers_prefijo").'	
									  					<input type="text" '.$nacex_ref_pers_DIS.' size="50" name="nacex_ref_pers_prefijo" value="'.Tools::getValue('nacex_ref_pers_prefijo', Configuration::get('NACEX_REF_PERS_PREFIJO')).'" />
									  					<p class="tip">Prefijo de la referencia del envío. No debe contener espacios en blanco ni carácteres expeciales. Acepta <b>$reference</b> para la referencia Prestashop<br>Ej: NACEX_</p>
									  				</td>
									  			</tr>
									  			<tr>
														<td class="columna1">Tipo de Seguro por defecto: </td>
														<td class="columna2">
																<select name="nacex_default_tip_seg" size="1">';
																	foreach ($nacexDTO->getSeguros() as $seg => $value){
																		$segname = $value["nombre"];
																		$segdesc = $value["descripcion"];								

																		$html .= '<option '.  nacexutils::markSelectedOption("nacex_default_tip_seg", "NACEX_DEFAULT_TIP_SEG", $seg) .' value="'.$seg.'">'.$segname.'</option>';
																	}
															   $html .='</select>
															   <p class="tip">Tipo de seguro</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Importe asegurado por defecto: </td>
														<td class="columna2">
															'.showError($errores, "nacex_default_imp_seg").'
															<input type="text" '.$nacex_default_imp_seg_DIS.' id="nacex_default_imp_seg" name="nacex_default_imp_seg" value="'.Tools::getValue("nacex_default_imp_seg",Configuration::get('NACEX_DEFAULT_IMP_SEG')).'"  onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
														<p class="tip">Importe asegurado (€)</p>
														</td>
													</tr>
													<script>
														$(function() {
															$("select[name=\"nacex_default_tip_seg\"]").change(function(index) {
										    					if ($(this).val() != "N"){
										    						$("#nacex_default_imp_seg").removeAttr("disabled");
										    						$("#nacex_default_imp_seg").attr("value","'.Tools::getValue("nacex_default_imp_seg",Configuration::get('NACEX_DEFAULT_IMP_SEG')).'");
										    						$("#nacex_default_imp_seg").focus();
										    					}
										    					else{
										    						$("#nacex_default_imp_seg").attr("disabled","disabled");
										    						$("#nacex_default_imp_seg").attr("value","");
										    					}
															});
														});
													</script>
									  			<tr>
														<td class="columna1">Tipo de prealerta por defecto:</td>
														<td class="columna2" id="nacex_tip_preal">
															<input type="radio" onchange="javascript:disablePrealerta();" name="nacex_tip_preal" value="N" '.$nacex_tip_preal_n.'/>Sin prealerta
															&nbsp;
															<input type="radio" onchange="javascript:enablePrealerta();" name="nacex_tip_preal" value="S" '.$nacex_tip_preal_s.'/>SMS
															&nbsp;
															<input type="radio" onchange="javascript:enablePrealerta();" name="nacex_tip_preal" value="E" '.$nacex_tip_preal_e.'/>E-mail
															<p class="tip">Todos los envíos se generarán con la prealerta seleccionada.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Modo de prealerta por defecto:</td>
														<td class="columna2" id="nacex_mod_preal">
															<input type="radio" '.$nacex_tip_preal_DIS.' onchange="javascript:disableValor(\'nacex_preal_plus_txt\')" name="nacex_mod_preal" value="S" '.$nacex_mod_preal_s.'/>Standard
															&nbsp;
															<input type="radio" '.$nacex_tip_preal_DIS.' onchange="javascript:enableValor(\'nacex_preal_plus_txt\')" name="nacex_mod_preal" value="P" '.$nacex_mod_preal_p.'/>Plus
															&nbsp;
															<input type="radio" '.$nacex_tip_preal_DIS.' onchange="javascript:disableValor(\'nacex_preal_plus_txt\')" name="nacex_mod_preal" value="R" '.$nacex_mod_preal_r.'/>Reparto
															&nbsp;
															<input type="radio" '.$nacex_tip_preal_DIS.' onchange="javascript:enableValor(\'nacex_preal_plus_txt\')" name="nacex_mod_preal" value="E" '.$nacex_mod_preal_e.'/>Reparto Plus
															<p class="tip">Tipo de mensaje a enviar:<br>&nbsp;&nbsp;- Standard: Mensaje de alerta básico<br>&nbsp;&nbsp;- Plus: Mensaje tipo "Standard" con texto añadido<br>&nbsp;&nbsp;- Reparto: Mensaje tipo "Standard" enviado cuando el paquete llega a la agencia de destino.<br>&nbsp;&nbsp;- Reparto Plus: Mensaje tipo "Reparto" con texto añadido.</p>
														</td>
													</tr>
													<tr>
									  				<td class="columna1">Mensaje prealerta plus: </td>
									  				<td class="columna2" id="nacex_preal_plus_txt">
									  					'.showError($errores, "nacex_preal_plus_txt").'	
									  					<input type="text" '.$nacex_tip_preal_DIS.' size="50" name="nacex_preal_plus_txt" value="'.Tools::getValue('nacex_preal_plus_txt', Configuration::get('NACEX_PREAL_PLUS_TXT')).'" />
									  					<p class="tip">Texto adicional en la prealerta. Máximo 720 carácteres.</p>
									  				</td>
									  			</tr>
													<tr>
									  				<td class="columna1">Bultos del envío: </td>
									  				<td class="columna2" id="nacex_bultos">
									  					<input type="radio" onchange="javascript:disableValor(\'nacex_bultos_numero\')"  name="nacex_bultos" value="C" '.$nacex_bultos_cesta.'/>Artículos cesta	
									  					&nbsp;
									  					<input type="radio" onchange="javascript:enableValor(\'nacex_bultos_numero\')"  name="nacex_bultos" value="F" '.$nacex_bultos_fijo.'/>Bultos fijos
									  					<p class="tip">Forma de calcular el número de bultos del envío.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Número de bultos fijos: </td>
									  				<td class="columna2" id="nacex_bultos_numero">
									  					<input type="text" '.$nacex_bultos_DIS.' size="50" name="nacex_bultos_numero" value="'.Tools::getValue('nacex_bultos_numero', Configuration::get('NACEX_BULTOS_NUMERO')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,0);"/>
									  					<p class="tip">Indicar el número de bultos para todos los envíos</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Peso del envío: </td>
									  				<td class="columna2" id="nacex_peso">
									  					<input type="radio" onchange="javascript:disableValor(\'nacex_peso_numero\')" name="nacex_peso" value="C" '.$nacex_peso_cesta.'/>Artículos cesta	
									  					&nbsp;
									  					<input type="radio" onchange="javascript:enableValor(\'nacex_peso_numero\')" name="nacex_peso" value="F" '.$nacex_peso_fijo.'/>Peso fijo
									  					<p class="tip">Forma de calcular el peso del envío.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Número de kilos fijos: </td>
									  				<td class="columna2" id="nacex_peso_numero">
									  					<input type="text" '.$nacex_peso_cesta_DIS.' size="50" name="nacex_peso_numero" value="'.Tools::getValue('nacex_peso_numero', Configuration::get('NACEX_PESO_NUMERO')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
									  					<p class="tip">Indicar el peso con el que se enviarán todos los paquetes.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Aplicar gastos de manipulación: </td>
									  				<td class="columna2" id="nacex_gastos_manipulacion">
									  					<input type="radio" onchange="javascript:enableValor(\'nacex_gastos_manipulacion_val\')"  name="nacex_gastos_manipulacion" value="SI" '.$nacex_gastos_manipulacion_si.'/>'.$obj->l('SI').'
									  					<input type="radio" onchange="javascript:disableValor(\'nacex_gastos_manipulacion_val\')"  name="nacex_gastos_manipulacion" value="NO" '.$nacex_gastos_manipulacion_no.'/>'.$obj->l('NO').'
									  					<p class="tip">Permite añadir gastos extra al coste del envío.</p>
									  				</td>
									  			</tr>
									  			<tr>
									  				<td class="columna1">Gastos de manipulación: </td>
									  				<td class="columna2" id="nacex_gastos_manipulacion_val">
									  					'.showError($errores, "nacex_gastos_manipulacion_val").'
									  					<input type="text" '.$nacex_gastos_manipulacion_DIS.' onfocus="javascript:$(\'#info_nacex_gastos_manipulacion\').fadeIn(400);" onblur="javascript:$(\'#info_nacex_gastos_manipulacion\').fadeOut(400);"  size="50" name="nacex_gastos_manipulacion_val" value="'.Tools::getValue('nacex_gastos_manipulacion_val', Configuration::get('NACEX_GASTOS_MANIPULACION_VAL')).'" onkeypress="javascript:return soloNumeros(event);" onblur="javascript:ValidarNum(this.value, this,7,2);"/>
									  					'.$divInfoGastosMani.'
									  					<p class="tip">Importe del incremento en el precio del servicio por gastos de manipulación.</p>
									  				</td>
									  			</tr>							
									  		</table>
									  </fieldset>
										
									  
										<br/>
									
										<fieldset>
											<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Formulario frontend</legend>
								      	<table style="border: 0px;">
					      					<tr>				
														<td class="columna1">Anchura logotipos Servicios en Frontend: </td>
														<td class="columna2">
														<input type="text" size="1" maxlength="3" name="nacex_logoservs_width" value="'.Tools::getValue('nacex_logoservs_width', Configuration::get('NACEX_LOGOSERVS_WIDTH')).'" />px
														<p class="tip">Anchura en píxeles logotipos Servicios en Frontend. Dejar en blanco para conservar el tamaño original.</p>				
														</td>			
													</tr>
													<tr>
									  				<td class="columna1">Mostrar tarifas con coste 0 €: </td>
									  				<td class="columna2" id="nacex_mostrar_coste0">
									  					<input type="radio" name="nacex_mostrar_coste0" value="SI" '.$nacex_mostrar_coste0_si.'/>'.$obj->l('SI').'
									  					<input type="radio" name="nacex_mostrar_coste0" value="NO" '.$nacex_mostrar_coste0_no.'/>'.$obj->l('NO').'
									  					<p class="tip">Permite mostrar u ocultar los transportistas cuyo coste sea 0 €.</p>
									  				</td>
									  			</tr>	
								      		<tr>
														<td class="columna1">Mostrar formulario Generar Expedición para transportistas externos: </td>
														<td class="columna2" id="nacex_force_genform">
															<input type="radio" name="nacex_force_genform" value="SI" '.$force_genform_si.'/>'.$obj->l('SI').'
															<input type="radio" name="nacex_force_genform" value="NO" '.$force_genform_no.'/>'.$obj->l('NO').'
														<p class="tip">Mostrar formulario Generar Expedición para transportistas externos.</p>
														</td>
													</tr>
													<tr>
														<td class="columna1">Permitir cálculo automático del importe de los transportistas Nacex: </td>
														<td class="columna2" id="nacex_ws_importe">
															<input type="radio" name="nacex_ws_importe" value="SI" '.$nacex_ws_importe_si.'/>'.$obj->l('SI').'
															<input type="radio" name="nacex_ws_importe" value="NO" '.$nacex_ws_importe_no.'/>'.$obj->l('NO').'
														<p class="tip">Permite calcular el improte del envío automáticamente mediante el WebService de Nacex.p>
														</td>
													</tr>
								      	</table>
								    </fieldset>
										
									  
										<br/>
									
										<fieldset>
											<legend><img src="'.$nacexDTO->getPath().'nacex.png" alt="" style="width:75px"/>Debug</legend>
								      	<table style="border: 0px;">
													<tr>
														<td class="columna1">Activar logs Nacex:</td>
														<td class="columna2" id="nacex_save_log">
															<input type="radio" name="nacex_save_log" value="SI" '.$nacex_save_log_si.'/>'.$obj->l('SI').'
															<input type="radio" name="nacex_save_log" value="NO" '.$nacex_save_log_no.'/>'.$obj->l('NO').'
														<p class="tip">
															Guarda una traza de log diario en la carpeta modules/nacex/log/
															'.$link_log.'
														</p>														
														</td>
													</tr>
													<tr>
														<td class="columna1">Mostrar errores en tiempo de ejecución:</td>
														<td class="columna2" id="nacex_show_errors">
															<input type="radio" name="nacex_show_errors" value="SI" '.$show_errors_si.'/>'.$obj->l('SI').'
															<input type="radio" name="nacex_show_errors" value="NO" '.$show_errors_no.'/>'.$obj->l('NO').'
														<p class="tip">Para propósitos de depuración sólo cuando sea necesario.</p>
														</td>
													</tr>
								      	</table>
								    </fieldset>

								    <div id="divboton" style="width:200px;margin: 0 auto;text-align:center">
											<input class="ncx_button" onclick="procesando()" name="submitSave" type="submit" value="Guardar configuración">
										</div>

										</div>
									</div>
								</form>';
			
			return $html;
	}

	function validarFormularioConfiguracion($obj){

			$errores = array();
		
			$nacex_ws_url = Tools::getValue("nacex_ws_url");
			if(empty($nacex_ws_url)){
				$errores['nacex_ws_url'] = 'Debe informar del usuario de WebService.';
			}
			
			$nacex_print_url = Tools::getValue("nacex_print_url");
                        $nacex_print_iona = Tools::getValue("nacex_print_iona");
                        
                        if(empty($nacex_print_url) && empty($nacex_print_iona )){
				$errores['nacex_impresion_metodo'] = $obj->l('Debe informar un método impresión.');
			}else{
				if(!empty($nacex_print_url) && strpos(strtolower($nacex_print_url), "/applets")=== false){
					$errores['nacex_print_url'] = $obj->l('URL de impresión incorrecta.');	
				}
			}
			
			$nacex_print_model = Tools::getValue("nacex_print_model");
			if(empty($nacex_print_model)){
				$errores['nacex_print_model'] = 'Debe informar un modelo de etiquetadora compatible.';
			}
			
			$nacex_print_et = Tools::getValue("nacex_print_et");
			if(empty($nacex_print_et)){
				$errores['nacex_print_et'] = 'Debe informar del nombre de la impresora a utilizar.';
			}
			
			$nacex_wsusername = Tools::getValue("nacex_wsusername");
			if(empty($nacex_wsusername)){
				$errores['nacex_wsusername'] = 'Debe informar del nombre de usuario del Webservice de Nacex.';
			}
			
			$nacex_wspassword = Tools::getValue("nacex_wspassword");
			if(empty($nacex_wspassword)){
				$errores['nacex_wspassword'] = 'Debe informar del password del Webservice de Nacex.';
			}
			
			$nacex_agcli = Tools::getValue("nacex_agcli");
			if(empty($nacex_agcli)){
				$errores['nacex_agcli'] = 'Debe informar de la agencia y cliente.';
			}else{
				$agclis_arr = explode(",", $nacex_agcli);
				foreach ($agclis_arr as $agcli){
					$aux = explode("/", $agcli);
					if(strlen($aux[0]) != 4 || strlen($aux[1]) != 5){
						$errores['nacex_agcli'] = 'Formato de agencia y cliente erróneos.';
						break;		
					}
				}
			}
			
			$nacex_departamentos = Tools::getValue("nacex_departamentos");
			if($nacex_departamentos){
				$array_dpt = explode(",",$nacex_departamentos);
				foreach ($array_dpt as $dpt){
					if(strlen($dpt)>10){
						$errores['nacex_departamentos'] = 'El nombre del departamento no puede superar los 10 carácteres.';
						break;
					}
				}
			}
			
			
			$nacex_cprec = Tools::getValue("nacex_cprec");
			if(empty($nacex_cprec)){
				$errores['nacex_cprec'] = 'Debe informar del codigo postal de recogida.';
			}
			
			$nacex_gen_serv_name = Tools::getValue("nacex_gen_serv_name");
			if(empty($nacex_gen_serv_name)){
				$errores['nacex_gen_serv_name'] = 'Debe informar del nombre del transportista Nacex Genérico.';
			}
			
			$nacex_available_tip_ser = Tools::getValue("nacex_available_tip_ser");
			if(!$nacex_available_tip_ser){
				$errores['nacex_available_tip_ser'] = 'Debe seleccionar al menos un servicio Nacex estándard.';
			}else{

				$nacex_default_tip_ser = Tools::getValue("nacex_default_tip_ser");
				if(!in_array($nacex_default_tip_ser, $nacex_available_tip_ser)){
					$errores['nacex_default_tip_ser'] = 'Este servicio no ha sido seleccionado como servicio disponible.';
				}
			}
			
			// internacional
			$nacex_available_tip_ser_int = Tools::getValue("nacex_available_tip_ser_int");
		/*	if(!$nacex_available_tip_ser_int){
				$errores['nacex_available_tip_ser_int'] = $obj->l(utf8_encode('Debe seleccionar al menos un servicio Nacex internacional.'));
			}else{
		*/	
				$nacex_default_tip_ser_int = Tools::getValue("nacex_default_tip_ser_int");
				if(!in_array($nacex_default_tip_ser_int, $nacex_available_tip_ser_int)){
					$errores['nacex_default_tip_ser_int'] = 'Este servicio no ha sido seleccionado como servicio disponible.';
				}
		//	}
			
			
			$nacex_importe_fijo = Tools::getValue("nacex_importe_fijo");
			if($nacex_importe_fijo == "SI"){
				$nacex_importe_fijo_val = Tools::getValue("nacex_importe_fijo_val");
				if(empty($nacex_importe_fijo_val) && !is_numeric($nacex_importe_fijo_val)){
					$errores['nacex_importe_fijo_val'] = 'Debe informar del importe fijo.';	
				}else{
					$nacex_importe_fijo_val=str_replace(".", "", $nacex_importe_fijo_val);
					$nacex_importe_fijo_val=str_replace(",", ".", $nacex_importe_fijo_val);
					$importe = number_format($nacex_importe_fijo_val,2,",",".");
					if($importe<0){
						$errores['nacex_importe_fijo_val'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			$nacex_importe_min_grat = Tools::getValue("nacex_importe_min_grat");
			if($nacex_importe_min_grat == "SI"){
				$nacex_importe_min_grat_val = Tools::getValue("nacex_importe_min_grat_val");
				if(empty($nacex_importe_min_grat_val) && !is_numeric($nacex_importe_min_grat_val)){
					$errores['nacex_importe_min_grat_val'] = 'Debe informar del importe mínimo gratuito.';	
				}else{
					$nacex_importe_min_grat_val=str_replace(".", "", $nacex_importe_min_grat_val);
					$nacex_importe_min_grat_val=str_replace(",", ".", $nacex_importe_min_grat_val);
					$importe = number_format($nacex_importe_min_grat_val,2,",",".");
					if($importe<0){
						$errores['nacex_importe_min_grat_val'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			$nacexshop_gen_serv_name = Tools::getValue("nacexshop_gen_serv_name");
			if(empty($nacexshop_gen_serv_name)){
				$errores['nacexshop_gen_serv_name'] = 'Debe informar del nombre del transportista Nacex Shop Genérico.';
			}
			
			$nacex_available_tip_nxshop_ser = Tools::getValue("nacex_available_tip_nxshop_ser");
			if($nacex_available_tip_nxshop_ser){
				$nacex_default_tip_nxshop_ser = Tools::getValue("nacex_default_tip_nxshop_ser");
				if(!in_array($nacex_default_tip_nxshop_ser, $nacex_available_tip_nxshop_ser)){
					$errores['nacex_default_tip_nxshop_ser'] = 'Este servicio no ha sido seleccionado como servicio disponible.';
				}
			}
			
			$nacexshop_importe_fijo = Tools::getValue("nacexshop_importe_fijo");
			if($nacexshop_importe_fijo == "SI"){
				$nacexshop_importe_fijo_val = Tools::getValue("nacexshop_importe_fijo_val");
				if(empty($nacexshop_importe_fijo_val) && !is_numeric($nacexshop_importe_fijo_val)){
					$errores['nacexshop_importe_fijo_val'] = 'Debe informar del importe fijo.';	
				}else{
					$nacexshop_importe_fijo_val=str_replace(".", "", $nacexshop_importe_fijo_val);
					$nacexshop_importe_fijo_val=str_replace(",", ".", $nacexshop_importe_fijo_val);
					$importe = number_format($nacexshop_importe_fijo_val,2,",",".");
					if($importe<0){
						$errores['nacexshop_importe_fijo_val'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			$nacexshop_importe_min_grat = Tools::getValue("nacexshop_importe_min_grat");
			if($nacexshop_importe_min_grat == "SI"){
				$nacexshop_importe_min_grat_val = Tools::getValue("nacexshop_importe_min_grat_val");
				if(empty($nacexshop_importe_min_grat_val) && !is_numeric($nacexshop_importe_min_grat_val)){
					$errores['nacexshop_importe_min_grat_val'] = 'Debe informar del importe mínimo gratuito.';	
				}else{
					$nacexshop_importe_min_grat_val=str_replace(".", "", $nacexshop_importe_min_grat_val);
					$nacexshop_importe_min_grat_val=str_replace(",", ".", $nacexshop_importe_min_grat_val);
					$importe = number_format($nacexshop_importe_min_grat_val,2,",",".");
					if($importe<0){
						$errores['nacexshop_importe_min_grat_val'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			//Si informan de módulo de reembolso, comprobamos que exista
			$nacex_modulos_reembolso = Tools::getValue("nacex_modulos_reembolso");
			if(!empty($nacex_modulos_reembolso)){
                                $array_payment_modules = Module::getPaymentModules();
			
				$array_modulos_reembolso = explode(",",$nacex_modulos_reembolso);
				$errormodules = "";
				
				foreach ($array_modulos_reembolso as $mod_ree){
					$found = false;
					
					foreach ($array_payment_modules as $module){
						if(strtolower($mod_ree) == strtolower($module["name"])){
							$found = true;
						}
					}
					
					if(!$found){
						$errormodules .= "<b>".$mod_ree."</b>,";
					}	
				}
				
				if(!empty($errormodules)){
					$errormodules = substr($errormodules, 0, -1);
					$errores['nacex_modulos_reembolso'] = 'El módulo de reembolso ('.$errormodules.') no se encuentra instalado o es incorrecto.';	
				}
				
			}
			
			$nacex_ref_pers = Tools::getValue("nacex_ref_pers");
			if($nacex_ref_pers == "SI"){
				$nacex_ref_pers_prefijo = Tools::getValue("nacex_ref_pers_prefijo");
				if(empty($nacex_ref_pers_prefijo)){
					$errores['nacex_ref_pers_prefijo'] = 'Debe indicar un prefijo para la referencia personalizada.';
				}
			}
			
			
			$nacex_default_tip_seg = Tools::getValue("nacex_default_tip_seg");
			if($nacex_default_tip_seg != "N"){
				$nacex_default_imp_seg = Tools::getValue("nacex_default_imp_seg");
				if(empty($nacex_default_imp_seg) && !is_numeric($nacex_default_imp_seg)){
						$errores['nacex_default_imp_seg'] = 'Debe informar del importe del seguro.';
				}else{
					$nacex_default_imp_seg=str_replace(".", "", $nacex_default_imp_seg);
					$nacex_default_imp_seg=str_replace(",", ".", $nacex_default_imp_seg);
					$importe = number_format($nacex_default_imp_seg,2,",",".");
					if($importe<0){
						$errores['nacex_default_imp_seg'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			
			$nacex_gastos_manipulacion = Tools::getValue("nacex_gastos_manipulacion");
			if($nacex_gastos_manipulacion != "NO"){
				$nacex_gastos_manipulacion_val = Tools::getValue("nacex_gastos_manipulacion_val");
				if(empty($nacex_gastos_manipulacion_val) && !is_numeric($nacex_gastos_manipulacion_val)){
						$errores['nacex_gastos_manipulacion_val'] = 'Debe informar del importe de los gastos de manipulación.';
				}else{
					$nacex_gastos_manipulacion_val=str_replace(".", "", $nacex_gastos_manipulacion_val);
					$nacex_gastos_manipulacion_val=str_replace(",", ".", $nacex_gastos_manipulacion_val);
					$importe = number_format($nacex_gastos_manipulacion_val,2,",",".");
					if($importe<0){
						$errores['nacex_gastos_manipulacion_val'] = 'Importe incorrecto.';	
					}
				}	
			}
			
			return $errores;
	}
	
	function showError($errors, $key){
			if(isset($errors[$key]) && !empty($errors[$key])){
				return "<p class=\"tip\"><font color=\"#E50000\">** ".$errors[$key]."</font></p>";
			}
	}
	
	function showDivInfo($id, $titulo=null, $mensaje=null){
		
		$div = "<div class=\"optionsDescription\" id=\"".$id."\" style=\"display:none;opacity:0.85;position:absolute;margin-bottom:6px;width:450px;height:auto;padding:5px 5px 5px 50px;background-color:#CAE1FF;border:1px solid #89A9CC;border-radius:4px\">";
		if($titulo){
			$div .= "<b>".$titulo."</b>";
			$div .= "<br>";	
		}
		$div .= $mensaje;
		$div .= "</div>";
		
		return $div;
	}
	
	function guardarConfiguracion(){
				/**
		 * Configuración de conexiones 
		 */
		if(!Configuration::updateValue('NACEX_WS_URL', Tools::getValue('nacex_ws_url'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PRINT_URL', Tools::getValue('nacex_print_url'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PRINT_IONA', Tools::getValue('nacex_print_iona'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PRINT_MODEL', Tools::getValue('nacex_print_model'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PRINT_ET', Tools::getValue('nacex_print_et'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_WSUSERNAME', Tools::getValue('nacex_wsusername'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_WSPASSWORD_ORIGINAL', Tools::getValue('nacex_wspassword'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_WSPASSWORD', strtoupper(md5(Tools::getValue('nacex_wspassword'))))){
			return false;
		}			
		

		/**
		 * Configuració del abonado 
		 */
		if(!Configuration::updateValue('NACEX_AGCLI', Tools::getValue('nacex_agcli'))){
			return false;
		}			
		if(!Configuration::updateValue('NACEX_DEPARTAMENTOS', Tools::getValue('nacex_departamentos'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_CP_REC', Tools::getValue('nacex_cprec'))){
			return false;
		}			
		if(!Configuration::updateValue('NACEX_SERV_BACK_OR_FRONT', Tools::getValue('nacex_serv_back_or_front'))){
			return false;
		}			
		if(!Configuration::updateValue('NACEX_SHOW_F_EXPE_STATE', Tools::getValue('nacex_show_f_expe_state'))){
			return false;
		}			
		if(!Configuration::updateValue('NACEX_ACT_TRACKING', Tools::getValue('nacex_act_tracking'))){
			return false;
		}
		

		/**
		 * Nacex servicios estándard
		 */
		if(!Configuration::updateValue('NACEX_GEN_SERV_NAME', Tools::getValue("nacex_gen_serv_name"))){
			return false;
		}			

		// A partir de la version 1.5.4.4 se unifican los servicios de backend y frontend pero se siguen guardando 
		// por separado por compatibilidad con otras versiones anteriores del módulo
		
		//Nacional
		//BACKEND
		$nacex_available_tip_ser = Tools::getValue('nacex_available_tip_ser');
		if(!Configuration::updateValue('NACEX_AVAILABLE_TIP_SER', implode ("|", $nacex_available_tip_ser) )){
			return false;
		}
		
		//FRONTEND 
		$nacex_available_servs_f = Tools::getValue('nacex_available_tip_ser');
		if(!Configuration::updateValue('NACEX_AVAILABLE_SERVS_F', implode ("|", $nacex_available_servs_f) )){
			return false;
		}
		
		//Descripción servicios NACEX FRONTEND	 -- Deprecated a partir de la version 1.5.4.4
		/*	
		$nacex_available_servs_fd = Tools::getValue('nacex_available_servs_fd');
		$ava_servs_f_desc_piped = "";
		for ($i = 0; $i < count($nacex_available_servs_f); $i++){
			$ava_servs_f_desc_piped =  $ava_servs_f_desc_piped . $nacex_available_servs_fd[$i];
			if($i < count($nacex_available_servs_fd) -1) $ava_servs_f_desc_piped = $ava_servs_f_desc_piped . "|";
		}
		if(!Configuration::updateValue('NACEX_AVAILABLE_SERVS_FD', $ava_servs_f_desc_piped)){
			return false;
		}
		*/
		
		if(!Configuration::updateValue('NACEX_DEFAULT_TIP_SER', Tools::getValue('nacex_default_tip_ser'))){
			return false;
		}
		
		
		/**
		 * Internacional
		 */
		//BACKEND
		$nacex_available_tip_ser_int = Tools::getValue('nacex_available_tip_ser_int');
		if(!Configuration::updateValue('NACEX_AVAILABLE_TIP_SER_INT', implode ("|", $nacex_available_tip_ser_int) )){
			return false;
		}
		
		if(!Configuration::updateValue('NACEX_DEFAULT_TIP_SER_INT', Tools::getValue('nacex_default_tip_ser_int'))){
			return false;
		}

		if(!Configuration::updateValue('NACEX_DEFAULT_CONTENIDO', Tools::getValue('nacex_default_contenido'))){
			return false;
		}
		
		//FRONTEND
		$nacex_available_servs_int_f = Tools::getValue('nacex_available_tip_ser_int');
		if(!Configuration::updateValue('NACEX_AVAILABLE_SERVS_INT_F', implode ("|", $nacex_available_servs_int_f) )){
			return false;
		}
	
		
		/***
		 * 
		 */
		
		if(!Configuration::updateValue('NACEX_IMP_FIJO',Tools::getValue('nacex_importe_fijo'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_IMP_FIJO_VAL',Tools::getValue('nacex_importe_fijo_val'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_IMP_MIN_GRAT',Tools::getValue('nacex_importe_min_grat'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_IMP_MIN_GRAT_VAL',Tools::getValue('nacex_importe_min_grat_val'))){
			return false;
		}
		
		
		/**
		 * NacexShop servicios 
		 */
		if(!Configuration::updateValue('NACEXSHOP_GEN_SERV_NAME',Tools::getValue("nacexshop_gen_serv_name"))){
			return false;
		}
		
		// A partir de la version 1.5.4.4 se unifican los servicios de backend y frontend pero se siguen guardando 
		// por separado por compatibilidad con otras versiones anteriores del módulo
		
		//BACKEND
		$nacex_available_tip_nxshop_ser = Tools::getValue('nacex_available_tip_nxshop_ser');
		$ava_tip_nxshop_ser_piped = "";
		for ($i = 0; $i < count($nacex_available_tip_nxshop_ser); $i++){
			$ava_tip_nxshop_ser_piped =  $ava_tip_nxshop_ser_piped . $nacex_available_tip_nxshop_ser[$i];
			if($i < count($nacex_available_tip_nxshop_ser) -1) $ava_tip_nxshop_ser_piped = $ava_tip_nxshop_ser_piped . "|";
		}
		if(!Configuration::updateValue('NACEX_AVAILABLE_TIP_NXSHOP_SER', $ava_tip_nxshop_ser_piped)){
			return false;
		}
		
		//FRONTEND 
		$nacex_available_servs_nxshop_f = Tools::getValue('nacex_available_tip_nxshop_ser');
		$ava_servs_nxshop_f_piped = "";
		for ($i = 0; $i < count($nacex_available_servs_nxshop_f); $i++){
			$ava_servs_nxshop_f_piped =  $ava_servs_nxshop_f_piped . $nacex_available_servs_nxshop_f[$i];
			if($i < count($nacex_available_servs_nxshop_f) -1) $ava_servs_nxshop_f_piped = $ava_servs_nxshop_f_piped . "|";
		}
		if(!Configuration::updateValue('NACEX_AVAILABLE_SERVS_NXSHOP_F', $ava_servs_nxshop_f_piped)){
			return false;
		}
			
		//Descripción servicios NACEXSHOP FRONTEND -- Deprecated a partir de la version 1.5.4.4
		/*	
		$nacex_available_servs_nxshop_fd = Tools::getValue('nacex_available_servs_nxshop_fd');
		$ava_servs_nxshop_f_desc_piped = "";
		for ($i = 0; $i < count($nacex_available_servs_nxshop_fd); $i++){
			$ava_servs_nxshop_f_desc_piped =  $ava_servs_nxshop_f_desc_piped . $nacex_available_servs_nxshop_fd[$i];
			if($i < count($nacex_available_servs_nxshop_fd) -1) $ava_servs_nxshop_f_desc_piped = $ava_servs_nxshop_f_desc_piped . "|";
		}
		if(!Configuration::updateValue('NACEX_AVAILABLE_SERVS_NXSHOP_FD', $ava_servs_nxshop_f_desc_piped)){
			return false;
		}	
		*/
		if(!Configuration::updateValue('NACEX_DEFAULT_TIP_NXSHOP_SER', Tools::getValue('nacex_default_tip_nxshop_ser'))){
			return false;
		}

		if(!Configuration::updateValue('NACEXSHOP_IMP_FIJO',Tools::getValue('nacexshop_importe_fijo'))){
			return false;
		}
		if(!Configuration::updateValue('NACEXSHOP_IMP_FIJO_VAL',Tools::getValue('nacexshop_importe_fijo_val'))){
			return false;
		}
		if(!Configuration::updateValue('NACEXSHOP_IMP_MIN_GRAT',Tools::getValue('nacexshop_importe_min_grat'))){
			return false;
		}
		if(!Configuration::updateValue('NACEXSHOP_IMP_MIN_GRAT_VAL',Tools::getValue('nacexshop_importe_min_grat_val'))){
			return false;
		}
		
		
		/**
		 * Formulario backend
		 */
		if(!Configuration::updateValue('NACEX_TIP_COB', Tools::getValue('nacex_tip_cob'))){
			return false;
		}			
		if(!Configuration::updateValue('NACEX_MODULOS_REEMBOLSO', strtolower(Tools::getValue('nacex_modulos_reembolso')))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_TIP_REE', Tools::getValue('nacex_tip_ree'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_TIP_ENV', Tools::getValue('nacex_tip_env'))){
			return false;
		} 
		if(!Configuration::updateValue('NACEX_TIP_ENV_INT', Tools::getValue('nacex_tip_env_int'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_INS_ADI_Q_R', Tools::getValue('nacex_ins_adi_q_r'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_INST_PERS', Tools::getValue('nacex_ins_pers'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_CUSTOM_INST_ADI', Tools::getValue('nacex_custom_inst_pers'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_RET', Tools::getValue('nacex_ret'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_REF_PERS', Tools::getValue('nacex_ref_pers'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_REF_PERS_PREFIJO', Tools::getValue('nacex_ref_pers_prefijo'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_DEFAULT_TIP_SEG', Tools::getValue('nacex_default_tip_seg'))){
			return false;
		} 
		if(!Configuration::updateValue('NACEX_DEFAULT_IMP_SEG', Tools::getValue('nacex_default_imp_seg'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_TIP_PREAL',Tools::getValue('nacex_tip_preal'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_MOD_PREAL',Tools::getValue('nacex_mod_preal'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PREAL_PLUS_TXT',Tools::getValue('nacex_preal_plus_txt'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_BULTOS',Tools::getValue('nacex_bultos'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_BULTOS_NUMERO',Tools::getValue('nacex_bultos_numero'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PESO',Tools::getValue('nacex_peso'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_PESO_NUMERO',Tools::getValue('nacex_peso_numero'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_GASTOS_MANIPULACION',Tools::getValue('nacex_gastos_manipulacion'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_GASTOS_MANIPULACION_VAL',Tools::getValue('nacex_gastos_manipulacion_val'))){
			return false;
		}
		
		
		/**
		 * Formulario frontend
		 */
		if(!Configuration::updateValue('NACEX_LOGOSERVS_WIDTH',Tools::getValue('nacex_logoservs_width'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_MOSTRAR_COSTE_0', Tools::getValue('nacex_mostrar_coste0'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_FORCE_GENFORM', Tools::getValue('nacex_force_genform'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_WS_IMPORTE', Tools::getValue('nacex_ws_importe'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_SAVE_LOG', Tools::getValue('nacex_save_log'))){
			return false;
		}
		if(!Configuration::updateValue('NACEX_SHOW_ERRORS', Tools::getValue('nacex_show_errors'))){
			return false;
		}
		
		return true;
	}
        
      
?>
