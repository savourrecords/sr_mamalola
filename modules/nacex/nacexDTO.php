<?php

/*
 * mexpositop 2017
 */

include_once("AdminConfig.php");
include_once("nacexutils.php");


class nacexDTO {

    public $id_carrier;
    static $URL_PRO_Applets = "www.nacex.es/applets";
    static $_http = "http://";
    static $_https = "https://";
    private $SERV_SEP = " - ";
    
    private $SERVICIOS = array(
        "02" => array("nombre" => "NACEX 12:00H",
            "descripcion" => "Garant&iacute;a de entrega antes de las 12:00H del d&iacute;a siguiente laborable."),
        "04" => array("nombre" => "PLUS BAG",
            "descripcion" => "Garant&iacute;a de entrega en 24/48 horas en poblaciones con agencia NACEX."),
        "08" => array("nombre" => "NACEX 19:00H",
            "descripcion" => "Garant&iacute;a de entrega antes de las 19:00h del d&iacute;a siguiente laborable"),
        "09" => array("nombre" => "PUENTE URBANO",
            "descripcion" => "Servicio con garant&iacute;a de entrega en &Aacute;mbito urbano con varias frecuencias"),
        "11" => array("nombre" => "NACEX 08:30H",
            "descripcion" => "Garant&iacute;a de entrega antes de las 8:30h en d&iacute;a siguiente laborable"),
        "20" => array("nombre" => "NACEX MALLORCA MAR&Iacute;TIMO",
            "descripcion" => "Servicio entre Pen&iacute;nsula y Mallorca, garant&iacute;a de entrega 1-3 d&iacute;as."),
        "21" => array("nombre" => "NACEX SABADO",
            "descripcion" => "Entregas en s&aacute;bados de 9:00 h. a 13:00 h."),
        "26" => array("nombre" => "PLUS PACK",
            "descripcion" => "Env&iacute;os masivos monobulto a destinos peninsulares"),
        "27" => array("nombre" => "E-NACEX",
            "descripcion" => "Entregas de pedidos originados por e-commerce ")
    );
    private $SERVICIOS_INT = array(
        "G" => array("nombre" => "EURONACEX ECONOMY",
            "descripcion" => "Pen&iacute;nsula con tarifa especial al resto de Europa para env&iacute;os monobulto "),
        "H" => array("nombre" => "PLUSPACK EUROPE",
            "descripcion" => "Monobulto hasta 20 kg desde Pen&iacute;nsula a los principales pa&iacute;ses europeos")
    );
    private $SERVICIOS_NACEX_SHOP = array(
        "28" => array("nombre" => "PREMIUM",
            "descripcion" => "Entrega en puntos Nacex.shop a partir de las 10:00h del d&iacute;a siguiente laborable"),
        "31" => array("nombre" => "E-NACEXSHOP",
            "descripcion" => "Entrega en puntos Nacex.shop para clientes con plataforma e-commerce"),
        "90" => array("nombre" => "NACEX.SHOP",
            "descripcion" => "NACEX.SHOP")
    );
    private $SEGUROS = array(
        "N" => array("nombre" => "Sin seguro",
            "descripcion" => "Sin seguro"),
        "A" => array("nombre" => "Seguro general",
            "descripcion" => "Seguro general"),
        "B" => array("nombre" => "Joyer&iacute;a",
            "descripcion" => "Joyer&iacute;a"),
        "C" => array("nombre" => "Telefon&iacute;a",
            "descripcion" => "Telefon&iacute;a"),
        "D" => array("nombre" => "Varios",
            "descripcion" => "Varios"),
        "E" => array("nombre" => "Armas",
            "descripcion" => "Armas")
    );
    private $CONTENIDOS = array('OTROS', 'ARMAS (DOCUMENTACIÓN NECESARIA)', 'DOCUMENTS/DOCUMENTOS', 'MUESTRAS BIOLOGICAS',
        'ALIMENTOS / PREP. ALIMENTICIOS / PROD. DIETETICOS / VINOS', 'APARATOS MUSICA / VIDEOJUEGOS / DISCOS COMPACTOS',
        'CONFECCIONES / TEXTILES / CALZADO', 'EFECTOS PERSONALES', 'ETILÓMETRO', 'JOYERÍA / BISUTERÍA / RELOJES',
        'MANUF. MADERA / MANUF. ALUMINIO / MANUF. PLÁSTICO', 'MAT. INFORMÁTICO / MAT. ELECTRÓNICO / MAT. TELEFÓNICO',
        'MAT. MEDICO / MAT. ORTOPÉDICO / REACTIVOS', 'MAT. PAPELERIA / BOLIGRAFOS', 'MAT. PUBLICITARIO / CALENDARIOS',
        'MEDICAMENTOS', 'MERCANCÍA PELIGROSA', 'PROD. PARAFARMACIA / COSMÉTICOS / APÓSITOS (VENDAS,GASAS)',
        'REPUESTOS MAQUINARIA / OTROS REPUESTOS', 'TARJETA CON TIRA MAGNÉTICA');
    /*
      private $DESTINOS = array(
      "TOD"   => array(	"nombre" => "Qualquier destino"),
      "EPA" 	=> array(	"nombre" => "Esp/Port/And"),
      "PEN" 	=> array(	"nombre" => "Peninsular")
      );
     */
 
    public $PREFIJO_REFERENCIA = "pedido_";

     
     public function __construct() {
    
    }
    
    public function __destruct() {
    
    }
    
    public static function isNcxCarrier($id_carrier) {
        $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier = "' . $id_carrier . '"');

        if (isset($datoscarrier) && isset($datoscarrier[0])) {
            nacexutils::writeNacexLog("isNcxCarrier :: [" . $id_carrier . "] => true");
            return $datoscarrier[0]['external_module_name'] == "nacex";
        } else {
            nacexutils::writeNacexLog("isNcxCarrier :: [" . $id_carrier . "] => false");
            return false;
        }
    }

    public function isNacexCarrier($id_carrier) {
        $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier = "' . $id_carrier . '"');

        if (isset($datoscarrier) && isset($datoscarrier[0])) {
            nacexutils::writeNacexLog("isNcxCarrier :: [" . $id_carrier . "] => " . (($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] != "NACEXSHOP" && $datoscarrier[0]['ncx'] != "nacexshop") || ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['ncx'] == "nacex")));
            return (
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] != "NACEXSHOP" && $datoscarrier[0]['ncx'] != "nacexshop") ||
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['ncx'] == "nacex")
                    );
        } else {
            nacexutils::writeNacexLog("isNcxCarrier :: [" . $id_carrier . "] => false");
            return false;
        }
    }

    public static function isNacexShopCarrier($id_carrier) {
        $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier = "' . $id_carrier . '"');

        if (isset($datoscarrier) && isset($datoscarrier[0])) {
            nacexutils::writeNacexLog("isNacexShopCarrier :: [" . $id_carrier . "] => " . (($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] != "NACEX" && $datoscarrier[0]['ncx'] != "nacex") || ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['ncx'] == "nacexshop")));
            return (
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['name'] != "NACEX" && $datoscarrier[0]['ncx'] != "nacex") ||
                    ($datoscarrier[0]['external_module_name'] == "nacex" && $datoscarrier[0]['ncx'] == "nacexshop")
                    );
        } else {
            nacexutils::writeNacexLog("isNacexShopCarrier :: [" . $id_carrier . "] => false");
            return false;
        }
    }

    public static function getNacexIdCarrier() {
        return Configuration::get('TRANSPORTISTA_NACEX');
    }

    public static function getNacexShopIdCarrier() {
        return Configuration::get('TRANSPORTISTA_NACEXSHOP');
    }

    public function getSeguros() {
        return $this->SEGUROS;
    }

    public function getContenidos() {
        return $this->CONTENIDOS;
    }

    public function getServiciosNacex() {
        return $this->SERVICIOS;
    }

    public function getServiciosNacexInt() {
        return $this->SERVICIOS_INT;
    }

    public function getServiciosNacexShop() {
        return $this->SERVICIOS_NACEX_SHOP;
    }

    public function getAllServiciosNacex() {
        //Se hace de esta manera ya que array_merge() no devuelve lo esperado
        $aux = array();

        foreach ($this->SERVICIOS as $key => $servicio) {
            $aux[$key] = $servicio;
        }
        foreach ($this->SERVICIOS_NACEX_SHOP as $key => $servicio) {
            $aux[$key] = $servicio;
        }
        return $aux;
    }

    public static function getModuleNacexName() {
        return $this->_moduleName;
    }
    
    /* BASE URI PS 1.7 Options
            __PS_BASE_URI__;
            Tools::getHttpHost(true).__PS_BASE_URI__;
            $this->context->link->getPageLink('index',true);
            Context::getContext()->shop->getBaseURL(true);
    */
    public static function getPath() {
          return Context::getContext()->shop->getBaseURL(true)."modules/". nacexutils::_moduleName."/";
    }

    public function getServSeparador() {
        return $this->SERV_SEP;
    }

    /* public function getDestinos(){
      return $this->DESTINOS;
      } */


    public static function getURL_PRO_Applets() {

        $protocol = self::$_http;

        if (isset($_SERVER['HTTPS'])) {
            $protocol = self::$_https;
        } else {
            $protocol = self::$_http;
        }

        return $protocol . self::$URL_PRO_Applets;
    }

    public function getHostURLImpresion() {
        $printHost= Configuration::get('NACEX_PRINT_IONA')=="" ?
	//	substr(Configuration::get('NACEX_PRINT_URL'), 0, strpos(Configuration::get('NACEX_PRINT_URL'), "/applets")):
	Configuration::get('NACEX_PRINT_URL'):
	Configuration::get('NACEX_PRINT_IONA');
        return $printHost;
    }
}

?>