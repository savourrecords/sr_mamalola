<?php
session_start();

class CPuntoNacexShop{
	
	private $codigo;
	private $alias;
	private $nombre;
	private $direccion;
	private $cp;
	private $poblacion;
	private $provincia;
	private $telefono;
	
	
	public function getCodigo(){
		return $this->codigo;
	}
	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}
	
	public function getAlias(){
		return $this->alias;
	}
	public function setAlias($alias){
		$this->alias = $alias;
	}
	
	public function getNombre(){
		return $this->nombre;
	}
	public function setNombre($nombre){
		$this->nombre = $nombre;
	}
	public function getDireccion(){
		return $this->direccion;
	}
	public function setDireccion($direccion){
		$this->direccion = $direccion;
	}
	public function getCp(){
		return $this->cp;
	}
	public function setCp($cp){
		$this->cp = $cp;
	}
	public function getPoblacion(){
		return $this->poblacion;
	}
	public function setPoblacion($poblacion){
		$this->poblacion = $poblacion;
	}
	public function getProvincia(){
		return $this->provincia;
	}
	public function setProvincia($provincia){
		$this->provincia = $provincia;
	}
	public function getTelefono(){
		return $this->telefono;
	}
	public function setTelefono($telefono){
		$this->telefono = $telefono;
	}
	
	public function setDatosNXSH_Session($txt){
		$datos = explode("|", $txt);
		
		$cpnx = new CPuntoNacexShop;
		$cpnx->setCodigo($datos[0]);
		$cpnx->setAlias($datos[1]);
		$cpnx->setNombre($datos[2]);
		$cpnx->setDireccion($datos[3]);
		$cpnx->setCp($datos[4]);
		$cpnx->setPoblacion($datos[5]);
		$cpnx->setProvincia($datos[6]);
		$cpnx->setTelefono($datos[7]);
		
		$_SESSION['cpnd'] = $cpnx;
	}
	
	public function getDatosNXSH_Session(){
		
		$valor = "";
		
		if(isset($_SESSION['cpnd'])){
			$cpnx = $_SESSION['cpnd'];
			$valor.=$cpnx->getCodigo()."|";
			$valor.=$cpnx->getAlias()."|";
			$valor.=$cpnx->getNombre()."|";
			$valor.=$cpnx->getDireccion()."|";
			$valor.=$cpnx->getCp()."|";
			$valor.=$cpnx->getPoblacion()."|";
			$valor.=$cpnx->getProvincia()."|";
			$valor.=$cpnx->getTelefono();
		}
		
		return $valor;
	}
	
	public function unsetDatosNXSH_Session(){
		unset($_SESSION['cpnd']);
	}

}
	$txt = Tools::getValue('txt')!=''?Tools::getValue('txt'):'';
	
	$method = Tools::getValue('metodo_nacex');
	$aux = new CPuntoNacexShop();
	
	if($method=="setSession"){
		$aux->setDatosNXSH_Session($txt);
	}else if($method=="getSession"){
		echo $aux->getDatosNXSH_Session(); 
	}else if($method=="unsetSession"){
		$aux->unsetDatosNXSH_Session();
	}

