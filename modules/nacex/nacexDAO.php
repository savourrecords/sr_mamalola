<?php

include_once("AdminConfig.php");
include_once("nacexutils.php");
include_once("nacexDTO.php");
/*
 * mexpositop 2017
 */
//session_start(); mexpositop 20180201

class nacexDAO {

    public static function guardarExpedicion($agcli, $id_order, $putExpedicionResponse, $bultos, $array_shop_data, $ret, $serv_cod, $nacex_reembolso) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI guardarExpedicion :: id_order: " . $id_order);

        nacexDAO::createTablesIfNotExists();

        $shop_codigo = isset($array_shop_data[0]) ? $array_shop_data[0] : "";
        $shop_alias = isset($array_shop_data[1]) ? $array_shop_data[1] : "";
        $shop_nombre = isset($array_shop_data[2]) ? $array_shop_data[2] : "";
        $shop_direccion = isset($array_shop_data[3]) ? $array_shop_data[3] : "";
        $shop_cp = isset($array_shop_data[4]) ? $array_shop_data[4] : "";
        $shop_poblacion = isset($array_shop_data[5]) ? $array_shop_data[5] : "";
        $shop_provincia = isset($array_shop_data[6]) ? $array_shop_data[6] : "";
        $shop_telefono = isset($array_shop_data[7]) ? $array_shop_data[7] : "";
        $estado = "PENDIENTE";
        $imp_ree = isset($nacex_reembolso) && $nacex_reembolso != null ? number_format($nacex_reembolso, 2, ".", "") : 0;

        if (Db::getInstance()->ExecuteS('SELECT id_envio_order FROM ' . _DB_PREFIX_ . 'nacex_expediciones where id_envio_order = "' . $id_order . '"')) {
            $result = Db::getInstance()->execute(
                    'INSERT INTO ' . _DB_PREFIX_ . 'nacex_expediciones_his
			SELECT id_envio_order, agcli, fecha_alta, fecha_baja, ref,
			exp_cod, ag_cod_num_exp, color, ent_ruta, ent_cod,
			ent_nom, ent_tlf, serv_cod, serv, hora_entrega,
			barcode, fecha_objetivo, cambios, bultos, shop_codigo,
			shop_alias, shop_nombre, shop_direccion, shop_cp, shop_poblacion,
			shop_provincia, shop_telefono, ret, estado, fecha_estado, imp_ree  
			FROM ' . _DB_PREFIX_ . 'nacex_expediciones 
			WHERE id_envio_order = "' . $id_order . '"');

            nacexutils::writeNacexLog("INSERT guardarExpedicion :: Insertada expedicion en nacex_expediciones_his");
        }
        //nacexutils::getReferenciaGeneral().$id_order. referencia ajuste
        $result = Db::getInstance()->execute(
                'REPLACE INTO ' . _DB_PREFIX_ . 'nacex_expediciones (
                            id_envio_order, agcli, fecha_alta, ref,exp_cod, ag_cod_num_exp, color, ent_ruta, ent_cod, 
                            ent_nom, ent_tlf, serv_cod,serv, hora_entrega, barcode, fecha_objetivo, cambios, bultos,
                            shop_codigo, shop_alias,shop_nombre,shop_direccion,shop_cp,shop_poblacion,shop_provincia,
                            shop_telefono,ret,estado,fecha_estado,imp_ree 										
			)VALUES( 
                            "' . $id_order . '","' . $agcli . '","' . date('Y-m-d H:i:s') . '",
                            "' . $putExpedicionResponse["ref"] . '", "' . $putExpedicionResponse["exp_cod"] . '",
                            "' . $putExpedicionResponse["ag_cod_num_exp"] . '","' . $putExpedicionResponse["color"] . '",
                            "' . $putExpedicionResponse["ent_ruta"] . '","' . $putExpedicionResponse["ent_cod"] . '",
                            "' . $putExpedicionResponse["ent_nom"] . '","' . $putExpedicionResponse["ent_tlf"] . '",
                            "' . $serv_cod . '","' . $putExpedicionResponse["serv"] . '",
                            "' . $putExpedicionResponse["hora_entrega"] . '","' . $putExpedicionResponse["barcode"] . '",
                            "' . $putExpedicionResponse["fecha_objetivo"] . '","' . $putExpedicionResponse["cambios"] . '",
                            "' . $bultos . '","' . $shop_codigo . '","' . $shop_alias . '","' . $shop_nombre . '","' . $shop_direccion . '",
                            "' . $shop_cp . '","' . $shop_poblacion . '","' . $shop_provincia . '","' . $shop_telefono . '","' . $ret . '",
                            "' . $estado . '","' . date('Y-m-d H:i:s') . '",' . $imp_ree . ')'
        );

        $resultado = $result ? 'Insertada expedicion en nacex_expediciones' : 'ERROR expedicion en nacex_expediciones';
        nacexutils::writeNacexLog("INSERT guardarExpedicion :: " . $resultado);

        nacexutils::writeNacexLog("FIN guardarExpedicion :: id_order: " . $id_order);
        nacexutils::writeNacexLog("----");

        return $result;
    }

    public static function setTransportistasBackend() {

        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI setTransportistasBackend");

        $default_generic_nacex = Configuration::get('NACEX_DEFAULT_TIP_SER');
        $default_generic_nacexshop = Configuration::get('NACEX_DEFAULT_TIP_NXSHOP_SER');
        $default_generic_name_nacex = Configuration::get('NACEX_GEN_SERV_NAME');
        $default_generic_name_nacexshop = Configuration::get('NACEXSHOP_GEN_SERV_NAME');

        //Obtenemos el id del transportista genérico, si es que existe
        $default_generic_nacex_id = Configuration::get('TRANSPORTISTA_NACEX');
        $default_generic_nacexshop_id = Configuration::get('TRANSPORTISTA_NACEXSHOP');

        $tracking_url = substr(Configuration::get('NACEX_PRINT_URL'), 0, strpos(Configuration::get('NACEX_PRINT_URL'), '/applets')) . '/seguimientoFormularioExterno.do?intcli=@';

        //Comprobamos si el transportista con ese ID existe en BD
        if (isset($default_generic_nacex_id) && strlen($default_generic_nacex_id) > 0)
            $existe_gen_nacex = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier =' . $default_generic_nacex_id);
        else
            $existe_gen_nacex = false;

        if (isset($default_generic_nacexshop_id) && strlen($default_generic_nacexshop_id) > 0)
            $existe_gen_nacexshop = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.id_carrier =' . $default_generic_nacexshop_id);
        else
            $existe_gen_nacexshop = false;

        //Si no existe transportista genérico, lo creamos	
        if (!$existe_gen_nacex) {
            $carrierNacex = array(
                'name' => $default_generic_name_nacex,
                'id_tax_rules_group' => 0,
                'active' => true,
                'deleted' => 0,
                'shipping_handling' => false,
                'range_behavior' => 0,
                'delay' => array(Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => 'Con total entrega'),
                'id_zone' => 1,
                'is_module' => true,
                'shipping_external' => true,
                'external_module_name' => nacexutils::getModuleName(),
                'ncx' => 'nacex',
                'need_range' => true,
                'tip_serv' => $default_generic_nacex,
                'url' => $tracking_url
            );

            $id_transportista_nacex = self::instalarTransportista($carrierNacex, "nacex.png");
            Configuration::updateValue('TRANSPORTISTA_NACEX', (int) $id_transportista_nacex);

            nacexutils::writeNacexLog("setTransportistasBackend :: Instalado transportista NACEX GENERICO con id [" . $id_transportista_nacex . "]");
        } else {
            //Si existe transportista genérico actualizamos	
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier c SET c.deleted=0, 
																																			c.ncx="nacex", 
																																			c.name="' . $default_generic_name_nacex . '",
																																			c.tip_serv="' . $default_generic_nacex . '" ,
																																			c.url="' . $tracking_url . '"
																	WHERE c.id_carrier =' . $default_generic_nacex_id);

            //Si no existe logo lo añadimos									
            $nombre_fichero = _PS_SHIP_IMG_DIR_ . '/' . $default_generic_nacex_id . '.jpg';
            if (!file_exists($nombre_fichero)) {
                copy(dirname(__FILE__) . '/img/servicios/nacex.png', _PS_SHIP_IMG_DIR_ . '/' . $default_generic_nacex_id . '.jpg');
            }

            nacexutils::writeNacexLog("setTransportistasBackend :: Actualizado transportista NACEX GENERICO con id [" . $default_generic_nacex_id . "]");
        }

        //Si no existe transportista genérico nacexshop, lo creamos	
        if (!$existe_gen_nacexshop) {
            $carrierNacexShop = array(
                'name' => $default_generic_name_nacexshop,
                'id_tax_rules_group' => 0,
                'active' => true,
                'deleted' => 0,
                'shipping_handling' => false,
                'range_behavior' => 0,
                'delay' => array(Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => utf8_encode('Estamos cuando tú no estás')),
                'id_zone' => 1,
                'is_module' => true,
                'shipping_external' => true,
                'external_module_name' => nacexutils::getModuleName(),
                'ncx' => 'nacexshop',
                'need_range' => true,
                'tip_serv' => $default_generic_nacexshop,
                'url' => $tracking_url
            );

            $id_transportista_nacexshop = self::instalarTransportista($carrierNacexShop, "nacexshop.png");
            Configuration::updateValue('TRANSPORTISTA_NACEXSHOP', (int) $id_transportista_nacexshop);

            nacexutils::writeNacexLog("setTransportistasBackend :: Instalado transportista NACEXSHOP GENERICO con id [" . $id_transportista_nacexshop . "]");
        } else {
            //Si existe transportista genérico nacexshop actualizamos
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier c SET c.deleted=0, 
																																			c.ncx="nacexshop" , 
																																			c.name="' . $default_generic_name_nacexshop . '",
																																			c.tip_serv="' . $default_generic_nacexshop . '",
																																			c.url="' . $tracking_url . '" 
																	WHERE c.id_carrier =' . $default_generic_nacexshop_id);

            //Si no existe logo lo añadimos									
            $nombre_fichero = _PS_SHIP_IMG_DIR_ . '/' . $default_generic_nacexshop_id . '.jpg';
            if (!file_exists($nombre_fichero)) {
                copy(dirname(__FILE__) . '/img/servicios/nacexshop.png', _PS_SHIP_IMG_DIR_ . '/' . $default_generic_nacexshop_id . '.jpg');
            }

            nacexutils::writeNacexLog("setTransportistasBackend :: Actualizado transportista NACEXSHOP GENERICO con id [" . $default_generic_nacexshop_id . "]");
        }
        nacexutils::writeNacexLog("FIN setTransportistasBackend");
        nacexutils::writeNacexLog("----");
    }

    public static function setTransportistasFrontend() {

        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI setTransportistasFrontend");

        $nacexDTO = new nacexDTO();
        //------Para servicios estándard-----------------------------------------------------------------------

        $piped_servs_f = Configuration::get('NACEX_AVAILABLE_SERVS_F');
        if (!empty($piped_servs_f)) {
            $array_servs_f = explode("|", $piped_servs_f);
        } else {
            $array_servs_f = null;
        }
        $ids_instalados = "";

        $tracking_url = substr(Configuration::get('NACEX_PRINT_URL'), 0, strpos(Configuration::get('NACEX_PRINT_URL'), '/applets')) . '/seguimientoFormularioExterno.do?intcli=@';

        //Recorremos los servicios Frontend seleccionados en configuración para convertirlos en Transportistas
        for ($i = 0; $i < count($array_servs_f); $i++) {
            //Si no existe Transportista	
            if (!Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.name = "NACEX_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" and c.ncx = "nacex" ORDER BY c.id_carrier DESC LIMIT 1')) {
                //echo "<br><h1><font color='red'>NO EXISTE NACEX_".$array_servs_f[$i]."</font></h1><br>";							
                $carrierNacex = array(
                    'name' => 'NACEX_' . $array_servs_f[$i],
                    'id_tax_rules_group' => 0,
                    'active' => true,
                    'deleted' => 0,
                    'shipping_handling' => false,
                    'range_behavior' => 0,
                    'delay' => array(Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => $nacexDTO->getServiciosNacex()[$array_servs_f[$i]]["nombre"] . ". " . $nacexDTO->getServiciosNacex()[$array_servs_f[$i]]["descripcion"]),
                    'id_zone' => 1,
                    'is_module' => true,
                    'shipping_external' => true,
                    'external_module_name' => nacexutils::_moduleName,
                    'ncx' => 'nacex',
                    'need_range' => true,
                    'tip_serv' => $array_servs_f[$i],
                    'url' => $tracking_url
                );

                $ids_instalados .= self::instalarTransportista($carrierNacex, "nacex_servicio_" . $array_servs_f[$i] . ".png") . "|";
                nacexutils::writeNacexLog("setTransportistasFrontend :: instalado transportista NACEX_" . $array_servs_f[$i]);
            } else {
                //Si existe actualizamos ID por si fuera distinto	
                //antes nos aseguramos de que no está borrado	
                Db::getInstance()->execute(
                        'UPDATE ' . _DB_PREFIX_ . 'carrier c SET c.deleted=0, c.tip_serv="' . $array_servs_f[$i] . '", c.url="' . $tracking_url . '" WHERE c.name = "NACEX_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" and c.ncx = "nacex" ORDER BY c.id_carrier DESC LIMIT 1'
                );

                $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.name = "NACEX_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" AND c.ncx = "nacex" ORDER BY c.id_carrier DESC LIMIT 1');
                $ids_instalados .= (int) $datoscarrier[0]['id_carrier'] . "|";
                nacexutils::writeNacexLog("setTransportistasFrontend :: actualizado transportista NACEX_" . $array_servs_f[$i]);
            }
        }

        //Eliminamos último PIPE
        $ids_instalados = substr_replace($ids_instalados, "", strrpos($ids_instalados, "|"));
        Configuration::updateValue('NACEX_ID_TRANSPORTISTAS_F', $ids_instalados);

        //------Para servicios NacexShop-----------------------------------------------------------------------

        $piped_servs_f = Configuration::get('NACEX_AVAILABLE_SERVS_NXSHOP_F');
        if (!empty($piped_servs_f)) {
            $array_servs_f = explode("|", $piped_servs_f);
        } else {
            $array_servs_f = null;
        }
        $ids_instalados = "";

        //Recorremos los servicios Nacexshop del Frontend seleccionados en configuración para convertirlos en Transportistas
        for ($i = 0; $i < count($array_servs_f); $i++) {

            //Si no existe Transportista	
            if (!Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.name = "NACEXSHOP_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" AND c.ncx = "nacexshop" ORDER BY c.id_carrier DESC LIMIT 1')) {
                //echo "<br><h1><font color='red'>NO EXISTE NACEXSHOP_".$array_servs_f[$i]."</font></h1><br>";			
                $carrierNacex = array(
                    'name' => 'NACEXSHOP_' . $array_servs_f[$i],
                    'id_tax_rules_group' => 0,
                    'active' => true,
                    'deleted' => 0,
                    'shipping_handling' => false,
                    'range_behavior' => 0,
                    'delay' => array(Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => $nacexDTO->getServiciosNacexShop()[$array_servs_f[$i]]["nombre"] . ". " . $nacexDTO->getServiciosNacexShop()[$array_servs_f[$i]]["descripcion"]),
                    'id_zone' => 1,
                    'is_module' => true,
                    'shipping_external' => true,
                    'external_module_name' => nacexutils::_moduleName,
                    'ncx' => 'nacexshop',
                    'need_range' => true,
                    'tip_serv' => $array_servs_f[$i],
                    'url' => $tracking_url
                );

                $ids_instalados .= self::instalarTransportista($carrierNacex, "nacexshop_servicio_" . $array_servs_f[$i] . ".png") . "|";
                nacexutils::writeNacexLog("setTransportistasFrontend :: instalado transportista NACEXSHOP_" . $array_servs_f[$i]);
            } else {
                //Si existe actualizamos ID por si fuera distinto	
                //antes nos aseguramos de que no está borrado	
                Db::getInstance()->execute(
                        'UPDATE ' . _DB_PREFIX_ . 'carrier c SET c.deleted=0, c.tip_serv="' . $array_servs_f[$i] . '", c.url="' . $tracking_url . '" WHERE c.name = "NACEXSHOP_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" AND c.ncx = "nacexshop" ORDER BY c.id_carrier DESC LIMIT 1'
                );

                $datoscarrier = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.name = "NACEXSHOP_' . $array_servs_f[$i] . '" AND c.external_module_name = "nacex" AND c.ncx = "nacexshop" ORDER BY c.id_carrier DESC LIMIT 1');
                $ids_instalados .= (int) $datoscarrier[0]['id_carrier'] . "|";
                nacexutils::writeNacexLog("setTransportistasFrontend :: actualizado transportista NACEXSHOP_" . $array_servs_f[$i]);
            }
        }

        //Eliminamos último PIPE
        $ids_instalados = substr_replace($ids_instalados, "", strrpos($ids_instalados, "|"));
        Configuration::updateValue('NACEX_ID_TRANSPORTISTAS_NXSHOP_F', $ids_instalados);
        nacexutils::writeNacexLog("FIN setTransportistasFrontend");
        nacexutils::writeNacexLog("---");
    }

    public static function instalarTransportista($config, $logo) {

        $tracking_url = substr(Configuration::get('NACEX_PRINT_URL'), 0, strpos(Configuration::get('NACEX_PRINT_URL'), '/applets')) . '/seguimientoFormularioExterno.do?intcli=@';

        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];
        $carrier->url = $tracking_url;

        $languages = Language::getLanguages(true);
        foreach ($languages as $language) {
            if ($language['iso_code'] == 'fr')
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == 'en')
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
                $carrier->delay[(int) $language['id_lang']] = $config['delay'][$language['iso_code']];
        }

        if ($carrier->add()) {
            $groups = Group::getGroups(true);
            foreach ($groups as $group) {
                /* mexpositop 20171116
                 * 	Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_group', 
                  array(  'id_carrier' => (int)($carrier->id),
                  'id_group' => (int)($group['id_group'])),
                  'INSERT');
                 */
                Db::getInstance()->insert(_DB_PREFIX_ . 'carrier_group', array('id_carrier' => (int) ($carrier->id),
                    'id_group' => (int) ($group['id_group'])));
            }
            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '1000000000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '1000000000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone) {
                /* mexpositop 20171116
                  Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])), 'INSERT');
                  Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
                  Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
                 */
                Db::getInstance()->insert(_DB_PREFIX_ . 'carrier_zone', array('id_carrier' => (int) ($carrier->id),
                    'id_zone' => (int) ($zone['id_zone'])));
                Db::getInstance()->insert(_DB_PREFIX_ . 'delivery', array('id_carrier' => (int) ($carrier->id),
                    'id_range_price' => (int) ($rangePrice->id),
                    'id_range_weight' => NULL,
                    'id_zone' => (int) ($zone['id_zone']),
                    'price' => '0'));
                Db::getInstance()->insert(_DB_PREFIX_ . 'delivery', array('id_carrier' => (int) ($carrier->id),
                    'id_range_price' => NULL,
                    'id_range_weight' => (int) ($rangeWeight->id),
                    'id_zone' => (int) ($zone['id_zone']),
                    'price' => '0'));
            }

            // Copiamos el logo
            copy(dirname(__FILE__) . '/img/servicios/' . $logo, _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.jpg');


            Db::getInstance()->execute(
                    'UPDATE ' . _DB_PREFIX_ . 'carrier SET ncx="' . $config["ncx"] . '", tip_serv="' . $config["tip_serv"] . '", url="' . $tracking_url . '" WHERE id_carrier = "' . $carrier->id . '"'
            );
            // Return ID Carrier
            return (int) ($carrier->id);
        } else {
            return -110;
        }

        return -111;
    }

    public static function activarServicios($modo = "backend") {
        if ($modo == "backend") {
            //Desactivamos todo	
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=0, deleted=1 WHERE external_module_name = "nacex"');

            //Activamos Nacex y NacexShop			
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=1, deleted=0 WHERE id_carrier = "' . nacexDTO::getNacexIdCarrier() . '"');
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=1, deleted=0 WHERE id_carrier = "' . nacexDTO::getNacexShopIdCarrier() . '"');
        } else {
            //Desactivamos todo	
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=0, deleted=1 WHERE external_module_name = "nacex"');

            $ids_servicios_frontend = Configuration::get('NACEX_ID_TRANSPORTISTAS_F');

            if (isset($ids_servicios_frontend) && !empty($ids_servicios_frontend)) {
                $arrayids = explode("|", $ids_servicios_frontend);
                for ($i = 0; $i < count($arrayids); $i++) {
                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=1, deleted=0 WHERE id_carrier = "' . $arrayids[$i] . '"');
                }
            }

            $ids_servicios_nxshop_frontend = Configuration::get('NACEX_ID_TRANSPORTISTAS_NXSHOP_F');

            if (isset($ids_servicios_nxshop_frontend) && !empty($ids_servicios_nxshop_frontend)) {
                $arrayids = explode("|", $ids_servicios_nxshop_frontend);
                for ($i = 0; $i < count($arrayids); $i++) {
                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'carrier SET active=1, deleted=0 WHERE id_carrier = "' . $arrayids[$i] . '"');
                }
            }
        }
    }

    public static function getImporteWebservice($params, $shipping_cost, $id_carrier) {
        global $cookie;
        $result = 0;
        $id_customer = $params->id_customer;
        $id_shippin_addr = $params->id_address_delivery;

        if (CustomerCore::getAddressesTotalById($id_customer) > 0) {
            $textLog = "getImporteWebservice :: No lanzamos llamada a getValoracion. Sin CP o recuperado de cache. (Carrier:";
            $addr = new Address($id_shippin_addr);
            $carrier = self::getCarrierById($id_carrier);
            $cp_ent = $addr->postcode;
            $tip_ser = $carrier[0]["tip_serv"];
            $kil = $params->gettotalWeight() < 1 ? 1 : $params->gettotalWeight();

            if (isset($_SESSION[$cp_ent . "-" . $tip_ser . "-" . $kil])) {
                $result = $_SESSION[$cp_ent . "-" . $tip_ser . "-" . $kil];                
           } else {
                if (!empty($cp_ent) && strlen($cp_ent) > 0) {                 //Valoración si tenemos CP de entrega
                    $ws_response = nacexWS::ws_getValoracion($cp_ent, $tip_ser, $kil);
                    
                    if (isset($ws_response) && isset($ws_response[0])&& $ws_response[0] != "ERROR" && sizeof($ws_response) > 1) {
                            $result = floatval(str_replace(",", ".", $ws_response[1]));
                        }
                    
                    $_SESSION[$cp_ent . "-" . $tip_ser . "-" . $kil] = $result;
                    $textLog = "getImporteWebservice :: Calculamos importe con WS (Carrier:";
                }
            }
        }

        $shipping_cost = number_format($result, 2, ".", "");
        nacexutils::writeNacexLog($textLog . $id_carrier . " => " . $shipping_cost . " euros)");
        return $shipping_cost;
    }

    public static function actDatosNacexExpediciones($id_pedido, $getDatosWSExpedicion, $estado, $fecha_estado_exp, $hora_estado_exp) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI actDatosNacexExpediciones :: id_pedido: " . $id_pedido . " estado:" . $estado . " fecha_estado_exp:" . $fecha_estado_exp . " hora_estado_exp:" . $hora_estado_exp);


        nacexDAO::createTablesIfNotExists();
        $serv_cod = $getDatosWSExpedicion["serv_cod"];
        $referencias = explode(";", $getDatosWSExpedicion["ref"]);
        $newDate = $fecha_estado_exp . "" . $hora_estado_exp;

        if (!empty($newDate) && strlen($newDate) > 0) {
            $arrayfecha = explode("/", $fecha_estado_exp);
            $arrayhora = explode(":", $hora_estado_exp);
            $dia = $arrayfecha[0];
            $mes = $arrayfecha[1];
            $any = $arrayfecha[2];
            $hora = $arrayhora[0];
            $min = $arrayhora[1];
            $seg = $arrayhora[2];
            $newDate = date("Y-m-d H:i:s", mktime($hora, $min, $seg, $mes, $dia, $any));
        } else {
            $newDate = date("Y-m-d H:i:s");
        }


        $query = 'UPDATE ' . _DB_PREFIX_ . 'nacex_expediciones SET
					serv_cod="' . $serv_cod . '",
					estado="' . $estado . '",
					ref="' . $referencias[0] . '",
					fecha_estado = "' . $newDate . '"
					WHERE id_envio_order = "' . $id_pedido . '"';
        Db::getInstance()->execute($query);
        nacexutils::writeNacexLog("actDatosNacexExpediciones :: actualizados campos [" . $serv_cod . "," . $estado . "," . $referencias[0] . "," . $newDate . "] en [nacex_expediciones] del pedido:" . $id_pedido);
    }

    public static function getExpRelacionadas($id_pedido) {
        $arraydatos = array();
        $arraydatos = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'nacex_expediciones_his where id_envio_order = "' . $id_pedido . '" order by ag_cod_num_exp desc');
        return $arraydatos;
    }

    public function tieneExpedicion($id_pedido) {

        $sql = "SELECT * FROM " . _DB_PREFIX_ . "nacex_expediciones where fecha_baja is null and id_envio_order =" . $id_pedido;

        $result = Db::getInstance()->ExecuteS($sql);

        return $result;
    }

    public static function actualizarTrackingExpedicion($id_pedido, $numexp) {
        if (Configuration::get('NACEX_ACT_TRACKING') == "SI") {
            nacexutils::writeNacexLog("actualizarTrackingExpedicion :: añadimos informacion del tracking de la Expedición " . $numexp);
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'orders SET shipping_number="' . $numexp . '" WHERE id_order = "' . $id_pedido . '"');
            Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'order_carrier SET tracking_number="' . $numexp . '" WHERE id_order = "' . $id_pedido . '"');
        }
    }

    public static function setNacexShopAddressinBD($id_order, $id_cart, $id_address, $shop_datos) {

        nacexutils::writeNacexLog("-----");
        nacexutils::writeNacexLog("INI setNacexShopAddressinBD :: id_order: " . $id_order . "|id_cart: " . $id_cart . "|id_address: " . $id_address . "|shop_datos: " . $shop_datos);

        $array_shop_data = nacexutils::explodeShopData($shop_datos);

        $dirnacexshop = Db::getInstance()->ExecuteS(
                'SELECT * FROM ' . _DB_PREFIX_ . 'address WHERE 
				alias = "' . $array_shop_data["shop_codigo"] . '" AND 
				firstname= "' . $array_shop_data["shop_alias"] . '" AND 
				lastname = "' . $array_shop_data["shop_nombre"] . '" AND 
				address1 = "' . $array_shop_data["shop_direccion"] . '" AND 
				postcode = "' . $array_shop_data["shop_cp"] . '" AND 
				city = "' . $array_shop_data["shop_poblacion"] . '" AND 
				other = "' . $array_shop_data["shop_provincia"] . '"');

        if (!empty($dirnacexshop)) {
            //Si existe sobreescribimos id_address_delivery en ORDERS y CART
            nacexutils::writeNacexLog("setNacexShopAddressinBD :: Sobreescribimos id_address_delivery en ORDERS y CART");
            Db::getInstance()->execute(
                    'UPDATE ' . _DB_PREFIX_ . 'orders SET id_address_delivery="' . $dirnacexshop[0]["id_address"] . '" WHERE id_order = "' . $id_order . '"'
            );
            Db::getInstance()->execute(
                    'UPDATE ' . _DB_PREFIX_ . 'cart SET id_address_delivery="' . $dirnacexshop[0]["id_address"] . '" WHERE id_cart = "' . $id_cart . '"'
            );
        } else {
            //Si no existe la creamos
            nacexutils::writeNacexLog("setNacexShopAddressinBD :: Creamos nueva direccion NacexShop");
            $array_address = nacexDAO::getAddressById($id_address);
            $id_country = $array_address[0]['id_country'];

            $new_address = new Address();
            $new_address->id_customer = NULL;
            $new_address->id_manufacturer = NULL;
            $new_address->id_supplier = NULL;
            $new_address->id_country = $id_country;
            $new_address->id_state = 0;

            $new_address->alias = $array_shop_data["shop_codigo"];
            //Los dos siguientes son provisionales, luego se sobreescriben con mysql porque la clase no permite caracteres numéricos para estos dos campos
            $new_address->firstname = "nacexshop";
            $new_address->lastname = "nacexshop";
            $new_address->address1 = $array_shop_data["shop_direccion"];
            $new_address->postcode = $array_shop_data["shop_cp"];
            $new_address->city = $array_shop_data["shop_poblacion"];
            $new_address->other = $array_shop_data["shop_provincia"];
            //$new_address->phone		= $array_shop_data["shop_telefono"];
            $new_address->phone = $array_address[0]["phone"];
            $new_address->phone_mobile = $array_address[0]["phone_mobile"];

            $new_address->date_add = date("Y-m-d H:i:s");
            $new_address->date_upd = date("Y-m-d H:i:s");
            //La marcamos como borrada para que "no entre en el circuito"
            $new_address->deleted = 1;

            if ($new_address->add()) {

                $id_nueva = (int) ($new_address->id);
                Db::getInstance()->execute(
                        'UPDATE ' . _DB_PREFIX_ . 'address SET firstname="' . $array_shop_data["shop_alias"] . '", lastname="' . $array_shop_data["shop_nombre"] . '" WHERE id_address = "' . $id_nueva . '"'
                );
                nacexutils::writeNacexLog("UPDATE setNacexShopAddressinBD :: UPDATE " . _DB_PREFIX_ . "address SET firstname=" . $array_shop_data["shop_alias"] . ", lastname=" . $array_shop_data["shop_nombre"] . " WHERE id_address = " . $id_nueva);
                //Y sobreescribimos con el nuevo id_address el id_address_delivery en ORDERS y CART
                Db::getInstance()->execute(
                        'UPDATE ' . _DB_PREFIX_ . 'orders SET id_address_delivery="' . $id_nueva . '" WHERE id_order = "' . $id_order . '"'
                );
                nacexutils::writeNacexLog("UPDATE setNacexShopAddressinBD :: UPDATE " . _DB_PREFIX_ . "orders SET id_address_delivery=" . $id_nueva . " WHERE id_order = " . $id_order);
                Db::getInstance()->execute(
                        'UPDATE ' . _DB_PREFIX_ . 'cart SET id_address_delivery="' . $id_nueva . '" WHERE id_cart = "' . $id_cart . '"'
                );
                nacexutils::writeNacexLog("UPDATE setNacexShopAddressinBD :: UPDATE " . _DB_PREFIX_ . "cart SET id_address_delivery=" . $id_nueva . " WHERE id_cart = " . $id_cart);
            }
        }
        nacexutils::writeNacexLog("FIN setNacexShopAddressinBD :: id_order: " . $id_order);
        nacexutils::writeNacexLog("-----");
    }

    public static function getDatosExpedicionNacexShop($id_order) {
        return Db::getInstance()->ExecuteS('SELECT shop_codigo, shop_alias, shop_nombre, shop_direccion, shop_cp, shop_poblacion, shop_provincia, shop_telefono  FROM ' . _DB_PREFIX_ . 'nacex_expediciones WHERE id_envio_order = "' . $id_order . '"');
    }

    public static function getDatosExpedicionNacex($id_order) {
        return Db::getInstance()->ExecuteS('SELECT agcli, fecha_alta, ref, exp_cod, ag_cod_num_exp, color, ent_ruta, ent_cod, ent_nom, ent_tlf, serv, hora_entrega, barcode, fecha_objetivo, cambios, bultos, shop_codigo, shop_alias, shop_nombre, shop_direccion, shop_cp, shop_poblacion, shop_provincia, shop_telefono, ret FROM ' . _DB_PREFIX_ . 'nacex_expediciones WHERE fecha_baja IS null AND id_envio_order = "' . $id_order . '"');
    }

    public static function getDatosCartNacexShop($id_cart) {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI getDatosCartNacexShop :: id_cart:" . $id_cart);
        $array = Db::getInstance()->ExecuteS('SELECT ncx FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = "' . $id_cart . '"');
        //Devuelve : 254|0811-00|BARCELONA|P.COLON, 22|08002|BARCELONA|BARCELONA|654659897				
        nacexutils::writeNacexLog("getDatosCartNacexShop :: obtenidos datos nacexshop :" . $array[0]['ncx']);
        return nacexutils::explodeShopData($array[0]['ncx']);
    }

    public static function getAddressById($id_address) {
        return Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'address WHERE id_address = "' . $id_address . '"');
    }

    public static function getAddressInvoiceByOrder($id_order) {
        return Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'address WHERE id_address IN (select id_address_invoice from ' . _DB_PREFIX_ . 'orders where id_order = "' . $id_order . '")');
    }

    public static function getCarrierName($id_carrier) {
        $ret = Db::getInstance()->ExecuteS('SELECT name FROM ' . _DB_PREFIX_ . 'carrier WHERE id_carrier=' . $id_carrier);
        return @$ret[0]['name'];
    }

    public static function getCarrierById($id_carrier) {

        $query = "SELECT * 
				  FROM " . _DB_PREFIX_ . "carrier
				  where active=1 
				  and deleted = 0
				  and (upper(ncx) = 'NACEX' || upper(ncx)='NACEXSHOP')
				  and id_carrier = " . $id_carrier;

        return $datoscarrier = Db::getInstance()->ExecuteS($query);
    }

    public static function getDatosExpedicion($id_pedido) {

        $query = "SELECT id_envio_order, agcli, fecha_alta, ref,
						 exp_cod, ag_cod_num_exp, color, ent_ruta, 
						 ent_cod, ent_nom, ent_tlf, serv_cod,
						 serv,  hora_entrega, barcode, fecha_objetivo, 
						 cambios, bultos, shop_codigo, shop_alias,	
						 shop_nombre, shop_direccion, shop_cp, shop_poblacion,
						 shop_provincia, shop_telefono, ret, estado,
						 fecha_estado, imp_ree  
				  FROM " . _DB_PREFIX_ . "nacex_expediciones
				  where id_envio_order = " . $id_pedido;

        return $datosexpedicion = Db::getInstance()->ExecuteS($query);
    }

    public static function getDatosPedido($id_order) {
        return Db::getInstance()->ExecuteS(
                        'SELECT o.id_order,o.reference,o.module,u.email,a.firstname,
				a.lastname,a.address1,a.postcode,a.city,a.phone,a.phone_mobile,z.iso_code,c.ncx,o.id_carrier,
				case when o.total_paid_real > 0 
					then o.total_paid_real
				else
					o.total_paid
				end as total_paid_real 
				FROM ' . _DB_PREFIX_ . 'orders AS o
				JOIN ' . _DB_PREFIX_ . 'customer AS u
				JOIN ' . _DB_PREFIX_ . 'cart AS c
				JOIN ' . _DB_PREFIX_ . 'address a
				JOIN ' . _DB_PREFIX_ . 'country AS z
				WHERE o.id_order = "' . $id_order . '"
				AND u.id_customer = o.id_customer
				AND c.id_cart = o.id_cart
				AND a.id_address = o.id_address_delivery
				AND a.id_country = z.id_country'
        );
    }

    public static function cancelarExpedicion($id_order) {

        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI cancelarExpedicion :: id_order: " . $id_order);

        Db::getInstance()->execute(
                'UPDATE ' . _DB_PREFIX_ . 'nacex_expediciones SET
				fecha_baja = "' . date('Y-m-d H:i:s') . '",
				estado = "BAJA",
				fecha_estado="' . date('Y-m-d H:i:s') . '"
				WHERE id_envio_order = "' . $id_order . '"');

        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'orders SET shipping_number=null WHERE id_order = "' . $id_order . '"');

        nacexutils::writeNacexLog("UPDATE cancelarExpedicion :: Updateada expedicion en nacex_expediciones");
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("FIN cancelarExpedicion :: id_order: " . $id_order);
    }

    public static function createTablesIfNotExists() {
        nacexutils::writeNacexLog("----");
        nacexutils::writeNacexLog("INI createTablesIfNotExists");

        //Creamos la tabla para las expediciones
        $query = "CREATE TABLE IF NOT EXISTS " . _DB_PREFIX_ . "nacex_expediciones (
			id_envio_order int(11) NOT NULL,
			agcli varchar(10),
			fecha_alta datetime NOT NULL,
			fecha_baja datetime,
			ref varchar(20),
			exp_cod varchar(255),
			ag_cod_num_exp varchar(255),
			color varchar(255),
			ent_ruta varchar(255),
			ent_cod varchar(255),
			ent_nom varchar(255),
			ent_tlf varchar(255),
			serv_cod varchar(2),
			serv varchar(255),
			hora_entrega varchar(255),
			barcode varchar(255),
			fecha_objetivo varchar(255),
			cambios varchar(255),
			bultos varchar(3),			 
			shop_codigo 	varchar(6),
			shop_alias 	varchar(20),
			shop_nombre 	varchar(60),
			shop_direccion 	varchar(60),			
			shop_cp 	varchar(7),
			shop_poblacion varchar(60),
			shop_provincia 	varchar(60),
			shop_telefono 	varchar(60),
			ret 	varchar(2),
			estado varchar(20),
			fecha_estado datetime,
			imp_ree decimal(8,3),
		PRIMARY KEY (`id_envio_order`)
		) ENGINE=" . _MYSQL_ENGINE_ . " DEFAULT CHARSET=utf8";

        if (!Db::getInstance()->Execute($query)) {
            nacexutils::writeNacexLog("createTablesIfNotExists :: Error al crear tabla nacex_expediciones");
            return false;
        }

        //Creamos la tabla para el historico de expediciones 
        $query = "CREATE TABLE IF NOT EXISTS " . _DB_PREFIX_ . "nacex_expediciones_his (
			id_envio_order int(11) NOT NULL,
			agcli varchar(10),
			fecha_alta datetime NOT NULL,
			fecha_baja datetime,
			ref varchar(20),
			exp_cod varchar(255),
			ag_cod_num_exp varchar(255),
			color varchar(255),
			ent_ruta varchar(255),
			ent_cod varchar(255),
			ent_nom varchar(255),
			ent_tlf varchar(255),
			serv_cod varchar(2),
			serv varchar(255),
			hora_entrega varchar(255),
			barcode varchar(255),
			fecha_objetivo varchar(255),
			cambios varchar(255),
			bultos varchar(3),			 
			shop_codigo 	varchar(6),
			shop_alias 	varchar(20),
			shop_nombre 	varchar(60),
			shop_direccion 	varchar(60),			
			shop_cp 	varchar(7),
			shop_poblacion varchar(60),
			shop_provincia 	varchar(60),
			shop_telefono 	varchar(60),
			ret 	varchar(2),
			estado varchar(20),
			fecha_estado datetime,
			imp_ree decimal(8,3),
		PRIMARY KEY (`id_envio_order`,`ag_cod_num_exp`)
		) ENGINE=" . _MYSQL_ENGINE_ . " DEFAULT CHARSET=utf8";

        if (!Db::getInstance()->Execute($query)) {
            nacexutils::writeNacexLog("createTablesIfNotExists :: Error al crear tabla nacex_expediciones_his");
            return false;
        }

        //if(!Db::getInstance()->Execute("select ncx from "._DB_PREFIX_."cart limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "cart LIKE 'ncx'");
        if (Db::getInstance()->Affected_Rows() == 0) {

            //A�adimos campo adicional Nacex, en CART
            $query = "ALTER TABLE " . _DB_PREFIX_ . "cart ADD COLUMN ncx varchar (350)";
            try {
                Db::getInstance()->Execute($query);
            } catch (Exception $e) {
                nacexutils::writeNacexLog("createTablesIfNotExists :: Error al alter table [ncx] en " . _DB_PREFIX_ . "cart");
            }
        }
        //Forzamos a aumentar tama�o en este campo ncx de Cart para versiones anteriores con menor tama�o.		
        //En este campo guardamos info NacexShop
        $query = "ALTER TABLE " . _DB_PREFIX_ . "cart MODIFY ncx varchar (350)";
        Db::getInstance()->Execute($query);

        /* VERSIONES ANTERIORES */
        //A�adimos campos adicionales
        //if(!Db::getInstance()->Execute("select agcli from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'agcli'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN agcli varchar (10)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [agcli][varchar(10)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select bultos from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'bultos'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN bultos varchar (3)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [bultos][varchar(3)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_codigo from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_codigo'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_codigo varchar (6)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_codigo][varchar(6)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_alias from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_alias'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_alias varchar (20)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_alias][varchar(20)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_nombre from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_nombre'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_nombre varchar (60)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_nombre][varchar(60)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_direccion from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_direccion'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_direccion varchar (60)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_direccion][varchar(60)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }

        //if(!Db::getInstance()->Execute("select shop_cp from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_cp'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_cp varchar (7)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_cp][varchar(7)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_poblacion from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_poblacion'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_poblacion varchar (60)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_poblacion][varchar(60)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_provincia from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_provincia'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_provincia varchar (60)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_provincia][varchar(60)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select shop_telefono from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'shop_telefono'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN shop_telefono varchar (60)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [shop_telefono][varchar(60)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //if(!Db::getInstance()->Execute("select ret from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'ret'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN ret varchar (2)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [ret][varchar(2)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //Creamos columna ncx en Carrier
        //if(!Db::getInstance()->Execute("select ncx from "._DB_PREFIX_."carrier limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "carrier LIKE 'ncx'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "carrier ADD COLUMN ncx varchar (50)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [ncx][varchar(50)]en " . _DB_PREFIX_ . "carrier");
            } catch (Exception $e) {
                
            }
        }
        //Creamos campo serv_cod para documentacion nacex c@mbio
        //if(!Db::getInstance()->Execute("select serv_cod from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'serv_cod'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN serv_cod varchar (2)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [serv_cod][varchar(2)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //Creamos campo estado para documentacion nacex c@mbio
        //if(!Db::getInstance()->Execute("select estado from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'estado'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN estado varchar (20)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [estado][varchar(20)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //Creamos campo estado para documentacion nacex c@mbio
        //if(!Db::getInstance()->Execute("select fecha_estado from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'fecha_estado'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN fecha_estado datetime";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [fecha_estado][datetime]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //A�adimos columna en la tabla carrier para guardar el id del servicio nacex correspondiente a cada carrier.
        //if(!Db::getInstance()->Execute("select tip_serv from "._DB_PREFIX_."carrier limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "carrier LIKE 'tip_serv'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "carrier ADD COLUMN tip_serv varchar (2)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [tip_serv][varchar(2)]en " . _DB_PREFIX_ . "carrier");
            } catch (Exception $e) {
                
            }
        }
        //Creamos campo imp_ree en tabla nacex_expediciones 
        //if(!Db::getInstance()->Execute("select imp_ree from "._DB_PREFIX_."nacex_expediciones limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones LIKE 'imp_ree'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones ADD COLUMN imp_ree decimal(8,3)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [imp_ree][decimal(5,3)]en " . _DB_PREFIX_ . "nacex_expediciones");
            } catch (Exception $e) {
                
            }
        }
        //Creamos campo imp_ree en tabla nacex_expediciones_his
        //if(!Db::getInstance()->Execute("select imp_ree from "._DB_PREFIX_."nacex_expediciones_his limit 0")) {
        Db::getInstance()->Execute("SHOW COLUMNS FROM " . _DB_PREFIX_ . "nacex_expediciones_his LIKE 'imp_ree'");
        if (Db::getInstance()->Affected_Rows() == 0) {
            $query = "ALTER TABLE " . _DB_PREFIX_ . "nacex_expediciones_his ADD COLUMN imp_ree decimal(8,3)";
            try {
                Db::getInstance()->Execute($query);
                nacexutils::writeNacexLog("createTablesIfNotExists :: Alter table [imp_ree][decimal(5,3)]en " . _DB_PREFIX_ . "nacex_expediciones_his");
            } catch (Exception $e) {
                
            }
        }

        nacexutils::writeNacexLog("FIN createTablesIfNotExists");
        nacexutils::writeNacexLog("----");
    }

    public static function getDetallepedido($idorder) {
        $sql = 'SELECT o.id_order,o.module,u.email,a.firstname,
			a.lastname,a.address1,a.postcode,a.city,a.phone,a.phone_mobile,z.iso_code,
			case when o.total_paid_real > 0 
			then o.total_paid_real
			else
			o.total_paid
			end as total_paid_real
			FROM ' . _DB_PREFIX_ . 'orders AS o
			JOIN ' . _DB_PREFIX_ . 'customer AS u
			JOIN ' . _DB_PREFIX_ . 'address a
			JOIN ' . _DB_PREFIX_ . 'country AS z
			WHERE o.id_order = "' . $idorder . '"
			AND u.id_customer = o.id_customer
			AND a.id_address = o.id_address_delivery
			AND a.id_country = z.id_country';

        $result = Db::getInstance()->ExecuteS($sql);

        return $result;
    }

}

?>