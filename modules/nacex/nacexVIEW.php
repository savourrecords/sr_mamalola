<?php

/*
 * mexpositop 2017
 */
include_once("NacexDTO.php");
include_once("NacexDAO.php");
include_once("AdminConfig.php");
include_once("nacexutils.php");

class nacexVIEW {

    public static function showExpedicionForm($id_order, $datospedido, $datosexpedicion, $isnacexcambio,  $referenciaNacex) {

        $nacexDTO=new nacexDTO;
        $html = "";

        //echo "<h1>Carrier ID: " . $datospedido[0]['id_carrier'] . "</h1>";
        //var_dump($datospedido);

        $nacex_impseg = $isnacexcambio? Tools::getValue('nacex_imp_seg') ? Tools::getValue('nacex_imp_seg') : "" :
                                        Tools::getValue('nacex_imp_seg') ? Tools::getValue('nacex_imp_seg') : Configuration::get('NACEX_DEFAULT_IMP_SEG');
        $css_id = "ncx_info_exp";
        $webtext = "Ir a la web de Nacex";
        $webdir = "http://www.nacex.es";
        $webimg = "../modules/nacex/img/nacex_200.png";
        
   
         
        $tipo = $isnacexcambio ? " Nacex C@mbio": "Expedici&oacute;n";
        
        $titulo = "Generar ". $tipo . "&nbsp;<span  style=\"padding-left: 10px;\"><b>" . $referenciaNacex . "</b></span>";
       
        $shop_codigo = null;

   
        
        $modo_f = (Configuration::get("NACEX_SERV_BACK_OR_FRONT") == "F");

        $att = "";

        //Si ncx es "1" es Nacex normal, si no es un código NacexShop
        if (($datospedido[0]['ncx'] != "1" && 
             $datospedido[0]['id_carrier'] == nacexDTO::getNacexShopIdCarrier()) || 
             nacexDTO::isNacexShopCarrier($datospedido[0]['id_carrier'])) {
            $array_shop = explode("|", $datospedido[0]['ncx']);
            $shop_codigo = $array_shop[0];
            $css_id = "ncx_info_shop";
            $webtext = "Ir a la web de Nacex!Shop";
            $webdir = "http://www.nacexshop.com";
            $webimg = "../modules/nacex/img/nacexshop_200.png";

            $array_address_invoice = nacexDAO::getAddressInvoiceByOrder($id_order);
            $att = $array_address_invoice[0]["firstname"] . " " . $array_address_invoice[0]["lastname"];
        }

        $tlf = "";
        if (isset($datospedido[0]['phone']) && $datospedido[0]['phone'] != "") {
            $tlf = $datospedido[0]['phone'];
            if (isset($datospedido[0]['phone_mobile']) && $datospedido[0]['phone_mobile'] != "") {
                $tlf = $tlf . "/";
            }
        }
        if (isset($datospedido[0]['phone_mobile'])) {
            $tlf = $tlf . $datospedido[0]['phone_mobile'];
        }
        $cliente_tlf = $tlf;
        $cliente_email = $datospedido[0]['email'];

        $html .= '
				<script src="../modules/nacex/js/jquery.cluetip.js" type="text/javascript"></script>
				<script>
					var cli_tlf = "' . substr($cliente_tlf, 0, 50) . '";
					var cli_email = "' . substr($cliente_email, 0, 50) . '";
					function setprealerta(tipo){
						if(tipo=="N"){
							document.getElementById("nacex_pre1").disabled=true;	
							document.getElementById("nacex_pre1").value = "";
							deshabilitamodosprealerta();
						}else if(tipo == "S"){
							document.getElementById("nacex_pre1").disabled=false;	
							document.getElementById("nacex_pre1").value = cli_tlf;
					//		document.getElementById("nacex_pre1").focus();
							habilitamodosprealerta();
						}else if(tipo == "E"){
							document.getElementById("nacex_pre1").disabled=false;	
							document.getElementById("nacex_pre1").value = cli_email;
					//		document.getElementById("nacex_pre1").focus();
							habilitamodosprealerta();
						}
					}
					function setprealertaplus(tipo){
						if(eval(document.getElementById("nacex_pre1_plus"))){
							if(tipo=="S" || tipo=="R"){
								document.getElementById("nacex_pre1_plus").value = "";
								document.getElementById("nacex_pre1_plus").disabled = true;
							}else if(tipo == "P" || tipo=="E"){
								document.getElementById("nacex_pre1_plus").value = "' . Configuration::get('NACEX_PREAL_PLUS_TXT') . '";	
								document.getElementById("nacex_pre1_plus").disabled = false;
					//			document.getElementById("nacex_pre1_plus").focus();
							}
						}
					}
					function deshabilitamodosprealerta(){
						if(eval(document.getElementById("nacex_pre1_plus"))){
							document.getElementById("nacex_pre1_plus").value = "";
							document.getElementById("nacex_pre1_plus").disabled = true;
						}	
						var obj = document.getElementsByName("nacex_mod_pre1");
						for(i=0; i<obj.length; i++){
							obj[i].checked=false;
							obj[i].disabled=true;
						}
					}
					function habilitamodosprealerta(){
						var obj = document.getElementsByName("nacex_mod_pre1");
						for(i=0; i<obj.length; i++){
							if(i==0){
								obj[i].checked=true;
							}
							obj[i].disabled=false;
						}
					}
					function checkTipoPrealerta(tipo){
						var obj = document.getElementsByName("nacex_tip_pre1");
						for(i=0; i<obj.length; i++){
							if(obj[i].value==tipo){
								obj[i].checked = true;
								setprealerta(tipo);
							}
						}	
					}
					function checkModoPrealerta(modo){
						var obj = document.getElementsByName("nacex_mod_pre1");
						for(i=0; i<obj.length; i++){
							if(obj[i].value==modo){
								obj[i].checked = true;
								setprealertaplus(modo);
							}
						}	
					}
					
					function checkTipoSeguro(tipo){
						if (tipo != "N"){
    						document.getElementById("nacex_imp_seg").disabled = "";
    						document.getElementById("nacex_imp_seg").value = "' . $nacex_impseg . '";
    					}
    					else{
    						document.getElementById("nacex_imp_seg").disabled = "disabled";
    						document.getElementById("nacex_imp_seg").value = "";
    					}	
					}
									
				</script>
		';

        //PAGO CONTRA REEMBOLSO --------------------------------------------------------------------------------------------------		
        $metodo_pago = strtolower($datospedido[0]['module']);
        $array_modsree = explode(",", Configuration::get('NACEX_MODULOS_REEMBOLSO'));  //Nombres módulos pago contra reembolso indicados en Configuración
        $nacex_imp_ree= (in_array($metodo_pago, $array_modsree) || strpos($metodo_pago, 'cashondelivery') !== false) && is_numeric($datospedido[0]['total_paid_real'])? 
                            number_format ($datospedido[0]['total_paid_real'],2):   "0";
        //------------------------------------------------------------------------------------------------------------------------		

        $array_agclis = explode(",", Configuration::get('NACEX_AGCLI'));
        $select_agcli = "";
        for ($i = 0; $i < count($array_agclis); $i++) {
            $select_agcli = $select_agcli . "<option " . nacexutils::markSelectedOption("nacex_agcli", "NACEX_AGCLI", trim($array_agclis[$i])) . " value='" . trim($array_agclis[$i]) . "'>" . trim($array_agclis[$i]) . "</option>";
        }
        //Para documentar Nacex C@mbio debe usarse la misma agencia y cliente que la expedicion padre
        if ($isnacexcambio) {
            $select_agcli = "<option value='" . $datosexpedicion["agcli"] . "'>" . $datosexpedicion["agcli"] . "</option>";
        }

        $str_aviso_modo_f = "";
        $carrier_name = nacexDAO::getCarrierName($datospedido[0]['id_carrier']);

        $modo_f_con_b = false;
        $auxstr = "";
        if (nacexutils::isNacexGenericCarrier($datospedido[0]['id_carrier'])) {
            $modo_f_con_b = true;
            $auxstr = " genérico ";
        }

        if ($modo_f) {
            $str_aviso_modo_f = "<p>El cliente seleccion&oacute; el servicio " . $auxstr . "<b>" . str_replace("_", " ", $carrier_name) . "</b> desde el Frontend</p>";
        }

        $sel_serv = null;
        $def_serv = null;
        $opt_serv = null;
        $array_servicios = null;

        //mexpositop 20171115
        $internacional = false;

        if (isset($shop_codigo)) {
            $sel_serv = Tools::getValue('nacex_tip_nxshop_ser', NULL);
            $def_serv = Configuration::get('NACEX_DEFAULT_TIP_NXSHOP_SER');
            $opt_serv = explode('|', Configuration::get('NACEX_AVAILABLE_TIP_NXSHOP_SER'));
            $array_servicios = $nacexDTO->getServiciosNacexShop();
        } else {

            $sel_serv = Tools::getValue('nacex_tip_ser', NULL);
            $nacex_pais = $datospedido[0]['iso_code'];
            if ($nacex_pais == 'ES' || $nacex_pais == 'PT' || $nacex_pais == 'AD' || // envios considerados nacionales
                    $nacex_pais == null) { // ojo. añadimos el null como entrega no internacional
                $def_serv = Configuration::get('NACEX_DEFAULT_TIP_SER');
                $opt_serv = explode('|', Configuration::get("NACEX_AVAILABLE_TIP_SER"));
                $array_servicios = $nacexDTO->getServiciosNacex();
                //mexpositop 20171115
                //$internacional=false;
            } else {
                $def_serv = Configuration::get('NACEX_DEFAULT_TIP_SER_INT');
                $opt_serv = explode('|', Configuration::get("NACEX_AVAILABLE_TIP_SER_INT"));
                $array_servicios = $nacexDTO->getServiciosNacexInt();
                $internacional = true;
            }
        }

        $num_serv = isset(explode("_", $carrier_name)[1])?explode("_", $carrier_name)[1]:$def_serv;       
        $select_serv = "";

        if ($isnacexcambio && !$internacional) { // Internacional no admite cambios con el servicio 33
            //Para documentar Nacex C@mbio se obliga a usar el servicio 33
            $select_serv = "<option value='33'>33 - NACEX C@MBIO</option>";
        } else {

            if (isset($sel_serv)&& $sel_serv!= "") {
                $select_serv = "<optgroup label=\"Servicio por defecto\"><option selected='selected' value='" . $sel_serv . "'>" . $sel_serv . $nacexDTO->getServSeparador() . $array_servicios[$sel_serv]["nombre"] . "</option></optgroup>";
            } else {
                $select_serv = "<optgroup label=\"Servicio por defecto\"><option selected='selected' value='" . $def_serv . "'>" . $def_serv . $nacexDTO->getServSeparador() . $array_servicios[$def_serv]["nombre"] . "</option></optgroup>";
            }

            if (isset($opt_serv)) {
                $select_serv = $select_serv . "<optgroup label=\"Servicios disponibles\">";
            }

            for ($i = 0; $i < count($opt_serv); $i++) {
                if ($modo_f && !$modo_f_con_b) {
                    $select_serv = $select_serv . "<option " . nacexutils::markSelectedFrontendOption("nacex_tip_ser", $num_serv, $opt_serv[$i]) . " value='" . $opt_serv[$i] . "'>" . $opt_serv[$i] . $nacexDTO->getServSeparador()  . utf8_encode($array_servicios[$opt_serv[$i]]["nombre"]) . "</option>";
                } else {
                    $select_serv = $select_serv . "<option " . nacexutils::markSelectedOption("nacex_tip_ser", "NACEX_TIP_SER", $opt_serv[$i]) . " value='" . utf8_encode($opt_serv[$i] . "'>" . $opt_serv[$i] . $nacexDTO->getServSeparador()  . $array_servicios[$opt_serv[$i]]["nombre"]) . "</option>";
                }
            }
            if (!$modo_f_con_b && $modo_f && !in_array($num_serv, $opt_serv)) {
                $select_serv = $select_serv . "<option selected='selected' value='" . $num_serv . "'>" . $num_serv . $nacexDTO->getServSeparador()  . @$array_servicios[$num_serv]["nombre"] . "</option>";
            }
            if (isset($opt_serv)) {
                $select_serv = $select_serv . "</optgroup>";
            }
        }


        $select_tipseg = "";
        /**
         * TODO:
         * control de seguros en el FrontEnd
         */
        foreach ( $nacexDTO->getSeguros() as $seg => $value) {
            $segname = utf8_encode($value["nombre"]);
            if ($isnacexcambio){
                $select_tipseg.= "<option " . nacexutils::markSelectedFrontendOption("nacex_tip_seg", "N", $seg) . " value='" . $seg . "'>" . $segname . "</option>";
            }else{
                $select_tipseg.= "<option " . nacexutils::markSelectedFrontendOption("nacex_tip_seg", Configuration::get('NACEX_DEFAULT_TIP_SEG'), $seg) . " value='" . $seg . "'>" . $segname . "</option>";
            }
        }

        $prealertaval = "";

        //$prealertaplusval = "";
        $prealertaplusval = Tools::getValue("nacex_pre1_plus", Configuration::get('NACEX_PREAL_PLUS_TXT'));

        if (Tools::getValue("nacex_tip_pre1") == "N") {
            $prealertaval = "";
        } else if (Tools::getValue("nacex_tip_pre1") == "S") {
            $prealertaval = $cliente_tlf;
        } else if (Tools::getValue("nacex_tip_pre1") == "E") {
            $prealertaval = $cliente_email;
        }

        //CALCULAMOS EL NUMERO DE BULTOS POR DEFECTO
        //Obtenemos más detalles del pedido, como el peso y las referencias
        $numbultos = 0;
        $numproductos = 0;
        $valor = 0;

        $productospedido = Db::getInstance()->ExecuteS('SELECT product_quantity, product_weight, product_reference, total_price_tax_incl FROM ' . _DB_PREFIX_ . 'order_detail where id_order = "' . $id_order . '"');
        foreach ($productospedido as $producto) {
            $numproductos += $producto['product_quantity'];
            $valor+= floatval($producto['total_price_tax_incl']);
        }
        if (Configuration::get('NACEX_BULTOS') == "F") {
            $numbultos = Tools::getValue("nacex_bul", Configuration::get('NACEX_BULTOS_NUMERO'));
        } else if (Configuration::get('NACEX_BULTOS') == "C") {
            $numbultos = Tools::getValue("nacex_bul", $numproductos);
        }

        //SELECTOR DEPARTAMENTOS ----------------------------------------------------------------------------
        $departamentos = Configuration::get("NACEX_DEPARTAMENTOS");
        $select_dpt = "";
        if ($departamentos) {
            $array_dept = explode(",", $departamentos);
          //  $deptdef = $array_dept[0];
            foreach ($array_dept as $dpt) {
                $select_dpt.= "<option " . nacexutils::markSelectedFrontendOption("nacex_departamentos", $array_dept[0], $dpt) . " value='" . $dpt . "'>" . $dpt . "</option>";
            }
        }

        //INSTRUCCIONES ADICIONALES CUSTOMIZADAS----------------------------------------------------------------------------------------
        $instrucciones = "";
        $inst_adi_pers = (Configuration::get("NACEX_INST_PERS") == "SI");
        $inst_adi_val = Configuration::get("NACEX_CUSTOM_INST_ADI");
        $inst_adi_cantyref = (Configuration::get("NACEX_INS_ADI_Q_R") == "SI");

        if ($inst_adi_cantyref) {
            //Obtenemos más detalles del pedido, como el peso y las referencias
            $productospedido = Db::getInstance()->ExecuteS('SELECT product_quantity, product_weight, product_reference FROM ' . _DB_PREFIX_ . 'order_detail where id_order = "' . $id_order . '"');
            foreach ($productospedido as $producto) {
                $prodref = str_replace(";", ",", $producto['product_reference']);
                $instrucciones .= " ** " . $producto['product_quantity'] . " # " . $prodref;
            }
            $instrucciones .= " ";
        }
        $instrucciones .= "" . $inst_adi_val;

        $html .= '<span onClick="$(ncx_boxinfo).slideToggle();">
					<i class="icon-truck" style="color: orange;-moz-transform: scaleX(-1);-o-transform: scaleX(-1);-webkit-transform: scaleX(-1);transform: scaleX(-1); filter: FlipH;-ms-filter: "FlipH";></i>
				</span>					
				<div align="center" id="ncx_boxinfo">					
				  <a target="_blank" title="' . $webtext . '" href="' . $webdir . '" ><img style="margin-bottom:5px" src="' . $webimg . '" /></a>
				  <div id="' . $css_id . '">				  
				  <fieldset class="diana">
					<legend style="margin-left:5px;width: 90%;padding-left: 10px;">' . $titulo . '</legend>
					<form action="' . $_SERVER['REQUEST_URI'] . '" method="post" name="generarExpedicion">';
        if (isset($shop_codigo)) {
            $html .= '<span style="float:right;color:#ccc;margin-top:-20px;">' . $shop_codigo . '</span>';
        }
        $html .= '<p style="margin-left:5px;">Agencia/Cliente: 								
                    <select name="nacex_agcli" size="1" style="margin-left:15px;width: 130px;text-align:right;border: 0;">' . $select_agcli . '</select>
                </p>';

        if ($select_dpt != "") {
            $html .= '<p style="margin-left:5px;">Departamento: 								
			<select name="nacex_departamentos"  style="margin-left:15px;width: 130px;text-align:right;border: 0;" >' . $select_dpt . '</select>
		</p>';
        }
        $html .=$str_aviso_modo_f . '
				            				
							<p style="margin-left:5px;">Servicio: 														
								<select name="nacex_tip_ser" id="nacex_tip_ser" size="1" style="margin-left:15px;width:70%;text-align:right;border: 0;">
									' . $select_serv . '
								</select>
								<select name="nacex_frecuencia" id="nacex_frecuencia" size="1" style="display:none;margin-left:15px;width:70%;text-align:right;border: 0;">
									<option value="1">' . utf8_encode("1 - Frec. mañana") . '</option>
									<option selected="selected" value="2">' . utf8_encode("2 - Frec. tarde") . '</option>										
									<option value="8">' . utf8_encode("8 - Frec. nocturna") . '</option>
								</select>
								<script>
									
									var frecDep=0;
									
									if ( $("#nacex_tip_ser option:selected").val()== "09") { 				            						
				            							$("#nacex_frecuencia").slideToggle();	
				            							frecDep=1;	
												}
				            								
									$(document).on("change","#nacex_tip_ser",function(){  if ( $("#nacex_tip_ser option:selected").val()== "09") { 
					            						alert("' . utf8_encode("Seleccione frecuencia para el servicio Interdía.\\n Las frecuencias están disponibles entre ciertos códigos postales de entrega y recogida dependiendo del horario de solicitud del envío.") . '");
					            							$("#nacex_frecuencia").slideToggle();	
					            							frecDep=1;	
													}else{
					            						if (frecDep){
															$("#nacex_frecuencia").slideToggle();
															frecDep=0;}		
														}
					            		';

        if (substr($datospedido[0]["postcode"], 0, 2) != "07") {
            $html .= 'if ( $("#nacex_tip_ser option:selected").val()== "20") { 
					            					alert("' . utf8_encode("El CP de entrega del envío NO está en el ámbito de Baleares: ") . $datospedido[0]["postcode"] . '");	}';
        }

        $html .='	});
								</script>	
								</p>			
								<p style="margin-left:5px;">Bultos:
								<input type="text" id="nacex_bul" name="nacex_bul" value=' . $numbultos . ' size="2" length="1" maxlength="3" style="margin-left:15px;width: 100px;text-align:right;border: 0;" />		
								<script>
									$("#nacex_bul").live("keyup", function(e) {
									  $(this).val($(this).val().replace(/[^0-9]/g, ""));
									});
								</script>
																										
				            		</p>
				            <hr/>';

        if ($internacional) {
            $html .='<p style="margin-left:5px;">Contenido del envio: ';
            $html .='<select id="nacex_contenido" name="nacex_contenido" value="' . Tools::getValue('nacex_contenido', '') . '" style="width:375px; margin-left:15px;" length=1 maxlength="38">';
            foreach (nacexDTO::getContenidos() as $cont) {
                $html .='	<option ' . nacexutils::markSelectedOption("nacex_contenido", "NACEX_DEFAULT_CONTENIDO", utf8_encode($cont)) . ' value="' . utf8_encode($cont) . '">' . utf8_encode($cont) . '</option>';
            }
            $html .='</select> </p>';

            $html .='<p style="margin-left:5px;">Importe declarado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<br>									 
			<input type="text" id="nacex_impDeclarado" name="nacex_impDeclarado" value="' . $valor . '" style="margin-left:15px;width: 100px;text-align:right;border: 0;display:inline" length="1" maxlength="10"/> &euro;
                    </p>
                    <hr/>';
        }

        if (!$internacional) {
            $html .='<p style="margin-left:5px;">Portes:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="nacex_tip_cob" value="O" ' . nacexutils::markCheckedOption("nacex_tip_cob", "NACEX_TIP_COB", 'O') . '/>O - Origen  
			<input type="radio" name="nacex_tip_cob" value="D" ' . nacexutils::markCheckedOption("nacex_tip_cob", "NACEX_TIP_COB", 'D') . '/>D -  Destino
			<input type="radio" name="nacex_tip_cob" value="T" ' . nacexutils::markCheckedOption("nacex_tip_cob", "NACEX_TIP_COB", 'T') . '/>T - Tercera 
                    </p>';
        } else {
            $html .='<p style="margin-left:5px;">Portes:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="nacex_tip_cob" value="O" checked/>O - Origen									
                    </p>';
        }
   		
            if (!$internacional) {
                $html .= '<hr><p style="margin-left:5px;">Reembolso:
									<input type="radio" name="nacex_tip_ree" value="O" ' . nacexutils::markCheckedOption("nacex_tip_ree", "NACEX_TIP_REE", 'O') . '/>O - Origen
									<input type="radio" name="nacex_tip_ree" value="D" ' . nacexutils::markCheckedOption("nacex_tip_ree", "NACEX_TIP_REE", 'D') . '/>D - Destino
									<input type="radio" name="nacex_tip_ree" value="T" ' . nacexutils::markCheckedOption("nacex_tip_ree", "NACEX_TIP_REE", 'T') . '/>T - Tercera
								</p>
                                                                <p style="margin-left:5px;">Importe Ree.:
											 <input type="text" id="nacex_imp_ree" name="nacex_imp_ree" value="'.$nacex_imp_ree.'" size="20" maxlength="20" style="margin-left:15px;width: 100px;text-align:right;border: 0;display:inline"/> &euro;  											 
											 <a href="javascript:return false;"><img style="opacity:1" src="' . nacexDTO::getPath() . 'img/infoicon.png" width="20px" title="Importe TOTAL a abonar por parte del destinatario. Importe 0 NO tiene reembolso." alt="Importe TOTAL a abonar por parte del destinatario. Importe 0 NO tiene reembolso."/></a>
											 </p>';                
            } else {
                if (intval($nacex_imp_ree>0)){
                $html .= '<p style="margin-left:5px;">Reembolso: ATENCION: Sevicios internacionales NO gestionan REEMBOLSOS</p>';
                }
            }
            
        if (!$internacional) {
            $html .= '<hr><p style="margin-left:5px;">Envío:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								            			<input type="radio" name="nacex_tip_env" value="0" ' . nacexutils::markCheckedOption("nacex_tip_env", "NACEX_TIP_ENV", '0') . '/>0 - DOCS
								            			<input type="radio" name="nacex_tip_env" value="1" ' . nacexutils::markCheckedOption("nacex_tip_env", "NACEX_TIP_ENV", '1') . '/>1 - BAG
														<input type="radio" name="nacex_tip_env" value="2" ' . nacexutils::markCheckedOption("nacex_tip_env", "NACEX_TIP_ENV", '2') . '/>2 - PAQ																														
								            </p>';
        } else {
            $html .= '<p style="margin-left:5px;">Envío:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								            			
								            			<input type="radio" name="nacex_tip_env" value="M" checked="checked" ' . nacexutils::markCheckedOption("nacex_tip_env_int", "NACEX_TIP_ENV_INT", 'M') . '/> M - MUESTRA
								            			<input type="radio" name="nacex_tip_env" value="D" ' . nacexutils::markCheckedOption("nacex_tip_env_int", "NACEX_TIP_ENV_INT", 'D') . '/> D - DOCUMENTO														
								            </p>';
        }

        if (!$internacional) { //Internacional no admite retornos
            if ($isnacexcambio) {
                $html .= '<p style="margin-left:5px;">Retorno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									            	<input type="radio" name="nacex_ret" value="NO" disabled="true"/>NO
													<input type="radio" name="nacex_ret" value="SI" checked="checked"/>SI 
									            </p>';
            } else {
                $html .= '<p style="margin-left:5px;">Retorno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									            	<input type="radio" name="nacex_ret" value="NO" ' . nacexutils::markCheckedOption("nacex_ret", "NACEX_RET", 'NO') . '/>NO
													<input type="radio" name="nacex_ret" value="SI" ' . nacexutils::markCheckedOption("nacex_ret", "NACEX_RET", 'SI') . '/>SI 
									            </p>';
            }
        }
//seguro
        $html .= '<hr/>
						   			 <p style="margin-left:5px;">Seguro:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						   						<select name="nacex_tip_seg" size="1" onchange="checkTipoSeguro(this.value);" style="width:200px; margin-left:15px;">
													' . $select_tipseg . '
												</select>
								      </p> 
								      <p style="margin-left:5px;">Importe asegurado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								      </p>
								      <p style="margin-left:5px;">				
						   					<input type="text" id="nacex_imp_seg" name="nacex_imp_seg" value="' . $nacex_impseg . '"  length=1 maxlength="35" ' . ($nacex_impseg == "" ? 'disabled="disabled"' : '') . ' style="margin-left:15px;width: 100px;text-align:right;border: 0;display:inline"/> &euro; 
								      </p>';

        $html .= '<hr/>';
        $html .= '<p style="margin-left:5px;">Prealerta:<br>
										<input type="radio" name="nacex_tip_pre1" value="N" ' . nacexutils::markCheckedOption("nacex_tip_pre1", "NACEX_TIP_PREAL", 'N') . ' onclick="setprealerta(&quot;N&quot;);"  checked="true"/>N - No&nbsp;&nbsp;
										<input type="radio" name="nacex_tip_pre1" value="S" ' . nacexutils::markCheckedOption("nacex_tip_pre1", "NACEX_TIP_PREAL", 'S') . ' onclick="setprealerta(&quot;S&quot;);" />S - SMS&nbsp;&nbsp;
										<input type="radio" name="nacex_tip_pre1" value="E" ' . nacexutils::markCheckedOption("nacex_tip_pre1", "NACEX_TIP_PREAL", 'E') . ' onclick="setprealerta(&quot;E&quot;);" />E - Email';
        $html .= '<br>';
        $html .= '<input style="margin-left:15px;border: 0;display:inline;text-align:center;width:375px" type="text" id="nacex_pre1" name="nacex_pre1" value="' . $prealertaval . '" length="1" maxlength="50" disabled="true"/></p>';

        $html .= '<p style="margin-left:5px;">Modo Prealerta:<br>
									<input type="radio" name="nacex_mod_pre1" value="S" ' . nacexutils::markCheckedOption("nacex_mod_pre1", "NACEX_MOD_PREAL", 'S') . ' onclick="setprealertaplus(&quot;S&quot;);"  />S - Standard';
        if (!isset($shop_codigo)) {
            $html .= '<input type="radio" name="nacex_mod_pre1" value="P" ' . nacexutils::markCheckedOption("nacex_mod_pre1", "NACEX_MOD_PREAL", 'P') . ' onclick="setprealertaplus(&quot;P&quot;);"  />P - Plus';
            $html .= '<input type="radio" name="nacex_mod_pre1" value="R" ' . nacexutils::markCheckedOption("nacex_mod_pre1", "NACEX_MOD_PREAL", 'R') . ' onclick="setprealertaplus(&quot;R&quot;);"  />R - Reparto';
            $html .= '<input type="radio" name="nacex_mod_pre1" value="E" ' . nacexutils::markCheckedOption("nacex_mod_pre1", "NACEX_MOD_PREAL", 'E') . ' onclick="setprealertaplus(&quot;E&quot;);"  />E - Reparto Plus';
            $html .= '<br> <input style="margin-left:15px;border: 0;display:inline;text-align:center;width:375px" type="text" id="nacex_pre1_plus" name="nacex_pre1_plus" value="' . $prealertaplusval . '"  length="1" maxlength="719" disabled="true"/>';
        }
        $html .= '</p>
				            
				            <hr style="margin-top:2px" />';        
        if (isset($shop_codigo)) {
            $html .= '
								<p style="margin-left:5px;">A la atenci&oacute;n de: 
								<input type="text" id="nacex_att" name="nacex_per_ent" value="' . $att . '" style="width:375px; margin-left:15px;" length=1 maxlength="38" length=1 maxlength="35"/></p>';
        }
        $html .= '<p style="margin-left:5px;">
					            Observaciones de la entrega(1)
					            <input type="text" id="nacex_obs1" name="nacex_obs1" value="' . Tools::getValue('nacex_obs1', '') . '" style="width:375px; margin-left:15px;" length=1 maxlength="38" length=1 maxlength="38"/>
					            Observaciones de la entrega (2)
					            <input type="text" id="nacex_obs2" name="nacex_obs2" value="' . Tools::getValue('nacex_obs2', '') . '" style="width:375px; margin-left:15px;" length=1 maxlength="38" length=1 maxlength="38"/>
				            </p>';
        if ($inst_adi_pers) {
            $html .= '<hr style="margin-top:2px" />
							 	<p style="margin-left:5px;">Instrucciones adicionales del envío:</p> 
							 	<textarea style="width:375px; margin-left:15px;height:75px;" length=1 maxlength="38" name="inst_adi">' . Tools::getValue("inst_adi", $instrucciones) . '</textarea>';
        } else {
            $html .= '<input type="hidden"  name="inst_adi" value="' . Tools::getValue("inst_adi", $instrucciones) . '" style="width:375px; margin-left:15px;height:75px;"/>';
        }
        

        
        $html .= '<div>									
		<input type="submit" onclick="procesando();" name="submitputexpedicion" value="Enviar" alt="Generar Expedición" title="Generar Expedición" class="button" />
		<br><br>
		</div>
		</form>	
		</fieldset>
		</div>
		</div>
		<br/>';        
  
        $tip_pre1= empty(Tools::getValue("nacex_tip_pre1", Configuration::get('NACEX_TIP_PREAL')))? 
                           "N":
                           Tools::getValue("nacex_tip_pre1", Configuration::get('NACEX_TIP_PREAL')) ;
        if (!isset($shop_codigo)) {
            $html .= '<script type="text/javascript"> 
                                checkTipoPrealerta  ("'. $tip_pre1 . '");
                                checkModoPrealerta  ("'. Tools::getValue("nacex_mod_pre1", Configuration::get("NACEX_MOD_PREAL")) .'");
                                checkTipoSeguro     ("'. Tools::getValue("nacex_tip_seg", Configuration::get("NACEX_DEFAULT_TIP_SEG")) . '");
                     </script>';
        } 
        /*else{
            $html .= '<script type="text/javascript"> 
                                checkTipoPrealerta  ("E");
                                checkModoPrealerta  ("S");
                                checkTipoSeguro     ("'. Tools::getValue("nacex_tip_seg", Configuration::get("NACEX_DEFAULT_TIP_SEG")) . '");
            </script>';
        }*/
        return $html;
    }

    public static function showExpedicionBoxInfo($datosexpedicion, $id_pedido) {

        //var_dump($datosexpedicion);
        $nacexDTO= new nacexDTO();
        $urlPrint = Configuration::get('NACEX_PRINT_URL');
        $modelPrint = Configuration::get('NACEX_PRINT_MODEL');
        $etPrint = Configuration::get('NACEX_PRINT_ET');
        $nacexWSusername = Configuration::get('NACEX_WSUSERNAME');
        $nacexWSpassword = Configuration::get('NACEX_WSPASSWORD');
        $parametros = "user=" . $nacexWSusername . "&pass=" . $nacexWSpassword . "&model=" . $modelPrint . "&et=" . $etPrint . "&ref=" . nacexutils::getReferenciaGeneral() . $id_pedido . "&expe_codigo=" . $datosexpedicion["exp_cod"];
        $urlPrint = $urlPrint . '?' . $parametros;
        $host = $nacexDTO->getHostURLImpresion();

        $mapacambios = nacexutils::getMapCambios($datosexpedicion["cambios"]);
        $html_tip_ser_cambiado = "";
        if (isset($mapacambios["tip_ser"])) {
            $html_tip_ser_cambiado = "El tipo de servicio fue cambiado al " . $mapacambios["tip_ser"] . " por el Web Service para poder documentar la Expedición";
        }

        $fecha_alta = date("H:i:s, d/m/Y");
        if (isset($datosexpedicion["fecha_alta"])) {
            $fecha_alta = $datosexpedicion["fecha_alta"];
        }


        if (!isset($datosexpedicion["ret"])){
            $datosexpedicion["ret"] = "NO";
        }
        $leyenda_agencia_entrega = 'Agencia Entrega';
        $agencia_entrega_codigo = $datosexpedicion["ent_cod"];
        $agencia_entrega_nombre = $datosexpedicion["ent_nom"];
        $leyenda_agencia_entrega_tlf_or_addres = 'Teléfono';
        $agencia_entrega_tlf_or_addres = $datosexpedicion["ent_tlf"];
        $a_visibilidad_agencia_entrega = ' style="display:block;" ';
        $fondo_nacexshop = "";
        $att = "";

        $webtext = "Ir a la web de Nacex";
        $webdir = "http://www.nacex.es";
        $webimg = "../modules/nacex/img/nacex_200.png";

        //¿Es NacexShop?
        if (isset($datosexpedicion["shop_codigo"]) && $datosexpedicion["shop_codigo"] != 1 && $datosexpedicion["shop_codigo"] != "") {
            $leyenda_agencia_entrega = 'Punto de Entrega';
            $agencia_entrega_codigo = htmlentities(@$datosexpedicion["shop_alias"]);
            $agencia_entrega_nombre = htmlentities(@$datosexpedicion["shop_nombre"]);
            $leyenda_agencia_entrega_tlf_or_addres = htmlentities('Dirección');
            $agencia_entrega_tlf_or_addres = htmlentities(@$datosexpedicion["shop_direccion"]);
            $a_visibilidad_agencia_entrega = ' style="display:none;" ';
            $fondo_nacexshop = ' class="bg_nacexshop" ';

            $webtext = "Ir a la web de Nacex!Shop";
            $webdir = "http://www.nacexshop.com";
            $webimg = "../modules/nacex/img/nacexshop_200.png";

            $array_address_invoice = nacexDAO::getAddressInvoiceByOrder($id_pedido);
            $att = "<p><i><b>Att:</b> " . $array_address_invoice[0]["firstname"] . " " . $array_address_invoice[0]["lastname"] . "</i></p>";
        }

       // $respuestaGetEstadoExpedicion = null;
        $respuestaGetEstadoExpedicion = nacexWS::ws_getEstadoExpedicion($datosexpedicion["exp_cod"]);
       // $getDatosWSExpedicion = null;
        $estado_exp = null;
        $fecha_estado_exp = null;
        $hora_estado_exp = null;
        $getDatosWSExpedicion = nacexWS::ws_getDatosWSExpedicion($datosexpedicion["exp_cod"]);

        $html_fieldset_body_estado = "";
        if (is_array($respuestaGetEstadoExpedicion)) {
            if (isset($respuestaGetEstadoExpedicion)) {

                if (isset($respuestaGetEstadoExpedicion[0]) && $respuestaGetEstadoExpedicion[0] == "ERROR") {

                    if (isset($respuestaGetEstadoExpedicion[2]) && $respuestaGetEstadoExpedicion[2] == "5611") {
                        $html_fieldset_body_estado = '<p align="center"><b><i>Pendiente de integración</i></b></p>';
                        $estado_exp = "PENDIENTE";
                    } else {
                        $html_fieldset_body_estado = '
							<div style="width:auto" class="alert error">
								<p>' . $respuestaGetEstadoExpedicion[0] . ' ' . $respuestaGetEstadoExpedicion[2] . '</p>
					  		</div>';
                    }
                } else {
                    $html_fieldset_body_estado = '
						<p><b>Fecha:</b> ' . $respuestaGetEstadoExpedicion["fecha"] . '</p>		
						<p><b>Hora:</b> ' . $respuestaGetEstadoExpedicion["hora"] . '</p>					
						<p><b>Obs.:</b> ' . $respuestaGetEstadoExpedicion["observaciones"] . '</p>
						<p><b>Estado:</b> ' .$respuestaGetEstadoExpedicion["estado"] . '</p>
				  	';
                    $estado_exp = utf8_encode($respuestaGetEstadoExpedicion["estado"]);
                    $fecha_estado_exp = $respuestaGetEstadoExpedicion["fecha"];
                    $hora_estado_exp = $respuestaGetEstadoExpedicion["hora"];
                }
            } else {
                $html_fieldset_body_estado = '<p align="center"><b><i>Sin datos</i></b></p>';
            }
        } else {
            $html_fieldset_body_estado = $respuestaGetEstadoExpedicion;
        }

        $html_fieldset_exp_rel = "";
        $array_exp_rel = !isset($datosexpedicion["estado"]) || $respuestaGetEstadoExpedicion["estado"]=="ANULADA"? null:nacexDAO::getExpRelacionadas($id_pedido);

           if (!empty($array_exp_rel)) {
            $html_fieldset_exp_rel .="<fieldset>"
                    . "<legend>Histórico de Expediciones</legend>"
                    . "<div style='position:relative;overflow:auto;width:375px'>"
                    . "<table width='100%' border='0'>"
                    . "<tr>"
                    . "<td nowrap='nowrap'><font style='font-size:8pt'><b>Agencia/Num Alb.</b></font></td>"
                    . "<td nowrap='nowrap'><font style='font-size:8pt'><b>Referencia</b></font></td>"
                    . "<td nowrap='nowrap'><font style='font-size:8pt'><b>Fecha Alta</b></font></td>"
                    . "<td nowrap='nowrap'><font style='font-size:8pt'><b>Estado</b></font></td>"
                    . "<td nowrap='nowrap'><font style='font-size:8pt'><b>Fecha Estado</b></font></td>"
                    . "</tr>";
            $i = 0;
            foreach ($array_exp_rel as $exp_rel) {               
                $html_fieldset_exp_rel .= $i % 2==0? "<tr bgcolor='#F7F7F7'>": "<tr bgcolor='#EEEEEE'>";
                $html_fieldset_exp_rel .="<td nowrap='nowrap'><font style='font-size:8pt'>" . $exp_rel['ag_cod_num_exp'] . "</font></td>"
                        . "<td nowrap='nowrap'><font style='font-size:8pt'>" . $exp_rel['ref'] . "</font></td>"
                        . "<td nowrap='nowrap'><font style='font-size:8pt'>" . $exp_rel['fecha_alta'] . "</font></td>"
                        . "<td nowrap='nowrap'><font style='font-size:8pt'>" . $exp_rel['estado'] . "</font></td>"
                        . "<td nowrap='nowrap'><font style='font-size:8pt'>" . $exp_rel['fecha_estado'] . "</font></td>"
                        . "</tr>";
                $i++;
            }
            $html_fieldset_exp_rel .="</table>"
                    . "</div>"
                    . "</fieldset>";
        }
        //Con esta función actualizamos el campo 'serv_cod' de la tabla nacex_expediciones (para exp creadas con versiones anteriores a la 1.4.9)
        //nacexDAO::actDatosNacexExpediciones($id_pedido, $getDatosWSExpedicion, $estado_exp, $fecha_estado_exp, $hora_estado_exp);


        echo '<span onClick="$(ncx_boxinfo).slideToggle();">
					<i class="icon-truck" style="color: orange;-moz-transform: scaleX(-1);-o-transform: scaleX(-1);-webkit-transform: scaleX(-1);transform: scaleX(-1); filter: FlipH;-ms-filter: "FlipH";></i>
				</span>		
			<div align="center" id="ncx_boxinfo" style="margin-left: 20px;">
					
			  <a target="_blank" title="' . $webtext . '" href="' . $webdir . '" ><img style="margin-bottom:5px" src="' . $webimg . '" /></a>
			  <div id="ncx_info_exp" class="ncx_box">		     	  
				<fieldset>					
					<legend>Expedición</legend>	
			  		<div id="ncx_cod_exp" style="position: relative; right: 18px;">' . $datosexpedicion["exp_cod"] . '</div>
			  		<p><b>Referencia:</b> ' . $getDatosWSExpedicion["ref"] . '</p>
			  		<p><b>Ag/Num:</b> ' . $datosexpedicion["ag_cod_num_exp"] . '</p>
			  		<p><b>Alta Expedición:</b> ' . $fecha_alta . '</p>
		 	  	</fieldset>
			  	<fieldset>
			  		<legend>Servicio</legend>
					<sub> ' . $html_tip_ser_cambiado . '</sub>
					<p><b>Servicio:</b> ' . $datosexpedicion["serv"] . ' <sub>(' . $datosexpedicion["hora_entrega"] . ')</sub></p>
					<p><b>Fecha Objetivo entrega:</b> ' . $datosexpedicion["fecha_objetivo"] . '</p>
					<p><b>Retorno:</b> ' . $datosexpedicion["ret"] . '</p>
			  	</fieldset>
			  	<fieldset ' . $fondo_nacexshop . '>
			  		<legend>' . $leyenda_agencia_entrega . '</legend>
					<a class="ncx_fieldset_icon zoomable" ' . $a_visibilidad_agencia_entrega . ' title="Ver detalles de la agencia" 
					   href="http://www.nacex.es/infoAgencia.do?codigo=' . $datosexpedicion["ent_cod"] . '" 
					   onClick="window.open(this.href, this.target,&quot;width=600,height=600,toolbar=no,scrollbars=no&quot;); return false;">
					   	<img title="Ver detalles de la agencia" alt="Ver detalles de la agencia" src="' . nacexDTO::getPath() . 'img/lupa.png" style="position: relative; right: 18px;top: 5px;"/>
					</a>
					<p><b>Código - Nombre:</b> ' . $agencia_entrega_codigo . ' - ' . $agencia_entrega_nombre . '</p>
					<p><b>' . $leyenda_agencia_entrega_tlf_or_addres . ':</b> ' . $agencia_entrega_tlf_or_addres . '</p>
					' . $att . '					
			  	</fieldset>	
			  	<fieldset>
			  		<legend>Estado</legend>';
        echo $html_fieldset_body_estado;
        echo '
			  	</fieldset>
			  	' . $html_fieldset_exp_rel . '				
			  	<br>			  				
					<script>
					function printiframe(){
						var array_exp = ["'.$datosexpedicion["exp_cod"].'"];
						';
			  					
				if (Configuration::get('NACEX_PRINT_IONA')==""){					
					echo '	// valida navegador con JAVA (Iexplorer, Edge, ...) '.$host.'
							if (navigator.javaEnabled()){
							imprimirEtiquetas(array_exp, "'.$nacexWSusername.'", "'.$nacexWSpassword.'", "'.$etPrint.'", "'.$modelPrint.'","'.$host.'"); 
							}else{
									alert("La configuraci\xF3n de impresi\xF3n con Applet requiere el Plugin de Java activado: Internet Explorer, Edge, ...");
								}
									';
					
				}else{
					echo '	imprimir_iona( array_exp); ';
					}
	 
				echo ' }
					function confirmar(msg){
						return confirm(msg);
					}
				</script>
				<a onClick="javascript:printiframe();" style="cursor:pointer;float:right;" title="Imprimir etiqueta Nacex"><img class="zoomable" src="' . nacexDTO::getPath() . 'img/print.png" title="Imprimir etiqueta Nacex" alt="Imprimir etiqueta Nacex" width="38px" /></a>
				<iframe id="iframe_print" height="0" width="0" src="" frameborder="0"></iframe>
				
			  	<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
					<input class="zoomable" width="37px" alt="Cancelar Expedición" title="Cancelar Expedición" type="image" name="submitcancelexpedicion" src="' . nacexDTO::getPath() . 'img/cancel.png" onclick="return confirmar(&quot;¿Deseas cancelar esta Expedición?&quot;)" />';

        $servicio = $getDatosWSExpedicion["serv_cod"];
        if (($servicio == "27" || $servicio == "33") && $estado_exp == "OK") {
            echo "&nbsp;&nbsp;&nbsp;&nbsp;<input style='opacity:0.8' class='zoomable' alt='Nacex C@mbio' title='Nacex C@mbio' type='image' name='submitcambioexpedicion' src='" . nacexDTO::getPath() . "img/nacexcambio.png' onclick='return confirmar(\"Se procederá a la documentación de una Expedición Nacex C@mbio.\\n¿Desea continuar?\")'/>";
        }

        echo '</form></div></div>';

 	if(Tools::getValue('nacex_print_iona', Configuration::get('NACEX_PRINT_IONA'))==""){ 
            echo self::showAppletImpresion();
 	}else{
            echo self::showIoNA();
 	} 
    }

    public static function mostrarErrores($errors = array()) {
        $error_message = "<div class=\"bootstrap\" style=\"margin-top:10px\"><div class=\"alert alert-danger error\" style=\"width:auto\">";

        foreach ($errors as $error) {
            $error_message .= $error . "<br/>";
        }

        $error_message .= "</div></div>";
        return $error_message;
    }

    public static function mostrarSuccess($success = array()) {
        $success_message = "<div class=\"bootstrap\" style=\"margin-top:10px\"><div class=\"alert alert-success conf\" style=\"width:auto\">";

        foreach ($success as $aux) {
            $success_message .= $aux . "<br/>";
        }

        $success_message .= "</div></div>";
        return $success_message;
    }
    
    public static function showIoNA(){
        
        $etiquetaURL= Configuration::get('NACEX_WS_URL');
        $etiquetaURL.=substr($etiquetaURL, -1)!='/'?    "/":    "";
        
		$html= "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>
		<script> 
				
			function imprimir_iona(refs){  
	    				
					refs.forEach(ionaPrint);
			}

					
			function ionaPrint(item, index){
							
				etiquetaURL = '". $etiquetaURL."'+
								'ws?method=getEtiqueta&user=".Configuration::get('NACEX_WSUSERNAME')."'+
								'&pass=".Configuration::get('NACEX_WSPASSWORD')."'+
								'&data=modelo=".Configuration::get('NACEX_PRINT_MODEL')."'+
								'|codexp=' +item;

										
				var posting = $.post( '".Configuration::get('NACEX_PRINT_IONA')."/print', { 
	    			url: etiquetaURL,
	    			impresora : '".Configuration::get('NACEX_PRINT_ET')."'
	       		  } );
  
	    					
 		       	posting.done(function( data ) {     /*  alert(data);  */     		 });
       			posting.fail(function( data ) {         alert('Error imprimiendo');  });	
                     
			  }		
	    			
	</script>";
	
		return $html;
	}
        
        public static function showAppletImpresion(){
		$html= "<script src='../modules/nacex/js/nacex.js' type='text/javascript'></script>
						<script type='text/javascript'>
								 if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){
									document.write(\"<OBJECT \",
				                   			\"tabindex='-1'\",			   
												   			\"codeBase='http://java.sun.com/products/plugin/autodl/jinstall-1_5_0-windows-i586.cab#Version=1,5,0,0'\",
				 								   			\"classid='clsid:8AD9C840-044E-11D1-B3E9-00805F499D93'\",
												   			\"id='ImpEtiqueta'\",
												   			\"width='1'\",
				                   			\"height='1'>\",
				                				\"<PARAM name='code' value='ImpEtiqueta.class'>\",
				                				\"<PARAM name='java_archive' value='NacexImpresion_signed.jar'/>\",
				                				\"<PARAM name='java_type' value='application/x-java-applet;version=1.5.0'/>\",
				                				\"<PARAM name='java_codebase' value='".Configuration::get('NACEX_PRINT_URL')."'/>\",
				                				\"<PARAM name='initial_focus' value='false'/>\",
				                				\"<PARAM NAME='MAYSCRIPT' VALUE='true'/>\",
											       		\"<PARAM NAME='SHOW_PRINTER_DIALOG' VALUE='true'/>\",
												   			\"<PARAM NAME='SINDATOS' VALUE='No hay resultados'/>\",
				                   			\"</OBJECT>\");
								 }else{
									document.write(\"<embed code='ImpEtiqueta.class' id='ImpEtiqueta'\",
												   			 \"codebase='".Configuration::get('NACEX_PRINT_URL')."'\",
												   			 \"width='0'\",
												   			 \"height='0'\",
												   			 \"archive='NacexImpresion_signed.jar'\",
												   			 \"type='application/x-java-applet;version=1.5'\",
												   			 \"initial_focus='false'\",
												   			 \"MAYSCRIPT='true'\",
												   			 \"SHOW_PRINTER_DIALOG='true'\",
												   			 \"SINDATOS='No hay resultados' >\");
								}
							</script>";
		
		return $html;
	}
}