<?php
include_once('nacexutils.php');
include_once('nacexDAO.php');

if(Configuration::get('NACEX_SHOW_ERRORS') == "SI"){
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
}else{
	error_reporting(0);
	ini_set('display_errors', '0');
}

/*
  		### putExpedicion ###
		id_envio_order
		agcli
		fecha_alta
		fecha_baja
		ref
		exp_cod
		ag_cod_num_exp
		color
		ent_ruta
		ent_cod
		ent_nom
		ent_tlf
		serv
		hora_entrega
		barcode
		fecha_objetivo
		cambios
		bultos
		shop_codigo, 	
		shop_alias, 	
		shop_nombre, 	
		shop_direccion 		
		shop_cp
		shop_poblacion
		shop_provincia
		shop_telefono
		ret
		
		### Datos Listado ###
		id_order
		nom_ent
		tel_ent
		dir_ent
		pob_ent
		cp_ent
		tel_ent
		email_ent
		pais_ent	
		peso
		bultos
		importe
		ree				
  		*/

//** mexpositop 20171115
class nacextab extends AdminController{
private $_html = '';
   public function initContent()    {
       
        global $cookie;
        $nacextab= new nacextab();
        
       	echo "<link type='text/css' rel='stylesheet' href='../modules/nacex/css/nacex.css' />";
        //parche js includes
        /*
 	$this->context->controller->addJS('../modules/nacex/js/nacex.js');
 	$this->context->controller->addJS('../modules/nacex/js/jquery.printElement.min.js');
        */ 

        $webtext = "Ir a la web de Nacex";
	$webdir = "http://www.nacex.es";
	$webimg = "../modules/nacex/img/nacex_200.png"; 
         
        $png_barcode_url = "http://www.nacex.es/impCodBarras.do?x=150&y=60&fontsizeB=10&codebar=";
	
 	$hoy_desde = date('Y-m-d').' 00:00:00';
 	$hoy_hasta = date('Y-m-d').' 23:59:59';
 		
 	$ayer_desde = date("Y-m-d", strtotime("-1 day")).' 00:00:00';
 	$ayer_hasta = date("Y-m-d", strtotime("-1 day")).' 23:59:59';
 		
	$estasemana_desde = date('Y-m-d',time()+( 1 - date('w'))*24*3600).' 00:00:00';
	$estasemana_hasta = date('Y-m-d',time()+( 7 - date('w'))*24*3600).' 23:59:59';	
		
		
	$timestamp_ultimodomingo = strtotime("last Sunday");	
	$semanapasada_desde = date("Y-m-d", $timestamp_ultimodomingo - 6 * 24 * 3600).' 00:00:00';
	$semanapasada_hasta = date("Y-m-d", $timestamp_ultimodomingo).' 23:59:59';	
			
 		
	$estemes_desde = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-date("d")+1, date("Y"))).' 00:00:00';
	$estemes_hasta = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , date("d")-date("d"), date("Y"))).' 23:59:59';
	
        $desde = Tools::getValue('date_from', date('Y-m-d H:i:s'));
        $hasta = Tools::getValue('date_to', date('Y-m-d H:i:s'));
        
        $nuevaConsulta = Tools::getValue('date_from','')!= '' &&  Tools::getValue('date_to','') != ''? 1:0;
        
        $this->_html .="<div class='toolbarBox toolbarHead'>
	  	<div class='pageTitle'>
                    <h3>Pedidos gestinados por Nacex</h3>
             	</div>
            </div>
            <script>";
        
            //parche js includes
        $this->_html .= "/*parche nacex.js */";
        //include("/js/nacexJS.php");
        $this->_html .= "function getCookie(e){if(cookie=\" \"+document.cookie,offset=cookie.indexOf(\" \"+e+\"=\"),-1!=offset)return offset+=e.length+2,end=cookie.indexOf(\";"
        . "\",offset),-1==end&&(end=cookie.length),unescape(cookie.substring(offset,end))}function establecerCookie(e,i,r,n,o)"
        . "{var t=\"\";t+=e+\"=\"+escape(i),t+=\"; expires=\"+r.toGMTString(),document.cookie=t}function imprimirEtiquetas(e,r,n,o,t,a)"
        . "{for(i=0;i<e.length;i++)t.indexOf(\"LASER\")>=0?imprimir_etiqueta_laser(e[i],r,n,o,t,a):imprimir_etiqueta_normal(e[i],r,n,o,t,null)}"
        . "function imprimir_etiqueta_normal(e,i,r,n,o,t){var a=\"/gestion_imprimir_ecommerce.do?user=\"+i+\"&valorImpresora=\"+o+\"&codExpedicion=\"+e;impresora=getCookie(\"impCodBarras\"),"
        . "null==impresora?impresora=document.getElementById(\"ImpEtiqueta\").Imprimir(new String(a),null):impresora="
        . "document.getElementById(\"ImpEtiqueta\").Imprimir(new String(a),new String(impresora));var c=new Date(2020,1,1,0,0,0);establecerCookie(\"impCodBarras\",impresora,c)}"
        . "function imprimir_etiqueta_laser(e,i,r,n,o,t){var a=t+\"/gestion_imprimir_laser.do?user=\"+i+\"&pass=\"+r+\"&imp_magento=\"+n+\"&codExpedicion=\"+e+\"&modelo=\"+o,"
        . "c=document.createElement(\"iframe\");c.id=\"frameprint_codExpedicion\",c.src=a,c.style.width=0,c.style.height=0,document.body.appendChild(c)}"
        . "function procesando(){\$(\"*\").css(\"cursor\",\"wait\"),$(\"#content\").fadeTo(\"fast\",.4)}function confirmar(e){return!!confirm(e)&&(procesando(),!0)}"
        . "function IsANumber(e,i){if(!parseFloat(e))return!1;XX=\"\";for(var r=0;r<e.length;r++){if(X=e.charAt(r),XX+=X,\"1\"!=i&&\".\"!=X&&\",\"!=X&&\"-\"!=X&&\"0\"!=X&&!parseInt(X))return!1;"
        . "if(\"1\"==i&&\"-\"!=X&&\"0\"!=X&&!parseInt(X))return!1}return!0}function potencia(e,i){var r=0,n=1;for(n=e,r=1;r<i;r++)n*=e;return n}function isNumeric(e){var r,n=!0;"
        . "for(i=0;i<e.length&&1==n;i++)r=e.charAt(i),-1==\"0123456789,\".indexOf(r)&&(n=!1);return n}function ValidarNum(e,i,r,n){if(1==i.readOnly)return!1"
        . ";0==IsANumber(e=e.replace(\",\",\".\"),2)&&(i.value=0);var o=0,t=0,a=new String(i.value),c=a.indexOf(\",\");"
        . "c<0&&(c=a.indexOf(\".\")),-1!=c?(o=a.substring(0,c).replace(\".\",\"\"),n>0&&(t=a.substring(c+1,c+n+1))):o=a,o<potencia(10,r)?i.value=n>0?o+\",\"+t:o:(i.value=0,i.focus())}"
        . "function soloNumeros(e){return key=document.all?event.keyCode:e.which,46==key||key<=13||44==key||key>=48&&key<=57}";

        $this->_html .= "/*parche jquery.printElement.min.js */";
        //include("/js/jquery.printElement.php") ;
        $this->_html .= ";(function(g){function k(c){c&&c.printPage?c.printPage():setTimeout(function(){k(c)},50)}function l(c){c=a(c);"
        . "a(\":checked\",c).each(function(){this.setAttribute(\"checked\",\"checked\")});a(\"input[type='text']\",c).each(function(){this.setAttribute(\"value\",a(this).val())});"
        . "a(\"select\",c).each(function(){var b=a(this);a(\"option\",b).each(function(){b.val()==a(this).val()&&this.setAttribute(\"selected\",\"selected\")})});"
        . "a(\"textarea\",c).each(function(){var b=a(this).attr(\"value\");if(a.browser.b&&this.firstChild)this.firstChild.textContent=b;"
        . "else this.innerHTML=b});return a(\"<div></div>\").append(c.clone()).html()}function m(c,b){var i=a(c);c=l(c);"
        . "var d=[];d.push(\"<html><head><title>\"+b.pageTitle+\"</title>\");if(b.overrideElementCSS){if(b.overrideElementCSS.length>0)for(var f=0;"
        . "f<b.overrideElementCSS.length;f++){var e=b.overrideElementCSS[f];typeof e==\"string\"?d.push('<link type=\"text/css\" rel=\"stylesheet\" "
        . "href=\"'+e+'\" >'):d.push('<link type=\"text/css\" rel=\"stylesheet\" href=\"'+e.href+'\" media=\"'+e.media+'\" >')}}else "
        . "a(\"link\",j).filter(function(){return a(this).attr(\"rel\").toLowerCase()==\"stylesheet\"}).each(function()"
        . "{d.push('<link type=\"text/css\" rel=\"stylesheet\" href=\"'+a(this).attr(\"href\")+'\" media=\"'+a(this).attr(\"media\")+'\" >')});"
        . "d.push('<base href=\"'+(g.location.protocol+\"//\"+g.location.hostname+(g.location.port?\":\"+g.location.port:\"\")+g.location.pathname)+'\" />');"
        . "d.push('</head><body style=\"'+b.printBodyOptions.styleToAdd+'\" class=\"'+b.printBodyOptions.classNameToAdd+'\">');"
        . "d.push('<div class=\"'+i.attr(\"class\")+'\">'+c+\"</div>\");d.push('<script type=\"text/javascript\">function printPage(){focus();print();"
        . "'+(!a.browser.opera&&!b.leaveOpen&&b.printMode.toLowerCase()==\"popup\"?\"close();\":\"\")+\"}<\/script>\");d.push(\"</body></html>\");"
        . "return d.join(\"\")}var j=g.document,a=g.jQuery;a.fn.printElement=function(c){var b=a.extend({},a.fn.printElement.defaults,c);"
        . "if(b.printMode==\"iframe\")if(a.browser.opera||/chrome/.test(navigator.userAgent.toLowerCase()))b.printMode=\"popup\";"
        . "a(\"[id^='printElement_']\").remove();return this.each(function(){var i=a.a?a.extend({},b,a(this).data()):b,d=a(this);d=m(d,i);var f=null,e=null;"
        . "if(i.printMode.toLowerCase()==\"popup\"){f=g.open(\"about:blank\",\"printElementWindow\",\"width=650,height=440,scrollbars=yes\");"
        . "e=f.document}else{f=\"printElement_\"+Math.round(Math.random()*99999).toString();var h=j.createElement(\"IFRAME\");"
        . "a(h).attr({style:i.iframeElementOptions.styleToAdd,id:f,className:i.iframeElementOptions.classNameToAdd,frameBorder:0,scrolling:\"no\",src:\"about:blank\"});"
        . "j.body.appendChild(h);e=h.contentWindow||h.contentDocument;if(e.document)e=e.document;h=j.frames?j.frames[f]:j.getElementById(f);"
        . "f=h.contentWindow||h}focus();e.open();e.write(d);e.close();k(f)})};a.fn.printElement.defaults={printMode:\"iframe\","
        . "pageTitle:\"\",overrideElementCSS:null,printBodyOptions:{styleToAdd:\"padding:10px;margin:10px;\",classNameToAdd:\"\"},"
        . "leaveOpen:false,iframeElementOptions:{styleToAdd:\"border:none;position:absolute;width:0px;height:0px;bottom:0px;left:0px;\",classNameToAdd:\"\"}};"
        . "a.fn.printElement.cssElement={href:\"\",media:\"\"}})(window);";

            //parche js includes    
        
	$this->_html .="
            function setHoy(){
			$('#ncx_desde').val('".$hoy_desde."');
			$('#ncx_hasta').val('".$hoy_hasta."');
		}
		function setAyer(){
			$('#ncx_desde').val('".$ayer_desde."');
			$('#ncx_hasta').val('".$ayer_hasta."');
		}
		function setEstaSemana(){
			$('#ncx_desde').val('".$estasemana_desde."');
			$('#ncx_hasta').val('".$estasemana_hasta."');
		}
		function setSemanaPasada(){
			$('#ncx_desde').val('".$semanapasada_desde."');
			$('#ncx_hasta').val('".$semanapasada_hasta."');
		}									
		function setEsteMes(){
			$('#ncx_desde').val('".$estemes_desde."');
			$('#ncx_hasta').val('".$estemes_hasta."');
		}								
		function printlistado(){							
                    $('#ncx_div_listado').printElement({
		          pageTitle: '[prestashop] listado Nacex',
			            leaveOpen: false,
			            printMode: 'popup',
			            overrideElementCSS: ['../modules/nacex/css/nacex.css'],
			            printBodyOptions:{
            				styleToAdd:'padding:0px;margin:0px; !important;'            
            			}
			        });									
				}
  	     	 </script>
                 <fieldset>
		<div class='ncx_listado'>
		<form method='post'>	
			<div align='center' style='margin-top:-25px;padding:20px;'>
				<a target='_blank' title='" . $webtext . "' href='" . $webdir . "'><img style='margin-bottom:5px' src='" . $webimg . "' /></a>
			</div>
			<div align='center'>				
				<b>Desde: </b>
				<input id='ncx_desde' type='text' style='width:135px;margin-right:10px' value='".$desde."' name='date_from' maxlength='19' size='4'>
				<b>Hasta: </b>
				<input id='ncx_hasta' type='text' style='width:135px' value='".$hasta."' name='date_to' maxlength='19' size='4'>
				<div align='center' style='margin-top:10px;margin-bottom:10px;'>
					<span class='ncx_minibutton' onclick='setHoy()'>Hoy</span>
					<span class='ncx_minibutton' onclick='setAyer()'>Ayer</span>
					<span class='ncx_minibutton' onclick='setEstaSemana()'>Esta Semana</span>
					<span class='ncx_minibutton' onclick='setSemanaPasada()'>Semana Pasada</span>
					<span class='ncx_minibutton' onclick='setEsteMes()'>Este Mes</span>
				</div>
			</div>
			<div align='center'>
				<input class='ncx_button' onclick='procesando();' type='submit' name='submitListado' value='Generar Listado'>		
			</div>		
		</form>
		</div>";	
        
        if($nuevaConsulta){
  			  			

  			  			  		
  			$expediciones = Db::getInstance()->ExecuteS(
  					'SELECT * FROM '._DB_PREFIX_.'nacex_expediciones where fecha_alta >= "'.$desde.'" and fecha_alta <= "'.$hasta.'" and fecha_baja is null ORDER BY fecha_alta DESC'
  			);  	

  			if($expediciones){  			
                            $this->_html .= "<div id='ncx_div_listado'>"
                                    . "<h1 align='center'>Listado albaranes de(" . $desde . ") a (" . $hasta . ") 
                                        <img id='print_icon' style='float:right;cursor:pointer;' alt='Imprimir listado' title='Imprimir listado' class='noprint' src='../modules/nacex/img/print_icon.png' onclick='printlistado();' /></h1>
                                        <table id='ncx_tabla_listado'>
		  				<tr style='border-bottom-width:2px'>
		  					<th style='text-align:center'>Id</th>
		  					<th style='text-align:center'>C.B.</th>
		  					<th>
		  						<p>Albar&aacute;n</p>
		  						<p>Nom. Destinatario</p>
		  						<p>Cp. Entrega Poblaci&oacute;n</p>	  						  				
		  					</th>
		  					<th>
								<p style='margin-top:4px;'>Referencia</p>
		  						<p style='margin-top:25px;'>Direcci&oacute;n</p>
		  					</th>
		  					<th>
								<p style='margin-top:4px;'>Servicio</p>
		  						<p style='margin-top:25px;'>Imp.Reem.</p>		  				
		  					</th>
		  					<th style='text-align:center;'>Bultos</th>
		  					<th style='text-align:center;'>Kilos</th>
		  				</tr>
		  		";
		  				 
		  		$token = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);

		  		$sum_bul = 0;
		  		$sum_kil = 0;
		  		$sum_ree = 0;
		  		$agclis = array();
		  		
		  		foreach ($expediciones as $exp){
		  		
		  			$isNacexShop = isset($exp['shop_codigo']) && $exp['shop_codigo'] != "1" && $exp['shop_codigo'] != "";

		  			$agcli = nacexutils::getDefValue($exp, "agcli", null);		
		  			if($agcli != null){
			  			$array_agclis = explode("/", $agcli);	
			  			if(count($array_agclis) == 2){  							  
			  				$agclis[$agcli] = $array_agclis[0] . " / " . $array_agclis[1];
			  			}
		  			}
		  			$datoslistado = nacexutils::getDatosListado($exp['id_envio_order']);
		  			$sum_bul += nacexutils::getDefValue($exp, "bultos", 1);
		  			$sum_kil += + $datoslistado["peso"];
		  			$sum_ree += + $datoslistado["ree"];
		  			$ref = nacexutils::getReferenciaGeneral().$exp['id_envio_order'];
		  			$codbar = preg_replace("/;/", "<br>", $exp['barcode']);		  			
		  			$imgbarcode =  $png_barcode_url.preg_replace("/\//","",$exp['ag_cod_num_exp'])."00"; 
		  					  			
		  			$array_agcod_numexp = explode("/", $exp['ag_cod_num_exp']);
		  			$agcod_numexp = $array_agcod_numexp[0] . " / " . $array_agcod_numexp[1];		  		
		  			
		  			 $this->_html .="
		  				<tr>
		  					<td style='text-align:center'><a target='_blank' href='" . $_SERVER['PHP_SELF'] . "?tab=AdminOrders&id_order=" . $exp['id_envio_order']. "&vieworder&token=" . $token . "'>". $exp['id_envio_order'] . "</a></td>
		  					<td width='150px' style='padding: 5px 5px 0 5px;'>
								<img src='".$imgbarcode."' />
		  					</td>			  				
			  				<td>		  					
		  						<p>".$agcod_numexp."</p>
		  						<p>".$datoslistado['nom_ent']."</p>";
		  					if($isNacexShop && isset($exp["shop_cp"]) && isset($exp["shop_poblacion"])){
		  						$this->_html .= "<p>".$exp["shop_cp"]." ".$exp["shop_poblacion"]."</p>";
		  					}else{
		  						$this->_html .= "<p>".$datoslistado["cp_ent"]." ".$datoslistado["pob_ent"]."</p>";
		  					}
		  					if($isNacexShop){
								$array_address_invoice = $nacex->getAddressInvoiceByOrder($exp['id_envio_order']);
								$this->_html .= "<p><i><b>Att:</b> " . $array_address_invoice[0]["firstname"] . " " . $array_address_invoice[0]["lastname"] . "</i></p>";		  						
		  					}	  					
		  					
		  					$this->_html .= "		  							  							  					
		  					</td>
			  				<td>
		  						<p style='margin-top:4px;'>".$exp['ref']."</p>";
							if($isNacexShop && isset($exp['shop_direccion'])){
								$this->_html .= "<p style='margin-top:25px;'>".$exp['shop_direccion']."</p>";
							}else{
								$this->_html .= "<p style='margin-top:25px;'>".$datoslistado['dir_ent']."</p>";
							}		  								  								  								  						  					
		  					$this->_html .= "
		  					</td>
			  				<td>
		  						<p style='margin-top:4px;'>".$exp['serv']."</p>
		  						<p style='margin-top:25px;'>".nacexutils::normalizarDecimales($datoslistado["ree"], 2, ',', ' ', true, true)."</p>			  					
		  					</td>		  					
		  					<td style='text-align:center;vertical-align:middle;'>";		  				
		  						$this->_html .=  nacexutils::getDefValue($exp, "bultos", "1");						
		  					$this->_html .= "</td>
		  					<td style='text-align:center;vertical-align:middle;'>".nacexutils::normalizarDecimales($datoslistado["peso"], 2, ',', ' ', true, true)."</td>
		  			    </tr>";
		  		}  		  		
		  		$this->_html .= "<tr style='border-top-width: 2px'>
		  				<td colspan='2' style='text-align:right;margin-right:10px'><sub>".Configuration::get('NACEX_WSUSERNAME').", ";
                                                    foreach ($agclis as $ag){	
                                                        if($ag != ""){$this->_html .= "<sub>C&oacute;d. Cliente: ".$ag."</sub>"; }
                                                        }
                                                    $this->_html .= "</sub></td>
                                                <td colspan='2' style='text-align:right;margin-right:10px'>Total</td>                                                        
		  				<td style='text-align:center;'>".number_format($sum_ree, 2, ',', ' ')."€</td>
		  				<td style='text-align:center;'>".$sum_bul."</td>
		  				<td style='text-align:center;'>".number_format($sum_kil, 2, ',', ' ')."</td>		  				  		
		  			 </tr>
		  			</table>
		  		  </div>";
  			}else{
  				$this->_html .= "<fieldset><div align='center' style='color:grey'><i>(Sin resultados)</i></div></fieldset>";
  			}
  		
                   $this->_html .= "</fieldset>";
  		}  	  
        
        
        $this->context->smarty->assign('content', $this->_html);
    }

    /**
	 * Sobrecarga de la función para solventar el problema de que no heredamos de un AdminController
	 * Add a new javascript file in page header.
	 *
	 * @param mixed $js_uri
	 * @return void
	 */
  /** mexpositop 20171115 
    	public function addJS($js_uri,$check_path = true){               
            $js_uri=is_array ($js_uri)?implode('\n',$js_uri):$js_uri;
            echo "<script type='text/javascript' src='".$js_uri."'></script>";
       
  }  */
}
?>