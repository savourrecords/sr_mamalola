<?php
/**
 *  Rectified Invoices for Prestashop 1.6.x
 *
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PaRectifiedInvoices extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'parectifiedinvoices';
        $this->tab = 'billing_invoicing';
        $this->version = '1.2.0';
        $this->author = 'Pronimbo';
        $this->module_key = '4dbcf7b7d59147ee98b560a4595e1a8a';
        $this->need_instance = 0;
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Rectified Invoices');
        $this->description = $this->l('Generator corrective invoices, you can create partial or complete remedial bill, or merge several corrections in a single amendment bill.');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('RI_REF_PROV', 'PROV');
        Configuration::updateValue('RI_LAST_NUM_PROV', '1');
        Configuration::updateValue('RI_REF_VALID', 'RI');
        Configuration::updateValue('RI_LAST_NUM_VALID', '1');
        include_once(_PS_MODULE_DIR_.'parectifiedinvoices'.DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'install.php');
        return parent::install() && $this->createTabs();
    }

    public function uninstall()
    {
        return $this->removeTab() && parent::uninstall();
    }

    private function createTabs()
    {
        $tab = Tab::getInstanceFromClassName('AdminRectifiedInvoices');
        $tab->module = $this->name;
        $tab->class_name = 'AdminRectifiedInvoices';
        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Rectified invoices');
        }
        $parent_class = version_compare(_PS_VERSION_, '1.7.0', '>=') ? 'SELL' : 'AdminOrders';
        $parent = Tab::getInstanceFromClassName($parent_class);
        $tab->id_parent = $parent->id;
        $res = $tab->save();
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            ImageManager::resize(_PS_IMG_DIR_.'t'.DIRECTORY_SEPARATOR.'AdminReturn.gif', _PS_IMG_DIR_.'t'.DIRECTORY_SEPARATOR.'AdminRectifiedInvoices.gif', 16, 16);
        }

        return $res;
    }

    private function removeTab()
    {
        $tab = Tab::getInstanceFromClassName('AdminRectifiedInvoices');
        return $tab->delete();
    }
}
