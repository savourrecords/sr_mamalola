<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2015 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'rectified_invoices` (
  `id_rectified_invoice` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `num` varchar(50) DEFAULT NULL,
  `reason` text,
  `total_wt` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `total_shipping` float DEFAULT NULL,
  `total_tax` float DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_address` int(11) DEFAULT NULL,
  `validated` tinyint(4) DEFAULT \'0\',
  `date_validated` datetime DEFAULT NULL,
  `id_currency` int(11) DEFAULT \'1\',
  PRIMARY KEY (`id_rectified_invoice`)
) ENGINE='._MYSQL_ENGINE_.' AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'rectified_invoices_items` (
  `id_rectified_invoice_item` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `tax_rate` float NOT NULL DEFAULT \'0\',
  `tax_amount` float NOT NULL DEFAULT \'0\',
  `amount` float NOT NULL DEFAULT \'0\',
  `discount` float NOT NULL DEFAULT \'0\',
  `price_per_unit` float NOT NULL DEFAULT \'0\',
  `total_per_line` float NOT NULL DEFAULT \'0\',
  `quantity` int(11) NOT NULL,
  `id_rectified_invoice` int(11) NOT NULL,
  `num_invoice` varchar(50) DEFAULT NULL,
  `item_type` int(11) NOT NULL,
  `id_entity` int(11) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rectified_invoice_item`)
) ENGINE='._MYSQL_ENGINE_.' AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;';


foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
