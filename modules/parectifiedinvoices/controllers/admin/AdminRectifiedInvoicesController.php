<?php
/**
 *  Rectified Invoices for Prestashop 1.6.x
 *
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
if (!class_exists('PrestaShopCollection')) {
    include_once(_PS_MODULE_DIR_.'parectifiedinvoices'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'PrestaShopCollection.php');
}

include_once(_PS_MODULE_DIR_.'parectifiedinvoices'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'RectifiedInvoice.php');
include_once(_PS_MODULE_DIR_.'parectifiedinvoices'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'RectifiedInvoiceItem.php');
include_once(_PS_MODULE_DIR_.'parectifiedinvoices'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'HTMLTemplateRectifiedInvoice.php');

class AdminRectifiedInvoicesController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
        $this->table = 'rectified_invoices';
        $this->list_id = 'id_rectified_invoice';
        $this->identifier = 'id_rectified_invoice';
        $this->className = 'RectifiedInvoice';
        $this->lang = false;
        $this->bulk_actions = array(
            'validate' => array(
                'text' => $this->l('Validate selected'),
                'confirm' => $this->l('Validate selected items?'),
                'icon' => 'icon-check'
            ),
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-remove'
            ),
            'printPDF' => array(
                'text' => $this->l('Print PDF Selected'),
                'confirm' => $this->l('Print PDF selected items?'),
                'icon' => 'icon-file-text'
            ),
            'sendEmail' => array(
                'text' => $this->l('Send email selected'),
                'confirm' => $this->l('Send email selected items?'),
                'icon' => 'icon-envelope-alt'
            ),
        );
        $this->fields_list = array(
            'id_rectified_invoice' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'num' => array(
                'title' => $this->l('Num'),
                'filter_key' => 'a!num'
            ),
            'customer' => array(
                'title' => $this->l('Customer'),
                'filter_key' => 'CONCAT(c!firstname, \' \', c!lastname)'
            ),
            'email' => array(
                'title' => $this->l('Email'),
                'filter_key' => 'c!email'
            ),
            'total' => array(
                'title' => $this->l('Subtotal'),
                'filter_key' => 'a!total',
                'type' => 'price'
            ),
            'total_wt' => array(
                'title' => $this->l('Total'),
                'filter_key' => 'a!total_wt',
                'type' => 'price'
            ),
            'date_add' => array(
                'title' => $this->l('Date add'),
                'type' => 'date',
                'filter_key' => 'a!date_add'
            ),
            'date_validated' => array(
                'title' => $this->l('Date validated'),
                'type' => 'date',
                'align' => 'center',
                'orderby' => false
            ),
            'validated' => array(
                'title' => $this->l('Validated'),
                'align' => 'center',
                'callback' => 'printInvoiceValidated',
                'class' => 'fixed-width-sm',
                'type' => 'bool',
                'orderby' => false
            ),
        );
        $this->_select .= ' CONCAT(c.firstname, \' \', c.lastname) as customer, c.email, ';
        $this->_select .= ' a.id_rectified_invoice as pdf, a.id_rectified_invoice as id_edit ';
        $this->_join = ' INNER JOIN '._DB_PREFIX_.'customer c ON a.id_customer = c.id_customer ';
        $this->_where = '';
        $this->_orderBy = 'a.date_add';
        $this->fields_options = array(
            'configuration' => array(
                'title' => $this->l('Configuration'),
                'fields' => array(
                    'RI_REF_PROV' => array(
                        'title' => $this->l('Provisional Invoice Reference'),
                        'validation' => 'isGenericName',
                        'required' => true,
                        'type' => 'text',
                        'class' => ' fixed-width-md',
                    ),
                    'RI_LAST_NUM_PROV' => array(
                        'title' => $this->l('Next Num Provisional Invoice'),
                        'validation' => 'isUnsignedInt',
                        'required' => true,
                        'cast' => 'intval',
                        'class' => ' fixed-width-md',
                        'type' => 'text'
                    ),
                    'RI_REF_VALID' => array(
                        'title' => $this->l('Validated Invoice Reference'),
                        'validation' => 'isGenericName',
                        'required' => true,
                        'type' => 'text',
                        'class' => ' fixed-width-md',
                    ),
                    'RI_LAST_NUM_VALID' => array(
                        'title' => $this->l('Next Num Validated Invoice'),
                        'validation' => 'isUnsignedInt',
                        'cast' => 'intval',
                        'type' => 'text',
                        'class' => ' fixed-width-md',
                    ),
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
        );
    }

    public function initPageHeaderToolbar()
    {
        if (Tools::getIsset('update'.$this->table) || Tools::getIsset('add'.$this->table)) {
            $this->page_header_toolbar_btn['backlist'] = array(
                'short' => 'Back',
                'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true),
                'icon' => 'process-icon-back',
                'desc' => $this->l('Back to list'),
            );
            $invoice = new RectifiedInvoice((int)Tools::getValue('id_rectified_invoice'));
            if (!Tools::getValue('id_rectified_invoice') || !$invoice->validated) {
                $this->page_header_toolbar_btn['delete'] = array(
                    'short' => 'Delete',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&deleterectified_invoices&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-trash',
                    'desc' => $this->l('Delete'),
                    'confirm' => $this->l('Are you sure to delete this invoice ?')
                );
                $this->page_header_toolbar_btn['viewPDF'] = array(
                    'short' => 'PDF',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=viewPDF&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-file-text',
                    'desc' => $this->l('View PDF'),
                );
                $this->page_header_toolbar_btn['validate'] = array(
                    'short' => 'Validate',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=validateInvoice&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon-ok',
                    'desc' => $this->l('Validate de invoice'),
                    'confirm' => $this->l('Are you sure to validate this invoice ?')
                );
            } else {
                $this->page_header_toolbar_btn['viewPDF'] = array(
                    'short' => 'PDF',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=viewPDF&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-file-text',
                    'desc' => $this->l('View PDF'),
                );
                $this->page_header_toolbar_btn['sendEmail'] = array(
                    'short' => 'Send email',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=sendEmail&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon-envelope',
                    'desc' => $this->l('Send invoice by email'),
                );
            }
        } else {
            $this->page_header_toolbar_btn['new'] = array(
                'short' => 'New Invoice',
                'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&add'.$this->table,
                'desc' => $this->l('New invoice'),
            );
        }
        $this->context->smarty->assign('toolbar_btn', $this->page_header_toolbar_btn);
        return parent::initPageHeaderToolbar();
    }

    public function initToolbar()
    {
        if (Tools::getIsset('update'.$this->table) || Tools::getIsset('add'.$this->table)) {
            $this->toolbar_btn['backlist'] = array(
                'short' => 'Back',
                'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true),
                'icon' => 'process-icon-back',
                'class' => 'process-icon-back',
                'desc' => $this->l('Back to list'),
            );
            $invoice = new RectifiedInvoice((int)Tools::getValue('id_rectified_invoice'));
            if (!Tools::getValue('id_rectified_invoice') || !$invoice->validated) {
                $this->toolbar_btn['delete'] = array(
                    'short' => 'Delete',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&deleterectified_invoices&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-trash',
                    'desc' => $this->l('Delete'),
                    'confirm' => $this->l('Are you sure to delete this invoice ?')
                );
                $this->toolbar_btn['viewPDF'] = array(
                    'short' => 'PDF',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=viewPDF&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-file-text',
                    'class' => 'process-icon-preview',
                    'desc' => $this->l('View PDF'),
                );
                $this->toolbar_btn['validate'] = array(
                    'short' => 'Validate',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=validateInvoice&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon-ok',
                    'class' => 'process-icon-save',
                    'desc' => $this->l('Validate de invoice'),
                    'confirm' => $this->l('Are you sure to validate this invoice ?')
                );
            } else {
                $this->toolbar_btn['viewPDF'] = array(
                    'short' => 'PDF',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=viewPDF&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon- icon-file-text',
                    'class' => 'process-icon-preview',
                    'desc' => $this->l('View PDF'),
                );
                $this->toolbar_btn['sendEmail'] = array(
                    'short' => 'Send email',
                    'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&action=sendEmail&id_rectified_invoice='.(int)Tools::getValue('id_rectified_invoice'),
                    'icon' => 'process-icon-envelope',
                    'class' => 'process-icon-edit',
                    'desc' => $this->l('Send invoice by email'),
                );
            }
        } else {
            $this->toolbar_btn['new'] = array(
                'short' => 'New Invoice',
                'href' => $this->context->link->getAdminLink('AdminRectifiedInvoices', true).'&add'.$this->table,
                'desc' => $this->l('New invoice'),
            );
        }
        $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
        $this->context->smarty->assign('toolbar_scroll', true);
        $this->context->smarty->assign('title', $this->module->displayName);
        return parent::initToolbar();
    }

    public function renderForm()
    {
        $invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'));
        $params = array(
            'id_rectified_invoice' => Tools::getValue('id_rectified_invoice'),
            'gif' => _MODULE_DIR_.'parectifiedinvoices/views/img/loading.gif',
            'rectified_invoice' => $invoice,
            'wharehouses' => Warehouse::getWarehouses(false, $this->context->shop->id),
            'stock_mv_reasons' => StockMvtReason::getStockMvtReasons($this->context->language->id, 1),
            'id_currency' => Configuration::get('PS_CURRENCY_DEFAULT'),
            'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
        );
        if ($params['rectified_invoice']->validated) {
            $params['customer'] = new Customer($params['rectified_invoice']->id_customer);
        }
        $this->context->smarty->assign($params);
        $out = $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/invoice-generator.tpl');
        return $out;
    }

    public function renderView()
    {
        return $this->renderForm();
    }

    public function ajaxProcessSearchCustomer()
    {
        $rectified_invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'));
        $customers = array();
        $customers[$rectified_invoice->id_customer] = new Customer((int)$rectified_invoice->id_customer);
        if (count($customers)) {
            $to_return = array(
            'customers' => $customers,
            'found' => true
            );
        } else {
            $to_return = array('found' => false);
        }
        $this->content = Tools::jsonEncode($to_return);
    }

    public function ajaxProcessSearchCustomers()
    {
        $searches = explode(' ', Tools::getValue('customer_search'));
        $customers = array();
        $searches = array_unique($searches);
        foreach ($searches as $search) {
            if (!empty($search) && $results = Customer::searchByName($search)) {
                foreach ($results as $result) {
                    $customers[$result['id_customer']] = $result;
                }
            }
        }
        if (empty($customers)) {
            if (!empty($search) && $results = $this->searchByNIF($search)) {
                foreach ($results as $result) {
                    $customers[$result['id_customer']] = $result;
                }
            }
        }
        if (count($customers)) {
            $to_return = array(
            'customers' => $customers,
            'found' => true
            );
        } else {
            $to_return = array('found' => false);
        }
        $this->content = Tools::jsonEncode($to_return);
    }

    public function searchByNIF($query)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'customer`
		WHERE id_customer IN (
			SELECT `id_customer`
				FROM `'._DB_PREFIX_.'address`
				WHERE
					`vat_number` LIKE \'%'.pSQL($query).'%\'
					OR `dni` LIKE \'%'.pSQL($query).'%\'
		)
		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER));
    }

    public function renderList()
    {
        $this->toolbar_title = $this->l('Rectified invoices');
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->addRowAction('validate');
            $this->addRowAction('pdf');
            $this->addRowAction('sendemail');
            $this->addRowAction('view');
            $this->addRowAction('edit');
            $this->addRowAction('delete');
        } else {
            $this->addRowAction('edit');
            $this->addRowAction('validate');
            $this->addRowAction('delete');
            $this->addRowAction('view');
            $this->addRowAction('pdf');
            $this->addRowAction('sendemail');
        }
        $this->list_no_link = true;
        return parent::renderList();
    }

    public function displayButtonsLink($token, $id, $name)
    {
        if ($id) {
            $rectified_invoice = new RectifiedInvoice($id);
        } else {
            $rectified_invoice = new stdClass();
        }
        $this->context->smarty->assign(array(
                'id_invoice' => $id,
                'invoice' => $rectified_invoice,
                'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
            ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/_print_edit_icon.tpl');
    }

    public function displayPdfLink($token, $id, $name)
    {
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
            'id' => $id,
            'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
        ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/pdf_link.tpl');
    }

    public function displayValidateLink($token, $id, $name)
    {
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
            'id' => $id,
            'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
        ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/validate_link.tpl');
    }

    public function displaySendemailLink($token, $id, $name)
    {
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
            'id' => $id,
            'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
        ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/send_email_link.tpl');
    }

    public function printInvoiceValidated($id_invoice, $tr)
    {
        $this->context->smarty->assign(array(
            'link' => $this->context->link,
            'validated' => (isset($tr['validated']) && $tr['validated']),
            'ps15' => version_compare(_PS_VERSION_, '1.6.0', '<'),
        ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/validated.tpl');
    }

    public function processGenerateinvoice()
    {
        if (Tools::getValue('id_customer')) {
            $customer = new Customer(Tools::getValue('id_customer'));
        } else {
            $this->errors[] = Tools::displayError($this->l('Unkwon customer, please select a customer'));
        }
        if (Tools::getValue('id_address')) {
            $address = new Address(Tools::getValue('id_address'));
        } else {
            $this->errors[] = Tools::displayError($this->l('Unkwon address, please select a address'));
        }
        if (Tools::getValue('invoice_type') == '2') {
            if (Tools::getValue('id_invoice')) {
                $invoice = new OrderInvoice(Tools::getValue('id_invoice'), $this->context->language->id);
                $details = $invoice->getProducts();
            } else {
                $this->errors[] = Tools::displayError($this->l('Unkwon invoice, please select a invoice'));
            }
        }
        if (empty($this->errors)) {
            //TODO:
            $this->context->smarty->assign(array(
                    'customer' => $customer,
                    'address' => $address,
                    'details' => $details,
                ));
            $this->display = 'generate';
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJS('/modules/'.$this->module->name.'/views/js/back.js');
        $this->addJqueryPlugin('fancybox');
        $this->addJqueryPlugin('typewatch');
        $this->addJqueryPlugin('autocomplete');
        $this->addJqueryPlugin('select2');
        if (version_compare(_PS_VERSION_, '1.6.0', '<') && in_array($this->display, array('add', 'edit', 'view'))) {
            $jspath = '/modules/'.$this->module->name.'/views/js/select2/jquery.select2.js';
            $this->addJS($jspath);
            $jspath = '/modules/'.$this->module->name.'/views/js/back15.js';
            $this->addJS($jspath);
            $csspath = '/modules/'.$this->module->name.'/views/css/back.css';
            $this->addCSS($csspath);
            $csspath = '/modules/'.$this->module->name.'/views/css/back15.css';
            $this->addCSS($csspath);
            $jspath = '/modules/'.$this->module->name.'/views/js/form.js';
            $this->addJS($jspath);
        }
    }

    public function ajaxProcessGetaddress()
    {
        $out = array('status' => 'nok',);
        if (Tools::getValue('id_address_invoice') || Tools::getValue('id_customer')) {
            if (Tools::getValue('id_customer')) {
                $customer = new Customer(Tools::getValue('id_customer'));
                $addresses = $customer->getAddresses($this->context->language->id);
            }
            if (Tools::getValue('id_addresss_invoice') && !Tools::getValue('id_customer')) {
                $addresses = array(array('id_address') => Tools::getValue('id_addresss_invoice'));
            }
            foreach ($addresses as $address) {
                $addr = new Address((int)$address['id_address']);
                $out['addresses'][] = array(
                    'id_address' => $address['id_address'],
                    'alias' => $address['alias'],
                    'formated_address' => AddressFormat::generateAddress($addr, array(), '<br />'),
                );
                if (Tools::getValue('id_address_invoice')) {
                    $out['id_address'] = $address['id_address'];
                }
            }
            if (Tools::getValue('id_address_invoice')) {
                $out['id_address'] = Tools::getValue('id_address_invoice');
            }
            $out['status'] = 'ok';
        } else {
            $out['error'] = Tools::displayError($this->module->l('Address incorrect'));
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessUpdatevalidateddate()
    {
        $out = array(
            'date_formated' => '',
            'result' => 'nok',
        );
        if (Tools::getValue('date') && Tools::getValue('id_rectified_invoice')) {
            $rectified_invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'));
            $rectified_invoice->date_validated = date('Y-m-d h:i:s', strtotime(Tools::getValue('date')));
            if ($rectified_invoice->update()) {
                $out = array(
                'date_formated' => $rectified_invoice->date_validated,
                'result' => 'ok',
                );
            }
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessGetaddressandinvoices()
    {
        $out = array(
            'invoices' => array(
                'id_adress' => '0',
                'name' => '-'
            ),
            'id_address' => '0',
            'status' => 'nok',
            'currencies' => array()
        );
        if (Tools::getValue('id_customer')) {
            $customer = new Customer(Tools::getValue('id_customer'));
            $addresses = $customer->getAddresses($this->context->language->id);
            foreach ($addresses as $address) {
                $addr = new Address((int)$address['id_address']);
                $out['addresses'][] = array(
                    'id_address' => $address['id_address'],
                    'alias' => $address['alias'],
                    'formated_address' => AddressFormat::generateAddress($addr, array(), '<br />'),
                );
                $out['id_address'] = $address['id_address'];
            }
            $invoices = RectifiedInvoice::getInvoicesByCustomer(Tools::getValue('id_customer'));
            if ($invoices) {
                $out['invoices'] = $invoices;
            }
            $out['currencies'] = Currency::getCurrenciesByIdShop($customer->id_shop);
            $out['id_currency'] = $this->context->currency->id;
            $out['currency_sign'] = $this->context->currency->sign;
            $out['status'] = 'ok';
        } else {
            $out['error'] = Tools::displayError($this->module->l('Customer incorrect'));
        }
        echo Tools::jsonEncode($out);
        die();
    }
    public function ajaxProcessGetinvoices()
    {
        $out = array(
            'invoices' => array(
                'id_adress' => '0',
                'name' => '-'
            ),
            'currencies' => array(),
            'last_id_order' => Tools::getValue('last_id_order', 0),
        );
        if (Tools::getValue('id_customer')) {
            $invoices = RectifiedInvoice::getInvoicesByCustomer(Tools::getValue('id_customer'), Tools::getValue('id_currency', null));
            if ($invoices) {
                $out['invoices'] = $invoices;
            }
            $out['status'] = 'ok';
        } else {
            $out['error'] = Tools::displayError($this->module->l('Customer incorrect'));
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessUpdatecurrency()
    {
        $currency = new Currency(Tools::getValue('id_currency'));
        $out = array(
            'status' => 'ok',
            'id_currency' => $currency->id,
            'confirmation' => $this->l('Currency has successfully selected'),
            'error' => '',
        );
        if (!Validate::isLoadedObject($currency)) {
            $out['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
            $out['error'] = $this->l('An error ocurred while try update the currency selected');
            $out['status'] = 'nok';
        } elseif (Tools::getValue('id_rectified_invoice')) {
            if (RectifiedInvoice::changeCurrency((int)Tools::getValue('id_rectified_invoice'), (int)$currency->id)) {
                $out = array(
                'status' => 'ok',
                'id_currency' => $currency->id,
                'currency_sign' => $currency->sign,
                'confirmation' => $currency->name.' ('.$currency->sign.') '.$this->l('has successfully selected as invoice currency'),
                );
            } else {
                $out = array(
                'status' => 'ok',
                'id_currency' => $currency->id,
                'currency_sign' => $currency->sign,
                'confirmation' => $currency->name.' ('.$currency->sign.') '.$this->l('has successfully selected as invoice currency'),
                );
            }
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessInitrectifiedinvoice()
    {
        $out = array(
            'invoices' => array(
                'id_adress' => '0',
                'name' => '-'
            ),
            'id_address' => '0',
            'status' => 'nok',
        );
        if (Tools::getValue('id_rectified_invoice')) {
            $invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'), true);
            if ($invoice->items) {
                foreach ($invoice->items as &$items) {
                    $items->total = Tools::displayPrice($items->tax_amount + $items->amount, (int)$invoice->id_currency);
                    $items->amount = Tools::displayPrice($items->amount, (int)$invoice->id_currency);
                    $items->price_per_unit = Tools::displayPrice($items->price_per_unit, (int)$invoice->id_currency);
                    $items->tax_rate = $items->tax_rate.'% ';
                    $items->total_per_line = Tools::displayPrice($items->total_per_line, (int)$invoice->id_currency);
                    $items->discount = Tools::displayPrice($items->discount, (int)$invoice->id_currency);
                    $items->tax_amount = Tools::displayPrice($items->tax_amount, (int)$invoice->id_currency);
                }
            }
            $out['summary'] = array(
                'items' => $invoice->items,
                'subtotal' => Tools::displayPrice($invoice->total, (int)$invoice->id_currency),
                'num_invoice' => $invoice->num,
                'tax_amount' => Tools::displayPrice($invoice->total_tax, (int)$invoice->id_currency),
                'total' => Tools::displayPrice($invoice->total_wt, (int)$invoice->id_currency),
                'id_rectified_invoice' => Tools::getValue('id_rectified_invoice'),
            );
            $customer = new Customer($invoice->id_customer);
            $customers = array();
            $customers[$customer->id] = $customer;
            $out['currencies'] = Currency::getCurrenciesByIdShop($customer->id_shop);
            if (!$invoice->id_currency) {
                $out['id_currency'] = $this->context->currency->id;
                $out['currency_sign'] = $this->context->currency->sign;
            } else {
                $currency = new Currency($invoice->id_currency);
                $out['id_currency'] = $currency->id;
                $out['currency_sign'] = $currency->sign;
            }
            if (count($customers)) {
                $out['customers'] = $customers;
                $out['customers_found'] = true;
            } else {
                $out['customer_found'] = false;
            }
            $addresses = $customer->getAddresses($this->context->language->id);
            foreach ($addresses as $address) {
                $addr = new Address((int)$address['id_address']);
                $out['addresses'][] = array(
                    'id_address' => $address['id_address'],
                    'alias' => $address['alias'],
                    'formated_address' => AddressFormat::generateAddress($addr, array(), '<br />'),
                );
                $out['id_address'] = $address['id_address'];
            }
            $invoices = RectifiedInvoice::getInvoicesByCustomer($invoice->id_customer, $invoice->id_currency);
            if ($invoices) {
                $out['invoices'] = $invoices;
            }
            $out['status'] = 'ok';
        } else {
            $out['error'] = Tools::displayError($this->module->l('Customer incorrect'));
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessGetsummary()
    {
        $out = array();
        $out['summary'] = array(
            'items' => array(),
            'subtotal' => 0,
            'tax_amount' => 0,
            'total' => 0,
            'id_rectified_invoice' => Tools::getValue('id_rectified_invoice'),
        );
        $out['status'] = 'ok';
        if (Tools::getValue('id_rectified_invoice')) {
            $invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'), true);
            if ($invoice->items) {
                foreach ($invoice->items as &$items) {
                    $items->total = Tools::displayPrice($items->tax_amount + $items->amount, (int)$invoice->id_currency);
                    $items->amount = Tools::displayPrice($items->amount, (int)$invoice->id_currency);
                    $items->price_per_unit = Tools::displayPrice($items->price_per_unit, (int)$invoice->id_currency);
                    $items->tax_rate = $items->tax_rate.'% ';
                    $items->total_per_line = Tools::displayPrice($items->total_per_line, (int)$invoice->id_currency);
                    $items->discount = Tools::displayPrice($items->discount, (int)$invoice->id_currency);
                    $items->tax_amount = Tools::displayPrice($items->tax_amount, (int)$invoice->id_currency);
                }
            }
            $out['summary'] = array(
                'items' => $invoice->items,
                'subtotal' => Tools::displayPrice($invoice->total, (int)$invoice->id_currency),
                'num_invoice' => $invoice->num,
                'tax_amount' => Tools::displayPrice($invoice->total_tax, (int)$invoice->id_currency),
                'total' => Tools::displayPrice($invoice->total_wt, (int)$invoice->id_currency),
                'id_rectified_invoice' => Tools::getValue('id_rectified_invoice'),
                'status' => 'ok',
            );
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessUpdatereason()
    {
        $out = array();
        if (Tools::getValue('id_rectified_invoice')) {
            $rectified_invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'));
            $rectified_invoice->reason = Tools::getValue('reason');
            $rectified_invoice->update();
            $out['status'] = 'ok';
            $out['confirmation'] = $this->l('The reason has successfully updated');
        } else {
            $out['status'] = 'nok';
            $out['error'] = $this->l('Add an item to invoice first, then try to set the reason');
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function ajaxProcessInvoicedetails()
    {
        $out = array();
        $order_invoice = new OrderInvoice(Tools::getValue('id_invoice'));
        $order = new Order($order_invoice->id_order);
        $shippings = $order->getShipping();
        $cart_rules = $order->getCartRules($order_invoice->id);
        //$ecotaxes = $order_invoice->getEcoTaxTaxesBreakdown();
        //$wrapping = $order->getWrappingTaxesBreakdown();
        $details = $order_invoice->getProductsDetail();
        if ($details) {
            foreach ($details as $detail) {
                $calculator = OrderDetail::getTaxCalculatorStatic($detail['id_order_detail']);
                $wharehouse_on = Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT');
                $product = new Product($detail['product_id'], false, null, $detail['id_shop']);
                $advanced_management = $product->advanced_stock_management;
                $out['items'][] = array(
                'id_item' => $detail['id_order_detail'],
                'item_type' => RectifiedInvoiceItem::TYPE_ORDER_DETAIL,
                'id_order' => $order_invoice->id_order,
                'quantity' => self::getQtyOrderDetailToAddItem((int)$detail['id_order_detail'], (int)$detail['product_quantity']),
                'quantity_rate' => $detail['product_quantity'],
                'unit_price' => Tools::displayPrice(Tools::convertPrice((float)(($detail['product_price'] == $detail['unit_price_tax_excl']) ? $detail['product_price'] : $detail['unit_price_tax_excl']), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'discount' => (($detail['product_price'] == $detail['unit_price_tax_excl']) ? (((float)$detail['reduction_percent'] > 0) ? $detail['reduction_percent'].'%' : Tools::displayPrice(Tools::convertPrice((float)$detail['reduction_amount_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency)) : ' - '),
                'item' => $detail['product_name'],
                'wharehouse_on' => (int)$wharehouse_on,
                'advanced_management' => (int)$advanced_management,
                'price' => Tools::displayPrice(Tools::convertPrice((float)$detail['total_price_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'tax_rate' => ((isset($calculator->taxes[0]) && Validate::isLoadedObject($calculator->taxes[0])) ? $calculator->taxes[0]->rate : 0),
                'tax' => Tools::displayPrice(Tools::convertPrice((float)$detail['total_price_tax_incl'] - (float)$detail['total_price_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'total_line' => Tools::displayPrice(Tools::convertPrice((float)$detail['total_price_tax_incl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency)
                );
            }
        }
        if ($cart_rules) {
            foreach ($cart_rules as $cart_rule) {
                $out['items'][] = array(
                'id_item' => $cart_rule['id_order_cart_rule'],
                'item_type' => RectifiedInvoiceItem::TYPE_ORDER_CART_RULE,
                'id_order' => $order_invoice->id_order,
                'quantity' => self::getQtyCartRuleToAddItem($cart_rule['id_order_cart_rule'], 1),
                'quantity_rate' => 1,
                'unit_price' => Tools::displayPrice(Tools::convertPrice((float)(-1 * $cart_rule['value_tax_excl']), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'unit_price_wt' => Tools::displayPrice(Tools::convertPrice((float)(-1 * $cart_rule['value']), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'wharehouse_on' => 0,
                'item' => $cart_rule['name'],
                'price' => Tools::displayPrice(Tools::convertPrice((float)(-1 * $cart_rule['value']), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'discount' => ' - ',
                'tax_rate' => 0,
                'tax' => Tools::displayPrice(Tools::convertPrice((float)(-1 * ($cart_rule['value'] - $cart_rule['value_tax_excl'])), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'total_line' => Tools::displayPrice(Tools::convertPrice((float)(-1 * $cart_rule['value']), (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                );
            }
        }
        if ($shippings) {
            foreach ($shippings as $shipping) {
                if (isset($shipping['carrier_name'])) {
                    $out['items'][] = array(
                            'id_item' => $shipping['id_order_carrier'],
                            'item_type' => RectifiedInvoiceItem::TYPE_ORDER_SHIPPING,
                            'id_order' => $order_invoice->id_order,
                            'quantity' => self::getQtyShippingToAddItem($order->id, 1),
                            'quantity_rate' => '1',
                            'unit_price' => Tools::displayPrice(Tools::convertPrice((float)$shipping['shipping_cost_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                            'unit_price_wt' => Tools::displayPrice(Tools::convertPrice((float)$shipping['shipping_cost_tax_incl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                            'item' => $this->l('Shipping order: ').' '.$order->reference.' '.$shipping['carrier_name'].' ',
                            'price' => Tools::displayPrice(Tools::convertPrice((float)$shipping['shipping_cost_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                            'tax_rate' => $order->carrier_tax_rate,
                            'wharehouse_on' => 0,
                            'tax' => Tools::displayPrice(Tools::convertPrice((float)$shipping['shipping_cost_tax_incl'], (int)$order->id_currency, (int)$order->id_currency) - Tools::convertPrice((float)$shipping['shipping_cost_tax_excl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                            'discount' => ' - ',
                            'total_line' => Tools::displayPrice(Tools::convertPrice((float)$shipping['shipping_cost_tax_incl'], (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                        );
                }
            }
        }
        //if ($ecotax['ecotax_tax_incl'] > 0)
//                $out['items'][] = array(
//                    'id_item' => $order_invoice->id_order * RectifiedInvoiceItem::TYPE_ORDER_ECOTAX,
//                    'item_type' => RectifiedInvoiceItem::TYPE_ORDER_ECOTAX,
//                    'id_order' => $order_invoice->id_order,
//                    'quantity' => self::getQtyEcotaxToAddItem($order->id, 1),
//                    'quantity_rate' => '1',
//                    'unit_price' => Tools::displayPrice(Tools::convertPrice((float)$ecotax['ecotax_tax_excl'], (int)$order->id_currency, (int)$id_currency), (int)$id_currency),
//                    'unit_price_wt' => Tools::displayPrice(Tools::convertPrice((float)$ecotax['ecotax_tax_incl'], (int)$order->id_currency, (int)$id_currency), (int)$id_currency),
//                    'item' => $this->l('Ecotax'),
//                    'price' => Tools::displayPrice(Tools::convertPrice((float)$ecotax['ecotax_tax_excl'], (int)$order->id_currency, (int)$id_currency), (int)$id_currency),
//                    'tax_rate' => $ecotax['ecotax_tax_rate'],
//                    'discount' => Tools::displayPrice(0, (int)$id_currency),
//                    'tax' => Tools::displayPrice((Tools::convertPrice((float)$ecotax['ecotax_tax_incl'], (int)$order->id_currency, (int)$id_currency) - Tools::convertPrice((float)$ecotax['ecotax_tax_excl'], (int)$order->id_currency, (int)$id_currency)), (int)$id_currency),
//                    'total_line' => Tools::displayPrice(Tools::convertPrice((float)$ecotax['ecotax_tax_excl'], (int)$order->id_currency, (int)$id_currency), (int)$id_currency),);
//
        if ($order->total_wrapping_tax_excl > 0) {
            $id_tax = Configuration::get('PS_GIFT_WRAPPING_TAX_RULES_GROUP');
            $address = new Address($order->id_address_invoice);
            $calculator = TaxRulesGroup::getTaxes($id_tax, $address->id_country, $address->id_state, $address->id_country);
            $out['items'][] = array(
                'id_item' => $order_invoice->id_order * RectifiedInvoiceItem::TYPE_ORDER_WRAPPING,
                'item_type' => RectifiedInvoiceItem::TYPE_ORDER_WRAPPING,
                'id_order' => $order_invoice->id_order,
                'quantity' => self::getQtyWrappingToAddItem($order->id, 1),
                'quantity_rate' => '1',
                'wharehouse_on' => 0,
                'unit_price' => Tools::displayPrice(Tools::convertPrice((float)$order->total_wrapping_tax_excl, (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'unit_price_wt' => Tools::displayPrice(Tools::convertPrice((float)$order->total_wrapping_tax_incl, (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'item' => $this->l('Wrapping order : ').' '.$order->reference,
                'price' => Tools::displayPrice(Tools::convertPrice((float)$order->total_wrapping_tax_excl, (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
                'tax_rate' => $calculator->taxes[0]->rate,
                'discount' => ' - ',
                'tax' => Tools::displayPrice((Tools::convertPrice((float)$order->total_wrapping_tax_incl, (int)$order->id_currency, (int)$order->id_currency) - Tools::convertPrice((float)$order->total_wrapping_tax_excl, (int)$order->id_currency, (int)$order->id_currency)), (int)$order->id_currency),
                'total_line' => Tools::displayPrice(Tools::convertPrice((float)$order->total_wrapping_tax_incl, (int)$order->id_currency, (int)$order->id_currency), (int)$order->id_currency),
            );
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public static function getQtyOrderDetailToAddItem($id_order_detail, $order_detail_quantity)
    {
        $query = new DbQuery();
        $query->select('sum(quantity) as quantity');
        $query->from('rectified_invoices_items');
        $query->where('id_entity = '.(int)$id_order_detail);
        $query->where('item_type = '.(int)RectifiedInvoiceItem::TYPE_ORDER_DETAIL);
        $quantity = (int)DB::getInstance()->getValue($query);
        return (int)((int)$order_detail_quantity - $quantity);
    }

    public static function getQtyCartRuleToAddItem($id_order, $pquantity)
    {
        $query = new DbQuery();
        $query->select('sum(quantity) as quantity');
        $query->from('rectified_invoices_items');
        $query->where('id_entity = '.(int)$id_order);
        $query->where('item_type = '.(int)RectifiedInvoiceItem::TYPE_ORDER_CART_RULE);
        $quantity = (int)DB::getInstance()->getValue($query);
        return (int)((int)$pquantity - $quantity);
    }

    public static function getQtyWrappingToAddItem($id_order, $pquantity)
    {
        $query = new DbQuery();
        $query->select('sum(quantity) as quantity');
        $query->from('rectified_invoices_items');
        $query->where('id_order = '.(int)$id_order);
        $query->where('item_type = '.(int)RectifiedInvoiceItem::TYPE_ORDER_WRAPPING);
        $quantity = (int)DB::getInstance()->getValue($query);
        return (int)((int)$pquantity - $quantity);
    }

    public static function getQtyShippingToAddItem($id_order, $pquantity)
    {
        $query = new DbQuery();
        $query->select('sum(quantity) as quantity');
        $query->from('rectified_invoices_items');
        $query->where('id_order = '.(int)$id_order);
        $query->where('item_type = '.(int)RectifiedInvoiceItem::TYPE_ORDER_SHIPPING);
        $quantity = (int)DB::getInstance()->getValue($query);
        return (int)((int)$pquantity - $quantity);
    }

    public static function getQtyEcotaxToAddItem($id_order, $pquantity)
    {
        $query = new DbQuery();
        $query->select('sum(quantity) as quantity');
        $query->from('rectified_invoices_items');
        $query->where('id_order = '.(int)$id_order);
        $query->where('item_type = '.(int)RectifiedInvoiceItem::TYPE_ORDER_ECOTAX);
        $quantity = (int)DB::getInstance()->getValue($query);
        return (int)((int)$pquantity - $quantity);
    }

    public function ajaxProcessDeleteitem()
    {
        $out = array('status' => 'nok');
        $item = new RectifiedInvoiceItem(Tools::getValue('id_item'));
        $rectified_invoice = new RectifiedInvoice($item->id_rectified_invoice);
        if (!$rectified_invoice->validated) {
            if ($item->delete() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                $invoice = new RectifiedInvoice($rectified_invoice->id, true);
                if ($invoice->items) {
                    foreach ($invoice->items as &$items) {
                        $items->total = Tools::displayPrice($items->tax_amount + $items->amount, (int)$rectified_invoice->id_currency);
                        $items->amount = Tools::displayPrice($items->amount, (int)$rectified_invoice->id_currency);
                        $items->price_per_unit = Tools::displayPrice($items->price_per_unit, (int)$rectified_invoice->id_currency);
                        $items->tax_rate = $items->tax_rate.'% ';
                        $items->total_per_line = Tools::displayPrice($items->total_per_line, (int)$rectified_invoice->id_currency);
                        $items->discount = Tools::displayPrice($items->discount, (int)$rectified_invoice->id_currency);
                        $items->tax_amount = Tools::displayPrice($items->tax_amount, (int)$rectified_invoice->id_currency);
                    }
                }
                $out['summary'] = array(
                    'items' => $invoice->items,
                    'subtotal' => Tools::displayPrice($invoice->total, (int)$rectified_invoice->id_currency),
                    'num_invoice' => $invoice->num,
                    'tax_amount' => Tools::displayPrice($invoice->total_tax, (int)$rectified_invoice->id_currency),
                    'total' => Tools::displayPrice($invoice->total_wt, (int)$rectified_invoice->id_currency),
                    'id_rectified_invoice' => $rectified_invoice->id,
                    'status' => 'ok',
                );
                $out['status'] = 'ok';
                $out['confirmation'] = $this->l('Has successfully delete the item:');
            } else {
                $out['status'] = 'nok';
                $out['error'] = $this->l('An error ocurred while try delete the item:');
            }
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public function processViewpdf()
    {
        if (Tools::getIsset('id_rectified_invoice')) {
            $this->generateInvoicePDFByIdOrder(Tools::getValue('id_rectified_invoice'));
            die();
        } else {
            die(Tools::displayError('The invoice ID -- is missing.'));
        }
    }

    public function generateInvoicePDFByIdOrder($id_rectified_invoice, $no_return = true)
    {
        $invoice = new RectifiedInvoice((int)$id_rectified_invoice);
        if (!Validate::isLoadedObject($invoice)) {
            die(Tools::displayError('The order cannot be found within your database.'));
        }
        $addons_invoice_list = $this->getInvoicesCollection($id_rectified_invoice);
        return $this->generatePDF($addons_invoice_list, 'RectifiedInvoice', $no_return);
    }

    public function getInvoicesCollection($id)
    {
        $query = new DbQuery();
        $query->from('rectified_invoices');
        $query->where(RectifiedInvoice::$definition['primary'].'='.(int)$id);
        $res = DB::getInstance()->executeS($query);
        $order_invoices = ObjectModel::hydrateCollection('RectifiedInvoice', $res, $this->context->language->id);
        return $order_invoices;
    }

    public function processValidateinvoice()
    {
        if (Tools::getIsset('id_rectified_invoice')) {
            $invoice = new RectifiedInvoice((int)Tools::getValue('id_rectified_invoice'));
            if (!Validate::isLoadedObject($invoice)) {
                $this->errors[] = Tools::displayError('The invoice cannot be found within your database.');
            } elseif ($invoice->validated) {
                $this->errors[] = Tools::displayError('The invoice are validated yet.');
            } else {
                $invoice->validated = 1;
                $customer = new Customer($invoice->id_customer);
                $invoice->date_validated = date('Y-m-d h:i:s', strtotime('now'));
                $last_ref = Configuration::get('RI_LAST_NUM_VALID', false, $customer->id_shop_group, $customer->id_shop);
                $invoice->num = Configuration::get('RI_REF_VALID', false, $customer->id_shop_group, (int)$customer->id_shop).''.$last_ref;
                if ($invoice->update()) {
                    Configuration::updateValue('RI_LAST_NUM_VALID', (int)($last_ref + 1), false, $customer->id_shop_group, $customer->id_shop);
                    if (Tools::getValue('sendEmail') == '1') {
                        $this->processSendEmail();
                    }
                    $this->confirmations[] = $this->l('Invoice was validated successfully with reference: ').' '.$invoice->num;
                } else {
                    $this->errors[] = Tools::displayError('The invoice cannot be validated.');
                }
            }
        }
    }

    public function processSendEmail()
    {
        if (Tools::getValue('id_rectified_invoice')) {
            if ($this->sendNewInvoiceEmail(Tools::getValue('id_rectified_invoice'))) {
                $this->confirmations[] = $this->l('Email send successfully');
            } else {
                $this->errors[] = Tools::displayError($this->l('An error ocurred to try send de invoice by email'));
            }
        } else {
            $this->errors[] = Tools::displayError($this->l('Unkwon invoice, please select a invoice'));
        }
    }

    public function ajaxProcessAdditem()
    {
        $out = array('status' => 'nok');
        if (!(int)Tools::getValue('id_rectified_invoice')) {
            $rectified_invoice = new RectifiedInvoice();
            $rectified_invoice->id_customer = Tools::getValue('id_customer');
            $customer = new Customer(Tools::getValue('id_customer'));
            $rectified_invoice->id_address = Tools::getValue('id_address');
            $rectified_invoice->date_add = date('Y-m-d h:i:s', strtotime('now'));
            $rectified_invoice->id_currency = 0;
            $last_ref = (int)Configuration::get('RI_LAST_NUM_PROV', false, $customer->id_shop_group, (int)$customer->id_shop);
            $rectified_invoice->num = Configuration::get('RI_REF_PROV', false, $customer->id_shop_group, (int)$customer->id_shop).''.$last_ref;
            if ($rectified_invoice->add()) {
                Configuration::updateValue('RI_LAST_NUM_PROV', ($last_ref + 1), false, $customer->id_shop_group, (int)$customer->id_shop);
            }
        } else {
            $customer = new Customer(Tools::getValue('id_customer'));
            $rectified_invoice = new RectifiedInvoice(Tools::getValue('id_rectified_invoice'), false, $customer->id_shop_group, (int)$customer->id_shop);
        }

        $id_currency = $rectified_invoice->id_currency;

        if (Tools::getValue('item_type') == RectifiedInvoiceItem::TYPE_ORDER_DETAIL) {
            $order_detail = new OrderDetail(Tools::getValue('id_item'));
            $tax_calculator = $order_detail->getTaxCalculator();
            $order = new Order($order_detail->id_order);
            $quantity = (int)Tools::getValue('quantity');
            if ($id_currency == 0) {
                $id_currency = $order->id_currency;
            }
            $quantity_to_add = self::getQtyOrderDetailToAddItem($order_detail->id, $order_detail->product_quantity);
            if ($quantity >= $quantity_to_add) {
                $quantity = $quantity_to_add;
            }
            if ($quantity > 0) {
                $item = new RectifiedInvoiceItem(self::getItemId(Tools::getValue('id_item')));
                $item->id_order = $order_detail->id_order;
                $item->item_type = RectifiedInvoiceItem::TYPE_ORDER_DETAIL;
                $item->id_rectified_invoice = $rectified_invoice->id;
                $item->num_invoice = Configuration::get('PS_INVOICE_PREFIX', Context::getContext()->language->id, null, $order->id_shop).sprintf('%06d', $order->invoice_number);
                $item->text = $order_detail->product_name;
                $item->id_entity = Tools::getValue('id_item');
                $item->quantity = (int)$item->quantity + $quantity;
                // price per quantity
                $spu = $order_detail->total_shipping_price_tax_excl / $order_detail->product_quantity;
                // price tax per unit
                $amount = Tools::convertPrice((($order_detail->unit_price_tax_excl + $spu) * $quantity), $order->id_currency, $id_currency);
                $item->amount = (float)$item->amount - (float)$amount;
                $item->price_per_unit = Tools::convertPrice((float)(($order_detail->product_price == $order_detail->unit_price_tax_excl) ? $order_detail->product_price : $order_detail->unit_price_tax_excl), (int)$order->id_currency, (int)$id_currency);
                $item->discount = (float)(($order_detail->product_price == $order_detail->unit_price_tax_excl) ? (((float)$order_detail->reduction_percent > 0) ? $order_detail->reduction_percent.'%' : Tools::displayPrice(Tools::convertPrice((float)$order_detail->reduction_amount_tax_excl, (int)$order->id_currency, (int)$id_currency), (int)$id_currency)) : 0);
                $item->total_per_line = (float)(Tools::convertPrice((float)$order_detail->total_price_tax_incl, (int)$order->id_currency, (int)$id_currency) * -1);
                $pprice_tax = $order_detail->unit_price_tax_incl - $order_detail->unit_price_tax_excl;
                //shipment tax per unit
                $spu_tax = ($order_detail->total_shipping_price_tax_incl - $order_detail->total_shipping_price_tax_excl) / $order_detail->product_quantity;
                //total tax line
                $tax_amount = ($pprice_tax + $spu_tax) * $quantity;
                $item->tax_amount = (float)$item->tax_amount - (float)Tools::convertPrice($tax_amount, $order->id_currency, $id_currency);
                $item->tax_rate = $tax_calculator->taxes[0]->rate;
                if ($item->save() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                    if (Tools::getValue('restock') !== 'false') {
                        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && Tools::getValue('advanced_management', 0)) {
                            $product = new Product($order_detail->product_id, false, null, $order_detail->id_shop);
                            $manager = StockManagerFactory::getManager();
                            $wharehouse = new Warehouse((int)Tools::getValue('id_wharehouse', 0));
                            if (Pack::isPack((int)$product->id)) {
                                // Gets items
                                if ($product->pack_stock_type == 1 || $product->pack_stock_type == 2 || ($product->pack_stock_type == 3 && Configuration::get('PS_PACK_STOCK_TYPE') > 0)) {
                                    $products_pack = Pack::getItems((int)$product->id, (int)Configuration::get('PS_LANG_DEFAULT'));
                                    // Foreach item
                                    foreach ($products_pack as $product_pack) {
                                        if ($product_pack->advanced_stock_management == 1) {
                                            $manager->addProduct(
                                                $product_pack->id,
                                                $product_pack->id_pack_product_attribute,
                                                $wharehouse,
                                                ($product_pack->pack_quantity * $quantity),
                                                Tools::getValue('reason'),
                                                ($amount),
                                                true
                                            );
                                        }
                                    }
                                }
                                if ($product->pack_stock_type == 0 || $product->pack_stock_type == 2 || ($product->pack_stock_type == 3 && (Configuration::get('PS_PACK_STOCK_TYPE') == 0 || Configuration::get('PS_PACK_STOCK_TYPE') == 2))) {
                                    $manager->addProduct(
                                        $order_detail->product_id,
                                        $order_detail->product_attribute_id,
                                        $wharehouse,
                                        $quantity,
                                        Tools::getValue('reason'),
                                        ($amount),
                                        true
                                    );
                                }
                            } else {
                                $manager->addProduct(
                                    $order_detail->product_id,
                                    $order_detail->product_attribute_id,
                                    $wharehouse,
                                    $quantity,
                                    Tools::getValue('reason'),
                                    ($amount),
                                    true
                                );
                            }
                            $id_product = $order_detail->product_id;
                            StockAvailable::synchronize($id_product);
                        } elseif (Tools::getValue('advanced_management', 0) == 0) {
                            StockAvailable::updateQuantity(
                                $order_detail->product_id,
                                $order_detail->product_attribute_id,
                                $quantity,
                                $order_detail->id_shop
                            );
                        }
                    }
                    $out['status'] = 'ok';
                    $out['confirmation'] = $this->l('Has successfully added the item:').' '.$item->text.' '.$this->l('quantity:').' '.$quantity;
                    $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                } else {
                    $out['status'] = 'nok';
                    $out['error'] = $this->l('An error ocurred while try add the item:').' '.$item->text.' '.$this->l('quantity:').' '.$quantity;
                    $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                }
            } else {
                $out['status'] = 'nok';
                $out['error'] = $this->l('An error ocurred while try add the item:').' '.$order_detail->product_name.' '.$this->l('quantity:').' '.(int)Tools::getValue('quantity');
                $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
            }
            if (isset($out['summary']['id_rectified_invoice'])) {
                $rectified_invoice = new RectifiedInvoice($rectified_invoice->id, true);
                if ($rectified_invoice->items) {
                    foreach ($rectified_invoice->items as &$items) {
                        $items->total = Tools::displayPrice($items->tax_amount + $items->amount, (int)$rectified_invoice->id_currency);
                        $items->amount = Tools::displayPrice($items->amount, (int)$rectified_invoice->id_currency);
                        $items->price_per_unit = Tools::displayPrice($items->price_per_unit, (int)$rectified_invoice->id_currency);
                        $items->tax_rate = $items->tax_rate.'% ';
                        $items->total_per_line = Tools::displayPrice($items->total_per_line, (int)$rectified_invoice->id_currency);
                        $items->discount = Tools::displayPrice($items->discount, (int)$rectified_invoice->id_currency);
                        $items->tax_amount = Tools::displayPrice($items->tax_amount, (int)$rectified_invoice->id_currency);
                    }
                }
                $out['summary'] = array(
                    'items' => $rectified_invoice->items,
                    'subtotal' => Tools::displayPrice($rectified_invoice->total, (int)$rectified_invoice->id_currency),
                    'num_invoice' => $rectified_invoice->num,
                    'tax_amount' => Tools::displayPrice($rectified_invoice->total_tax, (int)$rectified_invoice->id_currency),
                    'total' => Tools::displayPrice($rectified_invoice->total_wt, (int)$rectified_invoice->id_currency),
                    'id_rectified_invoice' => Tools::getValue('id_rectified_invoice'),
                    'status' => 'ok',
                );
            }
        } elseif (Tools::getValue('item_type') == RectifiedInvoiceItem::TYPE_FREE_ITEM) {
            $item = new RectifiedInvoiceItem(self::getItemId(Tools::getValue('id_item')), RectifiedInvoiceItem::TYPE_FREE_ITEM);
            $item->text = Tools::getValue('text');
            $item->id_entity = 0;
            $quantity_to_add = $item->quantity = (int)Tools::getValue('quantity', 0);
            $item->price_per_unit = (float)Tools::getValue('price_per_unit', 0);
            $item->discount = (float)Tools::getValue('discount', 0);
            $item->amount = (float)($item->quantity * $item->price_per_unit) - $item->discount;
            $item->tax_rate = (float)Tools::getValue('tax_rate', 0);
            $item->tax_amount = (float)($item->amount) * (($item->tax_rate / 100));
            $item->total_per_line = (float)$item->tax_amount + $item->amount;
            $item->id_order = 0;
            $item->item_type = RectifiedInvoiceItem::TYPE_FREE_ITEM;
            $item->id_rectified_invoice = $rectified_invoice->id;
            $invoice = new OrderInvoice(Tools::getValue('id_invoice', 0));
            if (Validate::isLoadedObject($invoice)) {
                $order = new Order($invoice->id_order);
                $item->num_invoice = Configuration::get('PS_INVOICE_PREFIX', Context::getContext()->language->id, null, $order->id_shop).sprintf('%06d', $invoice->number);
                if ($id_currency == 0) {
                    $id_currency = $order->id_currency;
                }
            } elseif ($id_currency == 0) {
                $id_currency = Configuration::get('PS_CURRENCY_DEFAULT');
            }

            if ($item->save() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                $out['status'] = 'ok';
                $out['confirmation'] = $this->l('Has successfully added the item:').' '.$item->text.' '.$this->l('quantity:').' '.$quantity_to_add;
                $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
            } else {
                $out['status'] = 'nok';
                $out['error'] = $this->l('An error ocurred while try add the item:').' '.$item->text.' '.$this->l('quantity:').' '.$quantity_to_add;
                $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
            }
        } elseif (Tools::getValue('item_type') == RectifiedInvoiceItem::TYPE_ORDER_SHIPPING) {
            $item = new RectifiedInvoiceItem(self::getItemId(Tools::getValue('id_item')), RectifiedInvoiceItem::TYPE_ORDER_SHIPPING);
            $order = new Order(Tools::getValue('id_order'));
            $shippings = $order->getShipping();
            if ($id_currency == 0) {
                $id_currency = $order->id_currency;
            }

            if ($shippings) {
                foreach ($shippings as $shipping) {
                    if ($shipping['id_order_carrier'] == Tools::getValue('id_item')) {
                        $item->item_type = RectifiedInvoiceItem::TYPE_ORDER_SHIPPING;
                        $item->id_order = $order->id;
                        $item->quantity = 1;
                        $item->id_entity = $shipping['id_order_carrier'];
                        $item->amount = (float)(-1 * (float)Tools::convertPrice($shipping['shipping_cost_tax_excl'], $order->id_currency, $order->id_currency));
                        $item->id_rectified_invoice = $rectified_invoice->id;
                        $item->tax_amount = Tools::convertPrice((-1 * ($shipping['shipping_cost_tax_incl'] - $shipping['shipping_cost_tax_excl'])), $order->id_currency, $order->id_currency);
                        $item->text = $this->l('Shipping order: ').' '.$order->reference.' '.$shipping['carrier_name'].' ';
                        $item->num_invoice = Configuration::get('PS_INVOICE_PREFIX', Context::getContext()->language->id, null, $order->id_shop).sprintf('%06d', $order->invoice_number);
                        $item->tax_rate = (float)$order->carrier_tax_rate;
                        $item->total_per_line = (float)Tools::convertPrice(-1 * (float)$shipping['shipping_cost_tax_incl'], (int)$order->id_currency, (int)$order->id_currency);
                        $item->price_per_unit = (float)Tools::convertPrice(-1 * (float)$shipping['shipping_cost_tax_excl'], (int)$order->id_currency, (int)$order->id_currency);
                        $item->discount = (float)0;
                        if ($item->save() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                            $out['status'] = 'ok';
                            $out['confirmation'] = $this->l('Has successfully added the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                            $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                        } else {
                            $out['status'] = 'nok';
                            $out['error'] = $this->l('An error ocurred while try add the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                            $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                        }
                    }
                }
            }
        } elseif (Tools::getValue('item_type') == RectifiedInvoiceItem::TYPE_ORDER_WRAPPING) {
            $item = new RectifiedInvoiceItem(self::getItemId(Tools::getValue('id_item')), RectifiedInvoiceItem::TYPE_ORDER_WRAPPING);
            $order = new Order(Tools::getValue('id_order'));
            if ($id_currency == 0) {
                $id_currency = $order->id_currency;
            }
            $item->item_type = RectifiedInvoiceItem::TYPE_ORDER_WRAPPING;
            $item->id_order = $order->id;
            $item->quantity = 1;
            $item->id_entity = $order->id;
            $item->amount = (float)(-1 * (float)Tools::convertPrice($order->total_wrapping_tax_excl, $order->id_currency, $order->id_currency));
            $item->id_rectified_invoice = $rectified_invoice->id;
            $item->tax_amount = Tools::convertPrice((-1 * ($order->total_wrapping_tax_incl - $order->total_wrapping_tax_excl)), $order->id_currency, $order->id_currency);
            $item->text = $this->l('Wrapping order : ').' '.$order->reference;
            $item->num_invoice = Configuration::get('PS_INVOICE_PREFIX', Context::getContext()->language->id, null, $order->id_shop).sprintf('%06d', $order->invoice_number);
            $item->tax_rate = (float)$order->carrier_tax_rate;
            $item->total_per_line = (float)Tools::convertPrice(-1 * (float)$order->total_wrapping_tax_incl, (int)$order->id_currency, (int)$order->id_currency);
            $item->price_per_unit = (float)Tools::convertPrice(-1 * (float)$order->total_wrapping_tax_excl, (int)$order->id_currency, (int)$order->id_currency);
            $item->discount = (float)0;

            if ($item->save() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                $out['status'] = 'ok';
                $out['confirmation'] = $this->l('Has successfully added the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
            } else {
                $out['status'] = 'nok';
                $out['error'] = $this->l('An error ocurred while try add the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
            }
        } elseif (Tools::getValue('item_type') == RectifiedInvoiceItem::TYPE_ORDER_CART_RULE) {
            $item = new RectifiedInvoiceItem(self::getItemId(Tools::getValue('id_item')), RectifiedInvoiceItem::TYPE_ORDER_CART_RULE);
            $order = new Order(Tools::getValue('id_order'));
            if ($id_currency == 0) {
                $id_currency = $order->id_currency;
            }
            $cart_rules = $order->getCartRules();
            if ($cart_rules) {
                foreach ($cart_rules as $cart_rule) {
                    if ($cart_rule['id_order_cart_rule'] == Tools::getValue('id_item')) {
                        $item->text = $cart_rule['name'];
                        $item->amount = Tools::convertPrice($cart_rule['value_tax_excl'], $order->id_currency, $id_currency);
                        $item->id_entity = $cart_rule['id_order_cart_rule'];
                        $item->quantity = (int)Tools::getValue('quantity');
                        $item->id_order = Tools::getValue('id_order');
                        $item->item_type = RectifiedInvoiceItem::TYPE_ORDER_CART_RULE;
                        $item->id_rectified_invoice = $rectified_invoice->id;
                        $item->num_invoice = Configuration::get('PS_INVOICE_PREFIX', Context::getContext()->language->id, null, $order->id_shop).sprintf('%06d', $order->invoice_number);
                        $item->tax_amount = Tools::convertPrice(($cart_rule['value'] - $cart_rule['value_tax_excl']), $order->id_currency, $id_currency);
                        $item->tax_rate = (float)0;
                        $item->discount = (float)0;
                        $item->total_per_line = (float)Tools::convertPrice((float)$cart_rule['value'], (int)$order->id_currency, (int)$id_currency);
                        $item->price_per_unit = (float)Tools::convertPrice((float)$cart_rule['value'], (int)$order->id_currency, (int)$id_currency);
                        if ($item->save() && RectifiedInvoice::updateTotals($rectified_invoice->id)) {
                            $out['status'] = 'ok';
                            $out['confirmation'] = $this->l('Has successfully added the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                            $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                        } else {
                            $out['status'] = 'nok';
                            $out['error'] = $this->l('An error ocurred while try add the item:').' '.$item->text.' '.$this->l('quantity:').' 1';
                            $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
                        }
                    }
                    $out['items'][] = array(
                    'id_item' => $cart_rule['id_order_cart_rule'],
                    'item_type' => RectifiedInvoiceItem::TYPE_ORDER_CART_RULE,
                    'id_order' => $order->id,
                    'quantity' => self::getQtyCartRuleToAddItem($cart_rule['id_order_cart_rule'], 1),
                    'quantity_rate' => 1,
                    'unit_price' => Tools::displayPrice(-1 * $cart_rule['value_tax_excl']),
                    'unit_price_wt' => Tools::displayPrice(-1 * $cart_rule['value']),
                    'item' => $cart_rule['name'],
                    'price' => Tools::displayPrice(-1 * $cart_rule['value']),
                    'tax_rate' => 0,
                    'tax' => Tools::displayPrice(-1 * ($cart_rule['value'] - $cart_rule['value_tax_excl'])),
                    );
                }
            }
        } else {
            $out['status'] = 'ok';
            $out['summary']['id_rectified_invoice'] = $rectified_invoice->id;
        }
        $id_rectified_invoice = Tools::getValue('id_rectified_invoice');
        if (!$id_rectified_invoice) {
            $id_rectified_invoice = $rectified_invoice->id;
        }
        if ($id_rectified_invoice) {
            $rectified_invoice = new RectifiedInvoice($id_rectified_invoice, true);
            $rectified_invoice->id_currency = $id_currency;
            $rectified_invoice->save();
            if ($rectified_invoice->items) {
                foreach ($rectified_invoice->items as &$items) {
                    $items->total = Tools::displayPrice($items->tax_amount + $items->amount, (int)$rectified_invoice->id_currency);
                    $items->amount = Tools::displayPrice($items->amount, (int)$rectified_invoice->id_currency);
                    $items->price_per_unit = Tools::displayPrice($items->price_per_unit, (int)$rectified_invoice->id_currency);
                    $items->tax_rate = $items->tax_rate.'% ';
                    $items->total_per_line = Tools::displayPrice($items->total_per_line, (int)$rectified_invoice->id_currency);
                    $items->discount = Tools::displayPrice($items->discount, (int)$rectified_invoice->id_currency);
                    $items->tax_amount = Tools::displayPrice($items->tax_amount, (int)$rectified_invoice->id_currency);
                }
            }
            $out['id_currency'] = $rectified_invoice->id_currency;
            $out['last_id_order'] = (Validate::isLoadedObject($order)) ? $order->id : 0;
            $out['summary'] = array(
                'items' => $rectified_invoice->items,
                'subtotal' => Tools::displayPrice($rectified_invoice->total, (int)$rectified_invoice->id_currency),
                'num_invoice' => $rectified_invoice->num,
                'tax_amount' => Tools::displayPrice($rectified_invoice->total_tax, (int)$rectified_invoice->id_currency),
                'total' => Tools::displayPrice($rectified_invoice->total_wt, (int)$rectified_invoice->id_currency),
                'id_rectified_invoice' => $id_rectified_invoice,
                'status' => 'ok',
            );
        }
        echo Tools::jsonEncode($out);
        die();
    }

    public static function getItemId($id_entity, $item_type = RectifiedInvoiceItem::TYPE_ORDER_DETAIL)
    {
        $query = new DbQuery();
        $query->select('id_rectified_invoice_item');
        $query->from('rectified_invoices_items');
        $query->where('id_entity = '.(int)$id_entity);
        $query->where('item_type = '.(int)$item_type);
        return (int)DB::getInstance()->getValue($query);
    }

    public function beforeUpdateOptions()
    {
        $params = array_keys($this->fields_options['configuration']['fields']);
        foreach ($params as $k) {
            Configuration::updateValue($k, Tools::getValue($k));
        }
    }

    protected function processBulkDelete()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->boxes = Tools::getValue('rectified_invoicesBox', array());
        } else {
            $this->boxes = Tools::getValue('id_rectified_invoiceBox', array());
        }

        parent::processBulkDelete();
    }

    protected function processBulkValidate()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->boxes = Tools::getValue('rectified_invoicesBox', array());
        } else {
            $this->boxes = Tools::getValue('id_rectified_invoiceBox', array());
        }

        if (is_array($this->boxes) && !empty($this->boxes)) {
            $object = new $this->className();
            if (isset($object->noZeroObject)) {
                $objects_count = count(call_user_func(array(
                            $this->className,
                            $object->noZeroObject
                        )));
                // Check if all object will be deleted
                if ($objects_count <= 1 || count($this->boxes) == $objects_count) {
                    $this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
                }
            } else {
                $result = true;
                $validated_ok = true;
                foreach ($this->boxes as $id) {
                    $to_validate = new $this->className($id);
                    if (!$to_validate->validated) {
                        $to_validate->validated = 1;
                        $to_validate->date_validated = date('Y-m-d h:i:s', strtotime('now'));
                        $customer = new Customer($to_validate->id_customer);
                        $num = Configuration::get('RI_LAST_NUM_VALID', false, $customer->id_shop_group, $customer->id_shop);
                        $to_validate->num = Configuration::get('RI_REF_VALID', false, $customer->id_shop_group, (int)$customer->id_shop).''.$num;
                        if ($to_validate->update()) {
                            Configuration::updateValue('RI_LAST_NUM_VALID', (int)$num + 1, false, $customer->id_shop_group, $customer->id_shop);
                        } else {
                            $result = false;
                        }
                    } else {
                        $result = true;
                    }

                    if ($validated_ok && class_exists('PrestaShopLogger')) {
                        PrestaShopLogger::addLog(sprintf($this->l('%s validated', 'AdminTab', false, false), $this->className), 1, null, $this->className, (int)$to_validate->id, true, (int)$this->context->employee->id);
                    } else {
                        $this->errors[] = sprintf(Tools::displayError('Can\'t validated #%d'), $id);
                    }
                }
                if ($result) {
                    $this->redirect_after = self::$currentIndex.'&conf=2&token='.$this->token;
                }
                $this->errors[] = Tools::displayError('An error occurred while validated this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to validate.');
        }
        if (isset($result)) {
            return $result;
        } else {
            return false;
        }
    }

    protected function processBulkSendemail()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->boxes = Tools::getValue('rectified_invoicesBox', array());
        } else {
            $this->boxes = Tools::getValue('id_rectified_invoiceBox', array());
        }

        if (is_array($this->boxes) && !empty($this->boxes)) {
            $object = new $this->className();
            if (isset($object->noZeroObject)) {
                $objects_count = count(call_user_func(array(
                            $this->className,
                            $object->noZeroObject
                        )));
                // Check if all object will be deleted
                if ($objects_count <= 1 || count($this->boxes) == $objects_count) {
                    $this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
                }
            } else {
                foreach ($this->boxes as $id) {
                    $rectified_invoice = new RectifiedInvoice($id);
                    $customer = new Customer($rectified_invoice->id_customer);
                    if ($this->sendNewInvoiceEmail($id)) {
                        $this->confirmations[] = sprintf($this->l('Send successfully email to %s'), $customer->email);
                    } else {
                        $this->errors[] = sprintf($this->l('An error ocurred while try send email to %s'), $customer->email);
                    }
                }
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to validate.');
        }
    }

    public function sendNewInvoiceEmail($id_invoice)
    {
        $invoice = new RectifiedInvoice($id_invoice, true);
        $items_table = '';
        foreach ($invoice->items as $key => $item) {
            $items_table .= '<tr style="background-color:'.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
            <td style="padding:0.6em 0.4em;">'.$item->num_invoice.'</td>
            <td style="padding:0.6em 0.4em;">
            <strong>'.$item->text.'</strong>
            </td>
            <td style="padding:0.6em 0.4em; text-align:right;">'.$item->tax_rate.'</td>
            <td style="padding:0.6em 0.4em; text-align:center;">'.Tools::displayPrice($item->tax_amount).'</td>
            <td style="padding:0.6em 0.4em; text-align:right;">'.Tools::displayPrice($item->tax_amount + $item->amount).'</td></tr>';
        }
        $customer = new Customer($invoice->id_customer);
        $id_shop = $customer->id_shop;
        $template_vars = array(
            '{invoice_ref}' => $invoice->num,
            '{date_validated}' => $invoice->date_validated,
            '{shop_name}' => Configuration::get('PS_SHOP_NAME'),
            '{items}' => $items_table,
            '{total_tax}' => $invoice->total_tax,
            '{total}' => $invoice->total_wt,
            '{subtotal}' => $invoice->total,
            '{shop_logo}' => $invoice->total,
        );
        if (Validate::isLoadedObject($invoice)) {
            if (isset($invoice->items[0])) {
                $item = $invoice->items[0];
            } else {
                $item = null;
            }

            if (Validate::isLoadedObject($item)) {
                $order = new Order($item->id_order);
            } else {
                $order = null;
            }

            if (Validate::isLoadedObject($order)) {
                $id_lang = $order->id_lang;
            } else {
                $id_lang = $this->context->language->id;
            }

            // Default language
            $mail_iso = Language::getIsoById($id_lang);
            $dir_mail = false;
            if (file_exists(_PS_MODULE_DIR_.$this->module->name.'/mails/'.$mail_iso.'/new_rectified_invoice.txt') && file_exists(_PS_MODULE_DIR_.'parectifiedinvoices/mails/'.$mail_iso.'/new_rectified_invoice.html')) {
                $dir_mail = _PS_MODULE_DIR_.$this->module->name.'/mails/';
            }
            if (file_exists(_PS_MAIL_DIR_.$mail_iso.'/new_rectified_invoice.txt') && file_exists(_PS_MAIL_DIR_.$mail_iso.'/new_rectified_invoice.html')) {
                $dir_mail = _PS_MAIL_DIR_;
            }
            if (!$dir_mail) {
                $this->recurseCopy(_PS_MODULE_DIR_.$this->module->name.'/mails/es/', _PS_MODULE_DIR_.$this->module->name.'/mails/'.$mail_iso.'/');
                $dir_mail = _PS_MODULE_DIR_.$this->module->name.'/mails/';
            }


            $file_attachement = array();
            $file_attachement['content'] = $this->generateInvoicePDFByIdOrder($invoice->id, false);
            $file_attachement['name'] = $invoice->num;
            $file_attachement['mime'] = 'application/pdf';
            $attachments = array();
            $attachments[] = $file_attachement;
            if ($dir_mail) {
                $title = sprintf(Mail::l('New invoice : %s', $id_lang), $invoice->num);
                $shop_name = Configuration::get('PS_SHOP_NAME', null, $customer->id_shop_group, $customer->id_shop);
                $shop_email = Configuration::get('PS_SHOP_EMAIL', null, $customer->id_shop_group, $customer->id_shop);
                try {
                    if (Mail::Send($id_lang, 'new_rectified_invoice', $title, $template_vars, $customer->email, null, $shop_email, $shop_name, $attachments, null, $dir_mail, false, $id_shop)) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception $e) {
                    $this->errors[] = $e->getMessage();
                    return false;
                }
            }
        }
        return false;
    }

    protected function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src.'/'.$file)) {
                    $this->recurseCopy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    Tools::copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }

        closedir($dir);
    }

    protected function processBulkPrintpdf()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->boxes = Tools::getValue('rectified_invoicesBox', array());
        } else {
            $this->boxes = Tools::getValue('id_rectified_invoiceBox', array());
        }

        if (is_array($this->boxes) && !empty($this->boxes)) {
            $object = new $this->className();
            if (isset($object->noZeroObject)) {
                $objects_count = count(call_user_func(array(
                            $this->className,
                            $object->noZeroObject
                        )));
                // Check if all object will be deleted
                if ($objects_count <= 1 || count($this->boxes) == $objects_count) {
                    $this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
                }
            } else {
                $order_invoices = new PrestaShopCollection('RectifiedInvoice');
                $order_invoices->where('id_rectified_invoice', 'IN', $this->boxes);
                $result = $this->generatePDF($order_invoices, 'RectifiedInvoice');
                $this->errors[] = Tools::displayError('An error occurred while validated this selection.');
            }
        } else {
            $this->errors[] = Tools::displayError('You must select at least one element to validate.');
        }

        if (isset($result)) {
            return $result;
        } else {
            return false;
        }
    }

    public function generatePDF($object, $template, $no_return = true)
    {
        $pdf = new PDF($object, $template, Context::getContext()->smarty);
        return $pdf->render($no_return);
    }

    public function initProcess()
    {
        $this->action = Tools::getValue('action');
        parent::initProcess();
    }
}
