<?php
/**
 *  Rectified Invoices for Prestashop 1.6.x
 *
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once('RectifiedInvoice.php');

class RectifiedInvoice extends ObjectModel
{
    public static $definition = array(
        'table' => 'rectified_invoices',
        'primary' => 'id_rectified_invoice',
        'multilang' => false,
        'fields' => array(
            'date_add' => array('type' => self::TYPE_DATE),
            'num' => array('type' => self::TYPE_STRING),
            'reason' => array('type' => self::TYPE_STRING),
            'total_wt' => array('type' => self::TYPE_FLOAT),
            'total' => array('type' => self::TYPE_FLOAT),
            'total_tax' => array('type' => self::TYPE_FLOAT),
            'id_customer' => array('type' => self::TYPE_INT,),
            'id_currency' => array('type' => self::TYPE_INT,),
            'id_address' => array('type' => self::TYPE_INT,),
            'validated' => array('type' => self::TYPE_INT,),
            'date_validated' => array('type' => self::TYPE_DATE,),
        ),
    );
    /** @var string Name */
    public $date_add;
    public $num;
    public $reason;
    public $total_wt;
    public $total;
    public $total_tax;
    public $id_customer;
    public $id_address;
    public $validated;
    public $date_validated;
    public $id_currency;
    public $items;

    /**
     * @see ObjectModel::$definition
     */
    public function __construct($id = null, $full = false, $id_shop = null)
    {
        parent::__construct($id, null, $id_shop);
        $this->identifier = 'id_rectified_invoices';
        $this->table = 'rectified_invoices';
        if ((int)$id > 0 && $full) {
            $this->items = RectifiedInvoiceItem::getItemsByInvoiceId($id);
        }
    }

    public static function getInvoicesByCustomer($id_customer, $id_currency = null)
    {
        $query = new DbQuery();
        $query->from('order_invoice', 'i');
        $query->innerJoin('orders', 'o', 'o.id_order = i.id_order');
        $query->where('o.id_customer = '.(int)$id_customer);
        if ($id_currency) {
            $query->where('o.id_currency = '.(int)$id_currency);
        }

        return DB::getInstance()->executeS($query);
    }

    public static function updateTotals($id)
    {
        $object = new RectifiedInvoice($id, true);
        $total = 0;
        $total_tax = 0;
        if ($object->items) {
            foreach ($object->items as $item) {
                $total += (float)$item->amount;
                $total_tax += (float)$item->tax_amount;
            }
        }
        $object->total = $total;
        $object->total_tax = $total_tax;
        $object->total_wt = $total + $total_tax;
        return $object->update();
    }

    public static function changeCurrency($id, $id_currency)
    {
        $currency = new Currency($id_currency);
        if (!Validate::isLoadedObject($currency)) {
            return false;
        }
        $object = new RectifiedInvoice($id, true);
        $res = true;
        if ($object->items) {
            foreach ($object->items as $item) {
                $item->amount = Tools::convertPrice($item->amount, $object->id_currency, $id_currency);
                $item->tax_amount = Tools::convertPrice($item->tax_amount, $object->id_currency, $id_currency);
                $res &= $item->update();
            }
        }
        $object->total = Tools::convertPrice($object->total, $object->id_currency, $id_currency);
        $object->total_wt = Tools::convertPrice($object->total_wt, $object->id_currency, $id_currency);
        $object->total_tax = Tools::convertPrice($object->total_tax, $object->id_currency, $id_currency);
        $object->id_currency = $id_currency;
        return $res && $object->update();
    }

    public static function getLastReference()
    {
        $query = new DbQuery();
        $query->select('num');
        $query->from('rectified_invoices');
        $query->orderBy('num DESC');
        return (int)DB::getInstance()->getValue($query);
    }

    public function delete()
    {
        if ($this->validated) {
            return false;
        } else {
            $object = new RectifiedInvoice($this->id, true);
            $res = true;
            foreach ($object->items as $item) {
                if (Validate::isLoadedObject($item)) {
                    $res &= $item->delete();
                }
            }

            if ($res) {
                return parent::delete();
            } else {
                return $res;
            }
        }
    }
}
