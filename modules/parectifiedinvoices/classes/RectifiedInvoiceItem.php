<?php
/**
 *  Rectified Invoices for Prestashop 1.6.x
 *
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class RectifiedInvoiceItem extends ObjectModel
{
    /** @var string Name */
    public $text;
    public $tax_rate;
    public $amount;
    public $tax_amount;
    public $price_per_unit;
    public $total_per_line;
    public $quantity;
    public $discount;
    public $id_rectified_invoice;
    public $num_invoice;
    public $item_type;
    public $id_entity;
    public $id_order;
    const TYPE_ORDER_DETAIL = 1;
    const TYPE_ORDER_SHIPPING = 2;
    const TYPE_ORDER_WRAPPING = 3;
    const TYPE_FREE_ITEM = 4;
    const TYPE_ORDER_ECOTAX = 5;
    const TYPE_ORDER_CART_RULE = 6;
    public static $definition = array(
        'table' => 'rectified_invoices_items',
        'primary' => 'id_rectified_invoice_item',
        'multilang' => false,
        'fields' => array(
            'text' => array('type' => self::TYPE_STRING),
            'tax_rate' => array('type' => self::TYPE_FLOAT),
            'discount' => array('type' => self::TYPE_FLOAT),
            'amount' => array('type' => self::TYPE_FLOAT),
            'price_per_unit' => array('type' => self::TYPE_FLOAT),
            'total_per_line' => array('type' => self::TYPE_FLOAT),
            'tax_amount' => array('type' => self::TYPE_FLOAT),
            'quantity' => array('type' => self::TYPE_INT),
            'id_rectified_invoice' => array('type' => self::TYPE_INT),
            'num_invoice' => array('type' => self::TYPE_STRING),
            'item_type' => array('type' => self::TYPE_INT),
            'id_entity' => array('type' => self::TYPE_INT),
            'id_order' => array('type' => self::TYPE_INT),
        ),
    );

    public static function getItemsByInvoiceId($id_rectified_invoice)
    {
        $query = new DbQueryCore();
        $query->from(self::$definition['table'], 'ri');
        $query->leftJoin('order_invoice', 'oi', 'oi.id_order = ri.id_order');
        $query->where('ri.id_rectified_invoice = '.(int)$id_rectified_invoice);
        return ObjectModel::hydrateCollection(__CLASS__, DB::getInstance()->executeS($query));
    }

    public function delete()
    {
        $rectified_invoice = new RectifiedInvoice($this->id_rectified_invoice);
        if ($rectified_invoice->validated) {
            return false;
        } else {
            return parent::delete();
        }
    }
}
