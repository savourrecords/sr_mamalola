<?php
/**
 *  Rectified Invoices for Prestashop 1.6.x
 *
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once('RectifiedInvoice.php');
include_once('RectifiedInvoiceItem.php');

class HTMLTemplateRectifiedInvoice extends HTMLTemplate
{
    public $customer;
    public $invoice;
    public $available_in_your_account = false;

    public function __construct(RectifiedInvoice $invoice, $smarty)
    {
        $this->invoice = new RectifiedInvoice($invoice->id, true);

        if (isset($this->invoice->items[0])) {
            $item = $this->invoice->items[0];
        } else {
            $item = null;
        }

        if (Validate::isLoadedObject($item)) {
            $order = new Order($item->id_order);
        } else {
            $order = null;
        }

        if (Validate::isLoadedObject($order)) {
            $id_lang = $order->id_lang;
        } else {
            $id_lang = Context::getContext()->language->id;
        }

        $this->customer = new Customer((int)$this->invoice->id_customer);
        $this->smarty = $smarty;
        if ($this->invoice->validated) {
            $this->date = Tools::displayDate($invoice->date_validated, $id_lang);
        } else {
            $this->date = Tools::displayDate($invoice->date_add, $id_lang);
        }

        $module = Module::getInstanceByName('parectifiedinvoices');
        $this->title = sprintf($module->l('Nº %1$s'), $this->invoice->num);
        // footer informations
        $this->shop = new Shop((int)$this->customer->id_shop);
    }

    /**
     * Returns the template's HTML content
     * @return string HTML content
     */
    public function getContent()
    {
        $invoice_address = new Address((int)$this->invoice->id_address);
        $country = new Country((int)$invoice_address->id_country);
        $formatted_invoice_address = AddressFormat::generateAddress($invoice_address, array(), '<br />', ' ');
        $this->invoice->total_wt = ((float)$this->invoide->total + (float)$this->invoide->total_tax);
        $data = array(
            'rectified_invoice' => $this->invoice,
            'invoice_address' => $formatted_invoice_address,
            'id_currency' => $this->invoice->id_currency,
            'customer' => $this->customer,
            'ref_inv' => ''
        );
        if (Tools::getValue('debug')) {
            die(Tools::jsonEncode($data));
        }
        $this->smarty->assign($data);
        return $this->smarty->fetch($this->getTemplateByCountry($country->iso_code));
    }

    /**
     * Returns the invoice template associated to the country iso_code
     * @param string $iso_country
     */
    protected function getTemplateByCountry($iso_country)
    {
        $file = 'invoice';
        // try to fetch the iso template
        $template = $this->getTemplate($file.'.'.$iso_country);
        // else use the default one
        if (!$template) {
            $template = $this->getTemplate($file);
        }
        return $template;
    }

    protected function getTemplate($template_name)
    {
        $template = false;
        $default_template = _PS_PDF_DIR_.'/'.$template_name.'.tpl';
        $overriden_template = _PS_MODULE_DIR_.'parectifiedinvoices/views/templates/admin/pdf/'.$template_name.'.tpl';
        if (file_exists($overriden_template)) {
            $template = $overriden_template;
        } elseif (file_exists($default_template)) {
            $template = $default_template;
        }
        return $template;
    }

    /**
     * Returns the template filename when using bulk rendering
     * @return string filename
     */
    public function getBulkFilename()
    {
        return 'seller-invoices.pdf';
    }

    /**
     * Returns the template filename
     * @return string filename
     */
    public function getFilename()
    {
        return $this->invoice->num.'.pdf';
    }
}
