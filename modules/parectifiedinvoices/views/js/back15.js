/**
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

$(document).ready(function () {
    $('._blank').click(function () {
        $(this).attr('target', '_blank');
        return true;
    });
    $(document).on('click', '.btn[data-toggle=modal]', function(){
        var selector = $(this).attr('data-target');
        $(selector).css({'display': 'block'});
    });
    $('.modal-dialog').on('click', '.btn, .close', function(){
        $(this).closest('.modal').css({'display': 'none'});
    });

});