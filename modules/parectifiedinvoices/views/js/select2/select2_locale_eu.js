/**
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () {
          return "Ez da bat datorrenik aurkitu";
        },
        formatInputTooShort: function (input, min) {
          var n = min - input.length;
          if (n === 1) {
            return "Idatzi karaktere bat gehiago";
          } else {
            return "Idatzi " + n + " karaktere gehiago";
          }
        },
        formatInputTooLong: function (input, max) {
          var n = input.length - max;
          if (n === 1) {
            return "Idatzi karaktere bat gutxiago";
          } else {
            return "Idatzi " + n + " karaktere gutxiago";
          }
        },
        formatSelectionTooBig: function (limit) {
          if (limit === 1 ) {
            return "Elementu bakarra hauta dezakezu";
          } else {
            return limit + " elementu hauta ditzakezu soilik";
          }
        },
        formatLoadMore: function (pageNumber) {
          return "Emaitza gehiago kargatzen…";
        },
        formatSearching: function () {
          return "Bilatzen…";
        }
    });
})(jQuery);
