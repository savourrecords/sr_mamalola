/**
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () { return "Δεν βρέθηκαν αποτελέσματα"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Παρακαλούμε εισάγετε " + n + " περισσότερο" + (n > 1 ? "υς" : "") + " χαρακτήρ" + (n > 1 ? "ες" : "α"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Παρακαλούμε διαγράψτε " + n + " χαρακτήρ" + (n > 1 ? "ες" : "α"); },
        formatSelectionTooBig: function (limit) { return "Μπορείτε να επιλέξετε μόνο " + limit + " αντικείμεν" + (limit > 1 ? "α" : "ο"); },
        formatLoadMore: function (pageNumber) { return "Φόρτωση περισσότερων…"; },
        formatSearching: function () { return "Αναζήτηση…"; }
    });
})(jQuery);