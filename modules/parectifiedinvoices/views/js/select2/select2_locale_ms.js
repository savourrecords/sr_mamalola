/**
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () { return "Tiada padanan yang ditemui"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Sila masukkan " + n + " aksara lagi"; },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Sila hapuskan " + n + " aksara"; },
        formatSelectionTooBig: function (limit) { return "Anda hanya boleh memilih " + limit + " pilihan"; },
        formatLoadMore: function (pageNumber) { return "Sedang memuatkan keputusan…"; },
        formatSearching: function () { return "Mencari…"; }
    });
})(jQuery);
