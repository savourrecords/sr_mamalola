/**
 * @package   parectifiedinvoiced
 * @version   1.0
 * @author    http://www.pronimbo.com
 * @copyright Copyright (C)  Pronimbo <@email:info@pronimbo.com>
 *               <info@pronimbo.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */

$(document).ready(function () {
    $('._blank').click(function () {
        $(this).attr('target', '_blank');
        return true;

    });
    $('.nav li a').click(function () {
        $('.nav li').removeClass('active');
        $('.tab-pane').removeClass('active');
        $($(this).attr('href')).addClass('active');
        $(this).closest('li').addClass('active');
        return false;
    });
    $('.lang-btn').click(function () {
        $(this).closest('div').addClass('open')
    });
});
function hideOtherLanguage(id) {
    $('.translatable-field').hide();
    $('.lang-' + id).show();

    var id_old_language = id_language;
    id_language = id;

    if (id_old_language != id)
        changeEmployeeLanguage();

    updateCurrentText();
}
function changeEmployeeLanguage() {
    if (typeof allowEmployeeFormLang !== 'undefined' && allowEmployeeFormLang)
        $.post("index.php", {
            action: 'formLanguage',
            tab: 'AdminEmployees',
            ajax: 1,
            token: employee_token,
            form_language_id: id_language
        });
}
function updateCurrentText() {
    $('#current_product').html($('#name_' + id_language).val());
}