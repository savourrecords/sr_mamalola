{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.com.
*  @copyright Pronimbo.com. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}

<a class="viewpdf" title="{l s='View PDF' mod='parectifiedinvoices'}" href="{$link->getAdminLink('AdminRectifiedInvoices', true)|escape:'html':'UTF-8'}&id_rectified_invoice={$id|intval|escape:'html':'UTF-8'}&action=viewPDF">
    {if !$ps15}
        <i class="icon-check"></i>
        {l s='View PDF' mod='parectifiedinvoices'}
    {else}
        <img src="../img/admin/pdf.gif">
    {/if}
</a>
