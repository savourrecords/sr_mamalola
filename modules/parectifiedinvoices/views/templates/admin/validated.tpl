{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.com.
*  @copyright Pronimbo.com. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}
{if !$ps15}
    {if $validated}
        <a class="list-action-enable action-enabled" title="{l s='Enabled' mod='parectifiedinvoices'}">
            <i class="icon-check"></i>
        </a>
    {else}
        <a class="list-action-enable action-disabled" title="{l s='Disabled' mod='parectifiedinvoices'}">
            <i class="icon-remove"></i>
        </a>
    {/if}
{else}
    {if $validated}
        <img alt="Activado" src="../img/admin/enabled.gif">
    {else}
        <img alt="Activado" src="../img/admin/disabled.gif">
    {/if}

{/if}
