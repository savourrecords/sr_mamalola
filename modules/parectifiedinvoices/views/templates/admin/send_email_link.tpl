{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.com.
*  @copyright Pronimbo.com. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}
<a class="viewpdf" title="{l s='Send by email' mod='parectifiedinvoices'}" onclick="{literal}if (confirm('{/literal}{l s='Are you sure to send by email this invoice ?' mod='parectifiedinvoices'}{literal}')){return true;}else{event.stopPropagation(); event.preventDefault();};{/literal}" href="{$link->getAdminLink('AdminRectifiedInvoices', true)|escape:'html':'UTF-8'}&id_rectified_invoice={$id|intval|escape:'html':'UTF-8'}&action=SendEmail">
    {if !$ps15}
        <i class="icon-envelope-alt"></i>
        {l s='Send by email' mod='parectifiedinvoices'}
    {else}
        <img src="../img/t/AdminEmails.gif">
    {/if}
</a>