{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.com.
*  @copyright Pronimbo.com. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}
<a class="viewpdf" title="{l s='Validate' mod='parectifiedinvoices'}" onclick="{literal}if (confirm('{/literal}{l s='Are you sure to validate this invoice ?' mod='parectifiedinvoices'}{literal}')){return true;}else{event.stopPropagation(); event.preventDefault();};{/literal}" href="{$link->getAdminLink('AdminRectifiedInvoices', true)|escape:'html':'UTF-8'}&id_rectified_invoice={$id|intval|escape:'html':'UTF-8'}&action=validateInvoice">
    {if !$ps15}
        <i class="icon-file-text"></i>
        {l s='Validate' mod='parectifiedinvoices'}
    {else}
        <img src="../img/admin/enabled.gif" alt="Activado">
    {/if}
</a>