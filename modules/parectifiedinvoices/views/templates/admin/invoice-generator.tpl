{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.
*  @copyright Pronimbo. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}

<style>
    .loading:before {ldelim}

        background: url('{$gif|escape:'html':'UTF-8'}') no-repeat center center #fff;
        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0.6;
        content: "";
        z-index: 495;
    {rdelim}

    .loading {ldelim}
        position: relative;
        z-index: 494;
    {rdelim}
</style>
<script type="text/javascript">
var id_customer = {$rectified_invoice->id_customer|intval};
var id_rectified_invoice = {$rectified_invoice->id|intval};
var validated = {$rectified_invoice->validated|intval};
var changed_shipping_price = false;
var current_index = '{$current|escape:'html':'UTF-8'}&token={$token|escape:'html':'UTF-8'}';
var currencies = new Array();
var id_currency = new Number(1);
var id_lang = new Number(1);
var currency_format = 5;
var currency_sign = '';
var currency_blank = false;
var priceDisplayPrecision = {$smarty.const._PS_PRICE_DISPLAY_PRECISION_|intval};

$(document).ready(function () {ldelim}
    $('#content').addClass('bootstrap');
    if (id_rectified_invoice < 1)
    {
        $('#page-header-desc-rectified_invoices-viewPDF, #desc-rectified_invoices-viewPDF').closest('li').hide();
        $('#page-header-desc-rectified_invoices-validate, #desc-rectified_invoices-validate ').closest('li').hide();
        $('#page-header-desc-rectified_invoices-delete, #desc-rectified_invoices-delete').closest('li').hide();
    }
    if (!validated)
    {
        $('#invoices,#id_address_invoice,#id_invoice_free_item').select2();
    }
    initRectifiedInvoice();
    $('#customer').typeWatch(
            {ldelim}
        captureLength: 1,
        highlight: true,
        wait: 100,
        callback: function () {ldelim}
            searchCustomer();
            {rdelim}
        {rdelim});
    $("#id_address_invoice").change(function () {ldelim}
        updateAddresses();
        {rdelim});
    $.ajaxSetup({ldelim}type: "post"{rdelim});
    resetBind();
    $('#invoice_reason').focusout(function (e) {ldelim}
        e.preventDefault();
        updateInvoiceReason();
        {rdelim});
    $('#customer').focus();
    $('.select-all').click(function () {ldelim}
        if ($(this).is(':checked'))
            $('#invoice_list tbody').find('input[type=checkbox]').attr('checked', 'checked');
        else
            $('#invoice_list tbody').find('input[type=checkbox]').removeAttr('checked');

        {rdelim});
    $('#customer_part').on('click', 'button.setup-customer', function (e) {ldelim}
        e.preventDefault();
        chooseCustomer($(this).attr('data-customer'));
        $(this).removeClass('setup-customer').addClass('change-customer').html('<i class="icon-refresh"></i>&nbsp;{l s='Change' mod='parectifiedinvoices'}').blur();
        $(this).closest('.customerCard').addClass('selected-customer');
        $('.selected-customer .panel-heading').prepend('<i class="icon-ok text-success"></i>');
        $('.customerCard').not('.selected-customer').remove();
        $('#search-customer-form-group').hide();
        $('html, body').animate({
            scrollTop: $("#address_part").offset().top - ($('.navbar-header').height() + $('.page-head').height() + 20)
        }, 1000);

        {rdelim});
    $('.recalc_total').keyup(function () {
        price = parseFloat($('input[name=price_per_unit].recalc_total').val());
        if (isNaN(price)) {
            price = 0;
            $('input[name=price_per_unit].recalc_total').val(price);

        }
        qty = parseInt($('input[name=quantity].recalc_total').val());
        if (isNaN(qty)) {
            qty = 1;
            $('input[name=quantity].recalc_total').val(qty);
        }
        discount = parseFloat($('input[name=discount].recalc_total').val());
        if (isNaN(discount)) {
            discount = 0;
            $('input[name=discount].recalc_total').val(discount);
        }

        tax_rate = parseFloat($('input[name=tax_rate].recalc_total').val());
        if (isNaN(tax_rate)) {
            tax_rate = 0;
            $('input[name=tax_rate].recalc_total').val(tax_rate);
        }

        total = (price * qty ) - discount;
        total_tax = total * ((tax_rate / 100));
        total_line = total + total_tax;
        $('input[name=amount].recalc_total').val(ps_round(total));
        $('input[name=tax_amount].recalc_total').val(ps_round(total_tax));
        $('input[name=total_line].recalc_total').val(ps_round(total_line));


    });
    $('#customer_part').on('click', 'button.change-customer', function (e) {ldelim}
        e.preventDefault();
        $('#search-customer-form-group').show();
        $('.customerCard').remove();
        $('#address_part, #address_part, #invoices_container, #free_items_part, #summary_part').slideUp();
        $(this).blur();
        {rdelim});

    {rdelim});
{literal}
function changeURLRequest(key, value) {
    url = '';
    uri = window.location.href.split('?');
    vars = uri[1].split('&');
    found = false;
    $.each(vars, function (item, val) {
        _var = val.split('=');
        if (_var[0] == key) {
            _var[1] = value;
            found = true;
        }
        vars[item] = _var.join('=');
    });
    if (!found) {
        vars[vars.length] = key + '=' + value;
    }
    uri[1] = vars.join('&');
    url = uri.join('?');
    window.history.replaceState({}, '', url);
    return false;
}
function changeURLGetVar(search, replace) {
    url = '';
    uri = window.location.href.split('?');
    vars = uri[1].split('&');
    found = false;
    $.each(vars, function (item, val) {
        _var = val.split('=');
        if (_var[0] == search) {
            _var[0] = replace;
            found = true;
        }
        else if (_var[0] == replace) {
            found = true;
        }
        vars[item] = _var.join('=');
    });
    if (!found) {
        vars[vars.length] = replace;
    }
    uri[1] = vars.join('&');
    url = uri.join('?');
    window.history.replaceState({}, '', url);
    return false;
}
function toggleDate() {
    if ($('.datepicker').hasClass('hide')) {
        $('.datepicker').removeClass('hide');
        $('.datepicker').datetimepicker();
        $('#toggle_datepicker').html('<i class="icon-pencil"></i>&nbsp;{/literal}{l s='Update' mod='parectifiedinvoices'}{literal}');
    }
    else {
        $('#invoice_details').addClass('loading');
        $('#toggle_datepicker').html('<i class="icon-pencil"></i>&nbsp;{/literal}{l s='Change' mod='parectifiedinvoices'}{literal}');
        updateValidatedDate();

    }

}
function updateValidatedDate() {
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: false,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "updateValidatedDate",
            id_rectified_invoice: id_rectified_invoice,
            date: $('.datepicker').val()
        },
        success: function (res) {
            if (res.result == 'ok') {
                $('.datepicker').addClass('hide');
                $('#date_text').html(res.date_formated);
                $('#invoice_details').removeClass('loading');
            }

        }
    });
}
function initRectifiedInvoice() {

    if (id_rectified_invoice) {
        $('#search-customer-form-group').slideUp();
        $.ajax({
            type: "POST",
            url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
            async: false,
            dataType: "json",
            data: {
                ajax: "1",
                token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
                tab: "AdminRectifiedInvoices",
                action: "initRectifiedInvoice",
                id_rectified_invoice: id_rectified_invoice
            },
            success: function (res) {

                setupCustomer(res);
//                updateCurrenciesOptions(res);
                updateInvoicesOptions(res);
                viewInvoice();
                updateSumaryDetails(res);
                updateAddressesList(res);
                $('input[name=price_per_unit].recalc_total,input[name=discount].recalc_total,input[name=amount].recalc_total,input[name=tax_amount].recalc_total,input[name=total_line].recalc_total').parent().find('.input-group-addon').html(res.currency_sign);
                $('#customer').slideDown();
                $('#address_part').slideDown();
                $('#invoices_container').slideDown();
                $('#summary_part').slideDown();
                $('#free_items_part').slideDown();
                $('.selected-customer').removeClass('loading');
                resetBind();

            }
        });
    }
}
function chooseCustomer(id_cust) {
    id_customer = id_cust;
    $('.selected-customer').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: false,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "Getaddressandinvoices",
            id_customer: id_customer
        },
        success: function (res) {
            if (typeof(res.customer_found) != 'undefined')
                $('#search-customer-form-group').slideUp();
            $('#invoice_list,#summary_part,#address_invoice').addClass('loading');
//            updateCurrenciesOptions(res);
            updateInvoicesOptions(res);
            viewInvoice();
            $('input[name=price_per_unit].recalc_total,input[name=discount].recalc_total,input[name=amount].recalc_total,input[name=tax_amount].recalc_total,input[name=total_line].recalc_total').parent().find('.input-group-addon').html(res.currency_sign);
            updateAddressesList(res);
            $('#customer').slideDown();
            $('#address_part').slideDown();
            $('#invoices_container').slideDown();
            $('#summary_part').slideDown();
            $('#free_items_part').slideDown();
            resetBind();
            $('#summary_part,.selected-customer, #invoice_list').removeClass('loading');
        }
    });
}
function updateInvoicesOptions(res) {
    if (res.status == 'ok' && res.invoices) {
        var html_invoices = '';
        var last_id_order = 0;
        $.each(res.invoices, function () {
            if (typeof(this.id_order_invoice) != 'undefined' && typeof(this.number) != 'undefined')
                html_invoices += '<option value="' + this.id_order_invoice + '">' + this.number + ' - '  + this.invoice_date + '</option>';
            if (typeof(res.last_id_order) == 'undefined')
                last_id_order = this.id_order_invoice;

        });
        if (typeof(res.last_id_order) != 'undefined')
            last_id_order = res.last_id_order;

        $('#invoices').html(html_invoices);
        $('#invoices').select2();
        if (last_id_order > 0)
        {
            $('#invoices option[value='+last_id_order+']').attr('selected', 'selected');
            $('#invoices').select2('val',last_id_order);
        }
        var html_free_invoices = '<option value="0">-</option>';
        html_free_invoices += html_invoices;
        $('#id_invoice_free_item').html(html_free_invoices);
        $('#id_invoice_free_item').select2();
    }
}
/*
function updateCurrenciesOptions(res) {

    if (res.status == 'ok' && res.currencies) {
        var html_currencies = '';
        $.each(res.currencies, function () {
            if (typeof(this.id_currency) != 'undefined' && typeof(this.sign) != 'undefined')
                if (this.id_currency == res.id_currency)
                    html_currencies += '<option value="' + this.id_currency + '" selected="selected">' + this.name + ' (' + this.sign + ')</option>';
                else
                    html_currencies += '<option value="' + this.id_currency + '">' + this.name + ' (' + this.sign + ')</option>';
            id_currency = res.id_currency;
        });
        $('#currency').html(html_currencies);
    }
}
*/

function resetBind() {
    $('.fancybox').fancybox({
        'type': 'iframe',
        'width': '90%',
        'height': '90%',
    });

    $('.fancybox_customer').fancybox({
        'type': 'iframe',
        'width': '90%',
        'height': '90%',
        'afterClose': function () {
            searchCustomer();
        }
    });
}

function searchCustomer() {
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices', true)|escape:'quotes':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            tab: "AdminRectifiedInvoices",
            action: "searchCustomers",
            customer_search: $('#customer').val()
        },
        success: function (res) {

            var html = '';
            if (res.found) {

                $.each(res.customers, function () {
                    html += '<div class="customerCard col-lg-4">';
                    html += '<div class="panel">';
                    html += '<div class="panel-heading">' + this.firstname + ' ' + this.lastname;
                    html += '<span class="pull-right">#' + this.id_customer + '</span></div>';
                    html += '<span>' + this.email + '</span><br/>';
                    html += '<span class="text-muted">' + ((this.birthday != '0000-00-00') ? this.birthday : '') + '</span><br/>';
                    html += '<div class="panel-footer">';
                    html += '<a href="{/literal}{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}{literal}&id_customer=' + this.id_customer + '&viewcustomer&liteDisplaying=1" class="btn btn-default fancybox"><i class="icon-search"></i> {/literal}{l s='Details' mod='parectifiedinvoices'} {literal}</a>';
                    html += '<button type="button"  data-customer="' + this.id_customer + '" class="setup-customer btn btn-default pull-right"><i class="icon-arrow-right"></i> {/literal} {l s='Choose' mod='parectifiedinvoices'} {literal}</button>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                });
            }
            else
                html = '<div class="alert alert-warning"><i class="icon-warning-sign"></i>&nbsp; {/literal}{l s='No customers found' mod='parectifiedinvoices'} {literal}</div>';
            $('#customers').html(html);
            resetBind();
        }
    });
}


function setupCustomer(res) {
    if (res.customers_found || res.found) {
        $('.selected-customer').removeClass('loading');

        var html = '';
        $.each(res.customers, function () {
            html += '<div class="customerCard col-lg-4">';
            html += '<div class="panel">';
            html += '<div class="panel-heading">' + this.firstname + ' ' + this.lastname;
            html += '<span class="pull-right">#' + this.id + '</span></div>';
            html += '<span>' + this.email + '</span><br/>';
            html += '<span class="text-muted">' + ((this.birthday != '0000-00-00') ? this.birthday : '') + '</span><br/>';
            html += '<div class="panel-footer">';
            html += '<a href="{/literal}{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}{literal}&id_customer=' + this.id + '&viewcustomer&liteDisplaying=1" class="btn btn-default fancybox pull-right"><i class="icon-search"></i>  {/literal}{l s='Details' mod='parectifiedinvoices'}{literal}</a>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            id_customer = this.id;
        });
    }
    else
        html = '<div class="alert alert-warning"><i class="icon-warning-sign"></i>&nbsp;{/literal}{l s='No customers found' mod='parectifiedinvoices'}{literal}</div>';
    $('#customers').html(html);
    $('#search-customer-form-group').hide();

}

function updateAddressesList(res) {
    var addresses_invoice_options = '';
    var address_invoice_detail = '';
    var invoice_address_edit_link = '';
    id_address_invoice = res.id_address;

    if (typeof(res.addresses) != 'undefined')
        $.each(res.addresses, function (k) {
            if (this.id_address == res.id_address) {
                address_invoice_detail = this.formated_address;
                invoice_address_edit_link = "{/literal}{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}{literal}&id_address=" + this.id_address + "&updateaddress&realedit=1&liteDisplaying=1&submitFormAjax=1#";
            }
            addresses_invoice_options += '<option value="' + this.id_address + '" ' + (id_address_invoice && this.id_address == id_address_invoice || !id_address_invoice && k == 0 ? 'selected="selected"' : '') + '>' + this.alias + '</option>';
            if (id_address_invoice && this.id_address == id_address_invoice || !id_address_invoice && k == 0)
                id_address_invoice = this.id_address;
                    });
    $('#addresses_err').hide();
    $('#address_invoice').show();

    $('#id_address_invoice').html(addresses_invoice_options);
    if (typeof(res.id_address) != 'undefined')
        $('#id_address_invoice').select2('val', id_address_invoice);

    $('#address_invoice_detail').html(address_invoice_detail);
    $('#edit_invoice_address').attr('href', invoice_address_edit_link);
    $('#address_invoice').removeClass('loading');

}

function updateAddresses() {
    $('#address_invoice').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "getAddress",
            id_customer: id_customer,
            id_address_invoice: $('#id_address_invoice option:selected').val()
        },
        success: function (res) {
            updateAddressesList(res);
            getSummaryDetails();
        }
    });
}
function deleteItem(id_item) {
    $('#invoice_list,#summary_part').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "deleteItem",
            id_item: id_item
        },
        success: function (res) {
            if (res.status == 'ok' && res.confirmation.length != 0)
                showSuccessMessage(res.confirmation);
            if (res.status == 'nok' && res.error.length != 0)
                showErrorMessage(res.error);
            viewInvoice();
            updateSumaryDetails(res);
        }
    });
}
function updateInvoiceReason() {
    $('#invoice_reason').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "updateReason",
            reason: $('#invoice_reason').val(),
            id_rectified_invoice: id_rectified_invoice
        },
        success: function (res) {
            $('#invoice_reason').removeClass('loading');
            if (res.status == 'ok' && res.confirmation.length != 0)
                showSuccessMessage(res.confirmation);
            if (res.status == 'nok' && res.error.length != 0)
                showErrorMessage(res.error);
        }
    });
}
function updateSumaryDetails(res) {
    if (typeof(res.status) != 'undefined' && res.status == 'ok') {
        id_rectified_invoice = res.summary.id_rectified_invoice;

        table = '';
        if (res.summary.items)
            $.each(res.summary.items, function () {
                table += '<tr id="tr_item_' + this.id + '"><td class="fixed-width-sm  text-left">' + this.num_invoice + '</td>' +
                '<td class="text-left">' + this.text + '</td>' +
                '<td class="fixed-width-sm  text-left">' + this.price_per_unit + '</td>' +
                '<td class="fixed-width-sm  text-left">' + this.quantity + '</td>' +
                '<td class="fixed-width-sm  text-left">' + this.discount + '</td>' +
                '<td class="fixed-width-sm  text-left">' + this.tax_rate + '</td>' +
                '<td class="fixed-width-sm text-left">' + this.tax_amount + '</td>' +
                '<td class="fixed-width-sm text-left">' + this.total + '</td>' +
                {/literal}
                {if $rectified_invoice->validated|intval eq 0}
                '<td class="fixed-width-sm text-left">'+
                {if !$ps15}
                    '<button class="btn btn-default" onclick="deleteItem(' + this.id + ');return false;"><i class="icon icon-remove"></i></button>'
                {else}
                    '<a href="javascript:void(0);" onclick="deleteItem(' + this.id + ');return false;"><img src="../img/admin/delete.gif"></a>'
                {/if}
                +'</td>' +
                {/if}
                {literal}
                '</tr>'
            });
        else
            table = '<tr><td colspan="7">{/literal}{l s='No items' mod='parectifiedinvoices'}{literal}</td></tr>';
        $('#summary_list tbody').html(table);
        if (typeof(res.summary.subtotal) != '')
            $('#summary_subtotal').text(res.summary.subtotal);
        if (typeof(res.summary.tax_amount) != '')
            $('#summary_tax_amount').text(res.summary.tax_amount);
        if (typeof(res.summary.total) != '')
            $('#summary_total').text(res.summary.total);
    }
    $('#summary_part').removeClass('loading');
}
function getSummaryDetails() {
    $('#summary_part').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "getSummary",
            id_rectified_invoice: id_rectified_invoice
        },
        success: function (res) {
            updateSumaryDetails(res);
        }
    });


}
function addItems(restock) {
    if (typeof(restock) == 'undefined')
    {
        restock = false;
    }
    $.each($('#invoice_list tbody tr'), function () {

        id = $(this).attr('id');
        if (id != null)
            id = id.replace('item_', '');
        else
            return;

        if ($(this).find('.id_item').is(':checked')) {
            $('#summary_part').addClass('loading');
            $.ajax({
                type: "POST",
                url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
                async: true,
                dataType: "json",
                data: {
                    ajax: "1",
                    token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
                    tab: "AdminRectifiedInvoices",
                    action: "addItem",
                    id_item: id,
                    id_currency: id_currency,
                    id_address: $('#id_address_invoice').val(),
                    id_customer: id_customer,
                    id_order: $('#item_' + id).find('.id_order').val(),
                    id_rectified_invoice: id_rectified_invoice,
                    restock: restock,
                    id_wharehouse: $('#wharehouse').val(),
                    advanced_management: $('#advanced_management').val(),
                    reason: $('#stock_mv_reason').val(),
                    item_type: $('#item_' + id).find('.item_type').val(),
                    quantity: $('#item_' + id).find('.quantity').val()
                },
                success: function (res) {
                    if (typeof (res.id_rectified) != 'undefined')
                        id_rectified_invoice = res.id_rectified_invoice;
                    if (res.status == 'ok' && res.confirmation.length != 0)
                        showSuccessMessage(res.confirmation);
                    if (res.status == 'nok' && res.error.length != 0)
                        showErrorMessage(res.error);
                    updateSumaryDetails(res);
                    id_currency = res.id_currency;
                    getInvoices($('#invoices').val());
                    viewInvoice();
                    $('.select-all').removeAttr('checked');
                    if (id_rectified_invoice)
                    {
                        $('a.toolbar_btn').each(function(){
                            $(this).attr('href', $(this).attr('href').replace('&id_rectified_invoice=0','&id_rectified_invoice='+id_rectified_invoice))
                        });
                        changeURLGetVar('addrectified_invoices', 'updaterectified_invoices');
                        changeURLRequest('id_rectified_invoice', id_rectified_invoice);
                        $('.toolbar_btn').closest('li').show();
                        $('.change-customer').remove();

                    }

                }


            });
        }
    });


    return false;
}
function getInvoices(last_id)
{
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "getinvoices",
            id_currency: id_currency,
            id_customer: id_customer,
            last_id_order: last_id
        },
        success: function (res) {
            updateInvoicesOptions(res);
        }
    });
}
function addItem(id, restock) {
    if (typeof(restock) == 'undefined')
    {
        restock = false;
    }
    params = id.split('-');
    $('#summary_part').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "addItem",
            id_item: params[0],
            id_address: $('#id_address_invoice').val(),
            id_customer: id_customer,
            id_currency: id_currency,
            restock: restock,
            id_wharehouse: $('#wharehouse').val(),
            advanced_management: $('#advanced_management').val(),
            reason: $('#stock_mv_reason').val(),
            id_order: params[1],
            id_rectified_invoice: id_rectified_invoice,
            item_type: params[2],
            quantity: $('#item_' + id).find('.quantity').val()
        },
        success: function (res) {
            if (typeof (res.id_rectified) != 'undefined')
                id_rectified_invoice = res.id_rectified_invoice;
            if (res.status == 'ok' && res.confirmation.length != 0)
                showSuccessMessage(res.confirmation);
            if (res.status == 'nok' && res.error.length != 0)
                showErrorMessage(res.error);
            id_currency = res.id_currency;
            getInvoices($('#invoices').val());
            viewInvoice();
            updateSumaryDetails(res);
            if (id_rectified_invoice)
            {
                $('a.toolbar_btn').each(function(){
                    $(this).attr('href', $(this).attr('href').replace('&id_rectified_invoice=0','&id_rectified_invoice='+id_rectified_invoice))
                });
                changeURLGetVar('addrectified_invoices', 'updaterectified_invoices');
                changeURLRequest('id_rectified_invoice', id_rectified_invoice);
                $('.toolbar_btn').closest('li').show();
                $('.change-customer').remove();
            }
        }
    });
    return false;
}
function addFreeLine() {
    $('.has-error').removeClass('has-error');
    error = false;
    if ($('#invoices_container').find('input[name=text]').val().trim().length == 0) {
        $('#invoices_container').find('input[name=text]').parent().addClass('has-error');
        error = true;
    }
    pricepu = parseFloat($('#invoices_container').find('input[name=price_per_unit]').val());
    if (isNaN(pricepu)) {
        $('#invoices_container').find('input[name=price_per_unit]').parent().addClass('has-error');
        error = true;
    }
    unit = parseInt($('#invoices_container').find('input[name=quantity]').val());
    if (isNaN(unit)) {
        $('#invoices_container').find('input[name=quantity]').parent().addClass('has-error');
        error = true;
    }
    tax_rate = parseFloat($('#invoices_container').find('input[name=tax_rate]').val());
    if (isNaN(tax_rate)) {
        $('#invoices_container').find('input[name=tax_rate]').parent().addClass('has-error');
        error = true;
    }
    discount = parseFloat($('#invoices_container').find('input[name=discount]').val());
    if (isNaN(discount)) {
        $('#invoices_container').find('input[name=discount]').parent().addClass('has-error');
        error = true;
    }
    if (error)
        return false;
    $('#summary_part').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "addItem",
            text: $('#invoices_container').find('input[name=text]').val(),
            id_customer: id_customer,
            id_rectified_invoice: id_rectified_invoice,
            id_invoice: $('#id_invoice_free_item').val(),
            id_address: $('#id_address_invoice').val(),
            item_type: 4,
            id_currency: id_currency,
            price_per_unit: $('#invoices_container').find('input[name=price_per_unit]').val(),
            quantity: $('#invoices_container').find('input[name=quantity]').val(),
            tax_rate: $('#invoices_container').find('input[name=tax_rate]').val(),
            discount: $('#invoices_container').find('input[name=discount]').val(),
            total_line: $('#invoices_container').find('input[name=total_line]').val(),
        },
        success: function (res) {
            if (typeof (res.id_rectified) != 'undefined')
                id_rectified_invoice = res.id_rectified_invoice;
            if (res.status == 'ok' && res.confirmation.length != 0) {
                $('#id_invoice_free_item').val(0);
                $('#invoices_container').find('input[name=text]').val('');
                $('#invoices_container').find('input[name=price_per_unit]').val('');
                $('#invoices_container').find('input[name=quantity]').val('');
                $('#invoices_container').find('input[name=tax_rate]').val('');
                $('#invoices_container').find('input[name=discount]').val('');
                $('#invoices_container').find('input[name=amount]').val('');
                $('#invoices_container').find('input[name=tax_amount]').val('');
                $('#invoices_container').find('input[name=total_line]').val('');
                showSuccessMessage(res.confirmation);
            }
            if (res.status == 'nok' && res.error.length != 0)
                showErrorMessage(res.error);
            updateSumaryDetails(res);
            if (id_rectified_invoice)
            {
                $('a.toolbar_btn').each(function(){
                    $(this).attr('href', $(this).attr('href').replace('&id_rectified_invoice=0','&id_rectified_invoice='+id_rectified_invoice))
                });
                $('.toolbar_btn').closest('li').show();
                $('.change-customer').remove();
                changeURLGetVar('addrectified_invoices', 'updaterectified_invoices');
                changeURLRequest('id_rectified_invoice', id_rectified_invoice);

            }


        }
    });


    return false;

}
function quantityDown(element) {
    input = $(element).closest('tr').find('.quantity');
    if (new Number($(input).val()) <= 0)
        value = 0;
    else
        value = new Number($(input).val()) - 1;
    $(input).val(value);
}
function quantityUp(element) {
    input = $(element).closest('tr').find('.quantity');
    max_item = $(input).attr('data-max-quantity');
    if (new Number($(input).val()) >= max_item)
        value = max_item;
    else
        value = new Number($(input).val()) + 1;
    $(input).val(value);
}
function checkQuantity(input) {
    max_item = $(input).attr('data-max-quantity');
    if (new Number($(input).val()) >= max_item)
        $(input).val(max_item);
}
function updateInvoiceDetails(res) {

    if (typeof(res.items) != 'undefined') {
        out = '';
        if (res.items)
            $.each(res.items, function () {
                out += '<tr id="item_' + this.id_item + '-'  + this.id_order + '-' + this.item_type +'">'
                + '<td class="fixed-width-xs"><input class="id_order" type="hidden" name="id_order[]" value="' + this.id_order + '"><input type="checkbox" class="id_item" name="id_item[]" value="' + this.id_item + '"><input type="hidden" class="item_type" name="item_type[]" value="' + this.item_type + '"></td>'
                + '<td class="text-left">' + this.item + '</td>'
                + '<td class="fixed-width-sm  text-left">' + this.unit_price + '</td>'
                + '<td class="fixed-width-sm  text-left">' + this.quantity + '/' + this.quantity_rate + '</td>'
                + '<td class="fixed-width-sm  text-left">' + this.discount + '</td>'
                + '<td class="fixed-width-sm  text-left">' + this.tax_rate + '</td>'
                + '<td class="center"><a class="btn btn-default button-minus col-xs-3" onclick="quantityDown($(this)); return false;"><span>-</span></a>'
                + '<div class="col-xs-4"><input data-max-quantity="' + this.quantity + '" type="text" class="quantity text form-control" name="quantity[]" onchange="checkQuantity($(this));" value="' + this.quantity + '"></div><a class="btn btn-default button-minus col-xs-3" onclick="quantityUp($(this)); return false;"><span>+</span></a></td>'
                + '<td class="fixed-width-sm  text-left">' + this.total_line + '</td>';
                if (this.quantity > 0)
                    if (this.advanced_management == 1)
                        if (this.wharehouse_on == 1)
                            out += '<td class="fixed-width-sm  text-left"><button onclick="$(\'#advanced_management\').val($(this).attr(\'advanced_management\'));$(\'#id_order_detail\').val(\'' + this.id_item + '-'  + this.id_order + '-' + this.item_type + '\'); $(\'#additems_btn\').addClass(\'hidden\'); $(\'#additem_btn, #wharehouse_container\').removeClass(\'hidden\'); return false;" class="btn btn-default" data-toggle="modal" data-target="#restock" advanced_management="1"><i class="icon-plus-square"></i>&nbsp;{/literal}{l s='Add item' mod='parectifiedinvoices'}{literal}</button></td>';
                        else
                            out += '<td class="fixed-width-sm  text-left"><button onclick="$(\'#advanced_management\').val($(this).attr(\'advanced_management\'));addItem(\'' + this.id_item + '-'  + this.id_order + '-' + this.item_type + '\', false); return false;" advanced_management="'+this.advanced_management+'" class="btn btn-default"><i class="icon-plus-square"></i>&nbsp;{/literal}{l s='Add item' mod='parectifiedinvoices'}{literal}</button></td>';
                    else
                        out += '<td class="fixed-width-sm  text-left"><button onclick="$(\'#advanced_management\').val($(this).attr(\'advanced_management\'));$(\'#id_order_detail\').val(\'' + this.id_item + '-'  + this.id_order + '-' + this.item_type + '\'); $(\'#additems_btn, #wharehouse_container\').addClass(\'hidden\'); $(\'#additem_btn\').removeClass(\'hidden\'); return false;" class="btn btn-default" data-toggle="modal" data-target="#restock" advanced_management="0"><i class="icon-plus-square"></i>&nbsp;{/literal}{l s='Add item' mod='parectifiedinvoices'}{literal}</button></td>';
                else
                    out += '<td class="fixed-width-sm  text-left"><button onclick="return false;" class="btn btn-default" disabled="disabled"><i class="icon-plus-square"></i>&nbsp;{/literal}{l s='Add item' mod='parectifiedinvoices'}{literal}</button></td>';
                out += '</tr>';

            });
}
    else{
            out = '<tr><td colspan="6">{/literal}{l s='Items not found' mod='parectifiedinvoices'}{literal}</td></td>';
    }

    $('#invoice_list tbody').html(out);
    $('#invoice_list').removeClass('loading');
}
function viewInvoice() {

    $('#invoice_list').addClass('loading');
    $.ajax({
        type: "POST",
        url: "{/literal}{$link->getAdminLink('AdminRectifiedInvoices')|escape:'html':'UTF-8'}{literal}",
        async: true,
        dataType: "json",
        data: {
            ajax: "1",
            token: "{/literal}{getAdminToken tab='AdminRectifiedInvoices'}{literal}",
            tab: "AdminRectifiedInvoices",
            action: "invoiceDetails",
            id_invoice: $('#invoices option:selected').val(),
        },
        success: function (res) {
            updateInvoiceDetails(res);
        }
    });
}
{/literal}
</script>
{if $ps15}
    {include file="toolbar.tpl" toolbar_btn=$toolbar_btn toolbar_scroll=$toolbar_scroll title=$title}
{/if}
<div class="leadin">{block name="leadin"}{/block}</div>
{if $rectified_invoice->validated|intval eq 0}
    <h2>1º - {l s='Select the invoice customer' mod='parectifiedinvoices'}</h2>
    <div class="panel form-horizontal" id="customer_part">
        <div class="panel-heading">
            <i class="icon-user"></i>
            {l s='Customer' mod='parectifiedinvoices'}
        </div>
        <div id="search-customer-form-group" class="form-group">
            <label class="control-label col-lg-3">
            <span title="" data-toggle="tooltip" class="label-tooltip"
                  data-original-title="{l s='Search for an existing customer by typing the first letters of his/her name.' mod='parectifiedinvoices'}">
                {l s='Search for a customer' mod='parectifiedinvoices'} {l s='(email, name or VAT Number)' mod='parectifiedinvoices'}
            </span>
            </label>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <input type="text" id="customer" value=""/>
                        <span class="input-group-addon">
                            <i class="icon-search"></i>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <span class="form-control-static">{l s='Or' mod='parectifiedinvoices'}&nbsp;</span>
                        <a class="fancybox_customer btn btn-default"
                           href="{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}&amp;addcustomer&amp;liteDisplaying=1&amp;submitFormAjax=1#">
                            <i class="icon-plus-sign-alt"></i>
                            {l s='Add new customer' mod='parectifiedinvoices'}
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div id="customers"></div>
        </div>

    </div>
{/if}
<div id="address_part" style="display:none;">
{if $rectified_invoice->validated eq 0}
<h2>2º - {l s='Select the invoice address' mod='parectifiedinvoices'}</h2>
{/if}
    <div class="panel" >
<div class="panel-heading">
    {if $rectified_invoice->validated eq 0}
        <i class="icon-home"></i>
        {l s='Addresses' mod='parectifiedinvoices'}
    {else}
        <i class="icon-home"></i>
        {l s='Details' mod='parectifiedinvoices'}

    {/if}
</div>
<div id="addresses_err" class="alert alert-warning" style="display:none;"></div>
<div class="row">
    <div id="address_invoice" class="col-lg-6">
        {*{if $rectified_invoice->validated eq 0}*}
            {*<div class="alert alert-info">{l s='Select the correct customer\'s address for the invoice' mod='parectifiedinvoices'}</div>*}
        {*{/if}*}

        <h4>
            {l s='Invoice  Address' mod='parectifiedinvoices'}
        </h4>
        {if $rectified_invoice->validated|intval eq 0}
            <div class="row-margin-bottom">
                <select id="id_address_invoice" name="id_address_invoice" class="fixed-width-xxl"></select>
            </div>
        {else}
            <input type="hidden" id="id_address_invoice" name="id_address_invoice"
                   value="{$rectified_invoice->id_address|escape:'html':'UTF-8'}">
        {/if}

        <div class="well">
            <a href="" id="edit_invoice_address" class="btn btn-default pull-right fancybox"><i
                        class="icon-pencil"></i> {l s='Edit' mod='parectifiedinvoices'}</a>

            <div id="address_invoice_detail"></div>
        </div>
    </div>


    {if $rectified_invoice->validated}
    <div id="invoice_details" class="col-lg-6">
        <h4>
            {l s='Invoice  Details' mod='parectifiedinvoices'}
        </h4>

        <div class="well">
            {*<button id="toggle_datepicker" class="button btn btn-default pull-right"*}
                    {*onclick="toggleDate(); return false;"><i*}
                        {*class="icon-pencil"></i>&nbsp; {l s='Edit' mod='parectifiedinvoices'}</button>*}

            <div class="row">
                <span>{l s='Invoice Reference:' mod='parectifiedinvoices'}</span> {$rectified_invoice->num|escape:'html':'UTF-8'}
                <br/>
            </div>
            <div class="row">
                <span>{l s='Customer:' mod='parectifiedinvoices'}</span>  {$customer->firstname|escape:'html':'UTF-8'} {$customer->lastname|escape:'html':'UTF-8'}
                <br/>
            </div>
            <div class="row">
                <span>{l s='Date:' mod='parectifiedinvoices'}</span><span
                        id="date_text">{$rectified_invoice->date_validated|date_format:' %Y-%m-%d %T'|escape:'html':'UTF-8'}</span><br>
                <input id="datepicker" type="text" class="datepicker input fixed-width-lg hide"
                       value="{$rectified_invoice->date_validated|escape:'html':'UTF-8'}">

            </div>
        </div>
    </div>
    {/if}
    </div>
    </div>
    {*<div class="row">*}
        {*{if $rectified_invoice->validated|intval eq 0}*}
            {*<div class="col-lg-offset-3 col-lg-6">*}
                {*<a class="fancybox btn btn-default" id="new_address"*}
                   {*href="{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}&amp;addaddress&amp;id_customer=42&amp;liteDisplaying=1&amp;submitFormAjax=1#">*}
                    {*<i class="icon-plus-sign-alt"></i>*}
                    {*{l s='Add a new address' mod='parectifiedinvoices'}*}
                {*</a>*}
            {*</div>*}
        {*{/if}*}
    {*</div>*}

<form class="form-horizontal"
      action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;submitAdd{$table|escape:'html':'UTF-8'}=1"
      method="post" autocomplete="off">
<input type="hidden" id="id_rectified_invoice" value="{$id_rectified_invoice|intval}" name="id_rectified_invoice">
<input type="hidden" id="id_currency" value="{$id_currency|intval}" name="id_currency">
{if $rectified_invoice->validated|intval eq 0}
    <h2>3º - {l s='Select the invoice to rectify' mod='parectifiedinvoices'}</h2>

    <div class="panel" id="invoices_container" style="display:none;">
        <div class="panel-heading">
            <i class="icon-folder-open"></i>
            {l s='Invoices' mod='parectifiedinvoices'}
        </div>
        <div class="form-group">
            {*<div class="row">*}
                {*<div class="col-lg-offset-3 col-lg-6">*}
                    {*<div class="alert alert-info">{l s='If you need to correct an invoice line, select an invoice from the list and click "View invoice details".' mod='parectifiedinvoices'}</div>*}
                {*</div>*}
            {*</div>*}

            <div class="col-lg-3 fixed-width-xxl">
                <div class="input-group">
                    <select id="invoices" name="invoices" class="fixed-width-xxl">
                        <option value="0">-</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 fixed-width-lg">
                <div class="input-group pull-right">
                    <button class="btn btn-default" onclick="viewInvoice(); return false;"
                            id="selectInvoice"><i
                                class="icon-search"></i>&nbsp;{l s='View invoice' mod='parectifiedinvoices'}</button>
                </div>
            </div>
        </div>
        <div id="restock" class="modal fade in">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title text-center"><i class="icon-bar-chart"></i>&nbsp;{l s='Restock this product ?' mod='parectifiedinvoices'}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        {if !empty($wharehouses)}
                            <div class="form-group hidden" id="wharehouse_container">
                                <div class="col-lg-offset-2  col-lg-4">
                                    <div class="input-group">
                                        <select id="wharehouse" name="id_wharehouse" class="fixed-width-xl">
                                            {foreach $wharehouses as $k => $wharehouse}
                                                <option {if $k eq 0} selected="selected" {/if}  value="{$wharehouse.id_warehouse|escape:'html':'UTF-8'}">{$wharehouse.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-offset-1 col-lg-2">
                                    <div class="input-group">
                                        <select id="stock_mv_reason" name="id_stock_mv_reason" class="fixed-width-lg">
                                            {foreach $stock_mv_reasons as $k => $stock_mv_reason}
                                                <option {if $k eq 0} selected="selected" {/if}  value="{$stock_mv_reason.id_stock_mvt_reason|escape:'html':'UTF-8'}">{$stock_mv_reason.name|escape:'html':'UTF-8'}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        {else}
                            <input id="wharehouse" name="id_wharehouse" value="0" type="hidden">
                        {/if}
                        <input id="id_order_detail" value="0" type="hidden">
                        <input id="advanced_management" value="0" type="hidden">
                        </div>
                            <div class="row">
                            <div id="additem_btn" class="col-xs-12 hidden" style="text-align: center; padding-top: 12px;">
                                <a class="btn btn-default" data-dismiss="modal" onclick="addItem($('#id_order_detail').val(), true);">
                                    <i class="icon-circle-arrow-up"></i>
                                    {l s='I wanna re-stock' mod='parectifiedinvoices'}
                                </a>
                                <a class="btn btn-default" data-dismiss="modal" onclick="addItem($('#id_order_detail').val(), false);">
                                    <i class="icon-minus-square-o"></i>
                                    {l s='No, I wan\'t re-stock' mod='parectifiedinvoices'}
                                </a>
                                <a class="btn btn-default" data-dismiss="modal">
                                    <i class="icon-remove"></i>
                                    {l s='Cancel' mod='parectifiedinvoices'}
                                </a>
                            </div>

                            <div id="additems_btn" class="col-xs-12 hidden" style="text-align: center; padding-top: 12px;">
                                <a class="btn btn-default" data-dismiss="modal" onclick="addItems(true);">
                                    <i class="icon-circle-arrow-up"></i>
                                    {l s='I wanna re-stock' mod='parectifiedinvoices'}
                                </a>
                                <a class="btn btn-default" data-dismiss="modal" onclick="addItems(false);">
                                    <i class="icon-minus-square-ol"></i>
                                    {l s='No, I wan\'t re-stock' mod='parectifiedinvoices'}
                                </a>
                                <a class="btn btn-default" data-dismiss="modal">
                                    <i class="icon-remove"></i>
                                    {l s='Cancel' mod='parectifiedinvoices'}
                                </a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="products_err" class="hide alert alert-danger"></div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table responsive" id="invoice_list">
                    <thead>
                    <tr>
                        <th class="fixed-width-xs"><label><input type="checkbox"
                                                                 class="select-all"></label></th>
                        <th class="text-left"><label>{l s='Item' mod='parectifiedinvoices'}</label>
                        </th>
                        <th class="fixed-width-xs  text-left">
                            <label>{l s='Price per unit' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-xs  text-left">
                            <label>{l s='Quantity' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-xs  text-left"><label>{l s='Discount' mod='parectifiedinvoices'}</label>
                        <th class="fixed-width-xs  text-left"><label>{l s='Tax rate' mod='parectifiedinvoices'}</label>
                        </th>
                     <th class="fixed-width-lg  text-center">
                            <label>{l s='Units' mod='parectifiedinvoices'}</label></th>
                    <th class="fixed-width-md  text-left">
                        <label>{l s='Total line' mod='parectifiedinvoices'}</label></th>
                    <th class="fixed-width-sm  text-left">
                        <label>{l s='Action' mod='parectifiedinvoices'}</label></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="10">
                            <button class="btn btn-default"
                                    onclick="$('#additem_btn').addClass('hidden'); $('#additems_btn').removeClass('hidden'); return false;" data-toggle="modal" data-target="#restock">{l s='Add items' mod='parectifiedinvoices'}</button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            </div>
            <div class="panel-heading">
            </div>
             <div class="panel-heading">
                 <i class="icon-paper-clip"></i>
                 {l s='Add free line' mod='parectifiedinvoices'}

             </div>

            <div class="row">
                <div id="free_line_container" class="row-margin-bottom text-center">
                    <table class="table responsive">
                        <thead>
                                <th class="fixed-width-lg"><label>{l s='Invoice' mod='parectifiedinvoices'}
                                    </label></th>
                                <th class="text-left"><label>{l s='Item' mod='parectifiedinvoices'}</label>
                                </th>
                                <th class="fixed-width-md  text-left">
                                    <label>{l s='Price per unit' mod='parectifiedinvoices'}</label></th>
                                <th class="fixed-width-sm  text-center">
                                    <label>{l s='Units' mod='parectifiedinvoices'}</label></th>
                                <th class="fixed-width-md  text-left"><label>{l s='Discount' mod='parectifiedinvoices'}</label>

                                <th class="fixed-width-sm  text-left"><label>{l s='Tax rate' mod='parectifiedinvoices'}</label>
                                </th>
                                <th class="fixed-width-md text-left">
                                    <label>{l s='Total line' mod='parectifiedinvoices'}</label></th>
                                <th class="fixed-width-md  text-left">
                                    <label>{l s='Action' mod='parectifiedinvoices'}</label></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select class="invoices fixed-width-xl" id="id_invoice_free_item" >
                                    <option value="0">-</option>
                                </select>
                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="text" name="text">
                            <span class="input-group-addon">
                            </span>
                                </div>

                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="text" name="price_per_unit" class="recalc_total">
                            <span class="input-group-addon">
                            </span>
                                </div>

                            </td>
                            <td>
                                <input type="text" name="quantity" class="recalc_total">
                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="text" name="discount" class="recalc_total">
                            <span class="input-group-addon">
                            </span>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="text" name="tax_rate" class="recalc_total">
                            <span class="input-group-addon">
                            %
                            </span>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <input readonly type="text" name="total_line" class="recalc_total">
                            <span class="input-group-addon">
                            </span>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-default pull-left"
                                        onclick="addFreeLine(); return false;">
                                    &nbsp;{l s='Add Free item' mod='parectifiedinvoices'}</button>
                            </td>
                        </tr>


                        </tbody>
                    </table>

                </div>

            </div>

    </div>


{/if}
{if $rectified_invoice->validated|intval eq 0}
<h2>4º - {l s='Check the rectified invoice summary' mod='parectifiedinvoices'}</h2>
{else}
    <div class="row"></div>
{/if}
<div class="panel" id="summary_part" style="display:none;">
    <div class="panel-heading">
        <i class="icon-list"></i>
        {l s='Summary' mod='parectifiedinvoices'}
    </div>

    <div id="send_email_feedback" class="hide alert"></div>

    <div id="cart_summary" class="row-margin-bottom text-center">
        <div class="row">
            <div class="col-lg-12">
                <table class="table" id="summary_list">
                    <thead>
                    <tr>
                        <th class="fixed-width-sm"><label>{l s='Ref. Invoice' mod='parectifiedinvoices'}</label>
                        </th>
                        <th><label>{l s='Item' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Price/Unit' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Quantity' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Discount' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Tax rate' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Tax' mod='parectifiedinvoices'}</label></th>
                        <th class="fixed-width-sm"><label>{l s='Total line' mod='parectifiedinvoices'}</label></th>
                        {if $rectified_invoice->validated|intval eq 0}
                            <th class="fixed-width-sm"><label>{l s='Action' mod='parectifiedinvoices'}</label></th>
                        {/if}
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="footer_tab col-lg-12">
                <div class="form-group">
                    <div class="col-lg-9  text-left">
                        <label class="control-label" for="invoice_reason">{l s='Reason for invoice rectificitation'  mod='parectifiedinvoices'}</label>
                        {if $rectified_invoice->validated|intval eq 0}
                            <textarea name="invoice_reason" id="invoice_reason" rows="3"
                                      cols="45">{$rectified_invoice->reason|escape:'html':'UTF-8'}</textarea>
                        {else}
                            <p>{$rectified_invoice->reason|escape:'html':'UTF-8'}</p>
                        {/if}
                    </div>
                    <div class="col-lg-3">
                        <label class="control-label">&nbsp;</label>
                        <table class="table" id="">
                            <tbody>
                            <tr>
                                <td><b>{l s='Subtotal' mod='parectifiedinvoices'}</b></td>
                                <td><span id="summary_subtotal">0</span></td>
                            </tr>
                            <tr>
                                <td><b>{l s='Tax' mod='parectifiedinvoices'}</b></td>
                                <td><span id="summary_tax_amount">0</span></td>
                            </tr>
                            <tr>
                                <td><b>{l s='Total' mod='parectifiedinvoices'}</b></td>
                                <td><span id="summary_total">0</span></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
</form>

<div id="loader_container">
    <div id="loader"></div>
</div>