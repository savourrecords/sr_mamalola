{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.
*  @copyright Pronimbo. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}

{* Generate HTML code for printing Invoice Icon with link *}
<span class="btn-group-action">
    <span class="btn-group">
    {if $id_invoice}
        <a class="button  button_mini btn btn-default _blank"
           href="{$link->getAdminLink('AdminRectifiedInvoices', true)|escape:'html':'UTF-8'}&amp;action=viewPDF&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'}">
            <i class="icon-file-text"></i>
        </a>
             &nbsp;
         <a class="button  button_mini btn btn-default"  {if $invoice->validated|intval eq 0} disabled="disabled" {else} href="{$link->getAdminLink('AdminRectifiedInvoices',true)|escape:'html':'UTF-8'}&amp;action=SendEmail&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'}" {/if}  >
            <i class="icon-envelope-alt"></i>
        </a>

                                        &nbsp;
        <a class="button  button_mini btn btn-default"  {if $invoice->validated|intval eq 0} href="{$link->getAdminLink('AdminRectifiedInvoices',true)|escape:'html':'UTF-8'}&amp;action=validateInvoice&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'} {else} disabled="disabled" {/if}  ">
        <i class="icon-check"></i>
        </a>
        &nbsp;
        <a class="button  button_mini btn btn-default" {if $invoice->validated|intval eq 0} href="{$link->getAdminLink('AdminRectifiedInvoices',true)|escape:'html':'UTF-8'}&amp;updaterectified_invoices&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'} {else} disabled="disabled" {/if}">

        <i class="icon-pencil"></i>
        </a>
        &nbsp;

        <a class=" button  button_mini btn btn-default" {if $invoice->validated|intval eq 0} href="{$link->getAdminLink('AdminRectifiedInvoices',true)|escape:'html':'UTF-8'}&amp;deleterectified_invoices&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'}" {else} disabled="disabled"{/if}>
            <i class="icon-remove"></i>
        </a>
        <a class="button  button_mini  btn btn-default" href="{$link->getAdminLink('AdminRectifiedInvoices',true)|escape:'html':'UTF-8'}&amp;updaterectified_invoices&amp;id_rectified_invoice={$id_invoice|intval|escape:'html':'UTF-8'}">
            <i class="icon-eye"></i>
        </a>
    {/if}
    </span>
</span>
