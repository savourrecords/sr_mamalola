{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.
*  @copyright Pronimbo. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}

<table width="100%;">
    <tr>
        <td>
            <span style="color: #9E9F9E; font-size: 10pt;"><b>{l s='Compañía' mod='parectifiedinvoices'}</b></span><br/>
            <span style="font-size:9pt;">
                FRANCISCO R. POMARES, S.L.<br>
                Avda. de Alicante, 73 - Entlo. (Edificio Princesa)<br>
                03202 - Elche (Alicante)<br>
                España<br>
                CIF. B53191177
            </span>
        </td>
        <td style="text-align:right;">
            <span style="color: #9E9F9E; font-size: 10pt;"><b>{l s='Billing Address.' mod='parectifiedinvoices'}</b></span><br/>
            <span style="font-size:9pt;">
                {$invoice_address|escape:'quotes':'UTF-8'}
            </span>
        </td>
    </tr>
</table><br><br><br><br>

<div>
    <!-- PRODUCTS TAB -->
                <table style="width: 100%; font-size: 8pt;">
                    <thead>
                    <tr >
                        <th style="text-align: left; width: 20%">{l s='Invoice Reference' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left;  width: 30%">{l s='Item' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left; width: 10%">{l s='Quantity' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left; width: 10%">{l s='Discount' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left; width: 10%">{l s='Amount' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left;  width: 10%">{l s='Tax rate' mod='parectifiedinvoices'}</th>
                        <th style="text-align: left;  width: 10%; text-align:right">{l s='Total' mod='parectifiedinvoices'}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {if $rectified_invoice->items}
                        <!-- PRODUCTS -->
                        {foreach $rectified_invoice->items as $item}
                            {cycle values='#FFF,#DDD' assign=bgcolor}
                            <tr style="background-color:{$bgcolor|escape:'html':'UTF-8'};">
                                <td style="text-align: left;  width: 20%">{$ref_inv|escape:'html':'UTF-8'}{$item->num_invoice|escape:'html':'UTF-8'}</td>
                                <td style="text-align: left;  width: 30%">{$item->text|escape:'html':'UTF-8'}</td>
                                <td style="text-align: left;  width: 10%">{$item->quantity|escape:'html':'UTF-8'}</td>
                                <td style="text-align: left;  width: 10%">{displayPrice currency=$id_currency price=$item->discount}</td>
                                <td style="text-align: left;  width: 10%">{displayPrice currency=$id_currency price=$item->amount}</td>
                                <td style="text-align: left;  width: 10%">{$item->tax_rate|escape:'html':'UTF-8'}%</td>
                                <td style="text-align: left;  width: 10%; text-align:right">{displayPrice currency=$id_currency price=$item->total_per_line}</td>

                            </tr>
                        {/foreach}
                        <!-- END PRODUCTS -->
                    {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="width: 83%; text-align: right; font-weight: bold">{l s='Subtotal' mod='parectifiedinvoices'}</td>
                        <td style="width: 17%; text-align: right;">{displayPrice currency=$id_currency price=$rectified_invoice->total}</td>
                    </tr>
                    <tr >
                        <td style="width: 83%; text-align: right; font-weight: bold">{l s='Tax' mod='parectifiedinvoices'}</td>
                        <td style="width: 17%; text-align: right;">{displayPrice currency=$id_currency price=$rectified_invoice->total_tax}</td>
                    </tr>
                    <tr >
                        <td style="text-align: right; font-weight: bold">{l s='Total' mod='parectifiedinvoices'}</td>
                        <td style="width: 17%; text-align: right;">{displayPrice currency=$id_currency price=($rectified_invoice->total_tax + $rectified_invoice->total)}</td>
                    </tr>

                    </tfoot>
                </table>

    <!-- / PRODUCTS TAB -->

    <div >&nbsp;</div>
    {if isset($tax_tab)}
        {$tax_tab|escape:'html':'UTF-8'}
    {/if}

    {if isset($rectified_invoice->reason) && $rectified_invoice->reason}
        <div >&nbsp;</div>
        <table style="width: 100%">
            <tr>
                <td>{l s='Observations' mod='parectifiedinvoices'}</td>
            </tr>
            <tr>
                <td>{$rectified_invoice->reason|escape:'html':'UTF-8'}</td>
            </tr>
        </table>
    {/if}

    {if isset($HOOK_DISPLAY_PDF)}
        <div >&nbsp;</div>
        <table style="width: 100%">
            <tr>
                <td style="width: 17%"></td>
                <td style="width: 83%">{$HOOK_DISPLAY_PDF|escape:'quotes':'UTF-8'}</td>
            </tr>
        </table>
    {/if}

</div>
{**} 