{*
*
*  Please read the terms of the CLUF license attached to this module(cf "licences" folder)
*
*  @author    Pronimbo.
*  @copyright Pronimbo. all rights reserved.
*  @license   http://www.pronimbo.com/licenses/license_en.pdf http://www.pronimbo.com/licenses/license_es.pdf https://www.pronimbo.com/licenses/license_fr.pdf
*
*}

{if $tax_exempt || ((isset($product_tax_breakdown) && $product_tax_breakdown|@count > 0) || (isset($ecotax_tax_breakdown) && $ecotax_tax_breakdown|@count > 0) || (isset($shipping_tax_breakdown) && $shipping_tax_breakdown|@count > 0))}
    <!--  TAX DETAILS -->
    <table style="width: 100%">
        <tr>
            <td style="width: 15%"></td>
            <td style="width: 60%">
                {if $tax_exempt}
                    {l s='Exempt of VAT according section 259B of the General Tax Code.' pdf='true' mod='parectifiedinvoices'}
                {else}
                    <table>
                        <tr style="line-height:5px;">
                            <td style="text-align: left; background-color: #4D4D4D; color: #FFF; padding-left: 10px; font-weight: bold; width: {if !$use_one_after_another_method}30%{else}60%{/if}">{l s='Tax Detail' pdf='true' mod='parectifiedinvoices'}</td>
                            <td style="text-align: right; background-color: #4D4D4D; color: #FFF; padding-left: 10px; font-weight: bold; width: 20%">{l s='Tax Rate' pdf='true' mod='parectifiedinvoices'}</td>
                            {if !$use_one_after_another_method}
                                <td style="text-align: right; background-color: #4D4D4D; color: #FFF; padding-left: 10px; font-weight: bold; width: 30%">{l s='Total (Tax Excl.)' pdf='true' mod='parectifiedinvoices'}</td>
                            {/if}
                            <td style="text-align: right; background-color: #4D4D4D; color: #FFF; padding-left: 10px; font-weight: bold; width: 20%">{l s='Total Tax' pdf='true' mod='parectifiedinvoices'}</td>
                        </tr>

                        {if isset($product_tax_breakdown)}
                            {foreach $product_tax_breakdown as $rate => $product_tax_infos}
                                <tr style="line-height:6px;background-color:{cycle values='#FFF,#DDD'};">
                                    <td style="width: {if !$use_one_after_another_method}30%{else}60%{/if}">
                                        {if !isset($pdf_product_tax_written)}
                                            {l s='Products' pdf='true' mod='parectifiedinvoices'}
                                            {assign var=pdf_product_tax_written value=1}
                                        {/if}
                                    </td>
                                    <td style="width: 20%; text-align: right;">{$rate|escape:'html':'UTF-8'} %</td>
                                    {if !$use_one_after_another_method}
                                        <td style="width: 30%; text-align: right;">
                                            {if isset($is_order_slip) && $is_order_slip}- {/if}{displayPrice currency=$order->id_currency price=$product_tax_infos.total_price_tax_excl}
                                        </td>
                                    {/if}
                                    <td style="width: 20%; text-align: right;">{if isset($is_order_slip) && $is_order_slip}- {/if}{displayPrice currency=$order->id_currency price=$product_tax_infos.total_amount}</td>
                                </tr>
                            {/foreach}
                        {/if}

                    </table>
                {/if}
            </td>
        </tr>
    </table>
    <!--  / TAX DETAILS -->
{/if}
