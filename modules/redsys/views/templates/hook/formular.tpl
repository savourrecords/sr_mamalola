<form action="{$lacaixa_params.url_pos}" method="post" id="lacaixa_form" class="hidden">	
	{if isset($lacaixa_params.amount)}<input type="hidden" name="Ds_Merchant_Amount" value="{$lacaixa_params.amount}" />{/if}
	{if isset($lacaixa_params.currency)}<input type="hidden" name="Ds_Merchant_Currency" value="{$lacaixa_params.currency}" />{/if}
	{if isset($lacaixa_params.order)}<input type="hidden" name="Ds_Merchant_Order" value="{$lacaixa_params.order}" />{/if}
	{if isset($lacaixa_params.products_description)}<input type="hidden" name="Ds_Merchant_ProductDescription" value="{$lacaixa_params.products_description}" />{/if}
	{if isset($lacaixa_params.card_holder)}<input type="hidden" name="Ds_Merchant_Cardholder" value="{$lacaixa_params.card_holder}" />{/if}
	{if isset($lacaixa_params.merchant_code)}<input type="hidden" name="Ds_Merchant_MerchantCode" value="{$lacaixa_params.merchant_code}" />{/if}
	{if isset($lacaixa_params.merchant_url)}<input type="hidden" name="Ds_Merchant_MerchantURL" value="{$lacaixa_params.merchant_url}" />{/if}
	{if isset($lacaixa_params.transaction_type)}<input type="hidden" name="Ds_Merchant_TransactionType" value="{$lacaixa_params.transaction_type}" />{/if}
	{if isset($lacaixa_params.titular)}<input type="hidden" name="Ds_Merchant_Titular" value="{$lacaixa_params.titular}" />{/if}
	{if isset($lacaixa_params.url_ok)}<input type="hidden" name="Ds_Merchant_UrlOK" value="{$lacaixa_params.url_ok}" />{/if}
	{if isset($lacaixa_params.url_cancel)}<input type="hidden" name="Ds_Merchant_UrlKO" value="{$lacaixa_params.url_cancel}" />{/if}
	{if isset($lacaixa_params.merchant_name)}<input type="hidden" name="Ds_Merchant_MerchantName" value="{$lacaixa_params.merchant_name}" />{/if}
	{if isset($lacaixa_params.consumer_language)}<input type="hidden" name="Ds_Merchant_ConsumerLanguage" value="{$lacaixa_params.consumer_language}" />{/if}
	{if isset($lacaixa_params.merchant_signature)}<input type="hidden" name="Ds_Merchant_MerchantSignature" value="{$lacaixa_params.merchant_signature}" />{/if}
	{if isset($lacaixa_params.terminal)}<input type="hidden" name="Ds_Merchant_Terminal" value="{$lacaixa_params.terminal}" />{/if}
</form>