<p class="payment_module">
	<a href="javascript:$('#redsys_form').submit();" title="{l s='Connection to the POS' mod='redsys'}" class="bankwire">
		<img src="{$bank_img}" alt="{l s='Connection to the POS' mod='redsys'}" />
		{l s='Payment by credit card (secure transaction with Redsys)' mod='redsys'}
	</a>
</p>

<form action="{$redsys_params.url_pos}" method="post" id="redsys_form" class="hidden">	
	{if isset($redsys_params.amount)}<input type="hidden" name="Ds_Merchant_Amount" value="{$redsys_params.amount|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.currency)}<input type="hidden" name="Ds_Merchant_Currency" value="{$redsys_params.currency|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.order)}<input type="hidden" name="Ds_Merchant_Order" value="{$redsys_params.order}" />{/if}
	{if isset($redsys_params.products_description)}<input type="hidden" name="Ds_Merchant_ProductDescription" value="{$redsys_params.products_description|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.card_holder)}<input type="hidden" name="Ds_Merchant_Cardholder" value="{$redsys_params.card_holder|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.merchant_code)}<input type="hidden" name="Ds_Merchant_MerchantCode" value="{$redsys_params.merchant_code|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.merchant_url)}<input type="hidden" name="Ds_Merchant_MerchantURL" value="{$redsys_params.merchant_url|escape:'htmlall':'UTF-8'}" />{/if}
	<input type="hidden" name="Ds_Merchant_TransactionType" value="0" />
	{if isset($redsys_params.titular)}<input type="hidden" name="Ds_Merchant_Titular" value="{$redsys_params.titular|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.url_ok)}<input type="hidden" name="Ds_Merchant_UrlOK" value="{$redsys_params.url_ok|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.url_cancel)}<input type="hidden" name="Ds_Merchant_UrlKO" value="{$redsys_params.url_cancel|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.merchant_name)}<input type="hidden" name="Ds_Merchant_MerchantName" value="{$redsys_params.merchant_name|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.consumer_language)}<input type="hidden" name="Ds_Merchant_ConsumerLanguage" value="{$redsys_params.consumer_language|escape:'htmlall':'UTF-8'}" />{/if}
	{if isset($redsys_params.merchant_signature)}<input type="hidden" name="Ds_Merchant_MerchantSignature" value="{$redsys_params.merchant_signature}" />{/if}
	{if isset($redsys_params.terminal)}<input type="hidden" name="Ds_Merchant_Terminal" value="{$redsys_params.terminal|escape:'htmlall':'UTF-8'}" />{/if}
	<input type="hidden" name="Ds_Merchant_PayMethods" value="C" />
</form>


{if $iupay == "1"}
	<p class="payment_module">
		<a href="javascript:$('#redsys_iupay_form').submit();" title="{l s='' mod='redsys'}" class="bankwire">
			<img src="{$module_img_dir}logo-iupay.png" width="100" height="45" alt="{l s='IUPAY' mod='redsys'}" />
			{l s='Payment with iupay' mod='redsys'}
		</a>
	</p>


	</form><form action="{$redsys_params.url_pos}" method="post" id="redsys_iupay_form" class="hidden">	
		{if isset($redsys_params.amount)}<input type="hidden" name="Ds_Merchant_Amount" value="{$redsys_params.amount|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.currency)}<input type="hidden" name="Ds_Merchant_Currency" value="{$redsys_params.currency|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.order)}<input type="hidden" name="Ds_Merchant_Order" value="{$redsys_params.order|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.products_description)}<input type="hidden" name="Ds_Merchant_ProductDescription" value="{$redsys_params.products_description|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.card_holder)}<input type="hidden" name="Ds_Merchant_Cardholder" value="{$redsys_params.card_holder|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.merchant_code)}<input type="hidden" name="Ds_Merchant_MerchantCode" value="{$redsys_params.merchant_code|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.merchant_url)}<input type="hidden" name="Ds_Merchant_MerchantURL" value="{$redsys_params.merchant_url|escape:'htmlall':'UTF-8'}" />{/if}
		<input type="hidden" name="Ds_Merchant_TransactionType" value="0" />
		{if isset($redsys_params.titular)}<input type="hidden" name="Ds_Merchant_Titular" value="{$redsys_params.titular|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.url_ok)}<input type="hidden" name="Ds_Merchant_UrlOK" value="{$redsys_params.url_ok|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.url_cancel)}<input type="hidden" name="Ds_Merchant_UrlKO" value="{$redsys_params.url_cancel|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.merchant_name)}<input type="hidden" name="Ds_Merchant_MerchantName" value="{$redsys_params.merchant_name|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.consumer_language)}<input type="hidden" name="Ds_Merchant_ConsumerLanguage" value="{$redsys_params.consumer_language|escape:'htmlall':'UTF-8'}" />{/if}
		{if isset($redsys_params.merchant_signature)}<input type="hidden" name="Ds_Merchant_MerchantSignature" value="{$redsys_params.merchant_signature}" />{/if}
		{if isset($redsys_params.terminal)}<input type="hidden" name="Ds_Merchant_Terminal" value="{$redsys_params.terminal|escape:'htmlall':'UTF-8'}" />{/if}
		<input type="hidden" name="Ds_Merchant_PayMethods" value="O" />
	</form>
{/if}