<p class="payment_module">
	<a href="javascript:$('#redsys_form256').submit();" title="{l s='Connection to the POS' mod='redsys'}" class="bankwire">
		<img src="{$bank_img}" alt="{l s='Connection to the POS' mod='redsys'}" />
		{l s='Payment by credit card (secure transaction with Redsys)' mod='redsys'}
	</a>
</p>

<form action="{$url_pos}" method="post" id="redsys_form256" class="hidden">	
	<input type="text" name="Ds_SignatureVersion" value="HMAC_SHA256_V1" />
	<input type="text" name="Ds_MerchantParameters" value="{$params}" />
	<input type="text" name="Ds_Signature" value="{$signature}" />
</form>

{if $IUPAY}
	<p class="payment_module">
		<a href="javascript:$('#redsys_form256').submit();" title="{l s='Connection to the POS' mod='redsys'}" class="bankwire">
			<img src="{$module_img_dir}logo-iupay.png" alt="{l s='Connection to the POS' mod='redsys'}" />
			{l s='Payment by credit card (secure transaction with IUPAY)' mod='redsys'}
		</a>
	</p>

	<form action="{$url_pos}" method="post" id="redsys_form256" class="hidden">	
		<input type="text" name="Ds_SignatureVersion" value="HMAC_SHA256_V1" />
		<input type="text" name="Ds_MerchantParameters" value="{$paramsIUPAY}" />
		<input type="text" name="Ds_Signature" value="{$signatureIUPAY}" />
	</form>
{/if}