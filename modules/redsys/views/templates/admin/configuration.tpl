{if $ps_version == '1.5'}
	<div class="bootstrap">
{/if}
		<!-- Module content -->
		<div id="modulecontent" class="clearfix">
			<!-- Nav tabs -->
			<div class="col-lg-2" style="position:fixed">
				<div class="list-group">
					<a href="#documentation" class="list-group-item {if !$submit}active{/if}" data-toggle="tab"><i class="icon-book"></i> {l s='Documentation' mod='redsys'}</a>
					<a href="#config" class="list-group-item {if $submit}active{/if}" data-toggle="tab"><i class="icon-indent"></i> {l s='Configuration' mod='redsys'}</a>
					<a href="#faq" class="list-group-item" data-toggle="tab"><i class="icon-envelope"></i> {l s='FAQ' mod='redsys'}</a>
					<a href="#contacts" class="list-group-item" data-toggle="tab"><i class="icon-envelope"></i> {l s='Contact' mod='redsys'}</a>
				</div>
				<div class="list-group">
					<a class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='redsys'} {$module_version|escape:'htmlall':'UTF-8'}</a>
				</div>
			</div>
			<!-- Tab panes -->
			<div class="tab-content col-lg-9" style="position:relative;left:19%">
				<div class="tab-pane {if !$submit}active{/if} panel" id="documentation">
					{include file="./tabs/documentation.tpl"}
				</div>

				<div class="tab-pane {if $submit}active{/if}" id="config">
					{include file="./tabs/config.tpl"}
				</div>	

				<div class="tab-pane panel" id="faq">
					{include file="./tabs/faq.tpl"}
				</div>

				{include file="./tabs/contact.tpl"}
			</div>
		</div>
{if $ps_version == '1.5'}
	</div>
{/if}