	{if $submit}
		{if isset($config_warnings) && !empty($config_warnings)}
			<div class="alert alert-danger">
				<ul>
				{foreach from=$config_warnings item=error}
					<li>{$error|escape:'htmlall':'UTF-8'}</li>
				{/foreach}
				</ul>
			</div>
		{else}
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{l s='Configuration successfully updated' mod='redsys'}
			</div>
		{/if}
	{/if}

<div class="panel">
	<div class="panel-heading redsys_toggle">
	<i class="icon-cogs"></i> <b>{l s='Welcome to the Redsys settings interface!' mod='redsys'}</b>
	</div>
	<div>
		{l s='To start, select the bank which you have taken out the virtual POS terminal in order to show the logo of your bank in the payment page.' mod='redsys'}<br />
		{l s='If your bank is not on the list, do not select any bank. The Redsys logo will appear by default.' mod='redsys'}<br /><br />
	</div>
	<div class="panel">
		<div class="panel-heading redsys_toggle">
		<i class="icon-cogs"></i> {l s='SELECT YOUR BANK' mod='redsys'} - <span style="font-style:italic;text-transform: lowercase;color:grey">{l s='Click here to show the section' mod='redsys'}</span>
		</div>
		<form method="POST" class="form-horizontal">
			<div id="redsysbank">
				<div class="row">
					{foreach from=$redsys_banks item=bank}
						<div class="radio_select col-sm-6 col-md-4 col-lg-3">
							<div class="well" {if $ps_version == '1.5'}style="padding-bottom:41px !important"{/if}>
								<h4 class="hideOverflow">{$bank.displayName|escape:'htmlall':'UTF-8'}</h4>
								<span id="{$bank.techName|escape:'htmlall':'UTF-8'}" class="switch prestashop-switch input-group col-lg-12 col-md-8">
									<input type="radio" name="{$bank.techName|escape:'htmlall':'UTF-8'}" id="{$bank.techName|escape:'htmlall':'UTF-8'}_yes" {if $redsys_bank == $bank.techName}checked="checked"{/if} value="{$bank.techName|escape:'htmlall':'UTF-8'}">
									<label for="{$bank.techName|escape:'htmlall':'UTF-8'}_yes" class="radioCheck">
										<i class="color_success"></i> {l s='Enable' mod='redsys'}
									</label>
									<input type="radio" class="switch_off" name="{$bank.techName|escape:'htmlall':'UTF-8'}" id="{$bank.techName|escape:'htmlall':'UTF-8'}_no" {if $redsys_bank != $bank.techName}checked="checked"{/if} value="">
									<label for="{$bank.techName|escape:'htmlall':'UTF-8'}_no" class="radioCheck">
										<i class="color_success"></i> {l s='Disable' mod='redsys'}
									</label>
									<a class="slide-button btn" data-original-title="" title=""></a>
								</span>
							</div>
						</div>
					{/foreach}
				</div>
				<ps-panel-footer>
					<ps-panel-footer-submit title="{l s='Save' mod='redsys'}" icon="fa fa-save fa-2x" img="{$module_dir}save.png" direction="right" name="submitRedsysBank"></ps-panel-footer-submit>
				</ps-panel-footer>
			</div>
		</form>
	</div>

	<div class="panel">
		<div class="panel-heading redsys_toggle">
		<i class="icon-cogs"></i>{l s='GENERAL SETTINGS' mod='redsys'}
		</div>
		<form class="form-horizontal" method="post">

			{if $ps_version == '1.5'}
				<div class="form-group" style="clear: both; padding-top:15px;">
					<label class="conf_title" {if $ps_version == '1.5'}style="width: 260px;"{/if} > <span class="label-tooltip" data-toggle="tooltip" data-html="true">{l s='Environment' mod='redsys'}</span>  
					</label>
					<div class="col-lg-4">
						<span id="env" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
						<input type="radio" name="env" id="env_yes" value="1">
						<label for="env_yes" class="radioCheck">
							<i class="color_success"></i> {l s='live' mod='redsys'}
						</label>
						<input type="radio" class="switch_off" name="env" id="env_no" checked="checked" value="0">
						<label for="env_no" class="radioCheck">
							<i class="color_success"></i> {l s='test' mod='redsys'}
						</label>
						<a class="slide-button btn" data-original-title="" title=""></a>
						</span>
						<p class="preference_description"><raw content="{l s='Before entering the production environment, you must carry out an accepted payment and a rejected payment in test mode (below you will find card information for the test mode). To enter production mode you must contact your bank.' mod='redsys'}" /></p>
					</div>

				</div>
				<div class="col-lg-9 col-lg-offset-3"></div>
			{else}
				<ps-switch name="env" hint="{l s='Before entering the production environment, you must carry out an accepted payment and a rejected payment in test mode (below you will find card information for the test mode). To enter production mode you must contact your bank.' mod='redsys'}" label="{l s='Environment' mod='redsys'}" yes="{l s='live' mod='redsys'}" no="{l s='test' mod='redsys'}" {if $env}active="true"{else}active="false"{/if}></ps-switch>
			{/if}
			

			{if in_array($redsys_bank, $iupay_banks)}
				{if $ps_version == '1.5'}
					<div class="form-group" style="clear: both; padding-top:15px;">
						<label class="conf_title" {if $ps_version == '1.5'}style="width: 260px;"{/if}> <span class="label-tooltip" data-toggle="tooltip" data-html="true">{l s='Do you want to activate IuPay payments?' mod='redsys'}</span>  
						</label>
						<div class="col-lg-4">
							<span id="iupay" class="switch prestashop-switch input-group col-sm-12 col-md-8 col-lg-8">
							<input type="radio" name="iupay" id="iupay_yes" value="1">
							<label for="iupay_yes" class="radioCheck">
								<i class="color_success"></i> {l s='live' mod='redsys'}
							</label>
							<input type="radio" class="switch_off" name="iupay" id="iupay_no" checked="checked" value="0">
							<label for="iupay_no" class="radioCheck">
								<i class="color_success"></i> {l s='test' mod='redsys'}
							</label>
							<a class="slide-button btn" data-original-title="" title=""></a>
							</span>
							<p class="preference_description"><raw content="{l s='This option will only be activated if your bank is a member of the Iupay network and you have previously subscribed to it with your bank.' mod='redsys'}" /></p>
						</div>
					</div>
					<div class="col-lg-9 col-lg-offset-3"></div>
				{else}
					<ps-switch name="iupay" hint="{l s='This option will only be activated if your bank is a member of the Iupay network and you have previously subscribed to it with your bank.' mod='redsys'}" label="{l s='Do you want to activate IuPay payments ?' mod='redsys'}" yes="{l s='yes' mod='redsys'}" no="{l s='no' mod='redsys'}" {if $iupay}active="true"{else}active="false"{/if}></ps-switch>
				{/if}
			{/if}
			
			<ps-input-text name="key" help="{l s='Information provided by your bank' mod='redsys'}" label="{l s='Secret Key' mod='redsys'}" {if $key}value="{$key|escape:'htmlall':'UTF-8'}"{else}value=""{/if} hint="{l s='Key made up of alphanumeric characters. Please check documentation Part 2.' mod='redsys'}"></ps-input-text>
			
			<ps-input-text name="merchant_name" help="{l s='Enter the name of your store' mod='redsys'}" label="{l s='Vendor name' mod='redsys'}" {if $merchant_name}value="{$merchant_name|escape:'htmlall':'UTF-8'}"{else}value=""{/if} hint="{l s='This name will be displayed on the purchase form' mod='redsys'}" ></ps-input-text>
			
			<ps-input-text name="merchant_code" label="{l s='Vendor code (FUC)' mod='redsys'}" {if $merchant_code}value="{$merchant_code|escape:'htmlall':'UTF-8'}"{else}value=""{/if} hint="{l s='Code made up of 9 numerical digits.' mod='redsys'}" ></ps-input-text>
			
			<ps-input-text name="merchant_url" hint="{l s='The default validation URL is the recommended one. If you are an advanced user and want to personalize the payment validation page, create a new validation file and change the URL.' mod='redsys'}" label="{l s='URL requested by Redsys for payment confirmation' mod='redsys'}" {if $merchant_url}value="{$merchant_url|escape:'htmlall':'UTF-8'}"{else}value=""{/if} >
			</ps-input-text>
			
			<ps-input-text name="terminal" label="{l s='Terminal number' mod='redsys'}" {if $terminal}value="{$terminal|escape:'htmlall':'UTF-8'}"{else}value=""{/if} help="{l s='If it is 001 for example, just enter 1' mod='redsys'}" ></ps-input-text>

			{l s='Test Credit Card Details' mod='redsys'}<br />
			{l s='Number' mod='redsys'}: 4548812049400004<br />
			{l s='Validity date (or other date in the future)' mod='redsys'}: 12/17<br />
			{l s='CVV' mod='redsys'}: 123<br />

			<ps-panel-footer>
				<ps-panel-footer-submit title="{l s='Save' mod='redsys'}" icon="fa fa-save fa-2x" img="{$module_dir}save.png" direction="right" name="submitGeneral"></ps-panel-footer-submit>
			</ps-panel-footer>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	redsys_bank = "{$redsys_bank}"
	if (typeof redsys_bank !== 'undefined')
		$('#redsysbank').hide();
});
</script>