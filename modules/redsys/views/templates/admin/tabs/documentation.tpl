<h3><i class="icon-book"></i> {l s='Documentation' mod='redsys'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
<p>{l s='Module Redsys is a virtual POS terminal that fully guarantees the integration of bank card payments into your store.' mod='redsys'}
{l s='Based on the new Redsys system for your bank, it delivers the best and simplest user experience, both for you and your customers.' mod='redsys'}</p><br />
{l s='With this module you can:' mod='redsys'}
<ul>
	<li><b>{l s='Configure bank card payments in your store.' mod='redsys'}</b></li>
	<li><b>{l s='Implement Iupay.' mod='redsys'}</b></li>
	<li><b>{l s='View all of the information about the payment.' mod='redsys'}</b></li>
</ul><br />

{l s='N.B.: To make full use of module Redsys, you must first take out the virtual POS terminal service with your bank or savings bank.' mod='redsys'}

<p>&nbsp;</p>
<div class="media">
	<a class="pull-left" target="_blank" href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}">
		<img heigh="64" width="64" class="media-object" src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/pdf.png" alt="" title=""/>
	</a>
	<div class="media-body">
		<h4 class="media-heading"><p>{l s='Attached you will find the documentation for your module. Please consult it to properly configure the module.' mod='redsys'}</p></h4>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Redsys User Guide' mod='redsys'}</a></p>
		<p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$migration_guide_link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Redsys Migration Guide' mod='redsys'}</a></p>
		<p>{l s='Access to Prestashop free documentation: ' mod='redsys'} <a href="http://doc.prestashop.com/" target="_blank">{l s='Click here' mod='redsys'}</a></p>
	</div>
</div>

<p>&nbsp;</p>
<p>{l s='Need help? You will find it on' mod='redsys'} <a href="#contacts" class="contactus" data-toggle="tab" data-original-title="" title="{l s='Need help?' mod='redsys'}">{l s='Contact tab.' mod='redsys'}</a></p>
