<h3>{l s='Frequently Asked Questions' mod='redsys'}<br /></h3>
<div class="faq items">
	<ul id="basics" class="faq-items">
		<li class="faq-item">
			<a class="faq-trigger">1) {l s='I\'ve taken out a virtual POS terminal service with my bank. What should I do now?' mod='redsys'}<br /></a>
			<div class="faq-content">

			{l s='Your bank must send you the information to set up the module (Secret key, Vendor number - FUC - and terminal number).' mod='redsys'}<br />
			{l s='You should receive these a few days after signing the contract with your bank.' mod='redsys'}<br />
			{l s='Once you have these details, enter them in the module\'s setup tab. See the user documentation for more information.' mod='redsys'}<br />
			{l s='If you have requested installation by PrestaShop, send us an email containing the information provided by your bank.' mod='redsys'}<br />

			</div> 
		</li>		
		<li class="faq-item">
			<a class="faq-trigger">2) {l s='My bank has told me to carry out test payments before entering production mode. How do I do these?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Set up the module in Test mode.' mod='redsys'}<br />
			{l s='To carry out a payment in test mode (no money will be transferred in test mode), go to your store and place an order.' mod='redsys'}<br />
			{l s='Use the following bank card information to complete the payment:' mod='redsys'}<br />
			<br />
			{l s='Type' mod='redsys'}: VISA<br />
			{l s='Number' mod='redsys'}: 4485177664453273<br />
			{l s='Expiry date' mod='redsys'}: 12/17 {l s='(or any other date in the future)' mod='redsys'}<br />
			{l s='CVV' mod='redsys'}: 123<br />
			<br />
			{l s='N.B.: Before entering the production environment, you must carry out an accepted payment and a rejected payment in test mode (in the setup tab you will find card information for carrying out a payment in test mode).' mod='redsys'}<br />
			{l s='Once all of the tests have been carried out, contact your bank to activate your gateway and select the Production mode in your module.' mod='redsys'}<br />
			{l s='Do not do this before you receive confirmation that it is active.' mod='redsys'}<br />
			{l s='For more information, see the user documentation section 3.' mod='redsys'}<br />

			</div> 
		</li>		
		<li class="faq-item">
			<a class="faq-trigger">3) {l s='I\'ve purchased a module for the bank XXX, but I\'m in negotiations with various banks. Can I use this module regardless of which bank I ultimately choose?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Yes, provided that the banks use the Redsys platform.' mod='redsys'}<br />
			{l s='N.B.: The module works for one bank at a time. It is not possible to use it for two or more banks simultaneously.' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">4) {l s='How many currencies does the module allow payment with?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Euros are supported by default. Other currencies can nevertheless be supported. To do so, contact your bank.' mod='redsys'}<br />
			{l s='Remember to ensure that these currencies have been correctly set up for the store.' mod='redsys'}<br />
			{l s='To do so, go to the Modules > Payment tab.' mod='redsys'}<br />
			{l s='Go down to the "Restrictions by currency" section and check the boxes corresponding to Redsys with the currencies you have set up with your bank.' mod='redsys'}<br />
			<img src="{$redsys_img_dir}doc1.png"><br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">5) {l s='Is the module compatible in multi-store mode?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Yes. All sales from all of the stores will go to the account arranged with your bank.' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">6) {l s='Where and when do I receive the money from sales paid through the module?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Redsys makes one deposit per day, usually the day after the payment is made at your store.' mod='redsys'}<br />
			{l s='You will receive it in the bank account with which you contracted the virtual POS terminal service.' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">7) {l s='I want to change the phrase that appears on the payment button "Payment by bank card (secure transaction with Redsys)". Where can I do this?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='To change the text on the payment button, go to the Redsys module setup area and click on the "Translate" button:' mod='redsys'}<br />
			<img width="900" src="{$redsys_img_dir}doc2.png"><br />
			{l s='Select Spanish and personalize the translation in the corresponding field:' mod='redsys'}<br />
			<img width="900" src="{$redsys_img_dir}doc3.png"><br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">8) {l s='I want to change the payment accepted or payment error confirmation phrase. Where do I do this?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Go to the Redsys module setup area and click on the Translate button. Select Spanish and personalize the phrase in the corresponding field.' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">9) {l s='Can I activate one-page payments with the Redsys module?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Yes. To activate it, go to the Preferences > Orders tab and under "Type of order process", select the option "One-page purchase process".' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger" >10) {l s='I\'ve activated and correctly set up the Redsys module, but when I go to make a payment at the store it doesn\'t appear in the payment options provided.' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Ensure that you have correctly set up the module\'s restrictions on the tab Modules > Your back office\'s payment.' mod='redsys'}<br />
			{l s='Check in the “Restrictions by group” section that you have activated the Redsys module for all customer groups.' mod='redsys'}<br />
			{l s='Check in the “Restrictions by country” that you have activated the Redsys module for all of the countries you have contracted with your bank.' mod='redsys'}<br />
			<img width="900" src="{$redsys_img_dir}doc4.png">
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger" >11) {l s='I can\'t carry over the changes from the test environment to the real environment. What can I do?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Contact your bank and make sure that the module has the production settings and that your bank has taken into account the switch to the production or real mode.' mod='redsys'}<br />
			{l s='If the problem persists, contact us by going to the contact tab and send us your back-office and FTP details so that we can assist you:' mod='redsys'}<br />
			{l s='Back office URL:' mod='redsys'}<br />
			{l s='Back office login:' mod='redsys'}<br />
			{l s='Back office password:' mod='redsys'}<br />

			{l s='FTP host:' mod='redsys'}<br />
			{l s='FTP login:' mod='redsys'}<br />
			{l s='FTP password:' mod='redsys'}<br />
			</div> 
		</li>
		<li class="faq-item">
			<a class="faq-trigger">12) {l s='¿Cómo puedo obtener la clave secreta de encriptación SHA256?' mod='redsys'}<br /></a>
			<div class="faq-content">
			{l s='Para obtener la firma es necesario utilizar una clave específica para cada terminal. Se puede obtener la clave accediendo a tu Back Office Redsys' mod='redsys'} <br />
			{l s='Test' mod='redsys'} : https://sis-t.redsys.es:25443/canales/<br />
			{l s='Prueba' mod='redsys'} : https://sis.redsys.es/canales/<br />
			{l s='Ve a Módulo de Administración, opción Consulta datos de Comercio, en el apartado “Ver clave”, tal y como se demuestra en la siguiente imagen:' mod='redsys'}<br />
			<img width="700" src="{$redsys_img_dir}doc5.png"><br />
			<img width="600" src="{$redsys_img_dir}doc6.png"><br />
			<img width="600" src="{$redsys_img_dir}doc7.png"><br />
			{l s='Nota importante: Esta clave debe ser almacenada en el servidor de comercio de la forma más segura posible para evitar un uso fraudulento de la misma. El comercio es responsable de la adecuada custodia y mantenimiento en secreto de dicha clave.' mod='redsys'}<br />
			</div> 
		</li>
	</ul>
</div>