
<div class="clearfix"></div>
<div class="tab-pane panel" id="contacts">
	<h3>
		Prestashop Addons
	</h3>
	<div class="form-group">
		<b>{l s='Thank you for choosing a module developed by the Addons Team of PrestaShop. ' mod='redsys'}</b><br /><br />
		{l s='If you encounter a problem using the module, our team is at your service via the ' mod='redsys'} <a target="_blank" href="http://addons.prestashop.com/contact-form.php?utm_source=redsys&utm_medium=module&utm_campaign=back-office-{strtoupper(Language::getIsoById($emp_lang))}&utm_content=Redsys">{l s='contact form' mod='redsys'}.</a><br /><br />

		{l s='To save you time, before you contact us:' mod='redsys'}<br />
		- {l s='make sure you have read the documentation well. ' mod='redsys'}<br />
		- {l s='in the event you would be contacting us via the form, do not hesitate to give us your first message, maximum of details on the problem and its origins (screenshots, reproduce actions to find the bug, etc. ..) ' mod='redsys'}<br /><br />
		{l s='This module has been developped by PrestaShop and can only be sold through' mod='redsys'} <a target="_blank" href="http://addons.prestashop.com">addons.prestashop.com</a>.<br /><br />
				
		The PrestaShop Addons Team<br />
	</div>
</div>