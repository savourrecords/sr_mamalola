// Main Function
var Main = function () {
	/**
	** Click Event
	*/
	var runEvent = function () {

		// Tab panel active
		$(".list-group-item").on('click', function() {
			var $el = $(this).parent().closest(".list-group").children(".active");
			if ($el.hasClass("active")) {
				target = $(this).find('i').attr('data-target');
				if (target !== undefined) {
					loadTable(target);
				}
				$el.removeClass("active");
				$(this).addClass("active");
			}
		});

		// Allow to switch only one element
		$(".row input[type='radio']").on('change',function () {
			$(this).closest('.radio_select').siblings().find('input.switch_off').attr('checked', true);
		});

		$('.redsys_toggle').on('click', function(e){
			e.preventDefault();
			$('#redsysbank').toggle('2');
		});
	}; 

	return {
		//main function to initiate template pages
		init: function () {
			runEvent();
		}
	};
}();

$(function() {
	// Load functions
	Main.init();
});
