<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/redsys.php');
require_once('api/apiRedsys.php');

Tools::safePostVars();
$redsys = new Redsys();

if (!empty($_POST)) // Tools::safePostVars(); just before
{
	if (Tools::getIsset('Ds_SignatureVersion') && Tools::getValue('Ds_SignatureVersion') == 'HMAC_SHA256_V1')
	{
		$redsysApi = new RedsysAPI;
		$version = Tools::getValue('Ds_SignatureVersion');
		$params = Tools::getValue('Ds_MerchantParameters');
		$received_signature = Tools::getValue('Ds_Signature');

		$decode = $redsysApi->decodeMerchantParameters($params);
		$key = $redsys->getMerchantKey();
		$calculated_signature = $redsysApi->createMerchantSignatureNotif($key, $params);

		if ($calculated_signature === $received_signature)
		{
			$response = $redsysApi->getParameter('Ds_Response');

			$order = $redsysApi->getParameter('Ds_Order');
			$order = explode('a', $order);
			$id_cart = $order[0];
			$cart = new Cart($id_cart);
			$customer = new Customer((int)$cart->id_customer);

			$amount = $redsysApi->getParameter('Ds_Amount');
			$total  = number_format($amount / 100, 2, '.', '');

			if ($response < 100 OR $response === 900)
			{
				$comment = $redsys->getSuccesComment($response);
				$redsys->validateOrder($cart->id, _PS_OS_PAYMENT_, $total, $redsys->displayName, $comment, array(), NULL, false, $customer->secure_key);
			}
			else
			{
				// $comment = $redsys->getCancelComment($response);
				$redsys->validateOrder($cart->id, _PS_OS_ERROR_, 0, $redsys->displayName, Tools::displayError('Errors:').$response, array(), NULL, false, $customer->secure_key);
			}
		}
	}
	else 
	{
		$posted_values = array();
		$posted_values['amount']		= Tools::getValue('Ds_Amount');
		$posted_values['order_id']		= Tools::getValue('Ds_Order');
		$posted_values['merchant_code']	= Tools::getValue('Ds_MerchantCode');
		$posted_values['currency']		= Tools::getValue('Ds_Currency');
		$posted_values['response']		= Tools::getValue('Ds_Response');
		$posted_values['signature']		= Tools::getValue('Ds_Signature');
		
		
		$full_signature = $posted_values['amount'].$posted_values['order_id'].$posted_values['merchant_code'].$posted_values['currency'].$posted_values['response'].$redsys->getMerchantKey();
		$encrypt_signature = sha1($full_signature);
		
		if (strtoupper($encrypt_signature) == $posted_values['signature'])
		{
			$arr_order = explode('a', $posted_values['order_id']);
			$posted_values['order_id'] = (int)$arr_order[0];
			$cart = new Cart($posted_values['order_id']);
			
			$customer = new Customer((int)$cart->id_customer);
			
			$total  = number_format($posted_values['amount'] / 100, 2, '.', '');
			$response = (int)$posted_values['response'];
			if ($response < 100 OR $response === 900)
			{
				$comment = $redsys->getSuccesComment($response);
				$redsys->validateOrder($cart->id, _PS_OS_PAYMENT_, $total, $redsys->displayName, $comment, array(), NULL, false, $customer->secure_key);
			}
			else
			{
				// $comment = $redsys->getCancelComment($response);
				$redsys->validateOrder($cart->id, _PS_OS_ERROR_, 0, $redsys->displayName, Tools::displayError('Errors:').$response, array(), NULL, false, $customer->secure_key);
			}
		}
	}
}
