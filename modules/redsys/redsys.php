<?php

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class redsys extends PaymentModule
{
    /**
     * Using for get URL regarding the environnement needed
     * define the test environnement
     * @var string
     */
    const ENV_TEST = '0';

    /**
     * Using for get URL regarding the environnement needed
     * define the test environnement
     * @var string
     */
    const ENV_REAL = '1';

    /**
     * Using for get URL regarding the environnement needed
     * list environnement url
     * @var array
     */
    private static $url_pos = array(
        '0' => 'https://sis-t.redsys.es:25443/sis/realizarPago',
        '1' => 'https://sis.redsys.es/sis/realizarPago',
    );

    /**
     * List of iso code language allowed by Lacaixa
     * @var array
     */
    private static $possible_lang = array(
        'es'    => 1,
        'en'    => 2,
        'ca'    => 3,
        'fr'    => 4,
        'de'    => 5,
        'nl'    => 6,
        'it'    => 7,
        'sv'    => 8,
        'pt'    => 9,
        'vc'    => 10,
    );

    /**
     * Configuration var : the environnement LaCaixa works
     * @var string
     */
    private static $ENVIRONNEMENT;

    private static $REDSYS_BANK;

    /**
     * Configuration var : the secure encrypted key (sent by LaCaixa)
     * @var string
     */
    private static $KEY;

    /**
     * Configuration var : Shop name by default is Configuration::get('PS_SHOP_NAME')
     * @var string
     */
    private static $MERCHANT_NAME;

    /**
     * Configuration var : Unique code of the merchant (sent by LaCaixa)
     * @var string
     */
    private static $MERCHANT_CODE;

    /**
     * Configuration var : Shop URL LaCaixa calls to validate payment order
     * @var string
     */
    private static $MERCHANT_URL;



    private static $IUPAY;

    /**
     * Configuration var : Terminal number of LaCaixa (sent by LaCaixa)
     * @var string
     */
    private static $TERMINAL;

    /**
     * The shop URL.
     * @var string
     */
    private static $site_url;

    /**
     * Directory path to access of files module
     * @var string
     */
    private static $base_dir;

    /**
     * Path to acces of LaCaixa file by URL
     * @var string
     */
    private static $base_path;

    /**
     * Html output string
     * @var string
     */
    private $_html;

    /**
     * Errors occured when post process
     * @var array
     */
    private $post_errors = array();

    /**
    * List of spanish banks, and those which work with Iupay!
    * @var array
    */
    private $banks_iupay = array();
    private $redsys_banks = array();

    /**
     * Smarty object (to get the global var)
     * @var Smarty
     */
    private $_smarty;

    /**
     * For 1.3 compatibility
     * @var array list of numeric iso code for currency.
     */
    private static $arr_currency_iso_num = array(
        'ADP' => '020', 'AED' => 784, 'AFA' => '004', 'ALL' => '008',
        'AMD' => '051', 'ANG' => 532, 'AOA' => 973, 'ARS' => '032',
        'AUD' => '036', 'AWG' => 533, 'AZM' => '031', 'BAM' => 977,
        'BBD' => '052', 'BDT' => '050', 'BGL' => 100, 'BGN' => 975,
        'BHD' => '048', 'BIF' => 108, 'BMD' => '060', 'BND' => '096',
        'BOB' => '068', 'BOV' => 984, 'BRL' => 986, 'BSD' => '044',
        'BTN' => '064', 'BWP' => '072', 'BYR' => 974, 'BZD' => '084',
        'CAD' => 124, 'CDF' => 976, 'CHF' => 756, 'CLF' => 990,
        'CLP' => 152, 'CNY' => 156, 'COP' => 170, 'CRC' => 188,
        'CUP' => 192, 'CVE' => 132, 'CYP' => 196, 'CZK' => 203,
        'DJF' => 262, 'DKK' => 208, 'DOP' => 214, 'DZD' => '012',
        'ECS' => 218, 'ECV' => 983, 'EEK' => 233, 'EGP' => 818,
        'ERN' => 232, 'ETB' => 230, 'EUR' => 978, 'FJD' => 242,
        'FKP' => 238, 'GBP' => 826, 'GEL' => 981, 'GHC' => 288,
        'GIP' => 292, 'GMD' => 270, 'GNF' => 324, 'GTQ' => 320,
        'GWP' => 624, 'GYD' => 328, 'HKD' => 344, 'HNL' => 340,
        'HRK' => 191, 'HTG' => 332, 'HUF' => 348, 'IDR' => 360,
        'ILS' => 376, 'INR' => 356, 'IQD' => 368, 'IRR' => 364,
        'ISK' => 352, 'JMD' => 388, 'JOD' => 400, 'JPY' => 392,
        'KES' => 404, 'KGS' => 417, 'KHR' => 116, 'KMF' => 174,
        'KPW' => 408, 'KRW' => 410, 'KWD' => 414, 'KYD' => 136,
        'KZT' => 398, 'LAK' => 418, 'LBP' => 422, 'LKR' => 144,
        'LRD' => 430, 'LSL' => 426, 'LTL' => 440, 'LVL' => 428,
        'LYD' => 434, 'MAD' => 504, 'MDL' => 498, 'MGF' => 450,
        'MKD' => 807, 'MMK' => 104, 'MNT' => 496, 'MOP' => 446,
        'MRO' => 478, 'MTL' => 470, 'MUR' => 480, 'MVR' => 462,
        'MWK' => 454, 'MXN' => 484, 'MXV' => 979, 'MYR' => 458,
        'MZM' => 508, 'NAD' => 516, 'NGN' => 566, 'NIO' => 558,
        'NOK' => 578, 'NPR' => 524, 'NZD' => 554, 'OMR' => 512,
        'PAB' => 590, 'PEN' => 604, 'PGK' => 598, 'PHP' => 608,
        'PKR' => 586, 'PLN' => 985, 'PYG' => 600, 'QAR' => 634,
        'ROL' => 642, 'RUB' => 643, 'RUR' => 810, 'RWF' => 646,
        'SAR' => 682, 'SBD' => '090', 'SCR' => 690, 'SDD' => 736,
        'SEK' => 752, 'SGD' => 702, 'SHP' => 654, 'SIT' => 705,
        'SKK' => 703, 'SLL' => 694, 'SOS' => 706, 'SRG' => 740,
        'STD' => 678, 'SVC' => 222, 'SYP' => 760, 'SZL' => 748,
        'THB' => 764, 'TJS' => 972, 'TMM' => 795, 'TND' => 788,
        'TOP' => 776, 'TPE' => 626, 'TRL' => 792, 'TRY' => 949,
        'TTD' => 780, 'TWD' => 901, 'TZS' => 834, 'UAH' => 980,
        'UGX' => 800, 'USD' => 840, 'UYU' => 858, 'UZS' => 860,
        'VEB' => 862, 'VND' => 704, 'VUV' => 548, 'XAF' => 950,
        'XCD' => 951, 'XOF' => 952, 'XPF' => 953, 'YER' => 886,
        'YUM' => 891, 'ZAR' => 710, 'ZMK' => 894, 'ZWD' => 716,
    );

    /**
     * Use for display the module in HookPayment
     * In Lacaixa::__construct() if parameters are wrong
     * boolean get false else true
     * @var boolean
     */
    private $front_active = false;

    public function __construct()
    {

        $this->name = 'redsys';
        if (version_compare('1.4', _PS_VERSION_, '<'))
            $this->tab = 'payments_gateways';
        else
            $this->tab = 'Payment';
        if (_PS_VERSION_ >= '1.5')
        {
            $this_context = Context::getContext();
            $this->id_shop = $this_context->shop->id;
        }
        else
            $this->id_shop = 0;
        $this->version = '3.0.4';

        parent::__construct();

        /** Backward compatibility */
        require(_PS_MODULE_DIR_.'/'.$this->name.'/backward_compatibility/backward.php');

        self::$site_url = Tools::getShopProtocol(). htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__;
        self::$base_dir = _PS_ROOT_DIR_.'/modules/'.$this->name.'/';
        self::$base_path = self::$site_url.'modules/'.$this->name.'/';

        $this->page = basename(__FILE__, '.php');
        $this->displayName = 'Redsys';
        $this->author = 'PrestaShop';
        $this->currencies_mode = 'checkbox';

        $this->js_path = $this->_path.'views/js/';
        $this->css_path = $this->_path.'views/css/';
        $this->img_path = $this->_path.'views/img/';
        $this->bootstrap = true;

        $this->module_key = 'b209199e8e7c828500b5a2cbb162d374';
        $this->author_address = '0x64aa3c1e4034d07015f639b0e171b0d7b27d01aa';

        $this->description = $this->l('Redsys\'s payment module.');

        if (_PS_VERSION_ >= '1.5')
        {
            self::$ENVIRONNEMENT    = Configuration::get('REDSYS_ENVIRONNEMENT', null, null, $this->id_shop);
            self::$KEY              = Configuration::get('REDSYS_KEY', null, null, $this->id_shop);
            self::$MERCHANT_NAME    = Configuration::get('REDSYS_MERCHANT_NAME', null, null, $this->id_shop);
            self::$MERCHANT_CODE    = Configuration::get('REDSYS_MERCHANT_CODE', null, null, $this->id_shop);
            self::$MERCHANT_URL     = Configuration::get('REDSYS_MERCHANT_URL', null, null, $this->id_shop);
            self::$TERMINAL         = Configuration::get('REDSYS_TERMINAL', null, null, $this->id_shop);
            self::$IUPAY            = Configuration::get('REDSYS_IUPAY', null, null, $this->id_shop);
            self::$REDSYS_BANK      = Configuration::get('REDSYS_BANK', null, null, $this->id_shop);
        }
        else
        {
            self::$ENVIRONNEMENT    = Configuration::get('REDSYS_ENVIRONNEMENT');
            self::$KEY              = Configuration::get('REDSYS_KEY');
            self::$MERCHANT_NAME    = Configuration::get('REDSYS_MERCHANT_NAME');
            self::$MERCHANT_CODE    = Configuration::get('REDSYS_MERCHANT_CODE');
            self::$MERCHANT_URL     = Configuration::get('REDSYS_MERCHANT_URL');
            self::$TERMINAL         = Configuration::get('REDSYS_TERMINAL');
            self::$IUPAY            = Configuration::get('REDSYS_IUPAY');
            self::$REDSYS_BANK      = Configuration::get('REDSYS_BANK');
        }

        if ((self::$ENVIRONNEMENT === false OR self::$ENVIRONNEMENT === '')
        OR (self::$KEY === false OR self::$KEY === '')
        OR (self::$MERCHANT_NAME === false OR self::$MERCHANT_NAME === '')
        OR (self::$MERCHANT_CODE === false OR self::$MERCHANT_CODE === '')
        OR (self::$TERMINAL === false OR self::$TERMINAL === '')
        OR (self::$MERCHANT_URL === false OR self::$MERCHANT_URL === '')
        OR (self::$REDSYS_BANK === false OR self::$REDSYS_BANK === ''))
        {
            $this->front_active = false;
        }
        else {
            $this->front_active = true;
        }

        $this->redsys_banks = array(
            array('displayName' => 'Caixabank', 'techName' => 'caixa'),
            array('displayName' => 'LaCaixa', 'techName' => 'lacaixa'),
            array('displayName' => 'BBVA', 'techName' => 'bbva'),
            array('displayName' => 'Bankia', 'techName' => 'bankia'),
            array('displayName' => 'Banco Santander, S.A.', 'techName' => 'santander'),
            array('displayName' => 'Banco Popular Español, S.A.', 'techName' => 'popular'),
            array('displayName' => 'Banco de Sabadell', 'techName' => 'sabadell'),
            array('displayName' => 'Banco Cooperativo Español', 'techName' => 'cooperativo'),
            array('displayName' => 'Bankinter', 'techName' => 'inter'),
            array('displayName' => 'ING', 'techName' => 'ing'),
            array('displayName' => 'Deutsche Bank, S.A.E.', 'techName' => 'deutsche'),
            array('displayName' => 'Citibank España', 'techName' => 'citibank')
        );

        $this->banks_iupay = array(
            'caixa',
            'lacaixa',
            'bbva',
            'bankia',
            'santander',
            'popular',
            'sabadell',
            'deutsche',
            'ing',
            'cooperativo');
    }

    public function reset()
    {
        if (!$this->uninstall())
            return false;
        if (!$this->install())
            return false;

        return true;
    }

    /**
     * To get comment (using by Lacaixa::validateCommand()) according to success code.
     *
     * @param string $response
     * @return string
     */
    public function getSuccesComment($response)
    {
        $return_str = '';
        if($response < 100)
        {
            $return_str = $this->l('Transaction authorized for payments').' ('.$this->l('code:').$response.')';
        }
        /*elseif($response == 900)
        {
            $return_str = $this->l('Transaction authorised for refunds and confirmations').' ('.$this->l('code:').$response.')';
        }*/
        return $return_str;
    }

    /**
     * @return string Lacaixa::$KEY
     */
    public function getMerchantKey()
    {
        return self::$KEY;
    }

    /**
     * @return string Lacaixa::$MERCHANT_CODE
     */
    public function getMerchantCode()
    {
        return self::$MERCHANT_CODE;
    }

    /**
     * @see PaymentModuleCore::install()
     * @return boolean
     */
    public function install()
    {
        if (_PS_VERSION_ >= '1.5')
            $list_shop = Shop::getCompleteListOfShopsID();
        else
            $list_shop = null;

        return (parent::install()
            AND $this->registerHook('orderConfirmation', $list_shop)
            AND $this->registerHook('paymentOptions', $list_shop)
            AND Configuration::updateValue('REDSYS_ENVIRONNEMENT', Redsys::ENV_TEST)
            AND Configuration::updateValue('REDSYS_MERCHANT_NAME', Configuration::get('PS_SHOP_NAME'))
            AND Configuration::updateValue('REDSYS_MERCHANT_URL', self::$base_path.'validation.php')
            AND Configuration::updateValue('REDSYS_TERMINAL', 1)
            AND Configuration::updateValue('REDSYS_BANK', 'redsys'));
    }

    /**
     * @see PaymentModuleCore::uninstall()
     * @return boolean
     */
    public function uninstall()
    {
        return (Configuration::deleteByName('REDSYS_ENVIRONNEMENT')
            AND Configuration::deleteByName('REDSYS_KEY')
            AND Configuration::deleteByName('REDSYS_MERCHANT_NAME')
            AND Configuration::deleteByName('REDSYS_MERCHANT_CODE')
            AND Configuration::deleteByName('REDSYS_MERCHANT_URL')
            AND Configuration::deleteByName('REDSYS_TERMINAL')
            AND Configuration::deleteByName('REDSYS_BANK')
            AND parent::uninstall());
    }

    /**
     * Get, check and saved posted settings on Module panel formular.
     * @return void
     */
    private function preProcess()
    {
        if (Tools::isSubmit('submitGeneral'))
        {
            $this->smarty->assign(array('submit' => true));
        
            self::$MERCHANT_CODE = Tools::getValue('merchant_code') != '' ? Tools::getValue('merchant_code') : '';
            self::$MERCHANT_URL = Validate::isUrl(Tools::getValue('merchant_url')) ? Tools::getValue('merchant_url') : '';
            self::$MERCHANT_NAME = Tools::getValue('merchant_name') != '' ? Tools::getValue('merchant_name') : '';
            self::$ENVIRONNEMENT = Tools::getValue('env') != '' ? Tools::getValue('env') : '';
            self::$KEY = Tools::getValue('key') != '' ? Tools::getValue('key') : '';
            self::$TERMINAL = Tools::getValue('terminal') != '' ? Tools::getValue('terminal') : '';
            self::$IUPAY = Tools::getValue('iupay') != '' ? Tools::getValue('iupay') : '0';

            // To set the currencies priority @see AdminPayment tab
            if (self::$MERCHANT_URL == '')
                $this->post_errors[] = $this->l('URL validation is required.');
            if (self::$KEY == '')
                $this->post_errors[] = $this->l('Secret encrypted key required');
            if (self::$MERCHANT_NAME == '')
                $this->post_errors[] = $this->l('Business name required'); // not sure
            if (self::$MERCHANT_CODE == '')
                $this->post_errors[] = $this->l('Business number required (FUC)');
            if (self::$TERMINAL == '')
                $this->post_errors[] = $this->l('Card terminal number required');

            Configuration::updateValue('REDSYS_MERCHANT_CODE', self::$MERCHANT_CODE, null, null, $this->id_shop); // key, values, html, id_shop_group, id_shop
            Configuration::updateValue('REDSYS_MERCHANT_URL', self::$MERCHANT_URL, null, null, $this->id_shop);
            Configuration::updateValue('REDSYS_MERCHANT_NAME', self::$MERCHANT_NAME, null, null, $this->id_shop);
            Configuration::updateValue('REDSYS_ENVIRONNEMENT', self::$ENVIRONNEMENT, null, null, $this->id_shop);
            Configuration::updateValue('REDSYS_KEY', self::$KEY, null, null, $this->id_shop);
            Configuration::updateValue('REDSYS_TERMINAL', self::$TERMINAL, null, null, $this->id_shop);
            Configuration::updateValue('REDSYS_IUPAY', self::$IUPAY, null, null, $this->id_shop);
        }

        if (Tools::isSubmit('submitRedsysBank'))
        {
            $this->smarty->assign(array('submit' => true));

            $redsys_bank = '';
            foreach ($this->redsys_banks as $bank)
            {
                $key = $bank['techName'];
                if ($key === Tools::getValue($key))
                {
                    $redsys_bank = $key;
                    break;
                }
            }
            self::$REDSYS_BANK = Tools::getValue($redsys_bank) != '' ? Tools::getValue($redsys_bank) : 'redsys';
            Configuration::updateValue('REDSYS_BANK', self::$REDSYS_BANK, null, null, $this->id_shop);
        }

        if (!empty($this->post_errors))
            $this->displayErrors();
    }

    /**
    * Loads asset resources
    */
    public function loadAsset()
    {
        $css_compatibility = $js_compatibility = $css = array();

        $return = '';

        // Load CSS
        $css = array(
            $this->css_path.'faq.css',
            $this->css_path.'font-awesome.min.css',
            );

        if (version_compare(_PS_VERSION_, '1.6', '<'))
        {
            $css_compatibility = array(
                $this->css_path.'bootstrap.min.css',
                $this->css_path.'bootstrap.extend.css',
                $this->css_path.'bootstrap-responsive.min.css',
                $this->css_path.$this->name.'.css',
            );

            $css = array_merge($css, $css_compatibility);
        }       

        $this->context->controller->addCSS($css, 'all');

        // Load JS
        $js = array(
            $this->js_path.'/jquery.form.min.js', // Used in BO for file upload ajax process
            $this->js_path.$this->name.'.js', // custom js
            $this->js_path.'riot_compiler.min.js', // Used in BO for configuration Form - https://github.com/Scritik/prestui
            $this->js_path.'faq.js'); // FAQ js

        if (version_compare(_PS_VERSION_, '1.5', '>=') && (version_compare(_PS_VERSION_, '1.6', '<')))
        {

            $js_compatibility = array(
                $this->js_path.'bootstrap.min.js',
            );

            $js = array_merge($js_compatibility, $js);
        }

        $this->context->controller->addJS($js);

        // Clean memory
        unset($js, $css, $js_compatibility, $css_compatibility);

        return $return;
    }


    /**
     * @see PaymentModule::getContent()
     * @return string
     */
    public function getContent()
    {
        $return = '';
        $return .= $this->loadAsset();
        $this->smarty->assign(array('submit' => false));

        // PostProcess
        $return .= $this->preProcess();

        /* Language for documentation in back-office */
        $iso_code = Context::getContext()->language->iso_code;
        if ($iso_code == 'ca' || $iso_code == 'es' || $iso_code == 'gl')
            $lang = 'ES';
        elseif (empty($lang))
            $lang = 'EN';

        $emp_lang = $this->context->employee->id_lang;

        // smarty variables Assign
        $this->smarty->assign(array(
            'emp_lang' => $emp_lang, // contact tpl utms
            'env' => self::$ENVIRONNEMENT,
            'key' => self::$KEY,
            'merchant_name' => self::$MERCHANT_NAME,
            'merchant_code' => self::$MERCHANT_CODE,
            'merchant_url' => self::$MERCHANT_URL,
            'iupay' => self::$IUPAY,
            'redsys_bank' => self::$REDSYS_BANK,
            'terminal' => self::$TERMINAL,
            'module_version' => $this->version,
            'module_display' => $this->displayName,
            'ps_version' => Tools::substr(_PS_VERSION_, 0, 3), // required by PrestUI
            'redsys_banks' => $this->redsys_banks,
            'iupay_banks' => $this->banks_iupay,
            'redsys_img_dir' => $this->img_path,
            'guide_link' => 'views/docs/Documentacion_Usuario_'.strtoupper($lang).'.pdf',
            'migration_guide_link' => 'views/docs/Documentacion_Migracion_Redsys.pdf',

        ));

        $return .= $this->display(__FILE__, 'views/templates/admin/configuration.tpl');

        return $return.$this->display(__FILE__, 'views/templates/admin/prestui/ps-tags.tpl'); // Using Emmanuel Marichal's PrestaUI
    }

    /**
     * Format the output errors
     */
    private function displayErrors()
    {
        if(!empty($this->post_errors))
            $this->smarty->assign(array('config_warnings' => $this->post_errors));
    }

    /**
     * Check if the currency is available for the module
     *
     * @see PaymentModule::getCurrency()
     * @param int $currency_to_check id of the currency
     */
    private function checkCurrency($currency_to_check)
    {
        $output = false;
        $currencies = $this->getCurrency();

        if(is_array($currencies))
            foreach ($currencies as $currency)
                if ((int)$currency['id_currency'] === (int)$currency_to_check)
                {
                    $output = $currency['id_currency'];
                    break;
                }

        if ($currencies instanceof Currency)
            if ((int)$currency_to_check === (int)$currencies->id)
            {
                $output = $currencies->id;
            }
        return $output;
    }

    /**
     * @param array $params
     * @return string;
     */
    public function hookPaymentOptions($params)
    {
        if(!$this->front_active OR !$this->active)
            return '';

        $redsys_params = array();

        $redsys_PaymentOptions = array();

        $currency_customer = $this->checkCurrency($params['cart']->id_currency);

        if ($currency_customer)
            $redsys_params = $this->getPaymentParams($params['cookie'], $params['cart'], $currency_customer);
        else
        {
            $redsys_params['url_pos'] = self::$base_path.'payment.php';
            $redsys_params['currency'] = 0;
        }
        $redsys_params['bank_img'] = $this->img_path.self::$REDSYS_BANK.'bank.png';
        // $this->smarty->assign('url_pos', $redsys_params['url_pos']);
        // $this->smarty->assign('bank_img', $redsys_params['bank_img']);

        require_once('api/apiRedsys.php');
        $redsysApi = new RedsysAPI;
        $redsysApi->setParameter("DS_MERCHANT_AMOUNT", $redsys_params['amount']);
        $redsysApi->setParameter("DS_MERCHANT_ORDER", $redsys_params['order']);
        $redsysApi->setParameter("DS_MERCHANT_MERCHANTCODE", $redsys_params['merchant_code']);
        $redsysApi->setParameter("DS_MERCHANT_CURRENCY", $redsys_params['currency']);
        $redsysApi->setParameter("DS_MERCHANT_TRANSACTIONTYPE", 0);
        $redsysApi->setParameter("DS_MERCHANT_MERCHANTNAME", $redsys_params['merchant_name']);
        $redsysApi->setParameter("DS_MERCHANT_TERMINAL", $redsys_params['terminal']);
        $redsysApi->setParameter("DS_MERCHANT_MERCHANTURL",$redsys_params['merchant_url']);
        $redsysApi->setParameter("DS_MERCHANT_URLOK", $redsys_params['url_ok']);
        $redsysApi->setParameter("DS_MERCHANT_URLKO", $redsys_params['url_cancel']);

        if (self::$IUPAY == '1')
        {
            $redsysApi->setParameter("Ds_Merchant_PayMethods", "O");
            $redsysApi2 = $redsysApi;
            $params = $redsysApi2->createMerchantParameters();
            $signature = $redsysApi2->createMerchantSignature(self::$KEY);


            $iupay_redsys = new PaymentOption();
            $iupay_redsys->setCallToActionText($this->l('Connection to the POS'))
                ->setAction($redsys_params['url_pos'])
                ->setInputs(array(
                    'Ds_SignatureVersion'   => ['name' => 'Ds_SignatureVersion', 'type' => 'hidden', 'value' => 'HMAC_SHA256_V1'],
                    'Ds_MerchantParameters' => ['name' => 'Ds_MerchantParameters', 'type' => 'hidden', 'value' => $params],
                    'Ds_Signature'          => ['name' => 'Ds_Signature', 'type' => 'hidden', 'value' => $signature]))
                ->setAdditionalInformation('')
                ->setLogo($this->img_path.'logo-iupay.png');
            
            $redsys_PaymentOptions[] = $iupay_redsys;
        }
        else
            $redsysApi->setParameter("Ds_Merchant_PayMethods", "C");

        $params = $redsysApi->createMerchantParameters();
        $signature = $redsysApi->createMerchantSignature(self::$KEY);

        $redsys = new PaymentOption();
        $redsys->setCallToActionText($this->l('Connection to the POS'))
            ->setAction($redsys_params['url_pos'])
            ->setInputs(array(
                'Ds_SignatureVersion'   => ['name' => 'Ds_SignatureVersion', 'type' => 'hidden', 'value' => 'HMAC_SHA256_V1'],
                'Ds_MerchantParameters' => ['name' => 'Ds_MerchantParameters', 'type' => 'hidden', 'value' => $params],
                'Ds_Signature'          => ['name' => 'Ds_Signature', 'type' => 'hidden', 'value' => $signature]))
            ->setAdditionalInformation('')
            ->setLogo($redsys_params['bank_img']);


        $redsys_PaymentOptions[] = $redsys;

        return $redsys_PaymentOptions;

            // $this->smarty->assign('params', $params);
            // $this->smarty->assign('signature', $signature);

            // return $this->display(__FILE__, 'views/templates/hook/payment256.tpl');
    }

    /**
     * Get all informations about the cart to send for LaCaixa.
     *
     * @return array params
     * @param Cookie $cookie
     * @param Cart $cart
     * @param int $id_currency
     */
    private function getPaymentParams(Cookie $cookie, Cart $cart, $id_currency)
    {
        $iso_lang = Language::getIsoById($cookie->id_lang);
        $redsys_params = array();

        // Check if the customer lang is allow by LaCaixa
        // 0 value use default language (es)
        if(array_key_exists($iso_lang, self::$possible_lang))
            $redsys_params['consumer_language'] = str_pad((string)self::$possible_lang[$iso_lang], 3, '0', STR_PAD_LEFT);
        else
            $redsys_params['consumer_language'] = 0;

        // Get the POS url regarding context configuration
        $redsys_params['url_pos'] = self::$url_pos[self::$ENVIRONNEMENT];
        $currency = new Currency((int)$id_currency);

        // for 1.3 compatibility
        if(!isset($currency->iso_code_num) OR $currency->iso_code_num == '')
            $redsys_params['currency'] = self::$arr_currency_iso_num[$currency->iso_code];
        else
            $redsys_params['currency'] = $currency->iso_code_num;

        $order_id = $cart->id;
        $customer = new Customer($cart->id_customer);

        $redsys_params['terminal'] = self::$TERMINAL;

        // The name displayed on LaCaixa window
        $redsys_params['card_holder'] = strtoupper($customer->firstname{0}).'.'.ucfirst($customer->lastname);

        if (version_compare(_PS_VERSION_, '1.5', '>'))
            $returnPage = self::$site_url.'index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->id.'&key='.$customer->secure_key.'&';
        else
            $returnPage = self::$site_url.'order-confirmation.php?id_cart='.$cart->id.'&id_module='.$this->id.'&key='.$customer->secure_key.'&';

        // The amount be in cents. ex : 15.50 must be formatted as : 1550
        $redsys_params['amount'] = number_format(Tools::convertPrice($cart->getOrderTotal(true, 3), $currency->id), 2, '.', '')*100;
        $redsys_params['order'] = str_pad($order_id, 7, "0", STR_PAD_LEFT).'a'.date('i').date('s');
        $redsys_params['merchant_code'] = self::$MERCHANT_CODE;
        $redsys_params['merchant_name'] = self::$MERCHANT_NAME;
        $redsys_params['merchant_url'] = self::$base_path.'validation.php';

        // Building the secure key for safe transaction
        $full_signature = $redsys_params['amount'].$redsys_params['order'].self::$MERCHANT_CODE.$redsys_params['currency'].'0'.self::$MERCHANT_URL.self::$KEY;
        $redsys_params['merchant_signature'] = sha1($full_signature);
        $redsys_params['url_ok'] = $returnPage;
        $redsys_params['url_cancel'] = $returnPage;
        if (version_compare(_PS_VERSION_, '1.5', '>'))
            $redsys_params['titular'] = ($this->context->customer->isLogged() ? $cookie->customer_firstname.' '.$cookie->customer_lastname : false);
        else
            $redsys_params['titular'] = ($cookie->isLogged() ? $cookie->customer_firstname.' '.$cookie->customer_lastname : false);

        $products = $cart->getProducts();
        $redsys_params['products_description'] = '';
        $end_products_key = key($products[count($products) - 1]);
        foreach ($products as $k=>$product) {
            $redsys_params['products_description'] .= $product['quantity'].' '.$product['name'].($k !== $end_products_key ? '<br />' : '');
        }
        return $redsys_params;
    }

    public function hookOrderConfirmation($params)
    {
        global $smarty, $cookie;

        $shop_name = Shop::getShop($params['cart']->id_shop);
        if ($params['order']->module != $this->name)
            return;

        if ($params['order']->valid)
            $smarty->assign(array(
                'status' => 'ok', 
                'id_order' => $params['order']->id, 
                'shop_name' => $shop_name['name'],
                'base_dir_ssl' => self::$base_dir

            ));
        else
            $smarty->assign('status', 'failed');
        return $this->display(__FILE__, 'hookorderconfirmation.tpl');
    }
}
