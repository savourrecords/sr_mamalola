<?php

class Tools extends ToolsCore
{
    public static function addonsRequest($request, $params = array())
    {
        if (Module::isEnabled('disableaddonsapi') && Configuration::get('DISABLEADDONSAPI_ENABLE')) {
            return false;
        } else {
            return parent::addonsRequest($request, $params);
        }
    }
}